################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : ingest_sqoop_upsert_wrapper.sh                                 #
# Description : This script will extract data from source using sqoop          #
#               into landing layer                                             #
################################################################################

# Function to print the logs with current utc time 
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
Show_Usage()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[ERROR:] Invalid arguments please pass exactly three arguments"
        log_print "Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename> <batch_load_nbr> "
        exit 1
}

load_resource()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        . $1
        if [ $? -eq 0 ]
        then    
            log_print "[INFO:] Successfully $1 Loaded "
        else
            log_print "[ERROR:] Failed to $1 Load " 
            exit 1
        fi
}

#Insert in audit table
audit_cleanup(){

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
LND_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
LND_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
LND_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
START_TIME_UTC="${16}"
log_print "********************************************Audit Entry*****************************************************"
log_print "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) values ($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','$END_TIME')"
`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,
data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,
src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) 
values ($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM',
'$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME',
'$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}')"`
if [ $? -eq 0 ]
then
        log_print  "[INFO:] successfully Inserted record into audit table"
fi

    
if [ "$JOB_STAT_NM" == "failure" ] && [ "$LND_DATA_DIRECTORY" != NULL ]
then
        #Clean Up Activity If Job is Failed
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[INFO:] Starting clean up activity..."
        log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
        hdfs dfs -test -d ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
        if [ $? -eq 0 ]
        then
                hdfs dfs -rm -r ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
                if [ $? -eq 0 ]
                then
                
                        log_print "[INFO:] Files deleted successfully in $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
                        
                else
                
                        log_print "[ERROR:] $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE files deletion failed"
            
                fi
        fi
        #Removing Partition For Respective Date 
        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $LND_DATABASE_NAME.$TABLE_NAME 
        DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
        if [ $? -eq 0 ]
        then
                    log_print "[INFO:] Successfully removed partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME" 
        else    
                    log_print "[ERROR:] failed to removed partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
        fi
        log_print "[ERROR:] Job Failed after Sqoop Utility.........Please check"
        exit 1
elif [ "$JOB_STAT_NM" == "failure" ] && [ "$LND_DATA_DIRECTORY" == NULL ]
then
        log_print "[ERROR:] Job Failed before Sqoop Utility.........Please check"
        exit 1
else
        log_print  "[INFO:] successfully Job record inserted into audit table"
        log_print "[INFO:] Job completed successfully"
fi 
END_TIME_UTC=$(date +%s)
log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
exit 0
}

if [ $# -lt 3 ]
then
        Show_Usage
fi


#Initializing all variables
START_TIME=$(date '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=`date +%s`
SHEDULED_DATE=$1
SRC_DB_NAME=$2
TABLE_NAME=$3
CEDL_BATCH_DATE=$( date -d "${SHEDULED_DATE} -1 days" '+%Y-%m-%d')
log_print "Cedl batch date : $CEDL_BATCH_DATE"
CRNT_BATCH_DATE=$(date '+%Y-%m-%d')
CRNT_BATCH_DATE=$( date -d "${CRNT_BATCH_DATE} -1 days" '+%Y-%m-%d')
LOG_DATE=$(date -d "$SHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
 ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"
flag=0
FAILURE=failure
SUCCESS=success
#Getting Extra Argument [Batch Load Number]
BATCH_LD_NBR=$4

# Loading environment based properties file.

load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.

load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .

 load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties


#Jobname for Audit: <lnd/ref/app>_ <modulename>_<sourcedb>_<tablename>
LND_JOB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"
REF_JOB_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"

#Data Directory for job     
LND_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"
REF_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${REF_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"

#Database Name For Landing and Refine Layer   <DB_prefix>_<source_db_name>  
LND_DATABASE_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}"
REF_DATABASE_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}"

#Creating directory for logging 
mkdir -p  "$CEDL_BASE_PATH"/"log"/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Successfully created Log path directory $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
        log_print "[ERROR:] Failed to create Log path directory $CEDL_BASE_PATH/log/$LOG_DATE/ failed"
        exit 1
        
fi
log_print "Logs Directory location:- $CEDL_BASE_PATH/log/$LOG_DATE/${LND_JOB_NAME}_${DIR_TIME}.log "

{

#Printing Argument Provided by User.
log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments : $*"

#Extracting source credentials
EDW4X_DB_HOST_IP_PORT=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f1`
EDW4X_DB_HOST_USER=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f2`
EDW4X_DB_HOST_PASSWORD=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f3-`
SQOOP_CONNNECTION_URL="jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} --username ${EDW4X_DB_HOST_USER}  --password ${EDW4X_DB_HOST_PASSWORD}"

#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

BEELINE_PRINT_HEADER="set hive.cli.print.header=FALSE;"

log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

log_print "*******************Business Logic Start From Here******************************"

#Getting job key and last processed batch load number
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select job_key from cedl_operations.cedl_job_t where job_nm='$LND_JOB_NAME'"` 
log_print "[INFO:] Job Key for $LND_JOB_NAME Job : $JOB_KEY"

#Getting Refine Layer batch_ld_nbr
LAST_PROCESS_BATCH_LD_NBR=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t
where job_nm='${REF_JOB_NAME}'"`
log_print "[INFO:] Max Batch Load Number from Previous Run : $LAST_PROCESS_BATCH_LD_NBR"

#If User pass extra argument [Re run scenario]
if [[ ! -z "$BATCH_LD_NBR" ]]
then
        
        
        #Getting Table Partition and Min Batch Load Number for respective Batch Load Number Argument.
        log_print "[INFO:] [Re run] Mysql query : select table_partition,min_batch_ld_nbr from cedl_operations.cedl_batch_mapping_t
        where job_nm='$LND_JOB_NAME' and min_batch_ld_nbr<=$BATCH_LD_NBR and max_batch_ld_nbr>=$BATCH_LD_NBR;"
        QUERY_OUTPUT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select table_partition,min_batch_ld_nbr from cedl_operations.cedl_batch_mapping_t
        where job_nm='$LND_JOB_NAME' and min_batch_ld_nbr<=$BATCH_LD_NBR and max_batch_ld_nbr>=$BATCH_LD_NBR;"`
        log_print "[INFO:] [In Rerun] For given batch load number '$BATCH_LD_NBR' Mysql query output   : $QUERY_OUTPUT"
        
        #Getting Partition Date and Min Batch Load Number
        TEMP_CEDL_BATCH_DATE=`echo "$QUERY_OUTPUT" | cut -f1 -d$'\t'`
        #Keeping Copy of Partition Date for Future use.
        TEMP_CEDL_BATCH_DATE_CP=$TEMP_CEDL_BATCH_DATE
        MIN_BATCH_LD_NBR=`echo "$QUERY_OUTPUT" | cut -f2 -d$'\t'`
		MIN_BATCH_LD_NBR=`expr $MIN_BATCH_LD_NBR - 1`
        log_print "[INFO:] [Re run] For given partition Batch date : $TEMP_CEDL_BATCH_DATE and Minimum Batch load number :$MIN_BATCH_LD_NBR"
        if [ "$TEMP_CEDL_BATCH_DATE" != 0 ] && [ ! -z "$TEMP_CEDL_BATCH_DATE" ] 
        then
                #Cleanup of Hive Partition and its Locations For Landing With Help of Above Partition Date $TEMP_CEDL_BATCH_DATE
                while [ "${TEMP_CEDL_BATCH_DATE}" != "${CRNT_BATCH_DATE}" ]
                do 
                        #Removing Partition For Respective Date [From $TEMP_CEDL_BATCH_DATE to Current Date<$CRNT_BATCH_DATE>]
                        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $LND_DATABASE_NAME.$TABLE_NAME DROP IF EXISTS PARTITION($PARTITION_COL='${TEMP_CEDL_BATCH_DATE}')"`
                        if [ $? -eq 0 ]
                        then
                                    log_print "[INFO:] [Re run] Successfully removed partition $TEMP_CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME" 
                        else    
                                    log_print "[ERROR:] [Re run] Failed to removed partition $TEMP_CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${START_TIME_UTC}"
                        fi
                        
                        #Removing directory For Respective Partition [From $TEMP_CEDL_BATCH_DATE to Current Date<$CRNT_BATCH_DATE>]
                        hdfs dfs -test -d $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE                
                        if [ $? -eq 0 ]
                        then
                                hdfs dfs -rm -r $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE
                                if [ $? -eq 0 ]
                                then
                                            log_print "[INFO:] [Re run] Successfully removed directory $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME" 
                                else    
                                            log_print "[ERROR:] [Re run] Failed to removed directory $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${START_TIME_UTC}"
                                fi
                        fi
                        
                        
                        TEMP_CEDL_BATCH_DATE=$(date -I -d "$TEMP_CEDL_BATCH_DATE + 1 day")
                done
                
                #Deleting Old Data From Audit Table<cedl_batch_mapping_t>
                log_print "[INFO;] [Re run] Mysql Query : delete from cedl_operations.cedl_batch_mapping_t where job_nm='$LND_JOB_NAME' and CAST(table_partition AS DATE) > '${TEMP_CEDL_BATCH_DATE_CP}';"
                `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "delete from cedl_operations.cedl_batch_mapping_t 
                where job_nm='$LND_JOB_NAME' and CAST(table_partition AS DATE) >= '${TEMP_CEDL_BATCH_DATE_CP}';"`
                if [ $? -eq 0 ]
                then
                        log_print "[INFO:] [Re run] Successfully deleted all records greater than equal to $TEMP_CEDL_BATCH_DATE"
                else
                        log_print "[ERROR:] [Re run] Failed to deleted all records greater than equal to $TEMP_CEDL_BATCH_DATE"
                        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${START_TIME_UTC}"
                fi
        else
                log_print "[ERROR:] [Re run] Failed to Get partition date from audit mapping table."
                log_print "[ERROR:] [Re run] Failed to run job for date : $CEDL_BATCH_DATE for given batch load number : $BATCH_LD_NBR "
                audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${START_TIME_UTC}"
        fi
        LAST_PROCESS_BATCH_LD_NBR=$MIN_BATCH_LD_NBR
fi

#In Normal Run Avoiding re run for same CEDL_BATCH_DATE
log_print "[INFO:] MySql Query : 'select count(*) from cedl_operations.cedl_batch_mapping_t where job_nm='$LND_JOB_NAME' and table_partition='$CEDL_BATCH_DATE';'"
CEDL_BATCH_DATE_COUNT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select count(*) from cedl_operations.cedl_batch_mapping_t where job_nm='$LND_JOB_NAME' and table_partition='$CEDL_BATCH_DATE';"`
log_print "[INFO:] Number of records for $CEDL_BATCH_DATE in audit mapping table : $CEDL_BATCH_DATE_COUNT"
if [ ${CEDL_BATCH_DATE_COUNT} -ne 0 ]
then
        log_print "[ERROR:] Failed Entry for date : $CEDL_BATCH_DATE is already present in cedl_batch_mapping_t table."
        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${START_TIME_UTC}"
fi

log_print "[INFO:] Batch Load Number : $LAST_PROCESS_BATCH_LD_NBR"

LOADTIMESTAMP=$(date -u  '+%Y-%m-%d %H:%M:%S')

log_print "*************SQOOP IMPORT JOB UTILITY*******************"
#Checking for directory
hdfs dfs -test -d ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
        if [ $? -eq 0 ]
        then
                hdfs dfs -rm -r ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
                if [ $? -eq 0 ]
                then
                
                        log_print "[INFO:] Files deleted successfully in $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
                        
                else
                
                        log_print "[ERROR:] $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE files deletion failed"
            
                fi
        fi


#With The Help of Global Property Files ... Desiding Sqoop Import Utility
if [[ $SQOOP_QUERY_IMPORT_TABLENAMES = *"$TABLE_NAME"* ]]
then
    for i in $(echo $SQOOP_QUERY_IMPORT_TABLENAMES | sed "s/|/ /g") 
    do
            if [[ $i = *"$TABLE_NAME"* ]]
            then 
                PRIMARY_KEY=`echo $i | cut -d ',' -f2 `
                log_print "[INFO:] PRIMARY_KEY : $PRIMARY_KEY"
                #For consumer_id_t table query option is used over direct for the raw binary column extraction 
                log_print "[INFO:] Running Sqoop Query using select * statement for table $TABLE_NAME."
                log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
                            --query 'select * from $TABLE_NAME where batch_ld_nbr > ${LAST_PROCESS_BATCH_LD_NBR} and \$CONDITIONS' \
                            --split-by $PRIMARY_KEY \
                            --fetch-size '1000' \
                            --hcatalog-database $LND_DATABASE_NAME \
                            --hcatalog-table $TABLE_NAME \
                            --hcatalog-partition-keys $PARTITION_COL \
                            --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                            --hcatalog-storage-stanza 'stored as orc tblproperties (orc.compress=ZLIB)' \
                            -m $DEF_SQOOP_UPSERT_MAPPER_CNT"
                sqoop import --connect $SQOOP_CONNNECTION_URL \
                            --query "select * from $TABLE_NAME where batch_ld_nbr > ${LAST_PROCESS_BATCH_LD_NBR} and \$CONDITIONS" \
                            --split-by $PRIMARY_KEY \
                            --fetch-size '1000' \
                            --hcatalog-database $LND_DATABASE_NAME \
                            --hcatalog-table $TABLE_NAME \
                            --hcatalog-partition-keys $PARTITION_COL \
                            --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                            --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
                            -m $DEF_SQOOP_UPSERT_MAPPER_CNT
                
            fi
    done
else
        log_print "[INFO:] Running Sqoop Query using where clause for table $TABLE_NAME"
        log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
        --table $TABLE_NAME \
        --direct \
        --where 'batch_ld_nbr > ${LAST_PROCESS_BATCH_LD_NBR}' \
        --hcatalog-database $LND_DATABASE_NAME \
        --hcatalog-table $TABLE_NAME \
        --hcatalog-partition-keys batch_date \
        --hcatalog-partition-values ${CEDL_BATCH_DATE} \
        --hcatalog-storage-stanza 'stored as orc tblproperties (orc.compress=ZLIB)' \
        --escaped-by '\' \
        -m $DEF_SQOOP_UPSERT_MAPPER_CNT"
        sqoop import --connect $SQOOP_CONNNECTION_URL \
            --table $TABLE_NAME \
            --direct \
            --where "batch_ld_nbr > ${LAST_PROCESS_BATCH_LD_NBR}" \
            --hcatalog-database $LND_DATABASE_NAME \
            --hcatalog-table $TABLE_NAME \
            --hcatalog-partition-keys batch_date \
            --hcatalog-partition-values ${CEDL_BATCH_DATE} \
            --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
            --escaped-by '\' \
            -m $DEF_SQOOP_UPSERT_MAPPER_CNT
fi
            
if [ $? -eq 0 ]
then
    
            log_print "[INFO:] Successfully completed sqoop extraction for date $CEDL_BATCH_DATE"
            #Getting Hive Count From Landing Layre For Respective Table
            HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "SELECT count(*) cnt from $LND_DATABASE_NAME.$TABLE_NAME 
            WHERE $PARTITION_COL='${CEDL_BATCH_DATE}'" | grep -Po "\d+" | tail -n1`
            log_print  "[INFO:] Hive count after sqoop utility from $LND_DATABASE_NAME.$TABLE_NAME : $HIVE_COUNT"
            
            #If sqoop extracted record is zero
            if [ $HIVE_COUNT == 0 ]
            then
                    log_print "[INFO:] For CEDL_BATCH_DATE date extracted count is zero"
                    audit_cleanup "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                    
            fi
            
            #Getting Min and Max Batch Load Number From Landing Layer For Respective Table
            BATCH_LD_NBRS=`${BEELINE_CONNECTION_URL} -e "SELECT min(batch_ld_nbr),max(batch_ld_nbr) bln from $LND_DATABASE_NAME.$TABLE_NAME WHERE batch_date='${CEDL_BATCH_DATE}' and batch_ld_nbr is NOT NULL" | grep -Po "\d+"`
            if [ $? -eq 0 ]
            then
                    JOB_STAT_NM="success"
                    MIN_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f2`
                    MAX_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f3`
                    log_print  "[INFO:] Max Batch load number after sqoop utility from $LND_DATABASE_NAME.$TABLE_NAME : $MAX_BATCH_LD_NBR"
                    if [ "$MAX_BATCH_LD_NBR" != 0 ] && [ ! -z "$MAX_BATCH_LD_NBR" ] 
                    then        
                            
                            #Insert Min and Max Batch Load Number Into Audit cedl_batch_mapping_t Table
                            `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "insert into cedl_operations.cedl_batch_mapping_t
                            (job_nm,table_partition,min_batch_ld_nbr,max_batch_ld_nbr) 
                            values('$LND_JOB_NAME','$CEDL_BATCH_DATE',$MIN_BATCH_LD_NBR,$MAX_BATCH_LD_NBR);"`
                            if [ $? -eq 0 ]
                            then
                                    log_print "[INFO:] Successfully insert records in cedl_batch_mapping_t for $LND_JOB_NAME job name"
                            else
                                    log_print "[ERROR:] Failed to insert records in cedl_batch_mapping_t for $LND_JOB_NAME job name"
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                            fi
                            
                            # Updating Max Batch_ld_nbr Into Audit Table For Landing Layer
                            `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t 
                            set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
                            src_column_nm='$ICR_LOAD_COL_NM',most_recent_val_txt=$MAX_BATCH_LD_NBR,cdc_tms='$LOADTIMESTAMP'
                            where job_nm = '$LND_JOB_NAME'"`
                            if [ $? -eq 0 ]
                            then
                            
                                    log_print  "[INFO:] Successfully updated max batch load number in cedl_operations.cedl_change_data_capture_t for job $LND_JOB_NAME."
                                
                            else
                                    
                                    log_print  "[ERROR:] Failed to update max batch load number in cedl_operations.cedl_change_data_capture_t for job $LND_JOB_NAME."
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                
                            fi
                            
                            # Updating Min Batch_ld_nbr Into Audit Table For Refine Layer
                            `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t 
                            set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',src_column_nm='$ICR_LOAD_COL_NM',most_recent_val_txt=$LAST_PROCESS_BATCH_LD_NBR,cdc_tms='$LOADTIMESTAMP' \
                            where job_nm = '$REF_JOB_NAME'"`
                            if [ $? -eq 0 ]
                            then
                            
                                    log_print  "[INFO:] Successfully updated min batch load number in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                    audit_cleanup "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" "${HIVE_COUNT}" "${MAX_BATCH_LD_NBR}"  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                
                            else
                                    
                                    log_print  "[ERROR:] Failed to update min batch load number in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                
                            fi
                    else
                            
                            log_print "[ERROR:] Failed to get maximum batch lod number from $LND_DATABASE_NAME.$TABLE" 
                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                    fi
            else
                    log_print "[ERROR:] Failed to get maximum and minimum batch lod number from $LND_DATABASE_NAME.$TABLE" 
                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
            
            fi      
    else
            
            log_print "[ERROR:] Failed while running sqoop job" 
            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${DATABASE_NAME}" "${TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL  "${LND_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
    
    
fi


} >> "$CEDL_BASE_PATH"/log/"$LOG_DATE"/"${LND_JOB_NAME}_${DIR_TIME}.log" 2>&1;
