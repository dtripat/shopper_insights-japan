################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : load_hive_fact_reinstate_wrapper.sh                            #
# Last Modified : 16/03/2018                                                   #
# Description : This script update fact data in refine layer with delta tables #
#               1. using Sqoop, extract delta table into Hive table            #
#               2. Using Hive map side join perform updation on fact table     #
#                   order_trade_item_fact_t                                    #
################################################################################
#commandline : sh /home/atambe/shopper-insights/data-ingestion/edw4x/src/main/bin/load_hive_fact_reinstate_wrapper.sh 2018-03-16 pn1useh1 order_trade_item_fact_t
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

# Function to print the logs with current utc time 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

#Usage descriptor for invalid input arguments to the wrapper
Show_Usage()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[INFO:] Invalid arguments please pass exactly three arguments"
        log_print "Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename>"
        exit 1
}

load_resource()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        . $1
        if [ $? -eq 0 ]
        then    
            log_print "[INFO:] Successfully $1 Loaded "
        else
            log_print "[ERROR:] Failed to $1 Load " 
            exit 1
        fi
}

#Insert in audit table
audit_cleanup(){

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
REF_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
REF_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
REF_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
START_TIME_UTC="${16}"
log_print "***********************************************Audit Entry**************************************************************"
log_print "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) values ($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','$END_TIME')"
`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
(job_key,job_shld_tm,job_nm,job_grp_nm,
job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,
src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) 
values ($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME',
'$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME',
'$TABLE_NAME','$REF_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}')"`
if [ $? -eq 0 ]
then
        log_print  "[INFO:] successfully Inserted record into audit table"
else
        log_print "[ERROR:] Failed to insert record in audit table"
fi

if [ "$JOB_STAT_NM" == "failure" ]
then
        log_print "[ERROR:] Job Failed after Hive job.........Please check"
        END_TIME_UTC=$(date +%s)
        log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
        log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
        deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
        printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
        exit 1
else
        log_print "[INFO:] Job completed successfully"
fi

END_TIME_UTC=$(date +%s)
log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))

}

if [ $# -ne 3 ]
then
        Show_Usage
fi


#Initializing all variables
START_TIME=$(date '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=`date +%s`
SHEDULED_DATE=$1
SRC_DB_NAME=$2
TABLE_NAME=$3
CEDL_BATCH_DATE=$( date -d "${SHEDULED_DATE} -1 days" '+%Y-%m-%d')
REF_CEDL_BATCH_DATE=$CEDL_BATCH_DATE
CRNT_BATCH_DATE=$(date '+%Y-%m-%d')
CRNT_BATCH_DATE=$( date -d "${CRNT_BATCH_DATE} -1 days" '+%Y-%m-%d')
LOG_DATE=$(date -d "$SHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"
HQL_ORDER_DESIG_CNSMR_ID_ASSN_DELTA_T="create_pn1useh1_temp_order_desig_cnsmr_id_assn_delta_t.hql"
HQL_ADHOC_ORDER_TRADE_ITEM_FACT_T="create_pn1useh1_temp_order_trade_item_fact_t.hql"
FAILURE=failure
SUCCESS=success
#Delta Table Database Name and Table Name 
DELTA_DATABASE_NAME="pn1wweh1"
DELTA_TABLE_NAME="order_desig_cnsmr_id_assn_delta_t"
echo "Delta table name : $DELTA_TABLE_NAME"

# Loading environment based properties file.

load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.

load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .

 load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties

#Jobname for Audit: <lnd/ref/app>_ <modulename>_<sourcedb>_<tablename>
LND_JOB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"
REF_JOB_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"
echo "Refine Job Nam : $REF_JOB_NAME"

#Data Directory for job     
LND_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"
REF_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${REF_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"
TEMP_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"
echo "Temp Data Directory : $TEMP_DATA_DIRECTORY"

#Database Name For Landing and Refine Layer   <DB_prefix>_<source_db_name>  
LND_DATABASE_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}"
REF_DATABASE_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}"
TEMP_DATABASE_NAME="${TEMP_DB_PREFIX}_${SRC_DB_NAME}"
echo "Temp DataBase Name : $TEMP_DATABASE_NAME"

#Creating directory for logging 
mkdir -p  "$CEDL_BASE_PATH"/"log"/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Log path directory created  $CEDL_BASE_PATH/log/$LOG_DATE/ Successfully"
else
        log_print "ERROR  : Log path directory creation  $CEDL_BASE_PATH/log/$LOG_DATE/ failed"
        exit 1
        
fi
log_print "Logs Directory location: \n\t\t\t - $CEDL_BASE_PATH/log/$LOG_DATE/${REF_JOB_NAME}_${DIR_TIME}.log "

#Extracting source credentials
EDW4X_DB_HOST_IP_PORT=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f1`
EDW4X_DB_HOST_USER=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f2`
EDW4X_DB_HOST_PASSWORD=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f3-`
SQOOP_CONNNECTION_URL="jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${DELTA_DATABASE_NAME} --username ${EDW4X_DB_HOST_USER}  --password ${EDW4X_DB_HOST_PASSWORD}"


#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

BEELINE_PRINT_HEADER="set hive.cli.print.header=FALSE;"
BEELINE_JOIN_NOCONDITIONALTASK="set hive.auto.convert.join.noconditionaltask = true;"
BEELINE_JOIN_NOCONDITIONALTASK_SIZE="set hive.auto.convert.join.noconditionaltask.size=${DEF_HIVE_MAPJOIN_MEMORY};"
BEELINE_DYNAMIC_PARTITION="set hive.exec.dynamic.partition=true;"
BEELINE_DYNAMIC_PARTITION_NONSTRICT="set hive.exec.dynamic.partition.mode=nonstrict;"

{
#Printing Argument Provided by User.
log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments : $*"

log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

# 1.    Fact_Adhoc table & delta table created already in temp area as external/managed table.
# 2.    Get the delta table in HDP environment via sqoop, overwrite existing delta table data. 
# 3.    Select max(partition) i.e. batch_date for original fact.
# 4.    Do map join to insert(overwrite)  Fact_Adhoc table (2.5 TB) using base(original fact) & delta table.
# 5.    Drop the directories in original fact table <= max(partition) retrieved. 
# 6.    Move the directories of Fact_Adhoc table into original fact table data location till max(partition).

#-------------------------------------------------------------------------------------------------------------------------
#Getting job key and last processed batch load number
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select job_key from cedl_operations.cedl_job_t where job_nm='$REF_JOB_NAME'"` 
log_print "[INFO:] Job Key for $REF_JOB_NAME job: $JOB_KEY"

#Get the delta table in HDP environment via sqoop, overwrite existing delta table data
#Dropping delta Table If Exists
`${BEELINE_CONNECTION_URL} -f ${CEDL_BASE_PATH}/hive/${HQL_ORDER_DESIG_CNSMR_ID_ASSN_DELTA_T} \
                                 -hivevar DB_NAME="$TEMP_DATABASE_NAME" \
                                 -hivevar CEDL_SI_ENV="${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/${SRC_DB_NAME}.db" \
                                 -hivevar TABLE_NAME="$DELTA_TABLE_NAME"`



if [ $? -eq 0 ]
then
        log_print "[INFO:] Successfully created table ${TEMP_DATABASE_NAME}.${DELTA_TABLE_NAME}"
else
        log_print "[ERROR:] Failed to create table ${TEMP_DATABASE_NAME}.${DELTA_TABLE_NAME}"
        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
fi


#Running Sqoop Utility
log_print "[INFO:] sqoop import jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${DELTA_DATABASE_NAME} \
            --table $DELTA_TABLE_NAME \
            --direct \
            --hcatalog-database $TEMP_DATABASE_NAME \
            --hcatalog-table $DELTA_TABLE_NAME \
            --hcatalog-storage-stanza 'stored as orc tblproperties (orc.compress=ZLIB)' \
            --escaped-by '\' \
            -m $DEF_SQOOP_UPSERT_MAPPER_CNT"

sqoop import --connect $SQOOP_CONNNECTION_URL \
            --table $DELTA_TABLE_NAME \
            --direct \
            --hcatalog-database $TEMP_DATABASE_NAME \
            --hcatalog-table $DELTA_TABLE_NAME \
            --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
            --escaped-by '\' \
            -m $DEF_SQOOP_UPSERT_MAPPER_CNT
if [ $? -eq 0 ]
then
        log_print "[INFO:] Sccessfully completed sqoop job"
        DELTA_HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "select count(*) cnt from $TEMP_DATABASE_NAME.$DELTA_TABLE_NAME;" | grep -Po "\d+"`
        if [ "$DELTA_HIVE_COUNT" != 0 ] && [ ! -z "$DELTA_HIVE_COUNT" ]
        then
                    
                    log_print "[INFO:] Delta Hive Count : $DELTA_HIVE_COUNT"
                    #Select max(partition) i.e. batch_date for original fact
                    log_print "[INFO:] select min(cast(table_partition as date)) as partition_date_min,max(cast(table_partition as date)) as partition_date
                    from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME';"
                    CEDL_BATCH_DATE=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select min(cast(table_partition as date)) as partition_date_min,
                    max(cast(table_partition as date)) as partition_date from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME';"`
                    if [ "$CEDL_BATCH_DATE" != 0 ] && [ ! -z "$CEDL_BATCH_DATE" ]
                    then
                            log_print "[INFO:] Max partition value for batch_date partition : $CEDL_BATCH_DATE"
                            log_print "[INFO:] Successfully to get Maximum Partition value from table cedl_batch_mapping_t for Job Name $REF_JOB_NAME"
                            MIN_CEDL_BATCH_DATE=`echo $CEDL_BATCH_DATE | cut -d" " -f1`
                            MAX_CEDL_BATCH_DATE=`echo $CEDL_BATCH_DATE | cut -d" " -f2`
                            log_print "[INFO:] Min Batch Date : $MIN_CEDL_BATCH_DATE and Max Batch Date : $MAX_CEDL_BATCH_DATE"
                            #Checking for Adhoc Fact Table Location in Temp DATABASE_NAME
                            `hdfs dfs -test -d ${TEMP_DATA_DIRECTORY}`
                            if [ $? -eq 0 ]
                            then
                                    `hdfs dfs -rm -r ${TEMP_DATA_DIRECTORY}/*`
                            fi
                            #Create Adhoc Fact table in Temp Database
                            `${BEELINE_CONNECTION_URL} -f ${CEDL_BASE_PATH}/hive/${HQL_ADHOC_ORDER_TRADE_ITEM_FACT_T} \
                                 -hivevar DB_NAME="$TEMP_DATABASE_NAME" \
                                 -hivevar CEDL_SI_ENV="${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/${SRC_DB_NAME}.db" \
                                 -hivevar TABLE_NAME="$TABLE_NAME"`
                            
                            #Perform map join to insert(overwrite) Adhoc Fact Table in Temp layer
                            log_print "${BEELINE_DYNAMIC_PARTITION} ${BEELINE_DYNAMIC_PARTITION_NONSTRICT} ${BEELINE_JOIN_NOCONDITIONALTASK} ${BEELINE_JOIN_NOCONDITIONALTASK_SIZE} INSERT INTO TABLE $TEMP_DATABASE_NAME.$TABLE_NAME partition(batch_date,ord_date_key) SELECT a.event_key,a.sesn_key,a.event_typ_cd,a.ord_time_key,
                            a.trade_item_key,a.ord_touchpoint_key,a.ord_event_key,a.tran_item_grp_key,
                            CASE 
                            WHEN (b.ord_event_key is NULL) then a.ord_designated_cnsmr_id_key 
                            ELSE
                            b.new_cnsmr_id_key
                            END as new_cnsmr_id_key,
                            CASE 
                            WHEN (b.ord_event_key is NULL) then a.cnsmr_id_typ_loy_cd 
                            ELSE
                            b.cnsmr_id_typ_loy_cd
                            END as cnmsr_id_typ_loy_cd,
                            CASE 
                            WHEN (b.ord_event_key is NULL) then a.unident_ind 
                            ELSE
                            b.unident_ind
                            END as unident_ind,
                            a.dist_chanl_cd,a.purch_qty,a.purch_amt,a.adj_amt,
                            a.vol_wgt_qty,a.ndc_ind,a.event_tms,a.hist_ntwk_id,a.data_qual_err_typ_cd,a.data_src_cd,a.mngng_cntry_cd,a.batch_ld_nbr,
                            a.loadtimestamp,
                            a.batch_date,a.ord_date_key
                            FROM $REF_DATABASE_NAME.$TABLE_NAME a LEFT OUTER JOIN $TEMP_DATABASE_NAME.$DELTA_TABLE_NAME b ON (a.ord_event_key=b.ord_event_key);"

                            `${BEELINE_CONNECTION_URL} -e "${BEELINE_DYNAMIC_PARTITION} ${BEELINE_DYNAMIC_PARTITION_NONSTRICT} ${BEELINE_JOIN_NOCONDITIONALTASK} ${BEELINE_JOIN_NOCONDITIONALTASK_SIZE} 
                            INSERT INTO TABLE $TEMP_DATABASE_NAME.$TABLE_NAME partition(batch_date,ord_date_key) SELECT a.event_key,a.sesn_key,a.event_typ_cd,a.ord_time_key,
                            a.trade_item_key,a.ord_touchpoint_key,a.ord_event_key,a.tran_item_grp_key,
                            CASE 
                            WHEN (b.ord_event_key is NULL) then a.ord_designated_cnsmr_id_key 
                            ELSE
                            b.new_cnsmr_id_key
                            END as new_cnsmr_id_key,
                            CASE 
                            WHEN (b.ord_event_key is NULL) then a.cnsmr_id_typ_loy_cd 
                            ELSE
                            b.cnsmr_id_typ_loy_cd
                            END as cnmsr_id_typ_loy_cd,
                            CASE 
                            WHEN (b.ord_event_key is NULL) then a.unident_ind 
                            ELSE
                            b.unident_ind
                            END as unident_ind,
                            a.dist_chanl_cd,a.purch_qty,a.purch_amt,a.adj_amt,
                            a.vol_wgt_qty,a.ndc_ind,a.event_tms,a.hist_ntwk_id,a.data_qual_err_typ_cd,a.data_src_cd,a.mngng_cntry_cd,a.batch_ld_nbr,
                            a.loadtimestamp,
                            a.batch_date,a.ord_date_key
                            FROM $REF_DATABASE_NAME.$TABLE_NAME a LEFT OUTER JOIN $TEMP_DATABASE_NAME.$DELTA_TABLE_NAME b ON (a.ord_event_key=b.ord_event_key);"`
                            if [ $? -ne 0 ]
                            then
                                    log_print "[ERROR:] Fail while running Hive job."
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                            fi
                            

                            #Avoid concurrency issue before moving data checking new partition is created in refine table
                            #Again getting max batch date from audit table to avoid data loss
                            CEDL_BATCH_DATE=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select max(cast(table_partition as date)) as partition_date 
                            from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME';"`
                            if [ "$CEDL_BATCH_DATE" == "$MAX_CEDL_BATCH_DATE" ]
                            then
                                    log_print "[INFO:] Same partitions present before and after Hive Job."
                                    #Moving Refine table location data to Temp location. [To avoid data lose]
                                    TEMP_LOCATION="${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/order_trade_item_fact_t_bkp"
                                    
                                    #Checking for Temp location
                                    `hdfs dfs -test -d ${TEMP_LOCATION}`
                                    if [ $? -eq 0 ]
                                    then
                                            `hdfs dfs -rm -r ${TEMP_LOCATION}/*`
                                    fi
                                    `hdfs dfs -mkdir -p ${TEMP_LOCATION}`
                                    `hdfs dfs -mv ${REF_DATA_DIRECTORY}/* ${TEMP_LOCATION}/`
                                    if [ $? -ne 0 ]
                                    then
                                            log_print "[ERROR:] Fail to move data from Refine table : ${REF_DATA_DIRECTORY} location to Temp location : ${TEMP_LOCATION}"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                                    log_print "[INFO:] Successfully move data from Refine table : ${REF_DATA_DIRECTORY} location to Temp location : ${TEMP_LOCATION}"
                                    #Moving our Temp table data into Refine table location.
                                    `hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* ${REF_DATA_DIRECTORY}/`
                                    if [ $? -ne 0 ]
                                    then
                                            log_print "[ERROR:] Fail to move data from Temp table : ${TEMP_DATA_DIRECTORY} location to Refine location : ${REF_DATA_DIRECTORY}"
                                            `hdfs dfs -mv ${TEMP_LOCATION}/* ${REF_DATA_DIRECTORY}/`
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                                    log_print "[INFO:] Successfully move data from Temp table : ${TEMP_DATA_DIRECTORY} location to Refine location : ${REF_DATA_DIRECTORY}"
                                    
                                    #Refine Table partition
                                    `${BEELINE_CONNECTION_URL} -e "MSCK REPAIR TABLE $REF_DATABASE_NAME.$TABLE_NAME"`
                                    if [ $? -eq 0 ]
                                    then
                                            log_print "[INFO:] Successfully alter partitions from table : $REF_DATABASE_NAME.$TABLE_NAME"
                                    else
                                            log_print "[ERROR:] Fail to alter partitions from table : $REF_DATABASE_NAME.$TABLE_NAME"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                                    
                            else
                                    #Move newly added partition to Temp table location
                                    `hdfs dfs -mv ${REF_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE} ${TEMP_DATA_DIRECTORY}/`
                                    if [ $? -ne 0 ]
                                    then
                                            log_print "[ERROR:] Fail to move partition data from Refine table : ${REF_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE} location to Refine location : ${TEMP_DATA_DIRECTORY}"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                                    log_print "[INFO:] Successfully move partition data from Refine table : ${REF_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE} location to Refine location : ${TEMP_DATA_DIRECTORY}"
                                    
                                    #Moving Refine table location data to Temp location. [To avoid data lose]
                                    TEMP_LOCATION="${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/order_trade_item_fact_t_bkp"
                                    #Checking for Temp location
                                    `hdfs dfs -test -d ${TEMP_LOCATION}`
                                    if [ $? -eq 0 ]
                                    then
                                            `hdfs dfs -rm -r ${TEMP_LOCATION}/*`
                                    fi
                                    `hdfs dfs -mkdir -p ${TEMP_LOCATION}`
                                    `hdfs dfs -mv ${REF_DATA_DIRECTORY}/* ${TEMP_LOCATION}/`
                                    if [ $? -ne 0 ]
                                    then
                                            log_print "[ERROR:] Fail to move data from Refine table : ${REF_DATA_DIRECTORY} location to Temp location : ${TEMP_LOCATION}"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                                    log_print "[INFO:] Successfully move data from Refine table : ${REF_DATA_DIRECTORY} location to Temp location : ${TEMP_LOCATION}"
                                    #Moving our Temp table data into Refine table location.
                                    `hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* ${REF_DATA_DIRECTORY}/`
                                    if [ $? -ne 0 ]
                                    then
                                            log_print "[ERROR:] Fail to move data from Temp table : ${TEMP_DATA_DIRECTORY} location to Refine location : ${REF_DATA_DIRECTORY}"
                                            `hdfs dfs -mv ${TEMP_LOCATION}/* ${REF_DATA_DIRECTORY}/`
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                                    log_print "[INFO:] Successfully move data from Temp table : ${TEMP_DATA_DIRECTORY} location to Refine location : ${REF_DATA_DIRECTORY}"
                                    
                                    #Refine Table partition
                                    `${BEELINE_CONNECTION_URL} -e "MSCK REPAIR TABLE $REF_DATABASE_NAME.$TABLE_NAME"`
                                    if [ $? -eq 0 ]
                                    then
                                            log_print "[INFO:] Successfully alter partitions from table : $REF_DATABASE_NAME.$TABLE_NAME"
                                    else
                                            log_print "[ERROR:] Fail to alter partitions from table : $REF_DATABASE_NAME.$TABLE_NAME"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                                    fi
                            fi
                            
                            
                            log_print "[INFO:] Successfully added all partitions in table $REF_DATABASE_NAME.$TABLE_NAME"
                            HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "select count(*) cnt from $REF_DATABASE_NAME.$TABLE_NAME" | grep -Po "\d+"`
                            if [ "$HIVE_COUNT" != 0 ] && [ ! -z "$HIVE_COUNT" ]
                            then
                                    MAX_BATCH_LD_NBR=`${BEELINE_CONNECTION_URL} -e "SELECT max(batch_ld_nbr) bln from $REF_DATABASE_NAME.$TABLE_NAME;" | grep -Po "\d+"`
                                    log_print "[INFO:] Hive count from table $REF_DATABASE_NAME.$TABLE_NAME : $HIVE_COUNT"
                                    audit_cleanup "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" "${HIVE_COUNT}" "${MAX_BATCH_LD_NBR}"  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                            else
                                    log_print "[ERROR:] Failed to get hive count from  table $REF_DATABASE_NAME.$TABLE_NAME"
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                            fi
                            
                                
                            
                    else
                            log_print "[ERROR:] Failed to get Maximum Partition value from table cedl_batch_mapping_t for Job Name $REF_JOB_NAME"
                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
                    fi
        
        else
                    log_print "[ERROR:] Failed to get count from table $TEMP_DATABASE_NAME.$DELTA_TABLE_NAME"
                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"
        fi
else
       log_print "[ERROR:]  Fail while running sqoop job" 
       audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${START_TIME_UTC}"

fi


} >> "$CEDL_BASE_PATH"/log/"$LOG_DATE"/"${REF_JOB_NAME}_${DIR_TIME}".log 2>&1;


