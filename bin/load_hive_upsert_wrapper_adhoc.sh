################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : load_hive_upsert_wrapper.sh                                    #
# Description : This script upsert the data from landing to refined layer      #
################################################################################
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

# Function to print the logs with current utc time 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

#Usage descriptor for invalid input arguments to the wrapper
Show_Usage()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[Error:] Invalid arguments please pass exactly three arguments"
        log_print "Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename> <merge key1,merge kye2,...(Comma Separated)> <batch_load_number>"
        exit 1
}

#Insert in audit table
audit_cleanup(){


END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
REF_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
REF_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
REF_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
REF_CEDL_BATCH_DATE="${16}"
log_print "***********************************************Audit Entry**************************************************************"
log_print "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) values ($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','$END_TIME')"
`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
(job_key,job_shld_tm,job_nm,job_grp_nm,
job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,
src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) 
values ($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME',
'$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME',
'$TABLE_NAME','$REF_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}')"`
if [ $? -eq 0 ]
then
        log_print  "[INFO:] successfully Inserted record into audit table"
fi

if [ "$JOB_STAT_NM" == "failure" ] && [ "$REF_DATA_DIRECTORY" != NULL ]
then
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        #Clean Up Activity If Job is Failed
        log_print "[INFO:] Starting clean up activity..."
        log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
        hdfs dfs -rm -r ${REF_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
        if [ $? -eq 0 ]
        then
        
                log_print "[INFO:] Successfully deleted directory : $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
                
        else
        
                log_print "[ERROR:] Failed to delete directory : $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"

        fi
        #Removing Partition For Respective Date 
        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME 
        DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
        if [ $? -eq 0 ]
        then
                    log_print "[INFO:] Successfully removed partition $CEDL_BATCH_DATE from $REF_DATABASE_NAME.$TABLE_NAME" 
        else    
                    log_print "[ERROR:] Failed to removed partition $CEDL_BATCH_DATE from $REF_DATABASE_NAME.$TABLE_NAME"
        fi
        #altering Hive table Old Partition as Base
        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME 
        ADD IF NOT EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE') LOCATION '$REF_DATA_DIRECTORY/$PARTITION_COL=$REF_CEDL_BATCH_DATE'"`
        if [ $? -eq 0 ]
        then
                log_print "[INFO:] Successfully add partition $REF_CEDL_BATCH_DATE into table $REF_DATABASE_NAME.$TABLE_NAME"
        else
                log_print "[ERROR:] Failed to add partition $REF_CEDL_BATCH_DATE into table $REF_DATABASE_NAME.$TABLE_NAME"
                            
        fi
        log_print "[INFO:] Job Failed after Hive job.........Please check"
        exit 1 
elif [ "$JOB_STAT_NM" == "failure" ] && [ "$REF_DATA_DIRECTORY" == NULL ]
then
        log_print "[INFO:] Job Failed before Hive job.........Please check"
        exit 1
else
        log_print "[INFO:] Job completed successfully"
        
fi 
END_TIME_UTC=$(date +%s)
log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
exit 0
}
load_resource()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        . $1
        if [ $? -eq 0 ]
        then    
            log_print "[INFO:] Successfully $1 Loaded "
        else
            log_print "[ERROR:] Failed to $1 Load " 
            exit 1
        fi
}


if [ $# -lt 4 ]
then
        Show_Usage
fi


#Initializing all variables
START_TIME=$(date '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=`date +%s` 
SHEDULED_DATE=$1
SRC_DB_NAME=$2
TABLE_NAME=$3
CEDL_BATCH_DATE=$( date -d "${SHEDULED_DATE} -1 days" '+%Y-%m-%d')
REF_CEDL_BATCH_DATE=$CEDL_BATCH_DATE
CRNT_BATCH_DATE=$(date '+%Y-%m-%d')
CRNT_BATCH_DATE=$( date -d "${CRNT_BATCH_DATE} -1 days" '+%Y-%m-%d')
LOG_DATE=$(date -d "$SHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
 ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"
FAILURE=failure
SUCCESS=success
#Getting Extra Argument [Batch Load Number]
BATCH_LD_NBR=$5
#Multiple Merge Key
MERGE_KEYS=$4
COUNT=1
MERGE_KEY=""
for i in $(echo $MERGE_KEYS | sed "s/,/ /g") 
do 
        if [ "$COUNT" -gt 1 ]
        then 
                MERGE_KEY+=" and a.$i=b.$i"
        else  
                COUNT=$(($COUNT+1)) 
                MERGE_KEY+="a.$i=b.$i"
        fi 
done
MERGE_KEY="("$MERGE_KEY")"
MERGE_KEY_IS_NULL=""
COUNT=1
for i in $(echo $MERGE_KEYS | sed "s/,/ /g") 
do
        if [ "$COUNT" -gt 1 ]
        then 
                MERGE_KEY_IS_NULL+=" or b.$i IS NULL"
        else 
                COUNT=$(($COUNT+1))
                MERGE_KEY_IS_NULL+="b.$i IS NULL"
        fi
done

# Loading environment based properties file.

load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.

load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .

 load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties


#Jobname for Audit: <lnd/ref/app>_ <modulename>_<sourcedb>_<tablename>
LND_JOB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"
REF_JOB_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"

#Data Directory for job     
LND_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"
REF_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${REF_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"
TEMP_DATA_DIRECTORY="${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${TABLE_NAME}"

#Database Name For Landing and Refine Layer   <DB_prefix>_<source_db_name>  
LND_DATABASE_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}"
REF_DATABASE_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}"
TEMP_DATABASE_NAME="${TEMP_DB_PREFIX}_${SRC_DB_NAME}"

#Creating directory for logging 
mkdir -p  "$CEDL_BASE_PATH"/"log"/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Successfully created Log path directory $CEDL_BASE_PATH/log/$LOG_DATE/"
else
        log_print "[ERROR:] Failed to create Log path directory $CEDL_BASE_PATH/log/$LOG_DATE/"
        exit 1
        
fi
log_print "Logs Directory location:- $CEDL_BASE_PATH/log/$LOG_DATE/${REF_JOB_NAME}_${DIR_TIME}.log "

#Extracting source credentials
EDW4X_DB_HOST_IP_PORT=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f1`
EDW4X_DB_HOST_USER=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f2`
EDW4X_DB_HOST_PASSWORD=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f3-`
SQOOP_CONNNECTION_URL="jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} --username ${EDW4X_DB_HOST_USER}  --password ${EDW4X_DB_HOST_PASSWORD}"

#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

BEELINE_PRINT_HEADER="set hive.cli.print.header=FALSE;"
BEELINE_JOIN_NOCONDITIONALTASK="set hive.auto.convert.join.noconditionaltask = true;"
BEELINE_JOIN_NOCONDITIONALTASK_SIZE="set hive.auto.convert.join.noconditionaltask.size=${DEF_HIVE_MAPJOIN_MEMORY};"
BEELINE_COMPUTE_STATS="set hive.compute.query.using.stats=false;"

{
#Printing Argument Provided by User.
log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments : $*"

log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

#Getting job key and last processed batch load number
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select job_key from cedl_operations.cedl_job_t where job_nm='$REF_JOB_NAME'"` 
log_print "[INFO:] Job Key for $REF_JOB_NAME job: $JOB_KEY"

#Getting Base Partition for Refine Table
REF_CEDL_BATCH_DATE=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select table_partition from cedl_operations.cedl_batch_mapping_t
where job_nm='$REF_JOB_NAME' order by table_partition desc limit 1;"`

#If user passs extra arguments [Re run scenario]
if [[ ! -z "$BATCH_LD_NBR" ]]
then
        log_print "[INFO:] Re run block Exection start....."
        #Getting Table Partition for respective Batch Load Number Argument.
        log_print "[INFO:] [Re run] Mysql Query : select max(table_partition) from cedl_operations.cedl_batch_mapping_t
        where max_batch_ld_nbr < (select min(max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t 
        where job_nm='$REF_JOB_NAME' and max_batch_ld_nbr >= $BATCH_LD_NBR and min_batch_ld_nbr <= $BATCH_LD_NBR) and job_nm='$REF_JOB_NAME';"
        QUERY_OUTPUT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select max(table_partition) from cedl_operations.cedl_batch_mapping_t
        where max_batch_ld_nbr < (select min(max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t 
        where job_nm='$REF_JOB_NAME' and max_batch_ld_nbr >= $BATCH_LD_NBR and min_batch_ld_nbr <= $BATCH_LD_NBR) and job_nm='$REF_JOB_NAME';"`
        
        log_print "[INFO:] [Re run] Maximum table partition date for given batch load number : $QUERY_OUTPUT"
        
        #Getting Partition Date and Min Batch Load Number
        REF_CEDL_BATCH_DATE=$QUERY_OUTPUT
        TEMP_CEDL_BATCH_DATE_CP=$REF_CEDL_BATCH_DATE
        if [ "$REF_CEDL_BATCH_DATE" != "NULL" ] && [ ! -z "$REF_CEDL_BATCH_DATE" ] 
        then
                
                #Cleanup of Hive Partition and its Locations For Refine With Help of Above Partition Date $TEMP_CEDL_BATCH_DATE
                log_print "[INFO:] [Re run] Current Date : $CRNT_BATCH_DATE"
                TEMP_CEDL_BATCH_DATE_CP=$(date -d "${TEMP_CEDL_BATCH_DATE_CP} + 1 days" '+%Y-%m-%d')
                log_print "[INFO:] [Re run] Copy of max table partition date + 1 : $TEMP_CEDL_BATCH_DATE_CP"
                while [ "${TEMP_CEDL_BATCH_DATE_CP}" != "${CRNT_BATCH_DATE}" ]
                do 
                        #Removing Partition For Respective Date [From $TEMP_CEDL_BATCH_DATE_CP to Current Date<$CRNT_BATCH_DATE>]
                        `${BEELINE_CONNECTION_URL}  -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME 
                        DROP IF EXISTS PARTITION($PARTITION_COL='${TEMP_CEDL_BATCH_DATE_CP}')"`
                        if [ $? -eq 0 ]
                        then
                                    log_print "[INFO:] [Re run] Successfully removed partition $TEMP_CEDL_BATCH_DATE_CP from $REF_DATABASE_NAME.$TABLE_NAME" 
                        else    
                                    log_print "[ERROR:] [Re run] Failed to removed partition $TEMP_CEDL_BATCH_DATE_CP from $REF_DATABASE_NAME.$TABLE_NAME"
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                        fi
                        
                        #Removing directory For Respective Partition [From $TEMP_CEDL_BATCH_DATE_CP to Current Date<$CRNT_BATCH_DATE>]
                        hdfs dfs -test -d $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP                
                        if [ $? -eq 0 ]
                        then
                                hdfs dfs -rm -r $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP
                                if [ $? -eq 0 ]
                                then
                                            log_print "[INFO:] [Re run] Successfully removed directory $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP from $REF_DATABASE_NAME.$TABLE_NAME" 
                                else    
                                            log_print "[ERROR:] [Re run] Failed to removed directory $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP from $REF_DATABASE_NAME.$TABLE_NAME"
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                                fi
                        fi
                        
                        
                        TEMP_CEDL_BATCH_DATE_CP=$(date -I -d "$TEMP_CEDL_BATCH_DATE_CP + 1 day")
                done
                #altering Hive table partition to new one getting from audit mapping table
                `${BEELINE_CONNECTION_URL} -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME 
                ADD IF NOT EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE') LOCATION '$REF_DATA_DIRECTORY/$PARTITION_COL=$REF_CEDL_BATCH_DATE'"`
                if [ $? -eq 0 ]
                then
                        log_print "[INFO:] [Re run] Successfully add partition $REF_CEDL_BATCH_DATE into table $REF_DATABASE_NAME.$TABLE_NAME"
                else
                        log_print "[ERROR:] [Re run] Failed to add partition $REF_CEDL_BATCH_DATE into table $REF_DATABASE_NAME.$TABLE_NAME"
                        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                                    
                fi
                #Deleting Old Data From Audit Table<cedl_batch_mapping_t>
                log_print "delete from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME' and CAST(table_partition AS DATE) > '${REF_CEDL_BATCH_DATE}';"
                `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "delete from cedl_operations.cedl_batch_mapping_t 
                where job_nm='$REF_JOB_NAME' and CAST(table_partition AS DATE) > '${REF_CEDL_BATCH_DATE}';"`
                if [ $? -eq 0 ]
                then
                        log_print "[INFO:] [Re run] Successfully deleted all records greater than $REF_CEDL_BATCH_DATE"
                else
                        log_print "[ERROR:] [Re run] Failed to deleted all records greater than $REF_CEDL_BATCH_DATE"
                        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                fi
        else
                log_print "[ERROR:] [Re run] Failed to Get partition date from audit mapping table."
                log_print "[ERROR:] [Re run] Failed to run job for date : $CEDL_BATCH_DATE for given batch load number : $BATCH_LD_NBR "
                audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
        log_print "[INFO:] Re run block Exection end....."
fi

#Getting Hive Count From Landing Layre For Respective Table
LND_HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "SELECT count(*) cnt from $LND_DATABASE_NAME.$TABLE_NAME 
WHERE $PARTITION_COL='${CEDL_BATCH_DATE}'" | grep -Po "\d+" | tail -n1`
log_print  "[INFO:] Hive count of Landing Layer for $LND_DATABASE_NAME.$TABLE_NAME : $LND_HIVE_COUNT"
            
#If sqoop extracted record is zero
if [ $LND_HIVE_COUNT == 0 ]
then
log_print "[INFO:] For CEDL_BATCH_DATE date extracted count is zero"
audit_cleanup "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi

#In Normal Run Avoiding re run for same CEDL_BATCH_DATE
CEDL_BATCH_DATE_COUNT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select count(*) from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME' and table_partition='$CEDL_BATCH_DATE';"`
log_print "[INFO:] Number of records for $CEDL_BATCH_DATE in audit mapping table : $CEDL_BATCH_DATE_COUNT"
if [ ${CEDL_BATCH_DATE_COUNT} -ne 0 ]
then
        log_print "[ERROR:] Failed Entry for date : $CEDL_BATCH_DATE is already present in cedl_batch_mapping_t table."
        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi


log_print "*************Hive Merge JOB UTILITY*******************"
#Replace Current Load Timestamp In The Options File.
LOADTIMESTAMP=$(date -u  '+%Y-%m-%d %H:%M:%S')


#Checking Directory before creation Directory Creation For Refine Table Name
log_print "[INFO:] Checking for current run date directory block start....."
hdfs dfs -test -d $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE                
if [ $? -eq 0 ]
then
        hdfs dfs -rm -r $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE
        if [ $? -eq 0 ]
        then
                    log_print "[INFO:] [Re run] Successfully drop directory $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE into $REF_DATABASE_NAME.$TABLE_NAME" 
                    `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DATABASE_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$CEDL_BATCH_DATE');"`
                    if [ $? -ne 0 ]
                    then
                            log_print "[ERROR:] Failed to drop schedule date : '$CEDL_BATCH_DATE' partition : $PARTITION_COL from refine table : ${TABLE_NAME}"
                    fi
                    log_print "[INFO:] Successfully drop schedule date : '$CEDL_BATCH_DATE' partition : $PARTITION_COL from refine table : ${TABLE_NAME}"
                    `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME 
                    ADD IF NOT EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE')"`                    
                    if [ $? -ne 0 ]
                    then
                            log_print "[ERROR:] While adding partition : $PARTITION_COL='$REF_CEDL_BATCH_DATE' into table $REF_DATABASE_NAME.$TABLE_NAME"
                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                    fi
        else    
                    log_print "[ERROR:] Failed to dropped directory $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE into $REF_DATABASE_NAME.$TABLE_NAME"
                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
fi
log_print "[INFO:] Checking for current run date directory block end....."

#Creating Directory at refine layer
hdfs dfs -mkdir -p $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE                        
if [ $? -eq 0 ]
then

        log_print "[INFO:] Successfully created directory $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
else
        log_print "[ERROR:] Failed to create directory $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi

log_print "[INFO:] Hive job execution block start....."
#Getting column names for Refine table
rm "$CEDL_BASE_PATH"/tmp/ref_columns_list.txt
rm "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt

${BEELINE_CONNECTION_URL} --silent=true  --headerInterval=1000 -e "describe ${REF_DATABASE_NAME}.${TABLE_NAME}" > "$CEDL_BASE_PATH"/tmp/ref_columns_list.txt
cat "$CEDL_BASE_PATH"/tmp/ref_columns_list.txt | awk -F" " '{print $2}'| sed 's/ //g'|sed 's/'"${PARTITION_COL}"'//g'| sed 's/col_name//g' | sed 's/loadtimestamp//g' | sed 's/|//g' | sed 's/#//g' | sed '/^$/d '|sed ':a;N;$!ba;s/\n/,/g' | sed 's/,/,a./g' > "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt

#Checking for file is not empty
if [[ -s "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt ]]
then
        log_print "[INFO:] REF_CEDL_BATCH_DATE $REF_CEDL_BATCH_DATE"
        COLUMNS_LIST=`cat "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt`   
        
        log_print "DROP TABLE IF EXISTS ${TEMP_DATABASE_NAME}.${TABLE_NAME};
        CREATE TABLE ${TEMP_DATABASE_NAME}.${TABLE_NAME} stored as orc location '${TEMP_DATA_DIRECTORY}' tblproperties ('orc.compress'='ZLIB') as SELECT /*+ MAPJOIN(b) */ a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${REF_DATABASE_NAME}.${TABLE_NAME} a
        LEFT OUTER JOIN ${LND_DATABASE_NAME}.${TABLE_NAME} b ON $MERGE_KEY  and b.${PARTITION_COL}='${CEDL_BATCH_DATE}'
        WHERE $MERGE_KEY_IS_NULL;
        INSERT INTO TABLE ${TEMP_DATABASE_NAME}.${TABLE_NAME} SELECT a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${LND_DATABASE_NAME}.${TABLE_NAME} a where ${PARTITION_COL}='$CEDL_BATCH_DATE';
        ${BEELINE_CONNECTION_URL} -e ALTER TABLE ${REF_DATABASE_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE');
        hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/
        ${BEELINE_CONNECTION_URL} -e ALTER TABLE ${REF_DATABASE_NAME}.${TABLE_NAME} ADD PARTITION($PARTITION_COL='$CEDL_BATCH_DATE');"
        
        `${BEELINE_CONNECTION_URL} -e "${BEELINE_JOIN_NOCONDITIONALTASK}${BEELINE_JOIN_NOCONDITIONALTASK_SIZE}
        DROP TABLE IF EXISTS ${TEMP_DATABASE_NAME}.${TABLE_NAME};
        CREATE TABLE ${TEMP_DATABASE_NAME}.${TABLE_NAME} stored as orc location '${TEMP_DATA_DIRECTORY}' 
        tblproperties ('orc.compress'='ZLIB','transactional'='false') as SELECT /*+ MAPJOIN(b) */ a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${REF_DATABASE_NAME}.${TABLE_NAME} a
        LEFT OUTER JOIN ${LND_DATABASE_NAME}.${TABLE_NAME} b ON $MERGE_KEY  and b.${PARTITION_COL}='${CEDL_BATCH_DATE}'
        WHERE $MERGE_KEY_IS_NULL;
        INSERT INTO TABLE ${TEMP_DATABASE_NAME}.${TABLE_NAME} SELECT a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${LND_DATABASE_NAME}.${TABLE_NAME} a 
        where ${PARTITION_COL}='$CEDL_BATCH_DATE';"`
        
        `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DATABASE_NAME}.${TABLE_NAME} ADD PARTITION($PARTITION_COL='$CEDL_BATCH_DATE');"`
        if [ $? -ne 0 ]
        then
                log_print "[ERROR:] Failed while Adding partition : $PARTITION_COL='$CEDL_BATCH_DATE' in table ${REF_DATABASE_NAME}.${TABLE_NAME}"
                audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
        hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/
        if [ $? -ne 0 ]
        then
                log_print "[ERROR:] Failed to move data from directory : ${TEMP_DATA_DIRECTORY} to $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/"
        fi
        `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DATABASE_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE');"`
        if [ $? -eq 0 ]
        then
                log_print "[INFO:] Successfully  Added partition : $PARTITION_COL='$CEDL_BATCH_DATE' in table ${REF_DATABASE_NAME}.${TABLE_NAME}"
                log_print "[INFO:] Successfully move data from directory : ${TEMP_DATA_DIRECTORY} to $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/"
                log_print "[INFO:] Successfully Drop older partition from $REF_DATABASE_NAME.$TABLE_NAME"
        else 
                log_print "[ERROR:] Failed to inserted data into $TEMP_DATABASE_NAME.$TABLE_NAME"
                audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
        
        
fi
log_print "[INFO:] Hive job execution block ended....."
log_print "[INFO:] ${BEELINE_COMPUTE_STATS} select count(*) cnt from $REF_DATABASE_NAME.$TABLE_NAME WHERE $PARTITION_COL='$CEDL_BATCH_DATE"
# `${BEELINE_CONNECTION_URL} -e "ANALYZE TABLE $REF_DATABASE_NAME.$TABLE_NAME PARTITION($PARTITION_COL='$CEDL_BATCH_DATE') COMPUTE STATISTICS;"`
# if [ $? -eq 0 ]
# then
        # log_print "[INFO:] Analyze table completed successfully."
# fi
HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "${BEELINE_COMPUTE_STATS} select count(*) cnt from $REF_DATABASE_NAME.$TABLE_NAME WHERE $PARTITION_COL='$CEDL_BATCH_DATE';" | grep -Po "\d+"`

if [ $? -eq 0 ]
then
        if [ "$HIVE_COUNT" != 0 ] && [ ! -z "$HIVE_COUNT" ]
        then
                    
                        log_print "[INFO:] Hive count from $REF_DATABASE_NAME.$TABLE_NAME table : $HIVE_COUNT"
                    #Getting min and Max Batch Load Number
                    BATCH_LD_NBRS=`${BEELINE_CONNECTION_URL} -e "SELECT min(batch_ld_nbr),max(batch_ld_nbr) bln from $REF_DATABASE_NAME.$TABLE_NAME WHERE batch_date='${CEDL_BATCH_DATE}' and batch_ld_nbr is NOT NULL" | grep -Po "\d+"`
                    if [ $? -eq 0 ]
                    then
                            
                            MIN_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f2`
                            MAX_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f3`
                            log_print "[INFO:] Min batch load number from $REF_DATABASE_NAME.$TABLE_NAME : $MIN_BATCH_LD_NBR and Max batch load number from $REF_DATABASE_NAME.$TABLE_NAME : $MAX_BATCH_LD_NBR"
                            if [ "$MAX_BATCH_LD_NBR" != 0 ] && [ ! -z "$MAX_BATCH_LD_NBR" ] 
                            then
                                    #Insert Min and Max Batch Load Number 
                                    `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "insert into cedl_operations.cedl_batch_mapping_t
                                    (job_nm,table_partition,min_batch_ld_nbr,max_batch_ld_nbr) 
                                    values('$REF_JOB_NAME','$CEDL_BATCH_DATE',$MIN_BATCH_LD_NBR,$MAX_BATCH_LD_NBR);"`
                                    if [ $? -eq 0 ]
                                    then
                                            log_print "[INFO:] Successfully insert records in cedl_batch_mapping_t"
                                    else
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                                            
                                    fi
                                    
                                    #Updating Max Batch Load Number into Audit Table for Landing Table
                                    `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t 
                                    set most_recent_val_txt=$MAX_BATCH_LD_NBR,cdc_tms='$LOADTIMESTAMP' where job_nm='$REF_JOB_NAME'"`
                                    if [ $? -eq 0 ]
                                    then
                                    
                                            log_print  "[INFO:] Successfully updated values in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                            audit_cleanup "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" "${HIVE_COUNT}" "${MAX_BATCH_LD_NBR}"  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                                        
                                    else
                                            
                                            log_print  "[ERROR:] Failed to update values in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                                        
                                    fi
                            else
                                    log_print "[ERROR:] Failed to get maximum batch lod number" 
                                    audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                            fi
                    else
                            
                            log_print "[ERROR:] Failed to get maximum and minimum batch lod number" 
                            audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                    fi
                
                
                
        else
                log_print "[ERROR:] Failed to get Hive table records count"
                audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi  
else
        log_print "[ERROR:] Failed to get Hive table records count"
        audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DATABASE_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi                


} >> "$CEDL_BASE_PATH"/log/"$LOG_DATE"/"${REF_JOB_NAME}_${DIR_TIME}.log" 2>&1;
