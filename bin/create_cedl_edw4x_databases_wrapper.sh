#############################################################################################################
#                               General Details                                                             #
#############################################################################################################
# Name        : Datametica                                                                                  #
# File        : create_cedl_edw4x_databases_wrapper.sh                                                      #
# Description : This script creates Databases in ADLS Location                                              #
# Inputs      : Sheduled date & Hive DB objects file name                                                   #
#                                                                                                           #
#############################################################################################################
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

# Function to print the logs with current utc time 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly two arguments"
        log_print "[INFO:] USAGE : "$0" <Sheduled date(YYYY-MM-DD)> <name of the cedl database file name>"
        exit 1
}
load_resource()
{
        log_print "[INFO:] Loading  $1 ."
        . $1
        if [ $? -eq 0 ]
        then    
            log_print "[INFO:] $1 Loaded successfully "
        else
            log_print "[ERROR:] $1 Loading  failed " 
            exit 1
        fi
}

if [ $# -ne 2 ]
then
    show_usage
fi

echo "****************Variable info*****************"
START_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=$(date +%s)
SHEDULED_DATE=$1
HIVE_DB_OBJECTS_FILE_NAME=$2
JOB_NAME=`echo ${HIVE_DB_OBJECTS_FILE_NAME} | cut -f1 -d '.'`
LOG_DATE=$(date -d "$SHEDULED_DATE" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"

# Loading environment based properties file.
load_resource $COMMON_RESOURCE_BASE_PATH/${ENVIRONMENT_FILE}

# Loading environment based properties file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties


log_print "[INFO:] JOB Name : $JOB_NAME"
#Creating directory for logging 
mkdir -p  "$CEDL_BASE_PATH"/log/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Log path directory created $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
        log_print "[INFO:] Log path directory creation $CEDL_BASE_PATH/log/$LOG_DATE/"
        exit 1
        
fi
log_print "[INFO:] Logs Directory location: ${CEDL_BASE_PATH}/log/${LOG_DATE}/${JOB_NAME}_${DIR_TIME}.log "

{
log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments <Sheduled date(YYYY-MM-DD)> <name of the cedl hive objects file name>  : $*"
log_print "[INFO:] Job started time = $START_TIME "
log_print "[INFO:] Environment source code  path = $CEDL_BASE_PATH"
log_print "[INFO:] JOB Name = $JOB_NAME"
log_print "[INFO:] HIVE_DB_OBJECTS_FILE_NAME = $HIVE_DB_OBJECTS_FILE_NAME"

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
log_print "######################################  Hive Environment Creation ############################################## "

for i in `cat $CEDL_BASE_PATH/resources/$HIVE_DB_OBJECTS_FILE_NAME | grep -vE '^#'`
do
        log_print "[INFO:] Hive Table Details : $i"
        log_print "[INFO:] Evaluating required variables"
        
        DB_NAME=`echo $i | cut -f1 -d '|'`
        DB_LOC=`echo $i | cut -f2 -d '|'`
        log_print "[INFO:] creating $DB_NAME database ...................... "
        log_print "CREATE DATABASE IF NOT EXISTS $DB_NAME location '$CEDL_ADLS_ENV_BUCKET/${DB_LOC}'"
        ${BEELINE_CONNECTION_URL} -e "CREATE DATABASE IF NOT EXISTS $DB_NAME location '$CEDL_ADLS_ENV_BUCKET/${DB_LOC}';"
             
        if [ $? -eq 0 ]
            then
                        log_print "[INFO:] $DB_NAME Database is created successfully"
            else
                        log_print "[ERROR:] $DB_NAME Database creation failed "
                        exit 1
        fi
       
        
done

END_TIME_UTC=$(date +%s)
log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
log_print "[INFO:] Job Completed successfully."
 
} >> "${CEDL_BASE_PATH}"/log/"${LOG_DATE}"/${JOB_NAME}_${DIR_TIME}.log 2>&1;
