################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : load_hive_append_wrapper_history.sh                            #
# Description : This script loads the history data month wise from hive landing#
#               to hive refined layer for fact table.As this is historical job,#
#               audit entry would not be done in case of failure.              #
################################################################################
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

# Function to print the logs with current utc time 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
        echo "INFO : invalid arguments please pass exactly 5 arguments"
        echo "Usage: "$0" <Month_Start_Date(YYYY-MM-DD)> <Scheduled date(YYYY-MM-DD)> <database-name> <table-name> <business partition column>"
        exit 1
}

load_resource()
{
        log_print "[INFO:] Loading  $1 ." 
        . $1
        if [ $? -eq 0 ]
        then    
            log_print " [INFO] : $1 Loaded successfully "
        else
            log_print " [ERROR]  : $1 Loading  failed " 
            exit 1
        fi

}


#To fetch column list of source hive table
get_column_lists() 
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[INFO:] The value of all input parameters <Database name> <Table name>: $*" 
        LND_DB_NAME=$1
        LND_TABLE_NAME=$2
        rm "$CEDL_BASE_PATH"/tmp/${LND_DB_NAME}_${LND_TABLE_NAME}.txt 
        rm "$CEDL_BASE_PATH"/tmp/final_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
        
        ${BEELINE_CONNECTION_URL} --silent=true -e "show columns in ${LND_DB_NAME}.${LND_TABLE_NAME}" > "$CEDL_BASE_PATH"/tmp/${LND_DB_NAME}_${LND_TABLE_NAME}.txt
        cat "$CEDL_BASE_PATH"/tmp/${LND_DB_NAME}_${LND_TABLE_NAME}.txt |sed 's/|$//g'|sed 's/^|//g'|sed -n '4,$p' | sed '$d'| sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g'| sed 's/,'"${PARTITION_COL}"'//g' | sed 's/,'"${BUS_PARTITION_COL}"'//g' > "$CEDL_BASE_PATH"/tmp/final_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
        
        COLUMNS_LIST=`cat "$CEDL_BASE_PATH"/tmp/final_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
}

if [ $# -ne 5 ]
then
    show_usage
fi

echo "****************Variable info*****************"
START_TIME=$(date '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=`date +%s`
DIR_TIME=$(date +'%Y-%m-%d:%H:%M:%S')
MONTH_START_DATE=$1
SRC_DB_NAME=$3
TABLE_NAME=$4
SCHEDULED_DATE=$2
BUS_PARTITION_COL=$5
CEDL_BATCH_DATE=$(date -d "$SCHEDULED_DATE " +%Y-%m-%d)
LOG_DATE=$(date -d "$SCHEDULED_DATE " +%Y%m%d)
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"


# Loading environment based properties file.
load_resource $COMMON_RESOURCE_BASE_PATH/${ENVIRONMENT_FILE} 

# Loading password file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties

#Jobname for Audit: <lnd/ref/app>_ <modulename>_<sourcedb>_<tablename>
JOB_NAME=$(echo ${REF_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME})
LND_JOB_NAME=$(echo ${LND_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME})
log_print "JOB Name : $JOB_NAME"
log_print "LND JOB Name : $LND_JOB_NAME"

#Creating directory for logging 
log_print "Creating directory for logging......."
mkdir -p  "$CEDL_BASE_PATH"/log/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Success : Log path directory created $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
        log_print "[ERROR:] Failed  : Log path directory creation $CEDL_BASE_PATH/log/$LOG_DATE/ "
        exit 1
        
fi
log_print "[INFO:] Logs Directory location: $CEDL_BASE_PATH/log/${LOG_DATE}/${JOB_NAME}_${DIR_TIME}.log"
{

log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments <Month_start_date(YYYY-MM-DD)> <Scheduled date(YYYY-MM-DD)> <database-name> <table-name> <business partition column> : $*"

#Fetching connection URL
#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

BEELINE_PRINT_HEADER="set hive.cli.print.header=FALSE;"
BEELINE_DYNAMIC_PARTITION="set hive.exec.dynamic.partition=true;"
BEELINE_DYNAMIC_PARTITION_NONSTRICT="set hive.exec.dynamic.partition.mode=nonstrict;"

log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH/"

log_print "*************Hive load JOB UTILITY*******************"

#Calculating required variables.

LOADTIMESTAMP=$(date -u  '+%Y-%m-%d %H:%M:%S')
LND_DB_NAME=$(echo ${LND_DB_PREFIX}_${SRC_DB_NAME})
REF_DB_NAME=$(echo ${REF_DB_PREFIX}_${SRC_DB_NAME})
LND_TABLE_NAME=$(echo ${TABLE_NAME}_history)
REF_TABLE_NAME=$(echo ${TABLE_NAME})
LZO_INDEX_PATH=$CEDL_ADLS_ENV_BUCKET/$LND_ADLS_BASEPATH/"$SRC_DB_NAME.db"/$LND_TABLE_NAME/$PARTITION_COL=$MONTH_START_DATE
 

#Indexing lzo file
log_print "INFO : Check if file $LZO_INDEX_PATH  exist or not"

hdfs dfs -test -d $LZO_INDEX_PATH
if [ $? -eq 0 ]
    then
        log_print "[INFO:] Folder $LZO_INDEX_PATH exists "
    else
        log_print "[ERROR:] Folder $LZO_INDEX_PATH does not exists"
        exit 1
fi


echo "Indexing lzo file : $LZO_INDEX_PATH"
hadoop jar ${LZO_INDEX_JAR_PATH} com.hadoop.compression.lzo.LzoIndexer ${LZO_INDEX_PATH}
if [ $? -eq 0 ]
    then
        log_print "[INFO:] Successfully Indexed"
    else
        log_print "[ERROR:] Indexing Failed"
        exit 1
fi

#updating metastore
log_print "INFO :Executing Partitioning"
${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME ADD IF NOT EXISTS PARTITION ($PARTITION_COL= '${MONTH_START_DATE}')";
if [ $? -eq 0 ]
    then
        log_print "[INFO:] Successfully added partition"
    else
        log_print "[ERROR:] Adding partition Failed"
        exit 1
fi


#loading data from  landing to refined
log_print "[INFO:] Running hive job to load data from temp to landing"

#Fetching columns list from lnd layer table
get_column_lists $LND_DB_NAME $LND_TABLE_NAME

log_print "[INFO:] $LND_DB_NAME.$LND_TABLE_NAME COLUMNS_LIST: $COLUMNS_LIST"

log_print "${BEELINE_DYNAMIC_PARTITION} ${BEELINE_DYNAMIC_PARTITION_NONSTRICT}
insert overwrite table $REF_DB_NAME.$REF_TABLE_NAME partition($PARTITION_COL= '$MONTH_START_DATE',${BUS_PARTITION_COL}) 
select 
$COLUMNS_LIST,
from_unixtime(${START_TIME_UTC},'y-MM-d HH:mm:s') as loadtimestamp,
${BUS_PARTITION_COL}
from $LND_DB_NAME.$LND_TABLE_NAME
where $PARTITION_COL= '${MONTH_START_DATE}'"

${BEELINE_CONNECTION_URL} -e "${BEELINE_DYNAMIC_PARTITION} ${BEELINE_DYNAMIC_PARTITION_NONSTRICT}
insert overwrite table $REF_DB_NAME.$REF_TABLE_NAME partition($PARTITION_COL= '${MONTH_START_DATE}',${BUS_PARTITION_COL})
select 
$COLUMNS_LIST,
from_unixtime(${START_TIME_UTC},'y-MM-d HH:mm:s') as loadtimestamp,
${BUS_PARTITION_COL}
from $LND_DB_NAME.$LND_TABLE_NAME
where $PARTITION_COL= '${MONTH_START_DATE}';"


if [ $? -eq 0 ]
then
    log_print "[INFO:] Successfully Ingested the data."
    log_print "[INFO:] Getting hive max batch_ld_nbr and target count....."
    
    #Getting hive Target count
    REF_HIVE_COUNT=`${BEELINE_CONNECTION_URL} --silent=true -e "ANALYZE TABLE $REF_DB_NAME.$REF_TABLE_NAME COMPUTE STATISTICS FOR COLUMNS; select count(*) cnt from $REF_DB_NAME.$REF_TABLE_NAME WHERE $PARTITION_COL='$MONTH_START_DATE'" | grep -Po "\d+"`
    if [ $? -eq 0 ]
    then
        log_print "[INFO:] Fetching Target hive table record count successful"
     
    else
        log_print "[ERROR:] Failed to get incoming table records count"
    fi
        #Getting min and Max Batch Load Number for landing layer
            BATCH_LD_NBRS=`${BEELINE_CONNECTION_URL} --silent=true -e "SELECT min($ICR_LOAD_COL_NM),max($ICR_LOAD_COL_NM) bln from $LND_DB_NAME.$LND_TABLE_NAME WHERE $PARTITION_COL='${MONTH_START_DATE}'" | grep -Po "\d+"`
            if [ $? -eq 0 ]
            then
                if [[ -n "${BATCH_LD_NBRS}" ]]
                then
                    JOB_STAT_NM="success"
                    MIN_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f2`
                    MAX_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f3`
                    INSERT_HIVE_COUNT=$REF_HIVE_COUNT
                    
                    #Insert Min and Max Batch Load Number 
                    mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "insert into cedl_operations.cedl_batch_mapping_t(job_nm,table_partition,min_batch_ld_nbr,max_batch_ld_nbr) values('$LND_JOB_NAME','$MONTH_START_DATE',$MIN_BATCH_LD_NBR,$MAX_BATCH_LD_NBR);"
                    if [ $? -eq 0 ]
                    then
                            log_print "INFO : Successfully insert records in cedl_batch_mapping_t"
                    else
                            log_print "ERROR: Failed to insert records in cedl_batch_mapping_t"
                            exit 1
                    fi
                else
                    log_print "[ERROR:] BATCH_LD_NBRS is null"
                    JOB_STAT_NM="failure"
                    INSERT_HIVE_COUNT=NULL
                    HIVE_MAX_BATCH_LD_NBR=NULL
                fi
            fi
            #Getting min and Max Batch Load Number for refined layer
            BATCH_LD_NBRS=`${BEELINE_CONNECTION_URL} --silent=true -e "SELECT min($ICR_LOAD_COL_NM),max($ICR_LOAD_COL_NM) bln from $REF_DB_NAME.$REF_TABLE_NAME WHERE $PARTITION_COL='${MONTH_START_DATE}'" | grep -Po "\d+"`
            if [ $? -eq 0 ]
            then
                if [[ -n "${BATCH_LD_NBRS}" ]]
                then
                    JOB_STAT_NM="success"
                    MIN_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f2`
                    MAX_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d" " -f3`
                    INSERT_HIVE_COUNT=$REF_HIVE_COUNT
                    HIVE_MAX_BATCH_LD_NBR=$MAX_BATCH_LD_NBR
                    #Insert Min and Max Batch Load Number 
                    mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "insert into cedl_operations.cedl_batch_mapping_t(job_nm,table_partition,min_batch_ld_nbr,max_batch_ld_nbr) values('$JOB_NAME','$MONTH_START_DATE',$MIN_BATCH_LD_NBR,$MAX_BATCH_LD_NBR);"
                    if [ $? -eq 0 ]
                    then
                            log_print "INFO : Successfully insert records in cedl_batch_mapping_t"
                    else
                            log_print "ERROR: Failed to insert records in cedl_batch_mapping_t"
                            exit 1
                    fi
                else
                    log_print "[ERROR:] BATCH_LD_NBRS is null"
                    JOB_STAT_NM="failure"
                    INSERT_HIVE_COUNT=NULL
                    HIVE_MAX_BATCH_LD_NBR=NULL
                fi
            fi  
                                
else
    log_print "[ERROR:] Failed to ingest data into target table" 
    JOB_STAT_NM="failure"
    INSERT_HIVE_COUNT=NULL
    HIVE_MAX_BATCH_LD_NBR=NULL  
fi

log_print "**************************Audit entry***********************************" 

#Fetching job key from cedl_operations.cedl_job_t without any extra log info
log_print "[INFO:] Fetching job key from cedl_operations.cedl_job_t......... "
log_print "[INFO:] JOB_KEY_SQL_QUERY : select job_key from cedl_operations.cedl_job_t where job_nm='$JOB_NAME'"
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm='$JOB_NAME'"` 
if [ $? -eq 0 ]
    then
        log_print "[INFO:] JOB_KEY: $JOB_KEY"
    else
        log_print "[ERROR:] Failed to fetch Job Key"
        exit 1
fi

#Inserting record in audit table
UPD_ROWS_CNT=NULL
DEL_ROWS_CNT=NULL

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Inserting record in audit table cedl_operations.cedl_audit_t...."
log_print "[INFO:] Audit Query : insert into cedl_operations.cedl_audit_t
                                 (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                 src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                 insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                 begin_tms,end_tms) 
                                 values
                                 ($JOB_KEY,'$START_TIME','$JOB_NAME','$JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                 '$SRC_SYSTEM_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$SRC_TABLE_NAME','$REF_DB_NAME','$TABLE_NAME',
                                 $INSERT_HIVE_COUNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,$HIVE_MAX_BATCH_LD_NBR,'STRING',
                                 '$START_TIME','$END_TIME')"

mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "insert into cedl_operations.cedl_audit_t
                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                begin_tms,end_tms) 
                                values
                                ($JOB_KEY,'$START_TIME','$JOB_NAME','$JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                '$SRC_SYSTEM_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$SRC_TABLE_NAME','$REF_DB_NAME','$TABLE_NAME',
                                $INSERT_HIVE_COUNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,$HIVE_MAX_BATCH_LD_NBR,'STRING',
                                '$START_TIME','$END_TIME')"


if [ $? -eq 0 ]
then
    log_print "[INFO:] success: inserted in audit table..."
    if [ "$JOB_STAT_NM"=="success" ]
    then
    #Updating maximum batch load number
        log_print "[INFO:] Updating maximum batch load number...."   
        log_print "[INFO:] Query to update audit table : update cedl_operations.cedl_change_data_capture_t set most_recent_val_txt=$HIVE_MAX_BATCH_LD_NBR,cdc_tms='$LOADTIMESTAMP' where job_nm='$JOB_NAME'"       
        mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "update cedl_operations.cedl_change_data_capture_t set most_recent_val_txt=$HIVE_MAX_BATCH_LD_NBR,cdc_tms='$LOADTIMESTAMP' where job_nm='$JOB_NAME'"
        if [ $? -eq 0 ]
        then
            log_print  "[INFO:] success: update in audit table..."
        else
            log_print  "[ERROR:] fail: update in audit table..."
            exit 1
        fi
    fi
else
    log_print "[ERROR:] fail: inserting in audit table..."
    exit 1
fi


END_TIME_UTC=$(date +%s)
log_print "Started at : `date -d @$START_TIME_UTC`"
log_print "Ended at : `date -d @$END_TIME_UTC`"
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf 'Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))

echo "Job Completed successfully."


} >> $"$CEDL_BASE_PATH"/log/"${LOG_DATE}"/${JOB_NAME}_${DIR_TIME}.log 2>&1;
echo "Job Completed successfully."
