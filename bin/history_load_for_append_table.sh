#################################################################################
#                               General Details                                 #
#################################################################################
# Name        : Datametica                                                    	#
# File        : ingest_sqoop_append_wrapper.sh                                  #
# Description : This script load full data(history load) of fact table from 	#
#source to landing layer														#
#################################################################################

set -x
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
	log_print "[INFO:] Function Name : ${FUNCNAME}()"
	log_print "[INFO:] The value of all command-line arguments : $*"
	log_print "[ERROR:] Invalid arguments please pass atleast three arguments"
	log_print "[INFO:] Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename>"
	exit 1
}

load_resource()
{
	log_print "[INFO:] Function Name : ${FUNCNAME}()"
	log_print "[INFO:] The value of all command-line arguments : $*"
	. $1
	if [ $? -eq 0 ]
	then    
		log_print "[INFO:] $1 Loaded successfully "
	else
		log_print "[ERROR:] $1 Loading  failed " 
		exit 1
	fi
}

#Insert in audit table
audit_cleanup(){

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
LND_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
LND_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
LND_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
EXTRACTION_MODE="${16}"
ICR_LOAD_COL_NM="${17}"

log_print "[INFO:] Function Name : ${FUNCNAME}()"
log_print "[INFO:] The value of all command-line arguments : $*"

log_print "[INFO:] Executing MySQL Query: insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,
src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,
val_datatype_nm,begin_tms,end_tms) 
values 
($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE',
'$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}');"

`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,
src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,
val_datatype_nm,begin_tms,end_tms) 
values 
($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE',
'$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}');"`
if [ $? -eq 0 ]
then
	log_print  "[INFO:] Successfully Job record inserted into audit table"
fi
if [ "${JOB_STAT_NM}" == "success" ] 
then          
	if [ "${EXTRACTION_MODE}" == "normal" ]
	then 
		log_print "[INFO:] Executing MySQL Query: update cedl_operations.cedl_change_data_capture_t set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
		src_column_nm='${ICR_LOAD_COL_NM}',most_recent_val_txt=${MAX_BATCH_LD_NBR},val_datatype_nm='INT',
		cdc_tms='$START_TIME' where job_nm='${LND_JOB_NAME}'"
		`mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
		src_column_nm='${ICR_LOAD_COL_NM}',most_recent_val_txt=${MAX_BATCH_LD_NBR},val_datatype_nm='INT',
		cdc_tms='$START_TIME' where job_nm='${LND_JOB_NAME}'"`
	fi
	END_TIME_UTC=`date +%s`
	log_print "[INFO:] Started at : `date -d @$START_TIME_UTC`"
	log_print "[INFO:] Ended at : `date -d @$END_TIME_UTC`"
	deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
	printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
	log_print "[INFO:] Job Completed Successfully."
else
	if [ "${EXTRACTION_MODE}" == "normal" ]
	then
		#deleting data from cedl_batch_mapping_t if it exists
		log_print "Executing MySQL Query: DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
		mysql $AUDIT_MYSQL_CONNECTION_URL -e "DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Success : Deleted mapping for ${CEDL_BATCH_DATE} in cedl_operations.cedl_batch_mapping_t  "
		else
			log_print "Failed to delete Mapping for ${CEDL_BATCH_DATE} in cedl_operations.cedl_batch_mapping_t" 
		fi
	fi
	if [ "${LND_DATA_DIRECTORY}" != "NULL" ]
	then 
		#Clean up activity
		log_print "[INFO:] Starting clean up activity..."
		log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $LND_DATA_DIRECTORY"
		hdfs dfs -rm -r  ${LND_DATA_DIRECTORY}
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Files deleted successfully in $LND_DATA_DIRECTORY"
		else
			log_print "[ERROR:] ${LND_DATA_DIRECTORY} doesn't exists"
		fi
		#Removing Partition For Respective Date 
		`${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DATABASE_NAME.$TABLE_NAME DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
		if [ $? -eq 0 ]
		then
					log_print "[INFO:] Successfully dropped partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME" 
		else	
					log_print "[ERROR:] failed to dropped partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
		fi
	fi
		log_print "[INFO:] Job Failed.........Please check"
		END_TIME_UTC=`date +%s`
		log_print "[INFO:] Started at : `date -d @$START_TIME_UTC`"
		log_print "[INFO:] Ended at : `date -d @$END_TIME_UTC`"
		deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
		printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))

		exit 1
fi	
}  

if [ $# -le 2 ]
then
    show_usage
fi

log_print "****************Variable info*****************"
START_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=`date +%s`
SCHEDULED_DATE=$1
SRC_DB_NAME=$2
LND_TABLE_NAME=$3
LND_TABLE_DISTRIBUTED_KEY=$4
START_BATCH_LD_NBR=$4
END_BATCH_LD_NBR=$5
CEDL_BATCH_DATE=$(date -d "$SCHEDULED_DATE - 1 days" +%Y-%m-%d)
LOG_DATE=$(date -d "$SCHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"
FAILURE=failure
INSERT_ROWS_CNT=NULL
LOADTIMESTAMP=$(date -u  '+%Y-%m-%d %H:%M:%S')


# Loading environment based properties file.
load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties

#Jobname for Audit: <lnd/ref/app>_ <modulename>_<sourcedb>_<tablename>
JOB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}_${LND_TABLE_NAME}"

LND_DB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}"
LND_JOB_NAME="${LND_DB_NAME}_${LND_TABLE_NAME}"

#function to get columns list for landing layer table that is to be processed
get_column_lists() {
log_print "[INFO:] Function Name : ${FUNCNAME}()"
log_print "[INFO:] The value of all command-line arguments : $*"
LND_DB_NAME=$1
LND_TABLE_NAME=$2
rm $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
rm $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

${BEELINE_CONNECTION_URL} --silent=true -e "show columns in ${LND_DB_NAME}.${LND_TABLE_NAME}" > $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
cat $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |sed 's/|$//g'|sed 's/^|//g'|sed -n '4,$p' | sed '$d'| sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g'| sed 's/,'"${PARTITION_COL}"'//g'  > $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

COLUMNS_LIST=`cat $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
log_print "COLUMNS_LIST:$COLUMNS_LIST"
}

log_print "[INFO:] Job Name : $LND_JOB_NAME"
#Creating directory for logging 
mkdir -p  $CEDL_BASE_PATH/log/"$LOG_DATE"/
if [ $? -eq 0 ]
then
        log_print "[INFO:] Success : Log path directory created $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
        log_print "[ERROR:] Failed  : Log path directory creation $CEDL_BASE_PATH/log/$LOG_DATE/"
        exit 1
fi
log_print "[INFO:] Logs Directory location:  - ${CEDL_BASE_PATH}/log/${LOG_DATE}/${LND_JOB_NAME}_${DIR_TIME}.log "
{

log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments : $*"


#Extracting source credentials
EDW4X_DB_HOST_IP_PORT=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f1`
EDW4X_DB_HOST_USER=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f2`
EDW4X_DB_HOST_PASSWORD=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f3-`
SQOOP_CONNNECTION_URL="jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} --username ${EDW4X_DB_HOST_USER}  --password ${EDW4X_DB_HOST_PASSWORD}"

#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_COMPUTE_STATS="set hive.compute.query.using.stats=false;"

#Populating job key value  
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '${LND_JOB_NAME}' "` 

LND_DB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}"
TEMP_DB_NAME="${TEMP_DB_PREFIX}_${SRC_DB_NAME}"
log_print "[INFO:] TEMP_DB_NAME:$TEMP_DB_NAME"
log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

log_print "[INFO:] *************SQOOP IMPORT JOB STARTS*******************"

if [ $# -eq 4 ]
then
	EXTRACTION_MODE=normal
	log_print "[INFO:] EXTRACTION_MODE: $EXTRACTION_MODE"
	#retrieving data from cedl_batch_mapping_t table  if data exists for $CEDL_BATCH_DATE date
	RUN_PARTITION_EXIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "Select 1 from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition='${CEDL_BATCH_DATE}'"`
	
	if [ $? -eq 0 ] 
	then
		if [ "${RUN_PARTITION_EXIST}" == "1" ]
		then 
		log_print "[ERROR:] batch_ld_nbr not provided for rerun for ${CEDL_BATCH_DATE}"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
	else
		log_print "[ERROR:] Failed to deleting data from cedl_batch_mapping_t table  if data exists for $CEDL_BATCH_DATE date"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	#retrieving max processed batch_ld_nbr from landing job 
	log_print "[INFO:] executing MySQL Query: select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${LND_JOB_NAME}';"
	LAST_PRC_BATCH_LD_NBR=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${LND_JOB_NAME}';"`
	
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully retrieved max processed batch_ld_nbr"
	else
		log_print "[ERROR:] Failed to retrieve max processed batch_ld_nbr"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	
	# retrieving distinct batch_ld_nbr from netezza to be processed
	log_print "[INFO:] Executing Sqoop Query: select distinct batch_ld_nbr from $LND_TABLE_NAME where
	${ICR_LOAD_COL_NM} > ${LAST_PRC_BATCH_LD_NBR}"
	sqoop eval --connect  $SQOOP_CONNNECTION_URL --query "select distinct batch_ld_nbr from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM} > ${LAST_PRC_BATCH_LD_NBR}" > $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully fetched distinct batch_ld_nbr from netezza to be processed"
	else
		log_print "[ERROR:] Failed to fetch distinct batch_ld_nbr from netezza to be processed"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	
	#cleansing the batch_ld_nbr for use 
	cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]\{10\}]*' | sed 's/^|//g' | sed 's/|.*$//g' > $CEDL_BASE_PATH/tmp/final_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	
	#comma separated list of batch_ld_nbr
	cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]\{10\}]*' | sed 's/^|//g' | sed 's/|.*$//g' | sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g' > $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	BATCH_LD_NBR_LIST=`cat $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
	if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ] 
	then
		log_print "[INFO:] Successfully fetched batch_ld_nbr list from Netezza to be process"
	else
		log_print "[INFO:] New data is not available in source after last successfully processed batch_ld_nbr: ${LAST_PRC_BATCH_LD_NBR}.  Please check."
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
	fi
	MIN_BATCH_LD_NBR=`cat $CEDL_BASE_PATH/tmp/final_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |  sort  |head -1`
	MAX_BATCH_LD_NBR=`cat $CEDL_BASE_PATH/tmp/final_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |  sort -r |head -1`
	log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
	log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
	log_print "[INFO:] BATCH_LD_NBR_LIST:$BATCH_LD_NBR_LIST"
elif [ $# -eq 5 ]
then
	EXTRACTION_MODE=single
	log_print "[INFO:] EXTRACTION_MODE: $EXTRACTION_MODE"

	#Executing query to get table_partition,min_batch_ld_nbr and max_batch_ld_nbr
	log_print  "[INFO:] Executing query to get table_partition,min_batch_ld_nbr and max_batch_ld_nbr: select min(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and max_batch_ld_nbr >= ${START_BATCH_LD_NBR} and min_batch_ld_nbr <= ${START_BATCH_LD_NBR};"
	TABLE_PARTITION=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select min(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and max_batch_ld_nbr >= ${START_BATCH_LD_NBR} and min_batch_ld_nbr <= ${START_BATCH_LD_NBR};"`
	
	BATCH_LD_NBRS_MAPPING=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select CONCAT_WS(',',min_batch_ld_nbr,max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and max_batch_ld_nbr >= ${START_BATCH_LD_NBR} and min_batch_ld_nbr <= ${START_BATCH_LD_NBR} and table_partition='${TABLE_PARTITION}';" `
	
	MIN_BATCH_LD_NBR=`echo ${BATCH_LD_NBRS_MAPPING} | cut -d ',' -f1`
	MAX_BATCH_LD_NBR=`echo ${BATCH_LD_NBRS_MAPPING} | cut -d ',' -f2`
	
	# retrieving distinct batch_ld_nbr from netezza to be processed
	log_print  "[INFO:] Executing Sqoop Query: select distinct batch_ld_nbr from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM}>= ${MIN_BATCH_LD_NBR} and ${ICR_LOAD_COL_NM}<=${MAX_BATCH_LD_NBR}"
	sqoop eval  --connect $SQOOP_CONNNECTION_URL  --query "select distinct batch_ld_nbr from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM} >= ${MIN_BATCH_LD_NBR} and ${ICR_LOAD_COL_NM} <= ${MAX_BATCH_LD_NBR}" > $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	
	#cleansing the batch_ld_nbr for use 
	cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]\{10\}]*' | sed 's/^|//g' | sed 's/|.*$//g' | sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g' > $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	BATCH_LD_NBR_LIST=`cat $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
	if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ] 
	then
		log_print "[INFO:] Successfully fetched batch_ld_nbr list from Netezza to be process"
	else
		log_print "[INFO:] Failed to fetch batch_ld_nbr list from Netezza to be process"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
	fi
	CEDL_BATCH_DATE=${TABLE_PARTITION}
	log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
	log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
	log_print "[INFO:] BATCH_LD_NBR_LIST:$BATCH_LD_NBR_LIST"
	log_print "[INFO:] TABLE_PARTITION:$TABLE_PARTITION"
	log_print "[INFO:] BATCH_LD_NBRS_MAPPING:${BATCH_LD_NBRS_MAPPING}"
else
	EXTRACTION_MODE=range
	log_print "[INFO:] EXTRACTION_MODE: $EXTRACTION_MODE"
	#min table partition and min batch_ld_nbr
	log_print "[INFO:] Executing MySQL Query: select CONCAT_WS(',',min_batch_ld_nbr,min(table_partition)) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr<=${START_BATCH_LD_NBR} and max_batch_ld_nbr>=${START_BATCH_LD_NBR}; group by min_batch_ld_nbr;"
	TABLE_PARTITION_MAPPING_MIN=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select CONCAT_WS(',',min_batch_ld_nbr,min(table_partition)) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr<=${START_BATCH_LD_NBR} and max_batch_ld_nbr>=${START_BATCH_LD_NBR} group by min_batch_ld_nbr;"`

	MIN_TABLE_PARTITION=`echo ${TABLE_PARTITION_MAPPING_MIN} | cut -d ',' -f2`
	MIN_BATCH_LD_NBR=`echo ${TABLE_PARTITION_MAPPING_MIN} | cut -d ',' -f1`
	
	#max table partition and max batch_ld_nbr
	TABLE_PARTITION_MAPPING_MAX=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select CONCAT_WS(',',table_partition,max(max_batch_ld_nbr)) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr <=${END_BATCH_LD_NBR} and max_batch_ld_nbr >=${END_BATCH_LD_NBR} group by table_partition;"`
	
	MAX_BATCH_LD_NBR=`echo ${TABLE_PARTITION_MAPPING_MAX} | cut -d ',' -f2`
	MAX_TABLE_PARTITION=`echo ${TABLE_PARTITION_MAPPING_MAX} | cut -d ',' -f1`
	
	# list of all table_partitions which lies between MIN_TABLE_PARTITION and MAX_TABLE_PARTITION
	log_print "[INFO:] Executing MySQL Query: select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition >='${MIN_TABLE_PARTITION}' and table_partition <= '${MAX_TABLE_PARTITION}';"
	TABLE_PARTITION_LIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition >='${MIN_TABLE_PARTITION}' and table_partition <= '${MAX_TABLE_PARTITION}';"`
	log_print "[INFO:] TABLE_PARTITION_LIST:$TABLE_PARTITION_LIST"
	
	# retrieving distinct batch_ld_nbr from netezza to be processed
	log_print  "[INFO:] Executing Sqoop Query: select distinct batch_ld_nbr from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM}>= ${MIN_BATCH_LD_NBR} and ${ICR_LOAD_COL_NM}<=${MAX_BATCH_LD_NBR}"
	sqoop eval  --connect $SQOOP_CONNNECTION_URL  --query "select distinct batch_ld_nbr from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM} >= ${MIN_BATCH_LD_NBR} and ${ICR_LOAD_COL_NM} <= ${MAX_BATCH_LD_NBR}" > $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]\{10\}]*' | sed 's/^|//g' | sed 's/|.*$//g' | sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g' > $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
	BATCH_LD_NBR_LIST=`cat $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
	if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ] 
	then
		log_print "[INFO:] Successfully fetched batch_ld_nbr list from Netezza to be process"
	else
		log_print "[INFO:] Failed to fetch batch_ld_nbr list from Netezza to be process"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
	fi
	CEDL_BATCH_DATE=${MAX_TABLE_PARTITION}
	# Printing necessary variables
	log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
	log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
	log_print "[INFO:] BATCH_LD_NBR_LIST:$BATCH_LD_NBR_LIST"
	log_print "[INFO:] TABLE_PARTITION_MAPPING_MIN:$TABLE_PARTITION_MAPPING_MIN"
	log_print "[INFO:] TABLE_PARTITION_MAPPING_MAX:$TABLE_PARTITION_MAPPING_MAX"
fi
if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ] 
then
	log_print "[INFO:] Successfully fetched batch_ld_nbr from Netezza to be process"
else
	log_print "[INFO:] New data is not available in source after last successfully processed batch_ld_nbr: ${LAST_PRC_BATCH_LD_NBR}.  Please check."
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
fi
log_print "[INFO:] CEDL_BATCH_DATE:$CEDL_BATCH_DATE"
DATA_DIRECTORY=${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${CEDL_BATCH_DATE}
TEMP_DATA_DIRECTORY=${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${CEDL_BATCH_DATE}
MOST_RECENT_VAL_TXT=$MAX_BATCH_LD_NBR
#deleting temp table 
`${BEELINE_CONNECTION_URL} -e "DROP TABLE IF EXISTS ${TEMP_DB_NAME}.${LND_TABLE_NAME}"`
if [ $? -eq 0 ] 
then
	log_print "[INFO:] Successfully dropped Temp table "
else
	log_print "[ERROR:] Failed to drop Temp table"
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
fi	
#calling function to get all columns list of landing layer table that is to be processed
get_column_lists $LND_DB_NAME $LND_TABLE_NAME
log_print "COLUMNS_LIST:$COLUMNS_LIST"
${BEELINE_CONNECTION_URL} -e "CREATE TABLE ${TEMP_DB_NAME}.${LND_TABLE_NAME} stored as ORC
location '${TEMP_DATA_DIRECTORY}'
tblproperties ('orc.compress'='ZLIB') as SELECT ${COLUMNS_LIST} from ${LND_DB_NAME}.${LND_TABLE_NAME} where 1=0;"
if [ $? -eq 0 ]
then
	log_print "[INFO:] Successfully created temp table with  ${TEMP_DATA_DIRECTORY} location"
else
	log_print "[ERROR:] Failed to create temp table with  ${TEMP_DATA_DIRECTORY} location"
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
fi
log_print "[INFO:] TEMP_DATA_DIRECTORY: $TEMP_DATA_DIRECTORY"
#Extraction logic

	# Running Sqooop Query to get data for all batch_ld_nbr in  Netezza based on batch_ld_nbr for history load
	log_print "[INFO:] Executing Sqoop Query: sqoop import --connect 'jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME}' \
	--table $LND_TABLE_NAME \
	--direct \
	--where 'batch_ld_nbr >= ${MIN_BATCH_LD_NBR} and batch_ld_nbr <= ${MAX_BATCH_LD_NBR} ' \
	--hcatalog-database $TEMP_DB_NAME \
	--hcatalog-table $LND_TABLE_NAME   \
	--hcatalog-storage-stanza 'stored as orc tblproperties ('orc.compress'='ZLIB')' \
	--escaped-by '\' \
	-m ${DEF_SQOOP_APPEND_MAPPER_CNT}"
	sqoop import --connect ${SQOOP_CONNNECTION_URL} \
	--table $LND_TABLE_NAME \
	--split-by $LND_TABLE_DISTRIBUTED_KEY \
	--where "batch_ld_nbr >= ${MIN_BATCH_LD_NBR} and batch_ld_nbr <= ${MAX_BATCH_LD_NBR} " \
	--hcatalog-database $TEMP_DB_NAME \
	--hcatalog-table $LND_TABLE_NAME   \
	--hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
	--escaped-by '\' \
	-m ${DEF_SQOOP_APPEND_MAPPER_CNT}
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully imported data to ${CEDL_BATCH_DATE} in $TEMP_DB_NAME.$LND_TABLE_NAME"
	else
		log_print "[ERROR:] Failed to import data to ${CEDL_BATCH_DATE} in $TEMP_DB_NAME.$LND_TABLE_NAME"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL  "${TEMP_DATA_DIRECTORY}" "${PARTITION_COL}"
	fi
	log_print "BATCH_LD_NBR in for loop of sqoop:${BATCH_LD_NBR}"


if [ "$EXTRACTION_MODE" == "normal" ]
then
	#DROP IF EXISTS PARTITION in metastore
	`${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME DROP IF EXISTS PARTITION ($PARTITION_COL='${CEDL_BATCH_DATE}')"`
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully dropped partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
	else
		log_print "[ERROR:] Failed to drop partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	hdfs dfs -test -d ${DATA_DIRECTORY}
	if [ $? -eq 0 ] 
	then 
		
		hdfs dfs -rm -r ${DATA_DIRECTORY}
		if [ $? -eq 0 ]
		then
			
			log_print "[INFO:] Successfully deleted old data for ${DATA_DIRECTORY}"
			hdfs dfs -mkdir ${DATA_DIRECTORY}
		else
			log_print "[ERROR:] Failed to delete old data for ${DATA_DIRECTORY}"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
		
	else
		hdfs dfs -mkdir ${DATA_DIRECTORY}
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Successfully created directory for $LND_DB_NAME.$LND_TABLE_NAME"
		else
			log_print "[ERROR:] Failed to create directory for $LND_DB_NAME.$LND_TABLE_NAME"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
	fi
elif [ "$EXTRACTION_MODE" == "single" ]
then
	#DROP IF EXISTS PARTITION in metastore
	`${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME DROP IF EXISTS PARTITION ($PARTITION_COL='${CEDL_BATCH_DATE}')"`
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully dropped partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
	else
		log_print "[ERROR:] Failed to drop partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	hdfs dfs -test -d ${DATA_DIRECTORY}
	if [ $? -eq 0 ] 
	then 
		hdfs dfs -rm -r ${DATA_DIRECTORY}
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Successfully deleted old data for ${DATA_DIRECTORY}"
		else
			log_print "[ERROR:] Failed to delete old data for ${DATA_DIRECTORY}"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
	fi
	hdfs dfs -mkdir ${DATA_DIRECTORY}
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully created directory for $LND_DB_NAME.$LND_TABLE_NAME"
	else
		log_print "[ERROR:] Failed to create directory for $LND_DB_NAME.$LND_TABLE_NAME"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi	
else 
	# removing data from all partitions 
	for TABLE_PARTITION in $(echo ${TABLE_PARTITION_LIST} | sed "s/,/ /g")
	do
		#Dropping Partition For Respective Date 
		`${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${LND_DB_NAME}.${LND_TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='${TABLE_PARTITION}')"`
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Successfully dropped partition ${TABLE_PARTITION} from ${LND_DB_NAME}.${LND_TABLE_NAME}" 
		else	
			log_print "[ERROR:] failed to dropped partition ${TABLE_PARTITION} from ${LND_DB_NAME}.${LND_TABLE_NAME}"
		fi
		hdfs dfs -test -d ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}
		if [ $? -eq 0 ] 
		then 
			hdfs dfs -rm -r ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}
			if [ $? -eq 0 ]
			then
				log_print "[INFO:] Successfully deleted old data for ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}/"
			else
				log_print "[ERROR:] Failed to delete old data for ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}/"
				audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}"
			fi
				
		fi
	done
	hdfs dfs -mkdir ${DATA_DIRECTORY}
	if [ $? -eq 0 ]
		then
			log_print "[INFO:] Successfully created directory for $LND_DB_NAME.$LND_TABLE_NAME"
		else
			log_print "[ERROR:] Failed to create directory for $LND_DB_NAME.$LND_TABLE_NAME"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi		
fi

#moving temp data to ${CEDL_BATCH_DATE} partition
	hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* ${DATA_DIRECTORY}
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Success : Data Ingested into  directory $DATA_DIRECTORY"
	else
		log_print "[ERROR:] Failed  : Data Sqoop Ingestion Job is Failed" 
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
	fi
	
#Adding partition in metastore
`${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME ADD IF NOT EXISTS PARTITION ($PARTITION_COL='${CEDL_BATCH_DATE}')"`
if [ $? -eq 0 ] 
then
	log_print "[INFO:] Successfully added partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
else
	log_print "[ERROR:] Failed to added partition $PARTITION_COL='${CEDL_BATCH_DATE}' for  $LND_DB_NAME.$LND_TABLE_NAME"
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
fi
	
if [ "${EXTRACTION_MODE}" != "single" ]
then
	if [ "${EXTRACTION_MODE}" == "range" ]
	then
		#updating data of cedl_batch_mapping_t
		log_print "Executing MySQL Query: UPDATE cedl_operations.cedl_batch_mapping_t set min_batch_ld_nbr=${MIN_BATCH_LD_NBR} where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
		mysql $AUDIT_MYSQL_CONNECTION_URL -e "UPDATE cedl_operations.cedl_batch_mapping_t set min_batch_ld_nbr=${MIN_BATCH_LD_NBR} where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
		log_print "TABLE_PARTITION_LIST:$TABLE_PARTITION_LIST"
		for TABLE_PARTITION in $(echo ${TABLE_PARTITION_LIST} | sed "s/,/ /g")
		do
			if [ "${TABLE_PARTITION}" != "${CEDL_BATCH_DATE}" ]
			then
				#deleting data from cedl_batch_mapping_t if it exists
				log_print "[INFO:] Executing MySQL Query: DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${TABLE_PARTITION}';"
				mysql $AUDIT_MYSQL_CONNECTION_URL -e "DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${TABLE_PARTITION}';"
				if [ $? -eq 0 ]
				then
					log_print "[INFO:] Success : Deleted mapping for ${TABLE_PARTITION} in cedl_operations.cedl_batch_mapping_t  "
				else
					log_print "[ERROR:] Failed  : Failed to delete mapping for ${TABLE_PARTITION} in cedl_operations.cedl_batch_mapping_t" 
					audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
				fi
			fi
		done
	else
		#executing mysql query to insert data in cedl_batch_mapping_t table 
		log_print "Executing MySQL Query: INSERT INTO cedl_operations.cedl_batch_mapping_t values ('${LND_JOB_NAME}','${CEDL_BATCH_DATE}',${MIN_BATCH_LD_NBR},${MAX_BATCH_LD_NBR});"
		`mysql $AUDIT_MYSQL_CONNECTION_URL -e "INSERT INTO cedl_operations.cedl_batch_mapping_t values ('${LND_JOB_NAME}','${CEDL_BATCH_DATE}',${MIN_BATCH_LD_NBR},${MAX_BATCH_LD_NBR});"`
		if [ $? -eq 0 ] 
		then
			log_print "[INFO:] Inserted data to cedl_batch_mapping_t table "
		else
			log_print "[ERROR:] Failed to Insert data to cedl_batch_mapping_t table "
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
		fi
	fi
fi
	
# Getting hive incoming table count for auditing .
#TRGT_TABLE_RECORD_COUNT=`${BEELINE_CONNECTION_URL} --silent=true -e "analyze table ${LND_DB_NAME}.${LND_TABLE_NAME} partition(${PARTITION_COL}) compute statistics;select count(*) cnt from ${LND_DB_NAME}.${LND_TABLE_NAME} WHERE ${PARTITION_COL}='${CEDL_BATCH_DATE}';" | grep -Po "\d+"`              

`${BEELINE_CONNECTION_URL} -e "analyze table ${LND_DB_NAME}.${LND_TABLE_NAME} partition(${PARTITION_COL}) compute statistics;"`
if [ $? -eq 0 ]
then
log_print "analyze table completed"
fi

TRGT_TABLE_RECORD_COUNT=`${BEELINE_CONNECTION_URL} --silent=true -e "${BEELINE_COMPUTE_STATS}  select count(*) cnt from ${LND_DB_NAME}.${LND_TABLE_NAME} WHERE ${PARTITION_COL}='${CEDL_BATCH_DATE}';" | grep -Po "\d+"`
if [ $? -eq 0 ]
then
	log_print "count query executed"
	if [ "$TRGT_TABLE_RECORD_COUNT" != 0 ] && [ ! -z "$TRGT_TABLE_RECORD_COUNT" ] 
	then
	log_print "[INFO:] Target hive table count = $TRGT_TABLE_RECORD_COUNT"
	JOB_STAT_NM="success"
	#Inserting value in Audit table 
	audit_cleanup "${JOB_STAT_NM}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" "${TRGT_TABLE_RECORD_COUNT}" "${MOST_RECENT_VAL_TXT}" "${DATA_DIRECTORY}" "${PARTITION_COL}" "${EXTRACTION_MODE}" "${ICR_LOAD_COL_NM}"
	else
	log_print "[ERROR:] Record Count is zero or empty. Please check "
	log_print "Record count is $TRGT_TABLE_RECORD_COUNT"
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}" "${EXTRACTION_MODE}" "${ICR_LOAD_COL_NM}"
	fi
else
	log_print "[ERROR:] Failed to get Target hive table records count" 
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}" "${EXTRACTION_MODE}" "${ICR_LOAD_COL_NM}"
fi
} >> ${CEDL_BASE_PATH}/log/${LOG_DATE}/${LND_JOB_NAME}_${DIR_TIME}.log 2>&1;
