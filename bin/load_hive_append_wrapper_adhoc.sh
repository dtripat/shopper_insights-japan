################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : load_hive_append_wrapper.sh                                         #
# Description : This script load data of fact table from landing layer(ORC) to #
#refined layer(ORC)with batch_date and ord_date as partition column.           #
#It will run for three scenarios.                                              #
#1. If total number of argument is 4 then it will run for normal run.          #
#2. If total number of argument is 5 then it will run for single batch_date    #
#which contains 4th argument(batch_ld_nbr) in it.                              #
#3. If total number of argumen is 6 then it will run for  batch_date           #
#which contains 4th argument(start batch_ld_nbr) and                           #
#5th argument (end batch_ld_nbr) in it.                                        #
################################################################################
set -x
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources
 
# Function to print the logs with current utc time including milliseconds 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
	log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
	log_print "[INFO:] The value of all command-line arguments : $*"
	log_print "[ERROR:] Invalid arguments please pass atleast four arguments"
	log_print "[INFO:] Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename> <business partition column> "
	exit 1
}

load_resource()
{
	log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
	log_print "[INFO:] The value of all command-line arguments : $*"
	. $1
	if [ $? -eq 0 ]
	then    
		log_print "[INFO:] $1 Loaded successfully "
	else
		log_print "[ERROR:] $1 Loading  failed " 
		exit 1
	fi
}

#Insert in audit table
audit_cleanup(){
log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
log_print "[INFO:] The value of all command-line arguments : $*"

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
REF_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
REF_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
REF_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
RUN_MODE="${16}"
ICR_LOAD_COL_NM="${17}"

log_print "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,
src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,
val_datatype_nm,begin_tms,end_tms) 
values 
($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME',
'$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$REF_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,
'INT','$START_TIME','$END_TIME')"
`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,
src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,
val_datatype_nm,begin_tms,end_tms) 
values 
($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
'$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$REF_DATABASE_NAME','$TABLE_NAME',
$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}')"`
if [ $? -eq 0 ]
then
	log_print  "[INFO:] Successfully Job Record Inserted into Audit Table"
fi
if [ "${JOB_STAT_NM}" == "success" ] 
then          
	if [ "${RUN_MODE}" == "normal" ]
	then 
		log_print "[INFO:] Executing MySQL Query: update cedl_operations.cedl_change_data_capture_t set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
		src_column_nm='${ICR_LOAD_COL_NM}',most_recent_val_txt=${MAX_BATCH_LD_NBR},val_datatype_nm='INT',cdc_tms='$START_TIME' where job_nm='${REF_JOB_NAME}'"
		`mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
		src_column_nm='${ICR_LOAD_COL_NM}',most_recent_val_txt=${MAX_BATCH_LD_NBR},val_datatype_nm='INT',
		cdc_tms='$START_TIME' where job_nm='${REF_JOB_NAME}'"`
	fi
	END_TIME_UTC=`date +%s`
	log_print "[INFO:] Started at : `date -d @$START_TIME_UTC`"
	log_print "[INFO:] Ended at : `date -d @$END_TIME_UTC`"
	deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
	printf 'Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
	log_print "[INFO:] Job Completed Successfully."
else
	if [ "${RUN_MODE}" == "normal" ]
	then
		#deleting data from cedl_batch_mapping_t if it exists
		log_print "Executing MySQL Query: DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
		mysql $AUDIT_MYSQL_CONNECTION_URL -e "DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Success : Deleted mapping for ${CEDL_BATCH_DATE} in cedl_operations.cedl_batch_mapping_t  "
		else
			log_print "Failed to delete Mapping for ${CEDL_BATCH_DATE} in cedl_operations.cedl_batch_mapping_t" 
		fi
	fi
	if [ "${REF_DATA_DIRECTORY}" != "NULL" ]
	then 
		#Clean up activity
		log_print "[INFO:] Starting clean up activity..."
		log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $REF_DATA_DIRECTORY"
		hdfs dfs -rm -r  ${REF_DATA_DIRECTORY}
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Files deleted successfully in $REF_DATA_DIRECTORY"
		else
			log_print "[ERROR:] ${REF_DATA_DIRECTORY} doesn't exists"
		fi
		#Removing Partition For Respective Date 
		`${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DATABASE_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
		if [ $? -eq 0 ]
		then
					log_print "[INFO:] Successfully removed partition $CEDL_BATCH_DATE from $REF_DATABASE_NAME.$TABLE_NAME" 
		else	
					log_print "[ERROR:] failed to removed partition $CEDL_BATCH_DATE from $REF_DATABASE_NAME.$TABLE_NAME"
		fi
	fi
		log_print "[INFO:] Job Failed.........Please check"
		END_TIME_UTC=`date +%s`
		log_print "[INFO:] Started at : `date -d @$START_TIME_UTC`"
		log_print "[INFO:] Ended at : `date -d @$END_TIME_UTC`"
		deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
		printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))

		exit 1
fi	
}

#function to get columns list for landing layer table that is to be processed
get_column_lists() {
log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
log_print "[INFO:] The value of all command-line arguments : $*"
LND_DB_NAME=$1
LND_TABLE_NAME=$2
rm $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
rm $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

${BEELINE_CONNECTION_URL} --silent=true -e "show columns in ${LND_DB_NAME}.${LND_TABLE_NAME}" > $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
cat $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |sed 's/|$//g'|sed 's/^|//g'|sed -n '4,$p' | sed '$d'| sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g'| sed 's/,'"${PARTITION_COL}"'//g' | sed 's/,'"${BUS_PARTITION_COL}"'//g' > $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

COLUMNS_LIST=`cat $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
}

if [ $# -le 3 ]
then
    show_usage
fi


log_print "****************Variable info*****************"
START_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=`date +%s`
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"
SRC_DB_NAME=$2
REF_TABLE_NAME=$3
LND_TABLE_NAME=$3
SCHEDULED_DATE=$1
CEDL_BATCH_DATE=$( date -d "${SCHEDULED_DATE} -1 days" '+%Y-%m-%d')
LOG_DATE=$(date -d "$SCHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
BUS_PARTITION_COL=$4
START_BATCH_LD_NBR=$5
END_BATCH_LD_NBR=$6
JOB_STAT_NM=failure
INSERT_ROWS_CNT=NULL
FAILURE=failure


# Loading environment based properties file.
load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties

# Extra variables
LND_DB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}"
REF_DB_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}"
LND_JOB_NAME="${LND_DB_NAME}_${LND_TABLE_NAME}"
REF_JOB_NAME="${REF_DB_NAME}_${REF_TABLE_NAME}"

log_print "[INFO:] Job Name : ${REF_JOB_NAME}"
#Creating directory for logging 
mkdir -p  ${CEDL_BASE_PATH}/log/${LOG_DATE}/

if [ $? -eq 0 ]
then
       log_print "[INFO:] Success : Log path directory created $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
	log_print "[ERROR:] Failed  : Log path directory creation $CEDL_BASE_PATH/log/$LOG_DATE/"
	exit 1  
fi
log_print "[INFO:] Logs Directory location:  - ${CEDL_BASE_PATH}/log/${LOG_DATE}/"${REF_JOB_NAME}_${DIR_TIME}".log "

{

log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments : $*"


#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

log_print  "[INFO:] Job started time : $START_TIME "
log_print  "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

log_print  "[INFO:] *************HIVE INSERT JOB UTILITY*******************"

#beeline variable
BEELINE_DYNAMIC_PARTITION_NONSTRICT="set hive.exec.dynamic.partition.mode=nonstrict;"

#Populating job key value without any extra log info
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm='$REF_JOB_NAME'"`
log_print "[INFO:] Job Key : $JOB_KEY"

if [ $# -eq 4 ]
then
	RUN_MODE=normal
	log_print  "[INFO:] Executing MySQL Query: Select 1 from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and table_partition='${CEDL_BATCH_DATE}'"
	RUN_PARTITION_EXIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "Select 1 from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and table_partition='${CEDL_BATCH_DATE}'"`
	
	if [ $? -eq 0 ] 
	then
		if [ "${RUN_PARTITION_EXIST}" == "1" ]
		then 
		log_print "[ERROR:] batch_ld_nbr not provided for rerun for ${CEDL_BATCH_DATE}"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
	else
		log_print "[ERROR:] Failed to retrieve data from cedl_batch_mapping_t for ${CEDL_BATCH_DATE}"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	#retrieving max processed batch_ld_nbr from landing job 
	log_print "[INFO:] Executing MySQL Query: select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${LND_JOB_NAME}';"
	LAST_PRC_BATCH_LD_NBR_LND=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${LND_JOB_NAME}';"` 
	log_print "[INFO:] LAST_PRC_BATCH_LD_NBR_LND:$LAST_PRC_BATCH_LD_NBR_LND"
	#retrieving max processed batch_ld_nbr from refined job 
	log_print "[INFO:] Executing MySQL Query: select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${REF_JOB_NAME}';"
	LAST_PRC_BATCH_LD_NBR_REF=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${REF_JOB_NAME}';"`
	
	if [ "${LAST_PRC_BATCH_LD_NBR_LND}" == "${LAST_PRC_BATCH_LD_NBR_REF}" ] 
	then
		log_print "[INFO:] No New Data avaialble in the Source after last successfully processed batch_ld_nbr ${LAST_PRC_BATCH_LD_NBR_LND}. PLease check."
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" NULL "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
	fi
	log_print "[INFO:] LAST_PRC_BATCH_LD_NBR_REF:$LAST_PRC_BATCH_LD_NBR_REF"
	TABLE_PARTITION_MIN=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}'  and min_batch_ld_nbr<=${LAST_PRC_BATCH_LD_NBR_REF} and max_batch_ld_nbr>=${LAST_PRC_BATCH_LD_NBR_REF};"`
	if [ "${TABLE_PARTITION_MIN}" != 0 ] && [ ! -z "${TABLE_PARTITION_MIN}" ] 
		then
			log_print "[INFO:] Successfully fetched TABLE_PARTITION_MIN value"
			log_print "[INFO:] TABLE_PARTITION_MIN:$TABLE_PARTITION_MIN"

		else 
			log_print "[INFO:] Please provide entry for ${LAST_PRC_BATCH_LD_NBR_LND} batch_ld_nbr in cedl_operations.cedl_batch_mapping_t table where job_nm='${REF_JOB_NAME}'"
			log_print "[INFO:] TABLE_PARTITION_MIN:$TABLE_PARTITION_MIN"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" NULL "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	TABLE_PARTITION_MAX=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr<=${LAST_PRC_BATCH_LD_NBR_LND} and max_batch_ld_nbr>=${LAST_PRC_BATCH_LD_NBR_LND};"`
	if [ "${TABLE_PARTITION_MAX}" != 0 ] && [ ! -z "${TABLE_PARTITION_MAX}" ] 
		then
			log_print "[INFO:] Successfully fetched TABLE_PARTITION_MAX value"
			log_print "[INFO:] TABLE_PARTITION_MAX:$TABLE_PARTITION_MAX"

		else 
			log_print "[INFO:] Please provide entry for ${LAST_PRC_BATCH_LD_NBR_LND} batch_ld_nbr in cedl_operations.cedl_batch_mapping_t table where job_nm='${LND_JOB_NAME}'"
			log_print "[INFO:] TABLE_PARTITION_MAX:$TABLE_PARTITION_MAX"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" NULL "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	#retrieving table partition from cedl_batch_mapping_t for landing job
	log_print "[INFO:] Executing MySQL Query: select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition > '${TABLE_PARTITION_MIN}' and table_partition <='${TABLE_PARTITION_MAX}';"
	SRC_TABLE_PARTITION=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition > '${TABLE_PARTITION_MIN}' and table_partition <='${TABLE_PARTITION_MAX}';"`
        # SRC_TABLE_PARTITION=`cat /apps/data-ingestion/edw4x/tmp/table_paritation_list_order_trade_item_fact_t.txt`	
if [ $? -eq 0 ]
	then
		log_print "[INFO:] SRC_TABLE_PARTITION:$SRC_TABLE_PARTITION"
	else
		log_print "[ERROR:] Failed to fetch table partition"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	log_print "[INFO:] SRC_TABLE_PARTITION:$SRC_TABLE_PARTITION"
	TABLE_PARTITION_LIST=${SRC_TABLE_PARTITION}
        #TABLE_PARTITION_LIST=`cat /apps/data-ingestion/edw4x/tmp/table_paritation_list_order_trade_item_fact_t.txt`
   echo "TABLE_PARTITION_LIST  $TABLE_PARTITION_LIST"
	
elif [ $# -eq 5 ]
then
	RUN_MODE=single
	#fetching table partition from cedl_batch_mapping_t for landing job
	SRC_TABLE_PARTITION=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and min_batch_ld_nbr <= ${START_BATCH_LD_NBR}  and max_batch_ld_nbr >= ${START_BATCH_LD_NBR};"`
	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Successfully fetched table partition from cedl_batch_mapping_t for landing job"
	else
		log_print "[ERROR:] Failed to fetch table partition from cedl_batch_mapping_t for landing job"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	fi
	log_print "[INFO:] SRC_TABLE_PARTITION:$SRC_TABLE_PARTITION"
	TABLE_PARTITION_LIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and min_batch_ld_nbr <= ${START_BATCH_LD_NBR}  and max_batch_ld_nbr >= ${START_BATCH_LD_NBR};"`
	
else
	RUN_MODE=range
	#retrieving table partitions which contains  batch_ld_nbr = ${START_BATCH_LD_NBR}
	TABLE_PARTITION1=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and min_batch_ld_nbr <= ${START_BATCH_LD_NBR} and max_batch_ld_nbr >= ${START_BATCH_LD_NBR};"`
	log_print "[INFO:] TABLE_PARTITION1$TABLE_PARTITION1"
	
	#retrieving table partitions which contains  batch_ld_nbr = ${END_BATCH_LD_NBR}
	TABLE_PARTITION2=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and min_batch_ld_nbr <= ${END_BATCH_LD_NBR} and max_batch_ld_nbr >= ${END_BATCH_LD_NBR};"`
	log_print "[INFO:] TABLE_PARTITION2$TABLE_PARTITION2"
	if [ "${TABLE_PARTITION1}" != "${TABLE_PARTITION2}" ] 
	then
		log_print "[ERROR:] invalid run. Source to Landing job has not been executed yet for batch_ld_nbr in between ${START_BATCH_LD_NBR} and ${END_BATCH_LD_NBR}"
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
	else
		SRC_TABLE_PARTITION=${TABLE_PARTITION1}
		TABLE_PARTITION1=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and min_batch_ld_nbr <= ${START_BATCH_LD_NBR} and max_batch_ld_nbr >= ${START_BATCH_LD_NBR};"`
		TABLE_PARTITION2=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select table_partition from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and min_batch_ld_nbr <= ${END_BATCH_LD_NBR} and max_batch_ld_nbr >= ${END_BATCH_LD_NBR};"`
		#list of table partition for batch_ld_nbr in between ${START_BATCH_LD_NBR} & ${END_BATCH_LD_NBR} for cleanup 
		TABLE_PARTITION_LIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and table_partition >='${TABLE_PARTITION1}' and table_partition <= '${TABLE_PARTITION2}';"`
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Successfully fetched table partition for batch_ld_nbr in between ${START_BATCH_LD_NBR} & ${END_BATCH_LD_NBR}"
		else
			log_print "[ERROR:] Failed to fetch table partition for batch_ld_nbr in between ${START_BATCH_LD_NBR} & ${END_BATCH_LD_NBR}"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
		
	fi
		
fi

log_print "[INFO:] TABLE_PARTITION_LIST:$TABLE_PARTITION_LIST"
if [ "${SRC_TABLE_PARTITION}" != 0 ] && [ ! -z "${SRC_TABLE_PARTITION}" ] && [ "${SRC_TABLE_PARTITION}" != "NULL" ] 
then
	log_print "[INFO:] Successfully fetched table partition from landing table to be process"
else
	log_print "[INFO:] SRC_TABLE_PARTITION has ${SRC_TABLE_PARTITION} value. Please check."
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${SRC_TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"

fi
if [ "${TABLE_PARTITION_LIST}" != 0 ] && [ ! -z "${TABLE_PARTITION_LIST}" ] && [ "${TABLE_PARTITION_LIST}" != "NULL" ] 
then
	log_print "[INFO:] Successfully fetched table partition from refined table"
else
	log_print "[INFO:] TABLE_PARTITION_LIST has ${TABLE_PARTITION_LIST} value. Please check."
	audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${TABLE_PARTITION_LIST}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"

fi
# Rerun Scenario - deleting directory if it already exists
	for TABLE_PARTITION in $(echo ${TABLE_PARTITION_LIST} | sed "s/,/ /g")
	do
	
		`${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DB_NAME}.${REF_TABLE_NAME} DROP IF EXISTS PARTITION(${PARTITION_COL}='${TABLE_PARTITION}');"`
		if [ $? -eq 0 ]
		then
			log_print "[INFO:] Successfully Dropped partition for ${REF_DB_NAME}.${REF_TABLE_NAME}"
		else
			log_print "[ERROR:] Failed to Dropped partition for ${REF_DB_NAME}.${REF_TABLE_NAME}"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
		fi
		hdfs dfs -test -d $CEDL_ADLS_ENV_BUCKET/$REF_ADLS_BASEPATH/${SRC_DB_NAME}.db/$REF_TABLE_NAME/$PARTITION_COL=${TABLE_PARTITION}
		if [ $? -eq 0 ]
		then
			log_print "[INFO :] Rerun Scenario"
			`hdfs dfs -rm -r $CEDL_ADLS_ENV_BUCKET/$REF_ADLS_BASEPATH/${SRC_DB_NAME}.db/$REF_TABLE_NAME/$PARTITION_COL=${TABLE_PARTITION}`

			if [ $? -eq 0 ]
			then
				log_print "[INFO:] Success: Deleted directory for ${TABLE_PARTITION} partition "
			else
				log_print "[ERROR:] No Files exist for ${TABLE_PARTITION}"
				audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
			fi

		fi
			#deleting data from cedl_operations.cedl_batch_mapping_t if it exists
			`mysql $AUDIT_MYSQL_CONNECTION_URL -e "DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${REF_JOB_NAME}' and table_partition='${TABLE_PARTITION}'"`
			if [ $? -eq 0 ] 
			then
				log_print "[INFO:] Deleted data from cedl_batch_mapping_t table  for $TABLE_PARTITION date"
			else
				log_print "[ERROR:] Failed to delete data from cedl_batch_mapping_t table for $TABLE_PARTITION date"
				audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${TABLE_PARTITION}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
			fi
		
	done

#calling function to get all columns list of landing layer table that is to be processed
get_column_lists $LND_DB_NAME $LND_TABLE_NAME
#Printing all necessary variable used in shell scripting
log_print "[INFO:] RUN_MODE: $RUN_MODE"
log_print "[INFO:] LND_DB_NAME:$LND_DB_NAME"
log_print "[INFO:] LND_TABLE_NAME:$LND_TABLE_NAME"
log_print "[INFO:] COLUMNS_LIST:$COLUMNS_LIST"
log_print "[INFO:] TABLE_PARTITION_LIST:$TABLE_PARTITION_LIST"

for PARTITION_VAL in $(echo ${SRC_TABLE_PARTITION} | sed "s/,/ /g")
do
	log_print "[INFO:] PARTITION_VAL: $PARTITION_VAL"
	log_print "[INFO:] Executing Hive Query: ${BEELINE_DYNAMIC_PARTITION_NONSTRICT}INSERT OVERWRITE TABLE ${REF_DB_NAME}.${REF_TABLE_NAME} partition(${PARTITION_COL}='${PARTITION_VAL}',${BUS_PARTITION_COL}) SELECT ${COLUMNS_LIST},
	from_unixtime(${START_TIME_UTC},'y-MM-d HH:mm:s'),
	${BUS_PARTITION_COL}
	FROM ${LND_DB_NAME}.${LND_TABLE_NAME}  WHERE ${PARTITION_COL} = '${PARTITION_VAL}';"

	`${BEELINE_CONNECTION_URL} -e "${BEELINE_DYNAMIC_PARTITION_NONSTRICT}INSERT OVERWRITE TABLE ${REF_DB_NAME}.${REF_TABLE_NAME} partition(${PARTITION_COL}='${PARTITION_VAL}',${BUS_PARTITION_COL}) SELECT ${COLUMNS_LIST},
	from_unixtime(${START_TIME_UTC},'y-MM-d HH:mm:s'),
	${BUS_PARTITION_COL}
	FROM ${LND_DB_NAME}.${LND_TABLE_NAME}  WHERE ${PARTITION_COL} = '${PARTITION_VAL}';"`

	if [ $? -eq 0 ]
	then
		log_print "[INFO:] Success: Ingested the data into  $REF_DB_NAME.$REF_TABLE_NAME"
		DATA_DIRECTORY=${CEDL_ADLS_ENV_BUCKET}/${REF_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${REF_TABLE_NAME}/${PARTITION_COL}=${PARTITION_VAL}
		# Getting hive refined table count for current ingestion.
		log_print "[INFO:] Executing Hive Query: select CONCAT(min(${ICR_LOAD_COL_NM}),',',max(${ICR_LOAD_COL_NM})) from ${REF_DB_NAME}.${REF_TABLE_NAME} where ${PARTITION_COL}='${PARTITION_VAL}';"
		BATCH_LD_NBR_MAPPING=`${BEELINE_CONNECTION_URL} -e  "select CONCAT(min(${ICR_LOAD_COL_NM}),',',max(${ICR_LOAD_COL_NM})) from ${REF_DB_NAME}.${REF_TABLE_NAME} where ${PARTITION_COL}='${PARTITION_VAL}';" | grep '[0-9]\{10\},[0-9]\{10\}' |sed 's/|//g'`
		if [ "$BATCH_LD_NBR_MAPPING" != 0 ] && [ ! -z "$BATCH_LD_NBR_MAPPING" ] 
		then
		MIN_BATCH_LD_NBR=`echo ${BATCH_LD_NBR_MAPPING} | cut -d ',' -f1`
		MAX_BATCH_LD_NBR=`echo ${BATCH_LD_NBR_MAPPING} | cut -d ',' -f2`
		log_print "[INFO:] BATCH_LD_NBR_MAPPING:$BATCH_LD_NBR_MAPPING"
		log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
		log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
		else
		audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${PARTITION_VAL}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
		fi

		log_print "[INFO:] Executing MySQL Query:INSERT INTO cedl_operations.cedl_batch_mapping_t values ('${REF_JOB_NAME}','${PARTITION_VAL}',${MIN_BATCH_LD_NBR},${MAX_BATCH_LD_NBR});"
		`mysql $AUDIT_MYSQL_CONNECTION_URL -e "INSERT INTO cedl_operations.cedl_batch_mapping_t values ('${REF_JOB_NAME}','${PARTITION_VAL}',${MIN_BATCH_LD_NBR},${MAX_BATCH_LD_NBR});"`
		if [ $? -eq 0 ] 
		then
				log_print "[INFO:] Inserting data to cedl_batch_mapping_t table "
		else
				log_print "[ERROR:] Failed to Insert data to cedl_batch_mapping_t table "
				audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${PARTITION_VAL}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
		fi
		log_print "[INFO:] PARTITION_VAL:$PARTITION_VAL"
		log_print "[INFO:] Executing Hive Query: select count(*) cnt from $REF_DB_NAME.$REF_TABLE_NAME WHERE $PARTITION_COL='${PARTITION_VAL}';"
		HIVE_COUNT=`${BEELINE_CONNECTION_URL} --silent=true -e "select count(*) cnt from $REF_DB_NAME.$REF_TABLE_NAME WHERE $PARTITION_COL='${PARTITION_VAL}';" | grep -Po "\d+"`
		if [ $? -eq 0 ]
		then
			if [ "$HIVE_COUNT" != 0 ] && [ ! -z "$HIVE_COUNT" ] 
			then
				log_print  "[INFO:] HIVE_COUNT : $HIVE_COUNT"
				JOB_STAT_NM="success" 
				audit_cleanup "${JOB_STAT_NM}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${PARTITION_VAL}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" "${HIVE_COUNT}" "${MAX_BATCH_LD_NBR}" "${DATA_DIRECTORY}" "${PARTITION_COL}" "${RUN_MODE}" "${ICR_LOAD_COL_NM}"
			else
				audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${PARTITION_VAL}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
			fi
		else
			log_print "[ERROR:] Failed to get incoming table records count"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${PARTITION_VAL}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
		fi
	else

			log_print "[ERROR:] Failed: Hive Insert Job Failed"
			audit_cleanup "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${PARTITION_VAL}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${REF_TABLE_NAME}" "${REF_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
	fi
done
} >> ${CEDL_BASE_PATH}/log/${LOG_DATE}/"${REF_JOB_NAME}_${DIR_TIME}.log" 2>&1; 
