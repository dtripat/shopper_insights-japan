################################################################################
# Name        : Datametica                                                     #
# File        : file_ingestion_wrapper.sh                                      #
# Description :                                                                #
################################################################################
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

#Loading namespace_dev.properties/common_function.sh / function.sh
.  ${SCRIPT_HOME}/../config/namespace_prod.properties
.  ${SCRIPT_HOME}/../Common/file_functions.sh
.  ${SCRIPT_HOME}/../Common/common_function.sh
.  ${SCRIPT_HOME}/../Common/functions.sh


if [ $# -ne 3 ]
then
    show_usage
fi

.  ${SCRIPT_HOME}/../resources/${SRC_NAME}_${FILE_NAME}.properties

#Check all the variables are set or not
fn_print_variable_is_set START_TIME $START_TIME
fn_print_variable_is_set SRC_NAME $SRC_NAME
fn_print_variable_is_set FILE_NAME $FILE_NAME
fn_print_variable_is_set FILE_FORMAT $FILE_FORMAT
fn_print_variable_is_set FILE_JOB_NAME $FILE_JOB_NAME
fn_print_variable_is_set FILE_TEMP_TABLE $FILE_TEMP_TABLE
fn_print_variable_is_set FILE_REF_TABLE $FILE_REF_TABLE
fn_print_variable_is_set FILE_REF_OLD_TABLE $FILE_REF_OLD_TABLE
fn_print_variable_is_set FILE_LOCATION $FILE_LOCATION
fn_print_variable_is_set FILE_REF_DATA_DIRECTORY $FILE_REF_DATA_DIRECTORY
fn_print_variable_is_set FILE_TEMP_DATA_DIRECTORY $FILE_TEMP_DATA_DIRECTORY
fn_print_variable_is_set FILE_ARCHIVE_DATA_DIRECTORY $FILE_ARCHIVE_DATA_DIRECTORY
fn_print_variable_is_set FILE_REF_OLD_DATA_DIRECTORY $FILE_REF_OLD_DATA_DIRECTORY

#Creating directory for logging
fn_create_dir "${CEDL_BASE_PATH}/log/${LOG_DATE}/"

log_print "[INFO:] Logs Directory location: - ${CEDL_BASE_PATH}/log/${LOG_DATE}/${FILE_JOB_NAME}_${DIR_TIME}.log "
{
log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

#File Validations
log_print "[INFO:]Starting file validations at location : $FILE_LOCATION "
fn_validate_files ${FILE_LOCATION} ${FILE_NAME} ${FILE_FORMAT} ${FILE_HEADER}
if [ $? -eq 0 ]
then
     log_print "[INFO:] ALL FILES VALIDATED SUCCESSFULLY"
         log_print "[INFO:] FILE VALIDATION COMPLETED SUCCESSFULLY.READY TO BE INGESTED"
else
     log_print "[ERROR:] FILE VALIDATION FAILED.PLEASE CHECK THE FILES AT LOCATION :$FILE_LOCATION. "
     log_print "[ERROR:] ABORTING THE INGESTION "
         audit_file_ingestion failure
fi


#Archival process
log_print "[INFO:] ARCHIVING FILES STARTED "
archiveFiles ${FILE_LOCATION} ${FILE_ARCHIVE_DATA_DIRECTORY}
if [ $? -eq 0 ]
then
    log_print "[INFO:]ARCHIVAL PROCESS SUCCESSFULLY COMPLETED"
else
    log_print "[ERROR:]FILE ARCHIVAL PROCESS FAILED"
        audit_file_ingestion failure
fi

#Creating temp hdfs directory for files
log_print "[INFO:]CREATING TEMP DIRECTORY $FILE_TEMP_DATA_DIRECTORY"
create_hdfs_dir ${FILE_TEMP_DATA_DIRECTORY}


#Record count of the files at source location
RECORD_CNT_SRC=`cat ${FILE_LOCATION}/${FILE_NAME}* | wc -l`
RECORD_CNT_SRC=`expr $RECORD_CNT_SRC - 1 `
log_print "[INFO:]RECORD COUNT OF THE FILES AT SOURCE LOCATION : $RECORD_CNT_SRC"


#Copying file from $FILE_LOCATION to $FILE_TEMP_DATA_DIRECTORY
log_print "[INFO:]COPYING FILE FROM $FILE_LOCATION TO $FILE_TEMP_DATA_DIRECTORY"
hadoop fs -put ${FILE_LOCATION}/${FILE_NAME}*${FILE_FORMAT} ${FILE_TEMP_DATA_DIRECTORY}
if [ $? -eq 0 ]
then
     log_print "[INFO:]SUCCESSFULLY COPIED FILES FROM $FILE_LOCATION TO $FILE_TEMP_DATA_DIRECTORY"
else
     log_print "[ERROR:]FAILED TO COPY FILES FROM $FILE_LOCATION TO $FILE_TEMP_DATA_DIRECTORY"
        audit_file_ingestion failure
fi

#For InfoScout : Move the current refine table to refine_old tables.
if [ ${SRC_NAME} = "infoscout" ]
then
        `${BEELINE_CONNECTION_URL} --silent=true -e "INSERT OVERWRITE TABLE ${FILE_REF_OLD_TABLE} SELECT * FROM ${FILE_REF_TABLE}"`
        if [ $? -eq 0 ]
        then
                log_print "[INFO:]SUCCESSFULLY MOVED FILES FROM $FILE_REF_DATA_DIRECTORY TO $FILE_REF_OLD_DATA_DIRECTORY"
        else
                log_print "[ERROR:]FAILED TO MOVE FILES FROM $FILE_REF_DATA_DIRECTORY TO $FILE_REF_OLD_DATA_DIRECTORY"
                        audit_file_ingestion failure
        fi

fi

#Moving files from ${FILE_TEMP_DATA_DIRECTORY} to ${FILE_REF_DATA_DIRECTORY}
log_print "[INFO:]MOVING FILES FROM ${FILE_TEMP_DATA_DIRECTORY} TO ${FILE_REF_DATA_DIRECTORY}"
log_print "[INFO:]OVERWRITING TABLE $FILE_REF_TABLE : INSERT OVERWRITE TABLE $FILE_REF_TABLE SELECT * FROM $FILE_TEMP_TABLE"
`${BEELINE_CONNECTION_URL} -e "INSERT OVERWRITE TABLE ${FILE_REF_TABLE} SELECT * FROM ${FILE_TEMP_TABLE}"`
if [ $? -eq 0 ]
then
     log_print "[INFO:]SUCCESSFULLY MOVED FILES FROM $FILE_TEMP_DATA_DIRECTORY TO $FILE_REF_DATA_DIRECTORY"
else
     log_print "[ERROR:]FAILED TO MOVE FILES FROM $FILE_TEMP_DATA_DIRECTORY TO $FILE_REF_DATA_DIRECTORY"
        audit_file_ingestion failure
fi

# Getting record count of files at Refine layer table
TRGT_TABLE_RECORD_COUNT=$(${BEELINE_CONNECTION_URL} -e "select count(*) cnt from $FILE_REF_TABLE" | grep -Po "\d+")
             if [ $? -eq 0 ]
             then
                    log_print "[INFO:] TARGET HIVE TABLE COUNT = $TRGT_TABLE_RECORD_COUNT"

             else
                    log_print "[ERROR:] FAILED TO GET TARGET HIVE TABLE RECORDS COUNT"
                                        audit_file_ingestion failure
              fi

if [ $TRGT_TABLE_RECORD_COUNT -eq $RECORD_CNT_SRC ]
then
        log_print "[INFO:] CLEANING UP THE LINUX TEMP FILE LOCATION : ${FILE_LOCATION}"
       # rm ${FILE_LOCATION}/${FILE_NAME}*${FILE_FORMAT}
        if [ $? -eq 0 ]
        then
                log_print "[INFO:]SUCCESSFULLY CLEANED UP FILES FROM LINUX TEMP LOCATION : ${FILE_LOCATION}/${FILE_NAME}*${FILE_FORMAT}"
        else
                log_print "[ERROR:]FAILED TO CLEAN UP FILES FROM LINUX TEMP LOCATION : ${FILE_LOCATION}/${FILE_NAME}*${FILE_FORMAT}"
                audit_file_ingestion failure
        fi

        log_print "[INFO:] CLEANING UP THE HDFS TEMP FILE LOCATION : ${FILE_TEMP_DATA_DIRECTORY}"
       # hadoop fs -rm ${FILE_TEMP_DATA_DIRECTORY}/*
        if [ $? -eq 0 ]
        then
                log_print "[INFO:]SUCCESSFULLY CLEANED UP FILES FROM HDFS TEMP LOCATION : ${FILE_TEMP_DATA_DIRECTORY}"
        else
                log_print "[ERROR:]FAILED TO CLEAN UP FILES FROM HDFS TEMP LOCATION : ${FILE_TEMP_DATA_DIRECTORY}"
                audit_file_ingestion failure
        fi
        audit_file_ingestion success $TRGT_TABLE_RECORD_COUNT
        log_print "[INFO:] SOURCE AND TARGET RECORD COUNTS ARE MATCHED "
        log_print "[INFO:] FILE = $FILE_NAME INGESTED SUCCESSFULLY"
else
        log_print "[INFO:] SOURCE AND TARGET RECORD COUNTS ARE NOT MATCHED. "
        log_print "[INFO:] FILE = $FILE_NAME INGESTION FAILED"
        audit_file_ingestion failure
fi


} >> "${CEDL_BASE_PATH}/log/${LOG_DATE}/${FILE_JOB_NAME}_${DIR_TIME}.log" 2>&1;

