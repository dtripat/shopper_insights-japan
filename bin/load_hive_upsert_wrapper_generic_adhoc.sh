################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : load_hive_upsert_wrapper_generic.sh                            #
# Description : This script upsert the data from landing to refined layer      #
################################################################################
# Extract SCRIPT_HOME
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

if [ $# -lt 3 ]
then
        fn_show_usage_for_load_hive_upsert
fi

#Loading namespace_dev.properties/common_function.sh / function.sh
.  ${SCRIPT_HOME}/../config/namespace_prod_adhoc.properties
.  ${SCRIPT_HOME}/../Common/common_function.sh
.  ${SCRIPT_HOME}/../Common/functions.sh
.  ${SCRIPT_HOME}/../resources/${SRC_DB_NAME}_${LND_TABLE_NAME}.properties

#MERGE_KEY PROCESSING
COUNT=1
MERGE_KEY=""
for i in $(echo $MERGE_KEYS | sed "s/,/ /g")
do
        if [ "$COUNT" -gt 1 ]
        then
                MERGE_KEY+=" and a.$i=b.$i"
        else
                COUNT=$(($COUNT+1))
                MERGE_KEY+="a.$i=b.$i"
        fi
done
MERGE_KEY="("$MERGE_KEY")"
MERGE_KEY_IS_NULL=""
COUNT=1
for i in $(echo $MERGE_KEYS | sed "s/,/ /g")
do
        if [ "$COUNT" -gt 1 ]
        then
                MERGE_KEY_IS_NULL+=" or b.$i IS NULL"
        else
                COUNT=$(($COUNT+1))
                MERGE_KEY_IS_NULL+="b.$i IS NULL"
        fi
done

#Creating directory for logging
fn_create_dir "$CEDL_BASE_PATH"/"log"/"$LOG_DATE"/

fn_log_print "Logs Directory location:- $CEDL_BASE_PATH/log/$LOG_DATE/${REF_JOB_NAME}_${DIR_TIME}.log "

{
#Printing Argument Provided by User.
fn_log_print "[INFO:] Wrapper Script Name : $0"
fn_log_print "[INFO:] The value of all command-line arguments : $*"

fn_log_print "[INFO:] Job started time : $START_TIME "
fn_log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"




#If user passs extra arguments [Re run scenario]
if [[ ! -z "$BATCH_LD_NBR" ]]
then
        fn_log_print "[INFO:] Re run block Exection start....."
        #Getting Table Partition for respective Batch Load Number Argument.
        fn_log_print "[INFO:] [Re run] Mysql Query : select max(table_partition) from cedl_operations.cedl_batch_mapping_t
        where max_batch_ld_nbr < (select min(max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t
        where job_nm='$REF_JOB_NAME' and max_batch_ld_nbr >= $BATCH_LD_NBR and min_batch_ld_nbr <= $BATCH_LD_NBR) and job_nm='$REF_JOB_NAME';"
        QUERY_OUTPUT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select max(table_partition) from cedl_operations.cedl_batch_mapping_t
        where max_batch_ld_nbr < (select min(max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t
        where job_nm='$REF_JOB_NAME' and max_batch_ld_nbr >= '$BATCH_LD_NBR' and min_batch_ld_nbr <= '$BATCH_LD_NBR') and job_nm='$REF_JOB_NAME';"`

        fn_log_print "[INFO:] [Re run] Maximum table partition date for given batch load number : $QUERY_OUTPUT"

        #Getting Partition Date and Min Batch Load Number
        REF_CEDL_BATCH_DATE=$QUERY_OUTPUT
        TEMP_CEDL_BATCH_DATE_CP=$REF_CEDL_BATCH_DATE
        if [ "$REF_CEDL_BATCH_DATE" != "NULL" ] && [ ! -z "$REF_CEDL_BATCH_DATE" ]
        then

                #Cleanup of Hive Partition and its Locations For Refine With Help of Above Partition Date $TEMP_CEDL_BATCH_DATE
                fn_log_print "[INFO:] [Re run] Current Date : $CRNT_BATCH_DATE"
                TEMP_CEDL_BATCH_DATE_CP=$(date -d "${TEMP_CEDL_BATCH_DATE_CP} + 1 days" '+%Y-%m-%d')
                fn_log_print "[INFO:] [Re run] Copy of max table partition date + 1 : $TEMP_CEDL_BATCH_DATE_CP"
                while [ "${TEMP_CEDL_BATCH_DATE_CP}" != "${CRNT_BATCH_DATE}" ]
                do
                        #Removing Partition For Respective Date [From $TEMP_CEDL_BATCH_DATE_CP to Current Date<$CRNT_BATCH_DATE>]
                        `${BEELINE_CONNECTION_URL}  -e "ALTER TABLE $REF_DB_NAME.$TABLE_NAME
                        DROP IF EXISTS PARTITION($PARTITION_COL='${TEMP_CEDL_BATCH_DATE_CP}')"`
                        if [ $? -eq 0 ]
                        then
                                    fn_log_print "[INFO:] [Re run] Successfully removed partition $TEMP_CEDL_BATCH_DATE_CP from $REF_DB_NAME.$TABLE_NAME"
                        else
                                    fn_log_print "[ERROR:] [Re run] Failed to removed partition $TEMP_CEDL_BATCH_DATE_CP from $REF_DB_NAME.$TABLE_NAME"
                                    audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                        fi

                        #Removing directory For Respective Partition [From $TEMP_CEDL_BATCH_DATE_CP to Current Date<$CRNT_BATCH_DATE>]
                        hdfs dfs -test -d $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP
                        if [ $? -eq 0 ]
                        then
                                hdfs dfs -rm -r $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP
                                if [ $? -eq 0 ]
                                then
                                            fn_log_print "[INFO:] [Re run] Successfully removed directory $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP from $REF_DB_NAME.$TABLE_NAME"
                                else
                                            fn_log_print "[ERROR:] [Re run] Failed to removed directory $REF_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE_CP from $REF_DB_NAME.$TABLE_NAME"
                                            audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                                fi
                        fi


                        TEMP_CEDL_BATCH_DATE_CP=$(date -I -d "$TEMP_CEDL_BATCH_DATE_CP + 1 day")
                done
                #altering Hive table partition to new one getting from audit mapping table
                `${BEELINE_CONNECTION_URL} -e "ALTER TABLE $REF_DB_NAME.$TABLE_NAME
                ADD IF NOT EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE') LOCATION '$REF_DATA_DIRECTORY/$PARTITION_COL=$REF_CEDL_BATCH_DATE'"`
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] [Re run] Successfully add partition $REF_CEDL_BATCH_DATE into table $REF_DB_NAME.$TABLE_NAME"
                else
                        fn_log_print "[ERROR:] [Re run] Failed to add partition $REF_CEDL_BATCH_DATE into table $REF_DB_NAME.$TABLE_NAME"
                        audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"

                fi
                #Deleting Old Data From Audit Table<cedl_batch_mapping_t>
                fn_log_print "delete from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME' and CAST(table_partition AS DATE) > '${REF_CEDL_BATCH_DATE}';"
                `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "delete from cedl_operations.cedl_batch_mapping_t
                where job_nm='$REF_JOB_NAME' and CAST(table_partition AS DATE) > '${REF_CEDL_BATCH_DATE}';"`
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] [Re run] Successfully deleted all records greater than $REF_CEDL_BATCH_DATE"
                else
                        fn_log_print "[ERROR:] [Re run] Failed to deleted all records greater than $REF_CEDL_BATCH_DATE"
                        audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                fi
        else
                fn_log_print "[ERROR:] [Re run] Failed to Get partition date from audit mapping table."
                fn_log_print "[ERROR:] [Re run] Failed to run job for date : $CEDL_BATCH_DATE for given batch load number : $BATCH_LD_NBR "
                audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
        fn_log_print "[INFO:] Re run block Exection end....."
fi

#Getting Hive Count From Landing Layre For Respective Table
LND_HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "SELECT count(*) cnt from $LND_DB_NAME.$TABLE_NAME
WHERE $PARTITION_COL='${CEDL_BATCH_DATE}'" | grep -Po "\d+" | tail -n1`
fn_log_print  "[INFO:] Hive count of Landing Layer for $LND_DB_NAME.$TABLE_NAME : $LND_HIVE_COUNT"

#If sqoop extracted record is zero
if [ $LND_HIVE_COUNT == 0 ]
then
fn_log_print "[INFO:] For CEDL_BATCH_DATE date extracted count is zero"
audit_cleanup_for_load_hive_upsert "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi

#In Normal Run Avoiding re run for same CEDL_BATCH_DATE
CEDL_BATCH_DATE_COUNT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select count(*) from cedl_operations.cedl_batch_mapping_t where job_nm='$REF_JOB_NAME' and table_partition='$CEDL_BATCH_DATE';"`
fn_log_print "[INFO:] Number of records for $CEDL_BATCH_DATE in audit mapping table : $CEDL_BATCH_DATE_COUNT"
if [ ${CEDL_BATCH_DATE_COUNT} -ne 0 ]
then
        fn_log_print "[ERROR:] Failed Entry for date : $CEDL_BATCH_DATE is already present in cedl_batch_mapping_t table."
        audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi


fn_log_print "*************Hive Merge JOB UTILITY*******************"
#Checking Directory before creation Directory Creation For Refine Table Name
fn_log_print "[INFO:] Checking for current run date directory block start....."

hdfs dfs -test -d $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE
if [ $? -eq 0 ]
then
        hdfs dfs -rm -r $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE
        if [ $? -eq 0 ]
        then
                    fn_log_print "[INFO:] [Re run] Successfully drop directory $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE into $REF_DB_NAME.$TABLE_NAME"
                    `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$CEDL_BATCH_DATE');"`
                    if [ $? -ne 0 ]
                    then
                            fn_log_print "[ERROR:] Failed to drop schedule date : '$CEDL_BATCH_DATE' partition : $PARTITION_COL from refine table : ${TABLE_NAME}"
                    fi
                    fn_log_print "[INFO:] Successfully drop schedule date : '$CEDL_BATCH_DATE' partition : $PARTITION_COL from refine table : ${TABLE_NAME}"
                    `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $REF_DB_NAME.$TABLE_NAME
                    ADD IF NOT EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE')"`
                    if [ $? -ne 0 ]
                    then
                            fn_log_print "[ERROR:] While adding partition : $PARTITION_COL='$REF_CEDL_BATCH_DATE' into table $REF_DB_NAME.$TABLE_NAME"
                            audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                    fi
        else
                    fn_log_print "[ERROR:] Failed to dropped directory $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE into $REF_DB_NAME.$TABLE_NAME"
                    audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL NULL "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
fi
fn_log_print "[INFO:] Checking for current run date directory block end....."

#Creating Directory at refine layer
fn_create_dir $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE

fn_log_print "[INFO:] Hive job execution block start....."
#Getting column names for Refine table
fn_get_column_list_upsert_refine

#Checking for file is not empty
if [[ -s "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt ]]
then
        fn_log_print "[INFO:] REF_CEDL_BATCH_DATE $REF_CEDL_BATCH_DATE"
        COLUMNS_LIST=`cat "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt`

        fn_log_print "DROP TABLE IF EXISTS ${TEMP_DB_NAME}.${TABLE_NAME};
        CREATE TABLE ${TEMP_DB_NAME}.${TABLE_NAME} stored as orc location '${TEMP_DATA_DIRECTORY}' tblproperties ('orc.compress'='ZLIB') as SELECT /*+ MAPJOIN(b) */ a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${REF_DB_NAME}.${TABLE_NAME} a
        LEFT OUTER JOIN ${LND_DB_NAME}.${TABLE_NAME} b ON $MERGE_KEY  and b.${PARTITION_COL}='${CEDL_BATCH_DATE}'
        WHERE $MERGE_KEY_IS_NULL;
        INSERT INTO TABLE ${TEMP_DB_NAME}.${TABLE_NAME} SELECT a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${LND_DB_NAME}.${TABLE_NAME} a where ${PARTITION_COL}='$CEDL_BATCH_DATE';
        ${BEELINE_CONNECTION_URL} -e ALTER TABLE ${REF_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE');
        hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/
        ${BEELINE_CONNECTION_URL} -e ALTER TABLE ${REF_DB_NAME}.${TABLE_NAME} ADD PARTITION($PARTITION_COL='$CEDL_BATCH_DATE');"

        `${BEELINE_CONNECTION_URL} -e "${BEELINE_JOIN_NOCONDITIONALTASK}${BEELINE_JOIN_NOCONDITIONALTASK_SIZE}
        DROP TABLE IF EXISTS ${TEMP_DB_NAME}.${TABLE_NAME};
        CREATE TABLE ${TEMP_DB_NAME}.${TABLE_NAME} stored as orc location '${TEMP_DATA_DIRECTORY}'
        tblproperties ('orc.compress'='ZLIB') as SELECT /*+ MAPJOIN(b) */ a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${REF_DB_NAME}.${TABLE_NAME} a
        LEFT OUTER JOIN ${LND_DB_NAME}.${TABLE_NAME} b ON $MERGE_KEY  and b.${PARTITION_COL}='${CEDL_BATCH_DATE}'
        WHERE $MERGE_KEY_IS_NULL;
        INSERT INTO TABLE ${TEMP_DB_NAME}.${TABLE_NAME} SELECT a.$COLUMNS_LIST,'${LOADTIMESTAMP}' FROM ${LND_DB_NAME}.${TABLE_NAME} a
        where ${PARTITION_COL}='$CEDL_BATCH_DATE';"`

        `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DB_NAME}.${TABLE_NAME} ADD PARTITION($PARTITION_COL='$CEDL_BATCH_DATE');"`
        if [ $? -ne 0 ]
        then
                fn_log_print "[ERROR:] Failed while Adding partition : $PARTITION_COL='$CEDL_BATCH_DATE' in table ${REF_DB_NAME}.${TABLE_NAME}"
                audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
        hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/
        if [ $? -ne 0 ]
        then
                fn_log_print "[ERROR:] Failed to move data from directory : ${TEMP_DATA_DIRECTORY} to $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/"
        fi
        `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${REF_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE');"`
        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully  Added partition : $PARTITION_COL='$CEDL_BATCH_DATE' in table ${REF_DB_NAME}.${TABLE_NAME}"
                fn_log_print "[INFO:] Successfully move data from directory : ${TEMP_DATA_DIRECTORY} to $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE/"
                fn_log_print "[INFO:] Successfully Drop older partition from $REF_DB_NAME.$TABLE_NAME"
        else
                fn_log_print "[ERROR:] Failed to inserted data into $TEMP_DB_NAME.$TABLE_NAME"
                audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi


fi
fn_log_print "[INFO:] Hive job execution block ended....."
fn_log_print "[INFO:] ${BEELINE_COMPUTE_STATS} select count(*) cnt from $REF_DB_NAME.$TABLE_NAME WHERE $PARTITION_COL='$CEDL_BATCH_DATE"
# `${BEELINE_CONNECTION_URL} -e "ANALYZE TABLE $REF_DB_NAME.$TABLE_NAME PARTITION($PARTITION_COL='$CEDL_BATCH_DATE') COMPUTE STATISTICS;"`
# if [ $? -eq 0 ]
# then
        # fn_log_print "[INFO:] Analyze table completed successfully."
# fi
HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "${BEELINE_COMPUTE_STATS} select count(*) cnt from $REF_DB_NAME.$TABLE_NAME WHERE $PARTITION_COL='$CEDL_BATCH_DATE';" | grep -Po "\d+"`

if [ $? -eq 0 ]
then
        if [ "$HIVE_COUNT" != 0 ] && [ ! -z "$HIVE_COUNT" ]
        then

                        fn_log_print "[INFO:] Hive count from $REF_DB_NAME.$TABLE_NAME table : $HIVE_COUNT"
                    #Getting min and Max Batch Load Number
                    BATCH_LD_NBRS=$(${BEELINE_CONNECTION_URL} -e "SELECT min($DELTA_COLUMN),max($DELTA_COLUMN) bln from $REF_DB_NAME.$TABLE_NAME WHERE batch_date='${CEDL_BATCH_DATE}' and $DELTA_COLUMN is NOT NULL")
                    if [ $? -eq 0 ]
                    then

                            MIN_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d"|" -f5 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
							MAX_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d"|" -f6 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
                            fn_log_print "[INFO:] Min batch load number from $REF_DB_NAME.$TABLE_NAME : '$MIN_BATCH_LD_NBR' and Max batch load number from $REF_DB_NAME.$TABLE_NAME : $MAX_BATCH_LD_NBR"
                            if [ "$MAX_BATCH_LD_NBR" != 0 ] && [ ! -z "$MAX_BATCH_LD_NBR" ]
                            then
                                    #Insert Min and Max Batch Load Number
                                    `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "insert into cedl_operations.cedl_batch_mapping_t
                                    (job_nm,table_partition,min_batch_ld_nbr,max_batch_ld_nbr)
                                    values('$REF_JOB_NAME','$CEDL_BATCH_DATE','$MIN_BATCH_LD_NBR','$MAX_BATCH_LD_NBR');"`
                                    if [ $? -eq 0 ]
                                    then
                                            fn_log_print "[INFO:] Successfully insert records in cedl_batch_mapping_t"
                                    else
                                            audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"

                                    fi

                                    #Updating Max Batch Load Number into Audit Table for Landing Table
                                    `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t
                                    set most_recent_val_txt='$MAX_BATCH_LD_NBR',cdc_tms='$LOADTIMESTAMP' where job_nm='$REF_JOB_NAME'"`
                                    if [ $? -eq 0 ]
                                    then

                                            fn_log_print  "[INFO:] Successfully updated values in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                            audit_cleanup_for_load_hive_upsert "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" "${HIVE_COUNT}" "${MAX_BATCH_LD_NBR}"  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"

                                    else

                                            fn_log_print  "[ERROR:] Failed to update values in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                            audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"

                                    fi
                            else
                                    fn_log_print "[ERROR:] Failed to get maximum batch lod number"
                                    audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                            fi
                    else

                            fn_log_print "[ERROR:] Failed to get maximum and minimum batch lod number"
                            audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
                    fi



        else
                fn_log_print "[ERROR:] Failed to get Hive table records count"
                audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
        fi
else
        fn_log_print "[ERROR:] Failed to get Hive table records count"
        audit_cleanup_for_load_hive_upsert "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${REF_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${TABLE_NAME}" "${REF_DB_NAME}" NULL NULL  "${REF_DATA_DIRECTORY}" "${PARTITION_COL}" "${REF_CEDL_BATCH_DATE}" "${START_TIME_UTC}"
fi


} >> "$CEDL_BASE_PATH"/log/"$LOG_DATE"/"${REF_JOB_NAME}_${DIR_TIME}.log" 2>&1;
