#################################################################################
#                               General Details                                 #
#################################################################################
# Name        : Datametica                                                      #
# File        : ingest_sqoop_append_wrapper_generic.sh                                  #
# Description : This script load data of fact table from source to landing layer#
#with batch_date as partition column. In normal run it expects 3 arguments              #
#whereas in rerun scenario the total number of arguments may be 4 or 5.                 #
#                       Rerun Scenario                                                                                                          #
#If the total number of argument is 4 then it will run for single batch_date    #
#which contains 4th argument(batch_ld_nbr) in it.                                                               #
#If the total number of argument is 5 then it will run for all  batch_date      #
# which contains 4th argument(start batch_ld_nbr) and                           #
#5th argument (end batch_ld_nbr) in it.                                         #
#################################################################################
set -x
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

#Loading namespace_prod.properties/common_function.sh / function.sh
.  ${SCRIPT_HOME}/../config/namespace_prod.properties
.  ${SCRIPT_HOME}/../Common/common_function.sh
.  ${SCRIPT_HOME}/../Common/functions.sh
.  ${SCRIPT_HOME}/../resources/${SRC_DB_NAME}_${LND_TABLE_NAME}.properties

if [ $# -le 2 ]
then
    fn_show_usage_for_ingest_sqoop_append
fi

log_print "****************Variable info*****************"
fn_print_variable_is_set START_TIME=$START_TIME
fn_print_variable_is_set START_TIME_UTC=$START_TIME_UTC
fn_print_variable_is_set SCHEDULED_DATE=$SCHEDULED_DATE
fn_print_variable_is_set SRC_DB_NAME=$SRC_DB_NAME
fn_print_variable_is_set LND_TABLE_NAME=$LND_TABLE_NAME
fn_print_variable_is_set START_BATCH_LD_NBR=$START_BATCH_LD_NBR
fn_print_variable_is_set END_BATCH_LD_NBR=$END_BATCH_LD_NBR
fn_print_variable_is_set CEDL_BATCH_DATE=$CEDL_BATCH_DATE
fn_print_variable_is_set LOG_DATE=$LOG_DATE
fn_print_variable_is_set DIR_TIME=$DIR_TIME
fn_print_variable_is_set ENVIRONMENT_FILE=$ENVIRONMENT_FILE
fn_print_variable_is_set PASSWORD_FILE=$PASSWORD_FILE
fn_print_variable_is_set FAILURE=$FAILURE
fn_print_variable_is_set INSERT_ROWS_CNT=$INSERT_ROWS_CNT
fn_print_variable_is_set LOADTIMESTAMP=$LOADTIMESTAMP



#function to get columns list for landing layer table that is to be processed

fn_log_print "[INFO:] Job Name : $LND_JOB_NAME"
#Creating directory for logging

fn_create_dir "$CEDL_BASE_PATH"/"log"/"$LOG_DATE"/

fn_log_print "[INFO:] Logs Directory location:  - ${CEDL_BASE_PATH}/log/${LOG_DATE}/${LND_JOB_NAME}_${DIR_TIME}.log "
{

fn_log_print "[INFO:] Wrapper Script Name : $0"
fn_log_print "[INFO:] The value of all command-line arguments : $*"
#function to get columns list for landing layer table that is to be processed
get_column_lists() {
log_print "[INFO:] Function Name : ${FUNCNAME}()"
log_print "[INFO:] The value of all command-line arguments : $*"
LND_DB_NAME=$1
LND_TABLE_NAME=$2
rm $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
rm $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

${BEELINE_CONNECTION_URL} --silent=true -e "describe ${LND_DB_NAME}.${LND_TABLE_NAME}" > $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
cat $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | awk -F" " '{print $2}'| sed 's/ //g'|sed 's/'"${PARTITION_COL}"'//g'| sed 's/col_name//g' |sed 's/|//g' | sed 's/#//g' | sed '/^$/d '|sed ':a;N;$!ba;s/\n/,/g' > $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

COLUMNS_LIST=`cat $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
log_print "COLUMNS_LIST:$COLUMNS_LIST"
}

#Populating job key value
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '${LND_JOB_NAME}' "`

fn_log_print "[INFO:] TEMP_DB_NAME:$TEMP_DB_NAME"
fn_log_print "[INFO:] Job started time : $START_TIME "
fn_log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

fn_log_print "[INFO:] *************SQOOP IMPORT JOB STARTS*******************"

if [ $# -eq 3 ]
then
        EXTRACTION_MODE=normal
        fn_log_print "[INFO:] EXTRACTION_MODE: $EXTRACTION_MODE"
        #retrieving data from cedl_batch_mapping_t table  if data exists for $CEDL_BATCH_DATE date
        RUN_PARTITION_EXIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "Select 1 from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition='${CEDL_BATCH_DATE}'"`

        if [ $? -eq 0 ]
        then
                if [ "${RUN_PARTITION_EXIST}" == "1" ]
                then
                fn_log_print "[ERROR:] batch_ld_nbr not provided for rerun for ${CEDL_BATCH_DATE}"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
                fi
        else
                fn_log_print "[ERROR:] Failed to deleting data from cedl_batch_mapping_t table  if data exists for $CEDL_BATCH_DATE date"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi
        #retrieving max processed batch_ld_nbr from landing job
        fn_log_print "[INFO:] executing MySQL Query: select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${LND_JOB_NAME}';"
        LAST_PRC_BATCH_LD_NBR=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t where job_nm='${LND_JOB_NAME}';"`

        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully retrieved max processed batch_ld_nbr"
        else
                fn_log_print "[ERROR:] Failed to retrieve max processed batch_ld_nbr"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi

        # retrieving distinct batch_ld_nbr from netezza to be processed
        fn_log_print "[INFO:] Executing Sqoop Query: select distinct batch_ld_nbr from $LND_TABLE_NAME where
        ${ICR_LOAD_COL_NM} > ${LAST_PRC_BATCH_LD_NBR}"

         #echo $ICR_LOAD_COL_NM'============================================='
        fn_sqoop_eval_for_bch_lst_nrml > $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully fetched distinct batch_ld_nbr from netezza to be processed"
        else
                fn_log_print "[ERROR:] Failed to fetch distinct batch_ld_nbr from netezza to be processed"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi

        #cleansing the batch_ld_nbr for use
        cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]' | sed 's/^|//g' | sed 's/|.*$//g' > $CEDL_BASE_PATH/tmp/final_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt


        #comma separated list of batch_ld_nbr
        cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]' | sed 's/^|//g' | sed 's/|.*$//g' | sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g' > $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
        BATCH_LD_NBR_LIST=`cat $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
        if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ]
        then
                fn_log_print "[INFO:] Successfully fetched batch_ld_nbr list from Netezza to be process"
        else
                fn_log_print "[INFO:] New data is not available in source after last successfully processed batch_ld_nbr: ${LAST_PRC_BATCH_LD_NBR}.  Please check."
                JOB_STAT_NM="success"
                audit_cleanup_for_ingest_sqoop_append "${JOB_STAT_NM}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
        exit 0;
        fi
        MIN_BATCH_LD_NBR=`cat $CEDL_BASE_PATH/tmp/final_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |  sort  |head -1 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
        MAX_BATCH_LD_NBR=`cat $CEDL_BASE_PATH/tmp/final_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |  sort -r |head -1 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
        fn_log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
        fn_log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
        fn_log_print "[INFO:] BATCH_LD_NBR_LIST:$BATCH_LD_NBR_LIST"
elif [ $# -eq 4 ]
then
        EXTRACTION_MODE=single
        fn_log_print "[INFO:] EXTRACTION_MODE: $EXTRACTION_MODE"

        #Executing query to get table_partition,min_batch_ld_nbr and max_batch_ld_nbr
        fn_log_print  "[INFO:] Executing query to get table_partition,min_batch_ld_nbr and max_batch_ld_nbr: select min(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and max_batch_ld_nbr >= ${START_BATCH_LD_NBR} and min_batch_ld_nbr <= ${START_BATCH_LD_NBR};"
        TABLE_PARTITION=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select min(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and max_batch_ld_nbr >= ${START_BATCH_LD_NBR} and min_batch_ld_nbr <= ${START_BATCH_LD_NBR};"`

        BATCH_LD_NBRS_MAPPING=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select CONCAT_WS(',',min_batch_ld_nbr,max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and max_batch_ld_nbr >= ${START_BATCH_LD_NBR} and min_batch_ld_nbr <= ${START_BATCH_LD_NBR} and table_partition='${TABLE_PARTITION}';" `

        MIN_BATCH_LD_NBR=`echo ${BATCH_LD_NBRS_MAPPING} | cut -d ',' -f1`
        MAX_BATCH_LD_NBR=`echo ${BATCH_LD_NBRS_MAPPING} | cut -d ',' -f2`



  #       retrieving distinct batch_ld_nbr from netezza to be processed

         fn_log_print  "[INFO:] Executing Sqoop Query: select distinct batch_ld_nbr from       $LND_TABLE_NAME where ${ICR_LOAD_COL_NM}>= ${MIN_BATCH_LD_NBR} and ${ICR_LOAD_COL_NM}<=${MAX_BATCH_LD_NBR}"

          fn_sqoop_eval_for_bch_lst_rerun_range > $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

        #cleansing the batch_ld_nbr for use
        cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]' | sed 's/^|//g' | sed 's/|.*$//g' | sed '1d'| sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g' > $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
        BATCH_LD_NBR_LIST=`cat $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
        if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ]
        then
                fn_log_print "[INFO:] Successfully fetched batch_ld_nbr list from Netezza to be process"
        else
                fn_log_print "[INFO:] Failed to fetch batch_ld_nbr list from Netezza to be process"
                 JOB_STAT_NM="success"
                audit_cleanup_for_ingest_sqoop_append "${JOB_STAT_NM}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
        exit 0;
        fi
        CEDL_BATCH_DATE=${TABLE_PARTITION}
        log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
        log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
        log_print "[INFO:] BATCH_LD_NBR_LIST:$BATCH_LD_NBR_LIST"
        log_print "[INFO:] TABLE_PARTITION:$TABLE_PARTITION"
        log_print "[INFO:] BATCH_LD_NBRS_MAPPING:${BATCH_LD_NBRS_MAPPING}"
else
        EXTRACTION_MODE=range
        fn_log_print "[INFO:] EXTRACTION_MODE: $EXTRACTION_MODE"
        #min table partition and min batch_ld_nbr
        fn_log_print "[INFO:] Executing MySQL Query: select CONCAT_WS(',',min_batch_ld_nbr,min(table_partition)) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr<=${START_BATCH_LD_NBR} and max_batch_ld_nbr>=${START_BATCH_LD_NBR}; group by min_batch_ld_nbr;"
        TABLE_PARTITION_MAPPING_MIN=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select CONCAT_WS(',',min_batch_ld_nbr,min(table_partition)) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr<=${START_BATCH_LD_NBR} and max_batch_ld_nbr>=${START_BATCH_LD_NBR} group by min_batch_ld_nbr;"`

        MIN_TABLE_PARTITION=`echo ${TABLE_PARTITION_MAPPING_MIN} | cut -d ',' -f2`
        MIN_BATCH_LD_NBR=`echo ${TABLE_PARTITION_MAPPING_MIN} | cut -d ',' -f1`

        #max table partition and max batch_ld_nbr
        TABLE_PARTITION_MAPPING_MAX=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select CONCAT_WS(',',table_partition,max(max_batch_ld_nbr)) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}'  and min_batch_ld_nbr <=${END_BATCH_LD_NBR} and max_batch_ld_nbr >=${END_BATCH_LD_NBR} group by table_partition;"`

        MAX_BATCH_LD_NBR=`echo ${TABLE_PARTITION_MAPPING_MAX} | cut -d ',' -f2`
        MAX_TABLE_PARTITION=`echo ${TABLE_PARTITION_MAPPING_MAX} | cut -d ',' -f1`

        # list of all table_partitions which lies between MIN_TABLE_PARTITION and MAX_TABLE_PARTITION
        fn_log_print "[INFO:] Executing MySQL Query: select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition >='${MIN_TABLE_PARTITION}' and table_partition <= '${MAX_TABLE_PARTITION}';"
        TABLE_PARTITION_LIST=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select GROUP_CONCAT(table_partition) from cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition >='${MIN_TABLE_PARTITION}' and table_partition <= '${MAX_TABLE_PARTITION}';"`
        fn_log_print "[INFO:] TABLE_PARTITION_LIST:$TABLE_PARTITION_LIST"

        # retrieving distinct batch_ld_nbr from netezza to be processed
        fn_log_print  "[INFO:] Executing Sqoop Query: select distinct batch_ld_nbr from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM}>= ${MIN_BATCH_LD_NBR} and ${ICR_LOAD_COL_NM}<=${MAX_BATCH_LD_NBR}"

         fn_sqoop_eval_for_bch_lst_rerun_range > $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

                cat $CEDL_BASE_PATH/tmp/batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt | grep '[[0-9]' | sed 's/^|//g' | sed 's/|.*$//g' | sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g' > $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
        BATCH_LD_NBR_LIST=`cat $CEDL_BASE_PATH/tmp/csv_batch_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
        if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ]
        then
                fn_log_print "[INFO:] Successfully fetched batch_ld_nbr list from Netezza to be process"
        else
                fn_log_print "[INFO:] Failed to fetch batch_ld_nbr list from Netezza to be process"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
        fi
        CEDL_BATCH_DATE=${MAX_TABLE_PARTITION}
        # Printing necessary variables
        fn_log_print "[INFO:] MIN_BATCH_LD_NBR:$MIN_BATCH_LD_NBR"
        fn_log_print "[INFO:] MAX_BATCH_LD_NBR:$MAX_BATCH_LD_NBR"
        fn_log_print "[INFO:] BATCH_LD_NBR_LIST:$BATCH_LD_NBR_LIST"
        fn_log_print "[INFO:] TABLE_PARTITION_MAPPING_MIN:$TABLE_PARTITION_MAPPING_MIN"
        fn_log_print "[INFO:] TABLE_PARTITION_MAPPING_MAX:$TABLE_PARTITION_MAPPING_MAX"
fi
if [ "${BATCH_LD_NBR_LIST}" != 0 ] && [ ! -z "${BATCH_LD_NBR_LIST}" ] && [ "${BATCH_LD_NBR_LIST}" != "NULL" ]
then
        fn_log_print "[INFO:] Successfully fetched batch_ld_nbr from Netezza to be process"
else
        fn_log_print "[INFO:] New data is not available in source after last successfully processed batch_ld_nbr: ${LAST_PRC_BATCH_LD_NBR}.  Please check."
        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
fi
fn_log_print "[INFO:] CEDL_BATCH_DATE:$CEDL_BATCH_DATE"
DATA_DIRECTORY=${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${CEDL_BATCH_DATE}
TEMP_DATA_DIRECTORY=${CEDL_ADLS_ENV_BUCKET}/${TEMP_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${CEDL_BATCH_DATE}
MOST_RECENT_VAL_TXT=$MAX_BATCH_LD_NBR
#deleting temp table
`${BEELINE_CONNECTION_URL} -e "DROP TABLE IF EXISTS ${TEMP_DB_NAME}.${LND_TABLE_NAME}"`
if [ $? -eq 0 ]
then
        fn_log_print "[INFO:] Successfully dropped Temp table "
else
        fn_log_print "[ERROR:] Failed to drop Temp table"
        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
fi
#calling function to get all columns list of landing layer table that is to be processed
get_column_lists $LND_DB_NAME $LND_TABLE_NAME
fn_log_print "COLUMNS_LIST:$COLUMNS_LIST"
${BEELINE_CONNECTION_URL} -e "CREATE TABLE ${TEMP_DB_NAME}.${LND_TABLE_NAME} stored as ORC
location '${TEMP_DATA_DIRECTORY}'
tblproperties ('orc.compress'='ZLIB','transactional'='false') as SELECT ${COLUMNS_LIST} from ${LND_DB_NAME}.${LND_TABLE_NAME} where 1=0;"
if [ $? -eq 0 ]
then
        fn_log_print "[INFO:] Successfully created temp table with  ${TEMP_DATA_DIRECTORY} location"
else
        fn_log_print "[ERROR:] Failed to create temp table with  ${TEMP_DATA_DIRECTORY} location"
        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
fi
fn_log_print "[INFO:] TEMP_DATA_DIRECTORY: $TEMP_DATA_DIRECTORY"
#Extraction logic
for BATCH_LD_NBR in $(echo ${BATCH_LD_NBR_LIST} | sed "s/,/ /g")
do
        # Running Sqooop Query to get data for all batch_ld_nbr in BATCH_LD_NBR_LIST from Netezza based on batch_ld_nbr
        fn_log_print "[INFO:] Executing Sqoop Query: sqoop import --connect 'jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME}' \
        --table $LND_TABLE_NAME \
        --direct \
        --where '${ICR_LOAD_COL_NM} = ${BATCH_LD_NBR}' \
        --hcatalog-database $TEMP_DB_NAME \
        --hcatalog-table $LND_TABLE_NAME   \
        --hcatalog-storage-stanza 'stored as orc tblproperties ('orc.compress'='ZLIB')' \
        --escaped-by '\' \
        -m ${DEF_SQOOP_APPEND_MAPPER_CNT}"
        sqoop import --connect ${SQOOP_CONNNECTION_URL} \
        --table $LND_TABLE_NAME \
        --direct \
        --where "${ICR_LOAD_COL_NM} = '${BATCH_LD_NBR}'" \
        --hcatalog-database $TEMP_DB_NAME \
        --hcatalog-table $LND_TABLE_NAME   \
        --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
        --escaped-by '\' \
        -m ${DEF_SQOOP_APPEND_MAPPER_CNT}
        if [ $? -eq 0 ]
        then
               fn_log_print "[INFO:] Successfully imported data to ${CEDL_BATCH_DATE} in $TEMP_DB_NAME.$LND_TABLE_NAME"
        else
                fn_log_print "[ERROR:] Failed to import data to ${CEDL_BATCH_DATE} in $TEMP_DB_NAME.$LND_TABLE_NAME"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL  "${TEMP_DATA_DIRECTORY}" "${PARTITION_COL}"
        fi
        fn_log_print "BATCH_LD_NBR in for loop of sqoop:${BATCH_LD_NBR}"
done

if [ "$EXTRACTION_MODE" == "normal" ]
then
        #DROP IF EXISTS PARTITION in metastore
        `${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME DROP IF EXISTS PARTITION ($PARTITION_COL='${CEDL_BATCH_DATE}')"`
        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully dropped partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
        else
                fn_log_print "[ERROR:] Failed to drop partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi
        hdfs dfs -test -d ${DATA_DIRECTORY}
        if [ $? -eq 0 ]
        then

                hdfs dfs -rm -r ${DATA_DIRECTORY}
                if [ $? -eq 0 ]
                then

                        fn_log_print "[INFO:] Successfully deleted old data for ${DATA_DIRECTORY}"
                        hdfs dfs -mkdir ${DATA_DIRECTORY}
                else
                        fn_log_print "[ERROR:] Failed to delete old data for ${DATA_DIRECTORY}"
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
                fi

        else
                hdfs dfs -mkdir ${DATA_DIRECTORY}
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Successfully created directory for $LND_DB_NAME.$LND_TABLE_NAME"
                else
                        fn_log_print "[ERROR:] Failed to create directory for $LND_DB_NAME.$LND_TABLE_NAME"
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
                fi
        fi
elif [ "$EXTRACTION_MODE" == "single" ]
then
        #DROP IF EXISTS PARTITION in metastore
        `${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME DROP IF EXISTS PARTITION ($PARTITION_COL='${CEDL_BATCH_DATE}')"`
        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully dropped partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
        else
                fn_log_print "[ERROR:] Failed to drop partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi
        hdfs dfs -test -d ${DATA_DIRECTORY}
        if [ $? -eq 0 ]
        then
                hdfs dfs -rm -r ${DATA_DIRECTORY}
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Successfully deleted old data for ${DATA_DIRECTORY}"
                else
                        fn_log_print "[ERROR:] Failed to delete old data for ${DATA_DIRECTORY}"
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
                fi
        fi
        hdfs dfs -mkdir ${DATA_DIRECTORY}
        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully created directory for $LND_DB_NAME.$LND_TABLE_NAME"
        else
                fn_log_print "[ERROR:] Failed to create directory for $LND_DB_NAME.$LND_TABLE_NAME"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi
else
        # removing data from all partitions
        for TABLE_PARTITION in $(echo ${TABLE_PARTITION_LIST} | sed "s/,/ /g")
        do
                #Dropping Partition For Respective Date
                `${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${LND_DB_NAME}.${LND_TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='${TABLE_PARTITION}')"`
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Successfully dropped partition ${TABLE_PARTITION} from ${LND_DB_NAME}.${LND_TABLE_NAME}"
                else
                        fn_log_print "[ERROR:] failed to dropped partition ${TABLE_PARTITION} from ${LND_DB_NAME}.${LND_TABLE_NAME}"
                fi
                hdfs dfs -test -d ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}
                if [ $? -eq 0 ]
                then
                        hdfs dfs -rm -r ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}.db/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}
                        if [ $? -eq 0 ]
                        then
                                fn_log_print "[INFO:] Successfully deleted old data for ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}/"
                        else
                                fn_log_print "[ERROR:] Failed to delete old data for ${CEDL_ADLS_ENV_BUCKET}/${LND_ADLS_BASEPATH}/${SRC_DB_NAME}/${LND_TABLE_NAME}/${PARTITION_COL}=${TABLE_PARTITION}/"
                                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DATABASE_NAME}" NULL NULL NULL "${PARTITION_COL}"
                        fi

                fi
        done
        hdfs dfs -mkdir ${DATA_DIRECTORY}
        if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Successfully created directory for $LND_DB_NAME.$LND_TABLE_NAME"
                else
                        fn_log_print "[ERROR:] Failed to create directory for $LND_DB_NAME.$LND_TABLE_NAME"
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL NULL "${PARTITION_COL}"
        fi
fi

#moving temp data to ${CEDL_BATCH_DATE} partition
        hdfs dfs -mv ${TEMP_DATA_DIRECTORY}/* ${DATA_DIRECTORY}
        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Success : Data Ingested into  directory $DATA_DIRECTORY"
        else
                fn_log_print "[ERROR:] Failed  : Data Sqoop Ingestion Job is Failed"
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
        fi

#Adding partition in metastore
`${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DB_NAME.$LND_TABLE_NAME ADD IF NOT EXISTS PARTITION ($PARTITION_COL='${CEDL_BATCH_DATE}')"`
if [ $? -eq 0 ]
then
        fn_log_print "[INFO:] Successfully added partition $PARTITION_COL='${CEDL_BATCH_DATE}' for $LND_DB_NAME.$LND_TABLE_NAME"
else
        fn_log_print "[ERROR:] Failed to added partition $PARTITION_COL='${CEDL_BATCH_DATE}' for  $LND_DB_NAME.$LND_TABLE_NAME"
        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
fi

if [ "${EXTRACTION_MODE}" != "single" ]
then
        if [ "${EXTRACTION_MODE}" == "range" ]
        then
                #updating data of cedl_batch_mapping_t
                fn_log_print "Executing MySQL Query: UPDATE cedl_operations.cedl_batch_mapping_t set min_batch_ld_nbr=${MIN_BATCH_LD_NBR} where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
                mysql $AUDIT_MYSQL_CONNECTION_URL -e "UPDATE cedl_operations.cedl_batch_mapping_t set min_batch_ld_nbr=${MIN_BATCH_LD_NBR} where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
                fn_log_print "TABLE_PARTITION_LIST:$TABLE_PARTITION_LIST"
                for TABLE_PARTITION in $(echo ${TABLE_PARTITION_LIST} | sed "s/,/ /g")
                do
                        if [ "${TABLE_PARTITION}" != "${CEDL_BATCH_DATE}" ]
                        then
                                #deleting data from cedl_batch_mapping_t if it exists
                                fn_log_print "[INFO:] Executing MySQL Query: DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${TABLE_PARTITION}';"
                                mysql $AUDIT_MYSQL_CONNECTION_URL -e "DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${TABLE_PARTITION}';"
                                if [ $? -eq 0 ]
                                then
                                        fn_log_print "[INFO:] Success : Deleted mapping for ${TABLE_PARTITION} in cedl_operations.cedl_batch_mapping_t  "
                                else
                                        fn_log_print "[ERROR:] Failed  : Failed to delete mapping for ${TABLE_PARTITION} in cedl_operations.cedl_batch_mapping_t"
                                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
                                fi
                        fi
                done
        else
                #executing mysql query to insert data in cedl_batch_mapping_t table
                fn_log_print "Executing MySQL Query: INSERT INTO cedl_operations.cedl_batch_mapping_t values ('${LND_JOB_NAME}','${CEDL_BATCH_DATE}','${MIN_BATCH_LD_NBR}','${MAX_BATCH_LD_NBR}');"
                `mysql $AUDIT_MYSQL_CONNECTION_URL -e "INSERT INTO cedl_operations.cedl_batch_mapping_t values ('${LND_JOB_NAME}','${CEDL_BATCH_DATE}','${MIN_BATCH_LD_NBR}','${MAX_BATCH_LD_NBR}');"`
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Inserted data to cedl_batch_mapping_t table "
                else
                        fn_log_print "[ERROR:] Failed to Insert data to cedl_batch_mapping_t table "
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}"
                fi
        fi
fi

# Getting hive incoming table count for auditing .
#TRGT_TABLE_RECORD_COUNT=`${BEELINE_CONNECTION_URL} --silent=true -e "analyze table ${LND_DB_NAME}.${LND_TABLE_NAME} partition(${PARTITION_COL}) compute statistics;select count(*) cnt from ${LND_DB_NAME}.${LND_TABLE_NAME} WHERE ${PARTITION_COL}='${CEDL_BATCH_DATE}';" | grep -Po "\d+"`

`${BEELINE_CONNECTION_URL} -e "analyze table ${LND_DB_NAME}.${LND_TABLE_NAME} partition(${PARTITION_COL}) compute statistics;"`
if [ $? -eq 0 ]
then
fn_log_print "analyze table completed"
fi

TRGT_TABLE_RECORD_COUNT=`${BEELINE_CONNECTION_URL} --silent=true -e "${BEELINE_COMPUTE_STATS}  select count(*) cnt from ${LND_DB_NAME}.${LND_TABLE_NAME} WHERE ${PARTITION_COL}='${CEDL_BATCH_DATE}';" | grep -Po "\d+"`
if [ $? -eq 0 ]
then
        fn_log_print "count query executed"
        if [ "$TRGT_TABLE_RECORD_COUNT" != 0 ] && [ ! -z "$TRGT_TABLE_RECORD_COUNT" ]
        then
        fn_log_print "[INFO:] Target hive table count = $TRGT_TABLE_RECORD_COUNT"
        JOB_STAT_NM="success"
        #Inserting value in Audit table
        audit_cleanup_for_ingest_sqoop_append "${JOB_STAT_NM}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" "${TRGT_TABLE_RECORD_COUNT}" "${MOST_RECENT_VAL_TXT}" "${DATA_DIRECTORY}" "${PARTITION_COL}" "${EXTRACTION_MODE}" "${ICR_LOAD_COL_NM}"
        else
        fn_log_print "[ERROR:] Record Count is zero or empty. Please check "
        fn_log_print "Record count is $TRGT_TABLE_RECORD_COUNT"
        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}" "${EXTRACTION_MODE}" "${ICR_LOAD_COL_NM}"
        fi
else
        fn_log_print "[ERROR:] Failed to get Target hive table records count"
        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" NULL NULL "${DATA_DIRECTORY}" "${PARTITION_COL}" "${EXTRACTION_MODE}" "${ICR_LOAD_COL_NM}"
fi
} >> ${CEDL_BASE_PATH}/log/${LOG_DATE}/${LND_JOB_NAME}_${DIR_TIME}.log 2>&1;

