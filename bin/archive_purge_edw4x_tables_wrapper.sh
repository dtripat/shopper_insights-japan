#############################################################################################################
#                               General Details                                                             #
#############################################################################################################
# Name        : Datametica                                                                                  #
# File        : archive_purge_edw4x_tables_wrapper.sh                                                       #
# Description : This script performs archive and purge job all tables                                       #
# Inputs      : Sheduled date & Hive objects file name                                                      #
#############################################################################################################

export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources
# Function to print the logs with current utc time 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly two arguments"
        log_print "[INFO:] USAGE : "$0" <Sheduled date(YYYY-MM-DD)> <name of the cedl objects file name> "
        exit 1
}
load_resource()
{
        log_print "[INFO:] Loading  $1 ."
        . $1
        if [ $? -eq 0 ]
        then    
            log_print "[INFO:] $1 Loaded successfully "
        else
            log_print "[ERROR:] $1 Loading  failed " 
            exit 1
        fi
}

if [ $# -ne 2 ]
then
    show_usage
fi

echo "****************Variable info*****************"
START_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=$(date +%s)
SHEDULED_DATE=$1
HIVE_OBJECTS_FILE_NAME=$2
JOB_NAME=`echo ${HIVE_OBJECTS_FILE_NAME} | cut -f1 -d '.'`
LOG_DATE=$(date -d "$SHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"

# Loading environment based properties file.
load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading password file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties

log_print "[INFO:] JOB Name : $JOB_NAME"
#Creating directory for logging 
mkdir -p  "$CEDL_BASE_PATH"/log/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Log path directory created $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
        log_print "[INFO:] Log path directory creation $CEDL_BASE_PATH/log/$LOG_DATE/"
        exit 1
        
fi
log_print "[INFO:] Logs Directory location: ${CEDL_BASE_PATH}/log/${LOG_DATE}/${JOB_NAME}_${DIR_TIME}.log "

{
log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments <Sheduled date(YYYY-MM-DD)> <name of the cedl hive objects file name>  : $*"
log_print "[INFO:] Job started time = $START_TIME "
log_print "[INFO:] Environment source code  path = $CEDL_BASE_PATH"
log_print "[INFO:] JOB Name = $JOB_NAME"
log_print "[INFO:] HIVE_OBJECTS_FILE_NAME = $HIVE_OBJECTS_FILE_NAME"

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

log_print "######################################  Hive Environment Creation ############################################## "

for i in `cat $CEDL_BASE_PATH/resources/$HIVE_OBJECTS_FILE_NAME | grep -vE '^#'`
do
        log_print "[INFO:] Hive Table Details : $i"
        log_print "[INFO:] Evaluating required variables"
        
        LAYER_NAME=`echo $i | cut -f1 -d '|'`
        SRC_DB_NAME=`echo $i | cut -f2 -d '|'`
        TABLE_NAME=`echo $i | cut -f3 -d '|'`
        NO_OF_DAYS=`echo $i | cut -f4 -d '|'`
        OPERATION_PERFORM=`echo $i | cut -f5 -d '|'`
        OLDER_THAN_DATE=$(date -d "$CEDL_BATCH_DATE - $NO_OF_DAYS days" +%Y-%m-%d)

        log_print "OLDER_THAN_DATE :$OLDER_THAN_DATE"
		log_print "OPERATION_PERFORM :$OPERATION_PERFORM"
		log_print "SRC_DB_NAME : $SRC_DB_NAME , LAYER_NAME = $LAYER_NAME"
        
        
        #Populating the hive DB NAME variable 
        HIVE_DB_NAME_PREFIX=`echo ${LAYER_NAME}_DB_PREFIX`
        eval "HIVE_DB_NAME_PREFIX=\$$HIVE_DB_NAME_PREFIX";
		#Populating the ADLS path Prefix variable 
        LAYER_ADLS_PATH_VAR=`echo ${LAYER_NAME}_ADLS_BASEPATH`
        ARCHIVE_LAYER_ADLS_PATH_VAR=`echo ARCHIVE_${LAYER_NAME}_ADLS_BASEPATH`
        eval "LAYER_ADLS_PATH=\$$LAYER_ADLS_PATH_VAR";
        eval "ARCHIVE_LAYER_ADLS_PATH=\$$ARCHIVE_LAYER_ADLS_PATH_VAR";
        
        HIVE_DB_NAME=`echo ${HIVE_DB_NAME_PREFIX}_${SRC_DB_NAME}`
		CEDL_SI_ENV=`echo $CEDL_ADLS_ENV_BUCKET/$LAYER_ADLS_PATH/$SRC_DB_NAME.db`
		CEDL_SI_ENV_ARCHIVE=`echo ${CEDL_ADLS_ENV_BUCKET}/${ARCHIVE_LAYER_ADLS_PATH}/$SRC_DB_NAME.db`
        
        
        log_print "[INFO:] Executing ${CEDL_BASE_PATH}/hive/${HIVE_FILE_NAME}..................."
        log_print "[INFO:] HIVE_TABLE_NAME = ${TABLE_NAME}"
        log_print "[INFO:] HIVE_TABLE_LOCATION = $CEDL_SI_ENV/$TABLE_NAME" 
        
        for PREVIOUS_DATE in `hdfs dfs -ls $CEDL_SI_ENV/$TABLE_NAME/ | grep $PARTITION_COL| sed 's/=/\n/g' | cut -d ' ' -f1 |  grep -o -e "[0-9]\{8\}" -e "[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}" | sort -n`
        do
            EXISTING_PARTITION_DATE=$(date +%Y%m%d -d "$PREVIOUS_DATE")
            OLDER_THAN_GIVEN_DATE=$(date +%Y%m%d -d "$OLDER_THAN_DATE")
            
            if [  $EXISTING_PARTITION_DATE -lt  $OLDER_THAN_GIVEN_DATE ]
            then
			 if [ "$OPERATION_PERFORM" == "BOTH" ]
			 then
			   log_print "[INFO:] Performing Archive operation..."
               v2=`hdfs dfs -ls $CEDL_SI_ENV/${TABLE_NAME}/$PARTITION_COL=$PREVIOUS_DATE/ | wc -l`
               if [ $v2 -eq 0 ]
               then
               
                   echo "[INFO]:NO DATA is present for this date partition:: $PREVIOUS_DATE &&  total files are $v2"
                   
               else
                    echo "[INFO]: DATA is present for this date partition:: $PREVIOUS_DATE &&  total files are $v2"
				fi
				hdfs dfs -test -d $CEDL_SI_ENV_ARCHIVE/${TABLE_NAME}
                if [ $? -eq 0 ]
                    then
                        log_print "[INFO:] $CEDL_SI_ENV_ARCHIVE/${TABLE_NAME} exists "
                else
                            log_print "[INFO:] Creating directory at path $CEDL_SI_ENV_ARCHIVE/${TABLE_NAME}"
                            log_print "[INFO:] Operations to be performed : ${OPERTAION_PERFORM} , Creating target directory..........."
                            hdfs dfs -mkdir -p $CEDL_SI_ENV_ARCHIVE/${TABLE_NAME}
				fi
				log_print "[INFO]:executing :  hdfs dfs -mv  $CEDL_SI_ENV/${TABLE_NAME}/$PARTITION_COL=$PREVIOUS_DATE  $CEDL_SI_ENV_ARCHIVE/${TABLE_NAME}/"
				
				hdfs dfs -mv  $CEDL_SI_ENV/${TABLE_NAME}/$PARTITION_COL=$PREVIOUS_DATE  $CEDL_SI_ENV_ARCHIVE/${TABLE_NAME}/
				if [ $? -eq 0 ]
				then
					log_print "[INFO]:DATA File moving successfully done for $PARTITION_COL='$PREVIOUS_DATE'"
					log_print "[INFO:] Dropping table partition....."
					log_print "[INFO:] ALTER TABLE ${HIVE_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$PREVIOUS_DATE');"
					${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${HIVE_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$PREVIOUS_DATE');"
					if [ $? -eq 0 ]
					then
						log_print "[INFO]:Dropping partition $PARTITION_COL=$PREVIOUS_DATE for ${HIVE_DB_NAME}.${TABLE_NAME} successful  "
					else
						log_print "[ERROR]:Dropping partition $PARTITION_COL=$PREVIOUS_DATE for ${HIVE_DB_NAME}.${TABLE_NAME} failed "
					fi
				else
					log_print "[ERROR]:DATA File moving is Failed for $PARTITION_COL='$PREVIOUS_DATE'"
				fi
			  elif [ "$OPERATION_PERFORM" == "PURGE" ]
			  then
					log_print "[INFO:] Performing Purge operation...."
					log_print "[INFO:] hdfs dfs -rm -r  $CEDL_SI_ENV/${TABLE_NAME}/$PARTITION_COL=$PREVIOUS_DATE"
					hdfs dfs -rm -r $CEDL_SI_ENV/${TABLE_NAME}/$PARTITION_COL=$PREVIOUS_DATE
					if [ $? -eq 0 ]
					then
						log_print "[INFO:] Dropping table partition....."
						log_print "[INFO:] ALTER TABLE ${HIVE_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$PREVIOUS_DATE');"
						${BEELINE_CONNECTION_URL} -e "ALTER TABLE ${HIVE_DB_NAME}.${TABLE_NAME} DROP IF EXISTS PARTITION($PARTITION_COL='$PREVIOUS_DATE');"
						if [ $? -eq 0 ]
						then
							log_print "[INFO]:Dropping partition $PARTITION_COL=$PREVIOUS_DATE for ${HIVE_DB_NAME}.${TABLE_NAME} successful  "
						else
							log_print "[ERROR]:Dropping partition $PARTITION_COL=$PREVIOUS_DATE for ${HIVE_DB_NAME}.${TABLE_NAME} failed "
						fi
					else
						log_print "[ERROR]:Dropping directory $PARTITION_COL=$PREVIOUS_DATE failed "
					fi
			  fi
            else
               log_print " [INFO]:$PREVIOUS_DATE is greater than  $OLDER_THAN_DATE, and there are no older dates data to be deleted. hence exiting the loop"
            fi
        done
done
END_TIME_UTC=$(date +%s)
log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
log_print "[INFO:] Job Completed successfully."
 
} >> "${CEDL_BASE_PATH}"/log/"${LOG_DATE}"/${JOB_NAME}_${DIR_TIME}.log 2>&1;
