################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : ingest_sqoop_append_wrapper.sh                                 #
# Description : This script will extract data from source using sqoop          #
#               into landing layer                                             #
################################################################################
# Extract SCRIPT_HOME
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources
set -x
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

#Loading namespace_dev.properties/common_function.sh / function.sh
.  ${SCRIPT_HOME}/../config/dev_namespace_prod_adhoc.properties
.  ${SCRIPT_HOME}/../Common/common_function.sh
.  ${SCRIPT_HOME}/../Common/functions.sh
.  ${SCRIPT_HOME}/../resources/${SRC_DB_NAME}_${LND_TABLE_NAME}.properties

if [ $# -lt 3 ]
then
        fn_show_usage_for_ingest_sqoop_append
fi


echo "LOAD TYPE : $LOAD_TYPE"

#Check all the variables are set or not
fn_print_variable_is_set START_TIME $START_TIME
fn_print_variable_is_set SRC_DB_NAME $SRC_DB_NAME
fn_print_variable_is_set LND_TABLE_NAME $LND_TABLE_NAME
fn_print_variable_is_set LND_JOB_NAME $LND_JOB_NAME
fn_print_variable_is_set REF_JOB_NAME $REF_JOB_NAME
fn_print_variable_is_set LND_DB_NAME $LND_DB_NAME
fn_print_variable_is_set REF_DB_NAME $REF_DB_NAME
fn_print_variable_is_set START_BATCH_LD_NBR $START_BATCH_LD_NBR
fn_print_variable_is_set END_BATCH_LD_NBR $END_BATCH_LD_NBR
fn_print_variable_is_set BATCH_LD_NBR $BATCH_LD_NBR
fn_print_variable_is_set DELTA_COLUMN $DELTA_COLUMN
fn_print_variable_is_set LND_DATA_DIRECTORY $LND_DATA_DIRECTORY
fn_print_variable_is_set REF_DATA_DIRECTORY $REF_DATA_DIRECTORY

#reating directory for logging
fn_create_dir "$CEDL_BASE_PATH"/"log"/"$LOG_DATE"/


fn_log_print "Logs Directory location:- $CEDL_BASE_PATH/log/$LOG_DATE/${LND_JOB_NAME}_${DIR_TIME}.log "

{
fn_log_print "[INFO:] Job started time : $START_TIME "
fn_log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

fn_log_print "*******************Business Logic Start From Here******************************"

#Getting job key and last processed value for delta column
fn_log_print "[INFO:] Job Key for $LND_JOB_NAME Job : $JOB_KEY"

#Getting Refine Layer LAST_PROCESS_VALUE for delta column
LAST_PROCESS_VALUE=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select most_recent_val_txt from cedl_operations.cedl_change_data_capture_t
where job_nm='${LND_JOB_NAME}'"`

fn_log_print "[INFO:] Max LAST_PROCESS_VALUE for $DELTA_COLUMN from Previous Run : $LAST_PROCESS_VALUE"

#If User pass extra argument [Re run scenario]
if [ ! -z "$RERUN_VALUE" ] && [ $# -eq 4 ]
then

                #Getting Table Partition and Min Batch Load Number for respective Batch Load Number Argument.
        fn_log_print "[INFO:] [Re run] Mysql query : select table_partition,min_batch_ld_nbr from cedl_operations.cedl_batch_mapping_t
        where job_nm='$LND_JOB_NAME' and min_batch_ld_nbr<='$RERUN_VALUE' and max_batch_ld_nbr>='$RERUN_VALUE';"
        QUERY_OUTPUT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select table_partition,min_batch_ld_nbr,max_batch_ld_nbr from cedl_operations.cedl_batch_mapping_t
        where job_nm='$LND_JOB_NAME' and min_batch_ld_nbr<='$RERUN_VALUE' and max_batch_ld_nbr>='$RERUN_VALUE';"`
        fn_log_print "[INFO:] [In Rerun] For given batch load number '$RERUN_VALUE' Mysql query output   : $QUERY_OUTPUT"

        #Getting Partition Date and Min Batch Load Number
        TEMP_CEDL_BATCH_DATE=`echo "$QUERY_OUTPUT" | cut -f1 -d$'\t'`
        #Keeping Copy of Partition Date for Future use.
        TEMP_CEDL_BATCH_DATE_CP=$TEMP_CEDL_BATCH_DATE
        MIN_BATCH_LD_NBR=`echo "$QUERY_OUTPUT" | cut -f2 -d$'\t'`
                MAX_BATCH_LD_NBR=`echo "$QUERY_OUTPUT" | cut -f3 -d$'\t'`
                #Subtracting 1 from MIN_BATCH_LD_NBR for further processing for Rerun.
                fn_check_data_type $MIN_BATCH_LD_NBR
                echo "DATA_TYPE = $DATA_TYPE"
                if [ $DATA_TYPE == "NUMERIC" ]; then
                MIN_BATCH_LD_NBR=`expr $MIN_BATCH_LD_NBR - 1`
                elif [ $DATA_TYPE == "DATE" ]; then
                MIN_BATCH_LD_NBR=$( date -d "${MIN_BATCH_LD_NBR} -1 days" '+%Y-%m-%d')
                fi
                echo "MIN_BATCH_LD_NBR=$MIN_BATCH_LD_NBR"

        fn_log_print "[INFO:] [Re run] For given partition Batch date : $TEMP_CEDL_BATCH_DATE and Minimum Batch load number :$MIN_BATCH_LD_NBR"
        if [ "$TEMP_CEDL_BATCH_DATE" != 0 ] && [ ! -z "$TEMP_CEDL_BATCH_DATE" ]
        then
                  #Removing Partition For Respective Date TEMP_CEDL_BATCH_DATE
                        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $LND_DB_NAME.$TABLE_NAME DROP IF EXISTS PARTITION($PARTITION_COL='${TEMP_CEDL_BATCH_DATE}')"`
                        if [ $? -eq 0 ]
                        then
                                    fn_log_print "[INFO:] [Re run] Successfully removed partition $TEMP_CEDL_BATCH_DATE from $LND_DB_NAME.$TABLE_NAME"
                        else
                                    fn_log_print "[ERROR:] [Re run] Failed to removed partition $TEMP_CEDL_BATCH_DATE from $LND_DB_NAME.$TABLE_NAME"
                                    audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
                        fi

                        #Removing directory For Respective Partition TEMP_CEDL_BATCH_DATE
                        hdfs dfs -test -d $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE
                        if [ $? -eq 0 ]
                        then
                                hdfs dfs -rm -r $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE
                                if [ $? -eq 0 ]
                                then
                                            fn_log_print "[INFO:] [Re run] Successfully removed directory $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE from $LND_DB_NAME.$TABLE_NAME"
                                else
                                            fn_log_print "[ERROR:] [Re run] Failed to removed directory $LND_DATA_DIRECTORY/$PARTITION_COL=$TEMP_CEDL_BATCH_DATE from $LND_DB_NAME.$TABLE_NAME"
                                            audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
                                fi
                        fi



                #Deleting Old Data From Audit Table<cedl_batch_mapping_t>
                fn_log_print "[INFO;] [Re run] Mysql Query : delete from cedl_operations.cedl_batch_mapping_t where job_nm='$LND_JOB_NAME' and CAST(table_partition AS DATE) > '${TEMP_CEDL_BATCH_DATE_CP}';"
                `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "delete from cedl_operations.cedl_batch_mapping_t
                where job_nm='$LND_JOB_NAME' and CAST(table_partition AS DATE) = '${TEMP_CEDL_BATCH_DATE}';"`
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] [Re run] Successfully deleted all records greater than equal to $TEMP_CEDL_BATCH_DATE"
                else
                        fn_log_print "[ERROR:] [Re run] Failed to deleted all records greater than equal to $TEMP_CEDL_BATCH_DATE"
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
                fi
        else
                fn_log_print "[ERROR:] [Re run] Failed to Get partition date from audit mapping table."
                fn_log_print "[ERROR:] [Re run] Failed to run job for date : $CEDL_BATCH_DATE for given batch load number : $BATCH_LD_NBR "
                audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
        fi

START_VALUE=$MIN_BATCH_LD_NBR
END_VALUE=$MAX_BATCH_LD_NBR
MODE="RANGE_OR_RERUN"
fi

#Range Scenario
if [ ! -z "$START_BATCH_LD_NBR" ] && [ ! -z "$END_BATCH_LD_NBR" ] && [ $# -eq 5 ]; then

echo "Ingestion Running in Range "
echo "START_BATCH_LD_NBR=$START_BATCH_LD_NBR"
echo "END_BATCH_LD_NBR=$END_BATCH_LD_NBR"
                #Subtracting 1 from START_BATCH_LD_NBR for further processing for Range and entry in refine table.
                fn_check_data_type $START_BATCH_LD_NBR
                echo "DATA_TYPE = $DATA_TYPE"
                if [ $DATA_TYPE == "NUMERIC" ]; then
                START_BATCH_LD_NBR=`expr $START_BATCH_LD_NBR - 1`
                elif [ $DATA_TYPE == "DATE" ]; then
                START_BATCH_LD_NBR=$( date -d "${START_BATCH_LD_NBR} -1 days" '+%Y-%m-%d')
                fi

START_VALUE=$START_BATCH_LD_NBR
END_VALUE=$END_BATCH_LD_NBR
MODE="RANGE_OR_RERUN"
fi

#In Normal Run Avoiding re run for same CEDL_BATCH_DATE
if [ $# -eq 3 ]; then
        fn_log_print "[INFO:] MySql Query : 'select count(*) from cedl_operations.cedl_batch_mapping_t where job_nm='$LND_JOB_NAME' and table_partition='$CEDL_BATCH_DATE';'"
        CEDL_BATCH_DATE_COUNT=`mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "select count(*) from cedl_operations.cedl_batch_mapping_t where job_nm='$LND_JOB_NAME' and table_partition='$CEDL_BATCH_DATE';"`
        fn_log_print "[INFO:] Number of records for $CEDL_BATCH_DATE in audit mapping table : $CEDL_BATCH_DATE_COUNT"
        if [ ${CEDL_BATCH_DATE_COUNT} -ne 0 ]
        then
                        fn_log_print "[ERROR:] Failed Entry for date : $CEDL_BATCH_DATE is already present in cedl_batch_mapping_t table."
                        audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
        fi

        fn_log_print "[INFO:] Batch Load Number : $LAST_PROCESS_VALUE"

        LOADTIMESTAMP=$(date -u  '+%Y-%m-%d %H:%M:%S')

        START_VALUE=$LAST_PROCESS_VALUE
        fn_check_data_type $LAST_PROCESS_VALUE
        if [ $DATA_TYPE == "DATE" ]; then
        END_VALUE=$CEDL_BATCH_DATE
        fi
MODE="NORMAL"
fi


fn_log_print "*************SQOOP IMPORT JOB UTILITY*******************"
#Checking for directory
fn_remove_hdfs_dir ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
echo "LAST_PROCESS_VALUE=$LAST_PROCESS_VALUE"
#Running Sqoop import Job for upsert
.  ${SCRIPT_HOME}/../resources/${SRC_DB_NAME}_${LND_TABLE_NAME}.properties


fn_sqoop_import_append $MODE

if [ $? -eq 0 ]
then

            fn_log_print "[INFO:] Successfully completed sqoop extraction for date $CEDL_BATCH_DATE"
            #Getting Hive Count From Landing Layre For Respective Table
            HIVE_COUNT=`${BEELINE_CONNECTION_URL} -e "SELECT count(*) cnt from $LND_DB_NAME.$TABLE_NAME
            WHERE $PARTITION_COL='${CEDL_BATCH_DATE}'" | grep -Po "\d+" | tail -n1`
            fn_log_print  "[INFO:] Hive count after sqoop utility from $LND_DB_NAME.$TABLE_NAME : $HIVE_COUNT"

            #If sqoop extracted record is zero
            if [ $HIVE_COUNT == 0 ]
            then
                    fn_log_print "[INFO:] For CEDL_BATCH_DATE date extracted count is zero"
                    audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"

            fi

            #Getting Min and Max Batch Load Number From Landing Layer For Respective Table
            BATCH_LD_NBRS=$(${BEELINE_CONNECTION_URL} -e "SELECT min($DELTA_COLUMN),max($DELTA_COLUMN) bln from $LND_DB_NAME.$TABLE_NAME WHERE batch_date='${CEDL_BATCH_DATE}' and $DELTA_COLUMN is NOT NULL" )
            if [ $? -eq 0 ]
            then
                    JOB_STAT_NM="success"
                    MIN_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d"|" -f5 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
                    MAX_BATCH_LD_NBR=`echo $BATCH_LD_NBRS | cut -d"|" -f6 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
                    fn_log_print  "[INFO:] Max Batch load number after sqoop utility from $LND_DB_NAME.$TABLE_NAME : $MAX_BATCH_LD_NBR"
                    if [ "$MAX_BATCH_LD_NBR" != 0 ] && [ ! -z "$MAX_BATCH_LD_NBR" ]
                    then

                            #Insert Min and Max Batch Load Number Into Audit cedl_batch_mapping_t Table
                            `mysql $AUDIT_MYSQL_CONNECTION_URL -N -e "insert into cedl_operations.cedl_batch_mapping_t
                            (job_nm,table_partition,min_batch_ld_nbr,max_batch_ld_nbr)
                            values('$LND_JOB_NAME','$CEDL_BATCH_DATE','$MIN_BATCH_LD_NBR','$MAX_BATCH_LD_NBR');"`
                            if [ $? -eq 0 ]
                            then
                                    fn_log_print "[INFO:] Successfully insert records in cedl_batch_mapping_t for $LND_JOB_NAME job name"
                            else
                                    fn_log_print "[ERROR:] Failed to insert records in cedl_batch_mapping_t for $LND_JOB_NAME job name"
                                    audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
                            fi

                            # Updating Max Batch_ld_nbr Into Audit Table For Landing Layer
                            `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t
                            set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
                            src_column_nm='$ICR_LOAD_COL_NM',most_recent_val_txt='$MAX_BATCH_LD_NBR',cdc_tms='$LOADTIMESTAMP'
                            where job_nm = '$LND_JOB_NAME'"`
                            if [ $? -eq 0 ]
                            then

                                    fn_log_print  "[INFO:] Successfully updated max batch load number in cedl_operations.cedl_change_data_capture_t for job $LND_JOB_NAME."

                            else

                                    fn_log_print  "[ERROR:] Failed to update max batch load number in cedl_operations.cedl_change_data_capture_t for job $LND_JOB_NAME."
                                    audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"

                            fi

                            # Updating Min Batch_ld_nbr Into Audit Table For Refine Layer
                            `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t
                            set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',src_column_nm='$ICR_LOAD_COL_NM',most_recent_val_txt='$LAST_PROCESS_VALUE',cdc_tms='$LOADTIMESTAMP' \
                            where job_nm = '$REF_JOB_NAME'"`
                            if [ $? -eq 0 ]
                            then

                                    fn_log_print  "[INFO:] Successfully updated min batch load number in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                    audit_cleanup_for_ingest_sqoop_append "${SUCCESS}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"

                            else

                                    fn_log_print  "[ERROR:] Failed to update min batch load number in cedl_operations.cedl_change_data_capture_t for job $REF_JOB_NAME."
                                    audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"

                            fi
                    else

                            fn_log_print "[ERROR:] Failed to get maximum batch lod number from $LND_DB_NAME.$TABLE"
                            audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"
                    fi
            else
                    fn_log_print "[ERROR:] Failed to get maximum and minimum batch lod number from $LND_DB_NAME.$TABLE"
                    audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"

            fi
    else

            fn_log_print "[ERROR:] Failed while running sqoop job"
            audit_cleanup_for_ingest_sqoop_append "${FAILURE}" "${JOB_KEY}" "${START_TIME}" "${LND_JOB_NAME}" "${CEDL_BATCH_DATE}" "${SRC_NAME}" "${SRC_DB_NAME}" "${SRC_DB_TYPE}" "${SRC_DB_NAME}" "${LND_TABLE_NAME}" "${LND_DB_NAME}" 0 NULL NULL "${PARTITION_COL}"


fi


} >> "$CEDL_BASE_PATH"/log/"$LOG_DATE"/"${LND_JOB_NAME}_${DIR_TIME}.log" 2>&1;

