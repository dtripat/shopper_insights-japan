export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`


#Loading namespace_dev.properties/common_function.sh / function.sh
.  ${SCRIPT_HOME}/../config/namespace_prod.properties
.  ${SCRIPT_HOME}/../Common/file_functions.sh
.  ${SCRIPT_HOME}/../Common/common_function.sh
.  ${SCRIPT_HOME}/../Common/functions.sh


if [ $# -ne 3 ]
then
    show_usage
fi

.  ${SCRIPT_HOME}/../resources/${SRC_NAME}_${FILE_NAME}.properties

#Check all the variables are set or not
fn_print_variable_is_set START_TIME $START_TIME
fn_print_variable_is_set SRC_NAME $SRC_NAME
fn_print_variable_is_set FILE_NAME $FILE_NAME
fn_print_variable_is_set FILE_FORMAT $FILE_FORMAT
fn_print_variable_is_set FILE_JOB_NAME $FILE_JOB_NAME
fn_print_variable_is_set FILE_TEMP_TABLE $FILE_TEMP_TABLE
fn_print_variable_is_set FILE_REF_TABLE $FILE_REF_TABLE
fn_print_variable_is_set FILE_REF_OLD_TABLE $FILE_REF_OLD_TABLE
fn_print_variable_is_set CSV_FILE_LOCATION $CSV_FILE_LOCATION
fn_print_variable_is_set FILE_REF_DATA_DIRECTORY $FILE_REF_DATA_DIRECTORY
fn_print_variable_is_set FILE_TEMP_DATA_DIRECTORY $FILE_TEMP_DATA_DIRECTORY
fn_print_variable_is_set FILE_ARCHIVE_DATA_DIRECTORY $FILE_ARCHIVE_DATA_DIRECTORY
fn_print_variable_is_set FILE_REF_OLD_DATA_DIRECTORY $FILE_REF_OLD_DATA_DIRECTORY
fn_print_variable_is_set REFINE_TABLE_OVERWRITE_QUERY "$REFINE_TABLE_OVERWRITE_QUERY"
fn_print_variable_is_set FILE_REF_OLD_DATA_DIRECTORY_BACKUP $FILE_REF_OLD_DATA_DIRECTORY_BACKUP

#Creating directory for logging
fn_create_dir "${CEDL_BASE_PATH}/log/${LOG_DATE}/"

log_print "[INFO:] Logs Directory location: - ${CEDL_BASE_PATH}/log/${LOG_DATE}/${FILE_JOB_NAME}_${DIR_TIME}.log "
{
log_print "[INFO:] Job started time : $START_TIME "
log_print "[INFO:] Environment source code  path : $CEDL_BASE_PATH"

MAIL_LIST="Mohammad.JavedAnsari@catalina.com,Devesh.DuttTripathi@catalina.com,Sridhar.Sathyanarayanan@catalina.com,Sridhar.Pothamsetti@catalina.com,DM_Catalina_ADLS@datametica.com,Anshul.Bindal@catalina.com,Sonny.Shah@catalina.com,Aaron.Augustine@catalina.com,Kumarasiri.Samaranayake@catalina.com,Tim.Taylor@catalina.com,John.Kane@catalina.com,Data.Operations@catalina.com,Dan.Cropsey@catalina.com,Denton.Zukowski@catalina.com,Jeanne.Kondelik@catalina.com,Gary.Feigler@catalina.com,Priethi.Alagesan@catalina.com,Shreyas.Bansode@catalina.com,andrea.cohen@catalina.com"

FILE_INGESTION_MAIL_BODY="${FILE_INGESTION_BASE_PATH}/tmp/file_ingestion_mail_body.html"
fn_create_file ${FILE_INGESTION_MAIL_BODY}

LAST_PROCESSED_ZIP_FILE_DATE=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select most_recent_val_txt from cedl_operations.cedl_audit_t where job_nm= '${SFTP_JOB_NAME}' and job_stat_nm='success' order by audit_key desc limit 1"`

#Create File LIST
FILE_NAME_LIST=`ls ${CSV_FILE_LOCATION}/${FILE_NAME}_${LAST_PROCESSED_ZIP_FILE_DATE}.csv`
if [ $? -eq 0 ]
then
     log_print "[INFO:]FILE LIST TO BE INGESTED = ${FILE_NAME_LIST}"
	FILE_NAME_LIST=`ls ${CSV_FILE_LOCATION}/${FILE_NAME}_${LAST_PROCESSED_ZIP_FILE_DATE}.csv` 

else
    log_print "[ERROR:]NO FILES TO BE INGESTED AT LOCATION = ${CSV_FILE_LOCATION}. "
     log_print "[ERROR:]FAILED TO GET THE FILE LIST TO BE INGESTED "
   

ZIP_FILE_NAME="catalina_${LAST_PROCESSED_ZIP_FILE_DATE}.zip"
cat <<EOT >> ${FILE_INGESTION_MAIL_BODY}
<!DOCTYPE html>
<html>
<body text=\" \">
<p>
<pre><b>Hello All ,

PFB STATUS FOR NUMERATOR-FILE INGESTION :-</b>
<b>FILE :</b> ${FILE_NAME}
<b>STATUS :</b> FILE NOT FOUND 
<b>LAST PROCESSED ZIP FILE NAME :</b> ${ZIP_FILE_NAME} 
</pre>
</p>
</body>
</HTML>
EOT
send_email "FILE NOT FOUND"
audit_file_ingestion success
exit 0
fi


#Remove ctrl-m character
log_print "[INFO:] Removal of ctrl-m character started "
for CSV_FILE in ${FILE_NAME_LIST}
do
perl -pi -e 's/\r\n/\n/g' ${CSV_FILE}
if [ $? -eq 0 ]
then
     log_print "[INFO:] SUCCESSFULLY REMOVED ctrl-m CHARACTER FOR FILE : ${CSV_FILE}"

else
     log_print "[ERROR:]FAILED TO REMOVE ctrl-m CHARACTER FOR FILE : ${CSV_FILE}"
     audit_file_ingestion failure
fi
done

#Remove special character
log_print "[INFO:] Removal of special character started "
for CSV_FILE in ${FILE_NAME_LIST}
do
sed -i -e 's/\\//g' $CSV_FILE
if [ $? -eq 0 ]; then
log_print "[INFO:]Special charcahter removed successfully from file: $CSV_FILE"
else
log_print "[INFO:]Special charcahter removal Failed for file: $CSV_FILE "
audit_file_ingestion failure
fi
done


#File Validations
log_print "[INFO:]Starting file validations at location : $CSV_FILE_LOCATION "
fn_validate_files ${CSV_FILE_LOCATION} "${FILE_NAME}_${LAST_PROCESSED_ZIP_FILE_DATE}" ${FILE_FORMAT} ${FILE_HEADER}
if [ $? -eq 0 ]
then
     log_print "[INFO:] ALL FILES VALIDATED SUCCESSFULLY"
         log_print "[INFO:] FILE VALIDATION COMPLETED SUCCESSFULLY.READY TO BE INGESTED"
else
     log_print "[ERROR:] FILE VALIDATION FAILED.PLEASE CHECK THE FILES AT LOCATION :$CSV_FILE_LOCATION. "
     log_print "[ERROR:] ABORTING THE INGESTION "
         audit_file_ingestion failure
fi

for CSV_FILE in ${FILE_NAME_LIST}
do

                #Creating temp hdfs directory for files
                log_print "[INFO:]CREATING TEMP DIRECTORY $FILE_TEMP_DATA_DIRECTORY"
                create_hdfs_dir ${FILE_TEMP_DATA_DIRECTORY}
		if [ $? -eq 0 ]
		then
        	log_print "[INFO:]SUCCESSFULLY CREATED TEMP DIRECTORY $FILE_TEMP_DATA_DIRECTORY"
        	else
        	log_print "[ERROR:]FAILED TO CREATE TEMP DIRECTORY $FILE_TEMP_DATA_DIRECTORY"
        	audit_file_ingestion failure
		fi


                #Record count of the files at source location
                RECORD_CNT_SRC=`cat ${CSV_FILE} | wc -l`
                RECORD_CNT_SRC=`expr $RECORD_CNT_SRC - 1 `
                log_print "[INFO:]RECORD COUNT OF THE FILES AT SOURCE LOCATION : $RECORD_CNT_SRC"

                #Copying file from ${CSV_FILE} to $FILE_TEMP_DATA_DIRECTORY
                log_print "[INFO:]COPYING FILE FROM ${CSV_FILE} TO $FILE_TEMP_DATA_DIRECTORY"
                hadoop fs -put ${CSV_FILE} ${FILE_TEMP_DATA_DIRECTORY}
                if [ $? -eq 0 ]
                then
                        log_print "[INFO:]SUCCESSFULLY COPIED FILES FROM ${CSV_FILE} TO $FILE_TEMP_DATA_DIRECTORY"
                        hadoop fs -chmod -R 777 $FILE_TEMP_DATA_DIRECTORY
                else
                        log_print "[ERROR:]FAILED TO COPY FILES FROM ${CSV_FILE} TO $FILE_TEMP_DATA_DIRECTORY"
                                audit_file_ingestion failure
                fi

                                #Droping and creating temp table
                                  TEMP_TABLE_DDL="${SCRIPT_HOME}/../hive/create_${FILE_TEMP_DB}_${FILE_NAME}_t.hql"
                                  `${BEELINE_CONNECTION_URL}  -f ${TEMP_TABLE_DDL}`
                 if [ $? -eq 0 ]
                 then
                                 log_print "[INFO:]SUCCESSFULLY CREATED TEMP TABLE = $FILE_TEMP_TABLE "
                 else
                                 log_print "[ERROR:]FAILED TO CREATE TEMP TABLE = $FILE_TEMP_TABLE"
                                                 audit_file_ingestion failure
                 fi


                                create_hdfs_dir ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}

                                #TAKE BACKUP OF THE OLD TABLE DATA.INCASE OF FAILURE BACKUP DATA WILL BE RESTORED IN OLD THE TABLE
                                log_print "[INFO:]TAKING BACKUP OF DATA FOR THE TABLE :-${FILE_REF_OLD_TABLE} "
                               hadoop fs -cp ${FILE_REF_OLD_DATA_DIRECTORY}/* ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}
                                if [ $? -eq 0 ]
                then
                                log_print "[INFO:]SUCCESSFULLY MOVED FILES FROM $FILE_REF_OLD_DATA_DIRECTORY TO $FILE_REF_OLD_DATA_DIRECTORY_BACKUP"
                else
                                log_print "[ERROR:]FAILED TO MOVE FILES FROM $FILE_REF_OLD_DATA_DIRECTORY TO $FILE_REF_OLD_DATA_DIRECTORY_BACKUP"
                                                audit_file_ingestion failure
                fi
                        #For InfoScout : Move the current refine table to refine_old tables.
                           if [ ${SRC_NAME} = "infoscout" ]
                then
                                log_print "[INFO:]MOVING FILES FROM ${FILE_REF_DATA_DIRECTORY} TO ${FILE_REF_OLD_DATA_DIRECTORY}"
                                log_print "[INFO:]OVERWRITING TABLE $FILE_REF_TABLE_OLD : INSERT OVERWRITE TABLE ${FILE_REF_OLD_TABLE} SELECT * FROM ${FILE_REF_TABLE}"
                                `${BEELINE_CONNECTION_URL} -e "INSERT OVERWRITE TABLE ${FILE_REF_OLD_TABLE} SELECT * FROM ${FILE_REF_TABLE}"`
                                if [ $? -eq 0 ]
                                then
                                                log_print "[INFO:]SUCCESSFULLY MOVED FILES FROM $FILE_REF_DATA_DIRECTORY TO $FILE_REF_OLD_DATA_DIRECTORY"
                                else
                                                log_print "[ERROR:]FAILED TO MOVE FILES FROM $FILE_REF_DATA_DIRECTORY TO $FILE_REF_OLD_DATA_DIRECTORY"
                                                                audit_file_ingestion failure
                                fi

                fi


                                #Moving files from ${FILE_TEMP_DATA_DIRECTORY} to ${FILE_REF_DATA_DIRECTORY}
                log_print "[INFO:]MOVING FILES FROM ${FILE_TEMP_DATA_DIRECTORY} TO ${FILE_REF_DATA_DIRECTORY}"
                log_print "[INFO:]OVERWRITING TABLE $FILE_REF_TABLE : INSERT OVERWRITE TABLE ${FILE_REF_TABLE} ${REFINE_TABLE_OVERWRITE_QUERY};"
`${BEELINE_CONNECTION_URL} -e "INSERT OVERWRITE TABLE ${FILE_REF_TABLE} ${REFINE_TABLE_OVERWRITE_QUERY};"`
                if [ $? -eq 0 ]
                then
                        log_print "[INFO:]SUCCESSFULLY MOVED FILES FROM $FILE_TEMP_DATA_DIRECTORY TO $FILE_REF_DATA_DIRECTORY"
                else
                        log_print "[ERROR:]FAILED TO MOVE FILES FROM $FILE_TEMP_DATA_DIRECTORY TO $FILE_REF_DATA_DIRECTORY"
                                hadoop fs -rmr ${FILE_REF_OLD_DATA_DIRECTORY}/*
                                                                hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}/* ${FILE_REF_OLD_DATA_DIRECTORY}
                                                                audit_file_ingestion failure
                fi

                   # Getting record count of files at temp layer table
                TEMP_TABLE_RECORD_COUNT=$(${BEELINE_CONNECTION_URL} -e "select count(*) cnt from $FILE_TEMP_TABLE" | grep -Po "\d+")
                                if [ $? -eq 0 ]
                                        then
                                                        log_print "[INFO:]  HIVE TEMP TABLE RECORD COUNT = $TEMP_TABLE_RECORD_COUNT"

                                        else
                                                        log_print "[ERROR:] FAILED TO GET HIVE TEMP TABLE COUNT"
                                                         hadoop fs -rmr ${FILE_REF_DATA_DIRECTORY}/*
        							                                                                                                                                                                 hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY}/* ${FILE_REF_DATA_DIRECTORY}/
	  						                                                                                                                                                                        hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}/* ${FILE_REF_OLD_DATA_DIRECTORY}/
							audit_file_ingestion failure
                                        fi
                                 # Getting record count of files at refine layer table
                                TRGT_TABLE_RECORD_COUNT=$(${BEELINE_CONNECTION_URL} -e "select count(*) cnt from $FILE_REF_TABLE" | grep -Po "\d+")
                                        if [ $? -eq 0 ]
                                        then
                                                        log_print "[INFO:]  REFINE HIVE TABLE COUNT = $TRGT_TABLE_RECORD_COUNT"
                                                                                                                if [ $TRGT_TABLE_RECORD_COUNT -eq $TEMP_TABLE_RECORD_COUNT ]
                                                                                                                then
                                                                                                                 log_print "[INFO:] REFINE HIVE TABLE COUNT is $TRGT_TABLE_RECORD_COUNT  "
                                                                                                                                                                        log_print "[INFO:] REFINE HIVE TABLE COUNT AND TEMP HIVE TABLE COUNT ARE MATCHED "
                                                                                                                                                                        log_print "[INFO:] FILE = $FILE_NAME INGESTED SUCCESSFULLY"
                                                                                                                                                                       else
                                                                                                                                                                         log_print "[ERROR:] REFINE HIVE TABLE COUNT is $TRGT_TABLE_RECORD_COUNT  "
                                                                                                                                                                         log_print "[ERROR:] TEMP HIVE TABLE COUNT is $TEMP_TABLE_RECORD_COUNT  "
                                                                                                                                                                         log_print "[ERROR:] REFINE HIVE TABLE COUNT AND TEMP HIVE TABLE COUNT ARE NOT MATCHED  "
                                                                                                                                                                         log_print "[ERROR:] FILE = $FILE_NAME INGESTION FAILED"
                                                                                                                                                                         hadoop fs -rmr ${FILE_REF_DATA_DIRECTORY}/*
                                                                                                                                                                         hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY}/* ${FILE_REF_DATA_DIRECTORY}/
                                                                                                                                                                         hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}/* ${FILE_REF_OLD_DATA_DIRECTORY}/
                                                                                                                                                                         audit_file_ingestion failure
                                                                                                                fi
                                        else
                                                        log_print "[ERROR:] FAILED TO GET REFINE HIVE TABLE RECORDS COUNT"
                                                        log_print "[ERROR:] FILE = $FILE_NAME INGESTION FAILED"
                                                                                                                hadoop fs -rmr ${FILE_REF_DATA_DIRECTORY}/*
                                                                                                                hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY}/* ${FILE_REF_DATA_DIRECTORY}/
                                                                                                                hadoop fs -mv ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}/* ${FILE_REF_OLD_DATA_DIRECTORY}/
                                                                                                                audit_file_ingestion failure


                                       fi

done
#Archival process
                log_print "[INFO:] ARCHIVING FILES STARTED "
                archiveFiles ${CSV_FILE_LOCATION} ${FILE_ARCHIVE_DATA_DIRECTORY}
                if [ $? -eq 0 ]
                then
                        log_print "[INFO:]ARCHIVAL PROCESS SUCCESSFULLY COMPLETED"
                else
                        log_print "[ERROR:]FILE ARCHIVAL PROCESS FAILED"
                                audit_file_ingestion failure
                fi


audit_file_ingestion success $TRGT_TABLE_RECORD_COUNT

cat <<EOT >> ${FILE_INGESTION_MAIL_BODY}
<!DOCTYPE html>
<html>
<body text=\" \">
<p>
<pre><b>Hello All ,

PFB STATUS FOR NUMERATOR-FILE INGESTION :-</b>
<b>FILE :</b> ${FILE_NAME}
<b>TABLE :</b> ${FILE_REF_TABLE}
<b>STATUS :</b> SUCCESSFULLY INGESTED</pre>
</p>
</body>
</HTML>
EOT
send_email

log_print "[INFO:] Removal of csv files started "
for CSV_FILE in ${FILE_NAME_LIST}
do
rm -f ${CSV_FILE}
done

log_print "[INFO:] Removal of data from temp directory started"
hadoop fs -rmr ${FILE_TEMP_DATA_DIRECTORY}/*
if [ $? -eq 0 ]
then
log_print "[INFO:]  SUCCESSFULLY REMOVED DATA FROM TEMP DIRECTORY : ${FILE_TEMP_DATA_DIRECTORY}"
else
log_print "[ERROR:] FAILED TO REMOVE DATA FROM TEMP DIRECTORY : ${FILE_TEMP_DATA_DIRECTORY}"
fi
hadoop fs -rmr ${FILE_REF_OLD_DATA_DIRECTORY_BACKUP}/*
if [ $? -eq 0 ]
then
log_print "[INFO:]  SUCCESSFULLY REMOVED DATA FROM TEMP DIRECTORY : ${FILE_TEMP_DATA_DIRECTORY}"
else
log_print "[ERROR:] FAILED TO REMOVE DATA FROM TEMP DIRECTORY : ${FILE_TEMP_DATA_DIRECTORY}"
fi

END_TIME_UTC=$(date +%s)
log_print "[INFO :] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO :] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO :] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
log_print "[INFO :] Job Completed successfully."

} >> "${CEDL_BASE_PATH}/log/${LOG_DATE}/${FILE_JOB_NAME}_${DIR_TIME}.log" 2>&1;

