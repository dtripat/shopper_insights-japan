// ORM class for table 'site_t'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed Dec 19 12:42:31 UTC 2018
// For connector: org.apache.sqoop.manager.DirectNetezzaManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import org.apache.sqoop.lib.JdbcWritableBridge;
import org.apache.sqoop.lib.DelimiterSet;
import org.apache.sqoop.lib.FieldFormatter;
import org.apache.sqoop.lib.RecordParser;
import org.apache.sqoop.lib.BooleanParser;
import org.apache.sqoop.lib.BlobRef;
import org.apache.sqoop.lib.ClobRef;
import org.apache.sqoop.lib.LargeObjectLoader;
import org.apache.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class site_t extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  public static interface FieldSetterCommand {    void setField(Object value);  }  protected ResultSet __cur_result_set;
  private Map<String, FieldSetterCommand> setters = new HashMap<String, FieldSetterCommand>();
  private void init0() {
    setters.put("site_key", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_key = (Long)value;
      }
    });
    setters.put("site_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_nm = (String)value;
      }
    });
    setters.put("site_local_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_local_nm = (String)value;
      }
    });
    setters.put("site_id_txt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_id_txt = (String)value;
      }
    });
    setters.put("site_src_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_src_id = (Integer)value;
      }
    });
    setters.put("site_data_src_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_data_src_cd = (String)value;
      }
    });
    setters.put("site_active_dt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_active_dt = (java.sql.Date)value;
      }
    });
    setters.put("site_inactive_dt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_inactive_dt = (java.sql.Date)value;
      }
    });
    setters.put("site_first_log_seen_dt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_first_log_seen_dt = (java.sql.Date)value;
      }
    });
    setters.put("site_last_log_seen_dt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_last_log_seen_dt = (java.sql.Date)value;
      }
    });
    setters.put("site_typ_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_typ_cd = (String)value;
      }
    });
    setters.put("site_typ_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_typ_nm = (String)value;
      }
    });
    setters.put("site_trade_class_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_trade_class_cd = (String)value;
      }
    });
    setters.put("site_trade_class_desc", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.site_trade_class_desc = (String)value;
      }
    });
    setters.put("lgl_entity_key", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.lgl_entity_key = (Long)value;
      }
    });
    setters.put("lgl_entity_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.lgl_entity_nbr = (Integer)value;
      }
    });
    setters.put("lgl_entity_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.lgl_entity_nm = (String)value;
      }
    });
    setters.put("lgl_entity_sub_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.lgl_entity_sub_id = (Integer)value;
      }
    });
    setters.put("lgl_entity_sub_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.lgl_entity_sub_nm = (String)value;
      }
    });
    setters.put("ntwk_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_id = (Integer)value;
      }
    });
    setters.put("ntwk_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_nm = (String)value;
      }
    });
    setters.put("ntwk_local_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_local_nm = (String)value;
      }
    });
    setters.put("ntwk_purpose_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_purpose_cd = (String)value;
      }
    });
    setters.put("ntwk_purpose_desc", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_purpose_desc = (String)value;
      }
    });
    setters.put("ntwk_seg_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_seg_cd = (String)value;
      }
    });
    setters.put("ntwk_seg_desc", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_seg_desc = (String)value;
      }
    });
    setters.put("ntwk_credit_card_masking_ind", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_credit_card_masking_ind = (String)value;
      }
    });
    setters.put("ntwk_catalina_trade_class_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_catalina_trade_class_cd = (String)value;
      }
    });
    setters.put("ntwk_catalina_trade_class_desc", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.ntwk_catalina_trade_class_desc = (String)value;
      }
    });
    setters.put("addr_ln_1_txt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.addr_ln_1_txt = (String)value;
      }
    });
    setters.put("addr_ln_2_txt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.addr_ln_2_txt = (String)value;
      }
    });
    setters.put("city_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.city_nm = (String)value;
      }
    });
    setters.put("postal_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.postal_cd = (String)value;
      }
    });
    setters.put("postal_cd_suffix_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.postal_cd_suffix_cd = (String)value;
      }
    });
    setters.put("cntry_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.cntry_cd = (String)value;
      }
    });
    setters.put("cntry_subdiv_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.cntry_subdiv_cd = (String)value;
      }
    });
    setters.put("cntry_subdiv_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.cntry_subdiv_nm = (String)value;
      }
    });
    setters.put("lat_coord", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.lat_coord = (java.math.BigDecimal)value;
      }
    });
    setters.put("long_coord", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.long_coord = (java.math.BigDecimal)value;
      }
    });
    setters.put("str_phone_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.str_phone_nbr = (Long)value;
      }
    });
    setters.put("mkt_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.mkt_nbr = (Integer)value;
      }
    });
    setters.put("mkt_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.mkt_nm = (String)value;
      }
    });
    setters.put("rgn_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.rgn_nm = (String)value;
      }
    });
    setters.put("rgn_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.rgn_nbr = (Integer)value;
      }
    });
    setters.put("district_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.district_nbr = (Integer)value;
      }
    });
    setters.put("mi_natl_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.mi_natl_nbr = (Integer)value;
      }
    });
    setters.put("unique_site_tkn_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.unique_site_tkn_nbr = (Long)value;
      }
    });
    setters.put("csdb_chn_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.csdb_chn_nbr = (Integer)value;
      }
    });
    setters.put("csdb_chn_str_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.csdb_chn_str_nbr = (Integer)value;
      }
    });
    setters.put("csdb_str_fips_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.csdb_str_fips_nbr = (Integer)value;
      }
    });
    setters.put("cdsb_td_mkt_dma_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.cdsb_td_mkt_dma_cd = (String)value;
      }
    });
    setters.put("csdb_td_mkt_dma_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.csdb_td_mkt_dma_nm = (String)value;
      }
    });
    setters.put("csdb_td_mkt_msa_mkt_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.csdb_td_mkt_msa_mkt_cd = (String)value;
      }
    });
    setters.put("csdb_td_mkt_msa_mkt_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.csdb_td_mkt_msa_mkt_nm = (String)value;
      }
    });
    setters.put("orig_lgl_entity_key", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_lgl_entity_key = (Long)value;
      }
    });
    setters.put("orig_lgl_entity_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_lgl_entity_nbr = (Integer)value;
      }
    });
    setters.put("orig_lgl_entity_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_lgl_entity_nm = (String)value;
      }
    });
    setters.put("orig_lgl_entity_sub_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_lgl_entity_sub_id = (Integer)value;
      }
    });
    setters.put("orig_lgl_entity_sub_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_lgl_entity_sub_nm = (String)value;
      }
    });
    setters.put("orig_ntwk_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_ntwk_id = (Integer)value;
      }
    });
    setters.put("orig_ntwk_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_ntwk_nm = (String)value;
      }
    });
    setters.put("orig_site_id_txt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_site_id_txt = (String)value;
      }
    });
    setters.put("orig_site_nm", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_site_nm = (String)value;
      }
    });
    setters.put("orig_csdb_chn_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_csdb_chn_nbr = (Integer)value;
      }
    });
    setters.put("orig_csdb_chn_str_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_csdb_chn_str_nbr = (Integer)value;
      }
    });
    setters.put("orig_site_active_dt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_site_active_dt = (java.sql.Date)value;
      }
    });
    setters.put("orig_site_inactive_dt", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.orig_site_inactive_dt = (java.sql.Date)value;
      }
    });
    setters.put("utc_offset_hh_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.utc_offset_hh_nbr = (java.math.BigDecimal)value;
      }
    });
    setters.put("utc_dst_offset_hh_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.utc_dst_offset_hh_nbr = (java.math.BigDecimal)value;
      }
    });
    setters.put("mngng_cntry_cd", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.mngng_cntry_cd = (String)value;
      }
    });
    setters.put("batch_ld_nbr", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        site_t.this.batch_ld_nbr = (Integer)value;
      }
    });
  }
  public site_t() {
    init0();
  }
  private Long site_key;
  public Long get_site_key() {
    return site_key;
  }
  public void set_site_key(Long site_key) {
    this.site_key = site_key;
  }
  public site_t with_site_key(Long site_key) {
    this.site_key = site_key;
    return this;
  }
  private String site_nm;
  public String get_site_nm() {
    return site_nm;
  }
  public void set_site_nm(String site_nm) {
    this.site_nm = site_nm;
  }
  public site_t with_site_nm(String site_nm) {
    this.site_nm = site_nm;
    return this;
  }
  private String site_local_nm;
  public String get_site_local_nm() {
    return site_local_nm;
  }
  public void set_site_local_nm(String site_local_nm) {
    this.site_local_nm = site_local_nm;
  }
  public site_t with_site_local_nm(String site_local_nm) {
    this.site_local_nm = site_local_nm;
    return this;
  }
  private String site_id_txt;
  public String get_site_id_txt() {
    return site_id_txt;
  }
  public void set_site_id_txt(String site_id_txt) {
    this.site_id_txt = site_id_txt;
  }
  public site_t with_site_id_txt(String site_id_txt) {
    this.site_id_txt = site_id_txt;
    return this;
  }
  private Integer site_src_id;
  public Integer get_site_src_id() {
    return site_src_id;
  }
  public void set_site_src_id(Integer site_src_id) {
    this.site_src_id = site_src_id;
  }
  public site_t with_site_src_id(Integer site_src_id) {
    this.site_src_id = site_src_id;
    return this;
  }
  private String site_data_src_cd;
  public String get_site_data_src_cd() {
    return site_data_src_cd;
  }
  public void set_site_data_src_cd(String site_data_src_cd) {
    this.site_data_src_cd = site_data_src_cd;
  }
  public site_t with_site_data_src_cd(String site_data_src_cd) {
    this.site_data_src_cd = site_data_src_cd;
    return this;
  }
  private java.sql.Date site_active_dt;
  public java.sql.Date get_site_active_dt() {
    return site_active_dt;
  }
  public void set_site_active_dt(java.sql.Date site_active_dt) {
    this.site_active_dt = site_active_dt;
  }
  public site_t with_site_active_dt(java.sql.Date site_active_dt) {
    this.site_active_dt = site_active_dt;
    return this;
  }
  private java.sql.Date site_inactive_dt;
  public java.sql.Date get_site_inactive_dt() {
    return site_inactive_dt;
  }
  public void set_site_inactive_dt(java.sql.Date site_inactive_dt) {
    this.site_inactive_dt = site_inactive_dt;
  }
  public site_t with_site_inactive_dt(java.sql.Date site_inactive_dt) {
    this.site_inactive_dt = site_inactive_dt;
    return this;
  }
  private java.sql.Date site_first_log_seen_dt;
  public java.sql.Date get_site_first_log_seen_dt() {
    return site_first_log_seen_dt;
  }
  public void set_site_first_log_seen_dt(java.sql.Date site_first_log_seen_dt) {
    this.site_first_log_seen_dt = site_first_log_seen_dt;
  }
  public site_t with_site_first_log_seen_dt(java.sql.Date site_first_log_seen_dt) {
    this.site_first_log_seen_dt = site_first_log_seen_dt;
    return this;
  }
  private java.sql.Date site_last_log_seen_dt;
  public java.sql.Date get_site_last_log_seen_dt() {
    return site_last_log_seen_dt;
  }
  public void set_site_last_log_seen_dt(java.sql.Date site_last_log_seen_dt) {
    this.site_last_log_seen_dt = site_last_log_seen_dt;
  }
  public site_t with_site_last_log_seen_dt(java.sql.Date site_last_log_seen_dt) {
    this.site_last_log_seen_dt = site_last_log_seen_dt;
    return this;
  }
  private String site_typ_cd;
  public String get_site_typ_cd() {
    return site_typ_cd;
  }
  public void set_site_typ_cd(String site_typ_cd) {
    this.site_typ_cd = site_typ_cd;
  }
  public site_t with_site_typ_cd(String site_typ_cd) {
    this.site_typ_cd = site_typ_cd;
    return this;
  }
  private String site_typ_nm;
  public String get_site_typ_nm() {
    return site_typ_nm;
  }
  public void set_site_typ_nm(String site_typ_nm) {
    this.site_typ_nm = site_typ_nm;
  }
  public site_t with_site_typ_nm(String site_typ_nm) {
    this.site_typ_nm = site_typ_nm;
    return this;
  }
  private String site_trade_class_cd;
  public String get_site_trade_class_cd() {
    return site_trade_class_cd;
  }
  public void set_site_trade_class_cd(String site_trade_class_cd) {
    this.site_trade_class_cd = site_trade_class_cd;
  }
  public site_t with_site_trade_class_cd(String site_trade_class_cd) {
    this.site_trade_class_cd = site_trade_class_cd;
    return this;
  }
  private String site_trade_class_desc;
  public String get_site_trade_class_desc() {
    return site_trade_class_desc;
  }
  public void set_site_trade_class_desc(String site_trade_class_desc) {
    this.site_trade_class_desc = site_trade_class_desc;
  }
  public site_t with_site_trade_class_desc(String site_trade_class_desc) {
    this.site_trade_class_desc = site_trade_class_desc;
    return this;
  }
  private Long lgl_entity_key;
  public Long get_lgl_entity_key() {
    return lgl_entity_key;
  }
  public void set_lgl_entity_key(Long lgl_entity_key) {
    this.lgl_entity_key = lgl_entity_key;
  }
  public site_t with_lgl_entity_key(Long lgl_entity_key) {
    this.lgl_entity_key = lgl_entity_key;
    return this;
  }
  private Integer lgl_entity_nbr;
  public Integer get_lgl_entity_nbr() {
    return lgl_entity_nbr;
  }
  public void set_lgl_entity_nbr(Integer lgl_entity_nbr) {
    this.lgl_entity_nbr = lgl_entity_nbr;
  }
  public site_t with_lgl_entity_nbr(Integer lgl_entity_nbr) {
    this.lgl_entity_nbr = lgl_entity_nbr;
    return this;
  }
  private String lgl_entity_nm;
  public String get_lgl_entity_nm() {
    return lgl_entity_nm;
  }
  public void set_lgl_entity_nm(String lgl_entity_nm) {
    this.lgl_entity_nm = lgl_entity_nm;
  }
  public site_t with_lgl_entity_nm(String lgl_entity_nm) {
    this.lgl_entity_nm = lgl_entity_nm;
    return this;
  }
  private Integer lgl_entity_sub_id;
  public Integer get_lgl_entity_sub_id() {
    return lgl_entity_sub_id;
  }
  public void set_lgl_entity_sub_id(Integer lgl_entity_sub_id) {
    this.lgl_entity_sub_id = lgl_entity_sub_id;
  }
  public site_t with_lgl_entity_sub_id(Integer lgl_entity_sub_id) {
    this.lgl_entity_sub_id = lgl_entity_sub_id;
    return this;
  }
  private String lgl_entity_sub_nm;
  public String get_lgl_entity_sub_nm() {
    return lgl_entity_sub_nm;
  }
  public void set_lgl_entity_sub_nm(String lgl_entity_sub_nm) {
    this.lgl_entity_sub_nm = lgl_entity_sub_nm;
  }
  public site_t with_lgl_entity_sub_nm(String lgl_entity_sub_nm) {
    this.lgl_entity_sub_nm = lgl_entity_sub_nm;
    return this;
  }
  private Integer ntwk_id;
  public Integer get_ntwk_id() {
    return ntwk_id;
  }
  public void set_ntwk_id(Integer ntwk_id) {
    this.ntwk_id = ntwk_id;
  }
  public site_t with_ntwk_id(Integer ntwk_id) {
    this.ntwk_id = ntwk_id;
    return this;
  }
  private String ntwk_nm;
  public String get_ntwk_nm() {
    return ntwk_nm;
  }
  public void set_ntwk_nm(String ntwk_nm) {
    this.ntwk_nm = ntwk_nm;
  }
  public site_t with_ntwk_nm(String ntwk_nm) {
    this.ntwk_nm = ntwk_nm;
    return this;
  }
  private String ntwk_local_nm;
  public String get_ntwk_local_nm() {
    return ntwk_local_nm;
  }
  public void set_ntwk_local_nm(String ntwk_local_nm) {
    this.ntwk_local_nm = ntwk_local_nm;
  }
  public site_t with_ntwk_local_nm(String ntwk_local_nm) {
    this.ntwk_local_nm = ntwk_local_nm;
    return this;
  }
  private String ntwk_purpose_cd;
  public String get_ntwk_purpose_cd() {
    return ntwk_purpose_cd;
  }
  public void set_ntwk_purpose_cd(String ntwk_purpose_cd) {
    this.ntwk_purpose_cd = ntwk_purpose_cd;
  }
  public site_t with_ntwk_purpose_cd(String ntwk_purpose_cd) {
    this.ntwk_purpose_cd = ntwk_purpose_cd;
    return this;
  }
  private String ntwk_purpose_desc;
  public String get_ntwk_purpose_desc() {
    return ntwk_purpose_desc;
  }
  public void set_ntwk_purpose_desc(String ntwk_purpose_desc) {
    this.ntwk_purpose_desc = ntwk_purpose_desc;
  }
  public site_t with_ntwk_purpose_desc(String ntwk_purpose_desc) {
    this.ntwk_purpose_desc = ntwk_purpose_desc;
    return this;
  }
  private String ntwk_seg_cd;
  public String get_ntwk_seg_cd() {
    return ntwk_seg_cd;
  }
  public void set_ntwk_seg_cd(String ntwk_seg_cd) {
    this.ntwk_seg_cd = ntwk_seg_cd;
  }
  public site_t with_ntwk_seg_cd(String ntwk_seg_cd) {
    this.ntwk_seg_cd = ntwk_seg_cd;
    return this;
  }
  private String ntwk_seg_desc;
  public String get_ntwk_seg_desc() {
    return ntwk_seg_desc;
  }
  public void set_ntwk_seg_desc(String ntwk_seg_desc) {
    this.ntwk_seg_desc = ntwk_seg_desc;
  }
  public site_t with_ntwk_seg_desc(String ntwk_seg_desc) {
    this.ntwk_seg_desc = ntwk_seg_desc;
    return this;
  }
  private String ntwk_credit_card_masking_ind;
  public String get_ntwk_credit_card_masking_ind() {
    return ntwk_credit_card_masking_ind;
  }
  public void set_ntwk_credit_card_masking_ind(String ntwk_credit_card_masking_ind) {
    this.ntwk_credit_card_masking_ind = ntwk_credit_card_masking_ind;
  }
  public site_t with_ntwk_credit_card_masking_ind(String ntwk_credit_card_masking_ind) {
    this.ntwk_credit_card_masking_ind = ntwk_credit_card_masking_ind;
    return this;
  }
  private String ntwk_catalina_trade_class_cd;
  public String get_ntwk_catalina_trade_class_cd() {
    return ntwk_catalina_trade_class_cd;
  }
  public void set_ntwk_catalina_trade_class_cd(String ntwk_catalina_trade_class_cd) {
    this.ntwk_catalina_trade_class_cd = ntwk_catalina_trade_class_cd;
  }
  public site_t with_ntwk_catalina_trade_class_cd(String ntwk_catalina_trade_class_cd) {
    this.ntwk_catalina_trade_class_cd = ntwk_catalina_trade_class_cd;
    return this;
  }
  private String ntwk_catalina_trade_class_desc;
  public String get_ntwk_catalina_trade_class_desc() {
    return ntwk_catalina_trade_class_desc;
  }
  public void set_ntwk_catalina_trade_class_desc(String ntwk_catalina_trade_class_desc) {
    this.ntwk_catalina_trade_class_desc = ntwk_catalina_trade_class_desc;
  }
  public site_t with_ntwk_catalina_trade_class_desc(String ntwk_catalina_trade_class_desc) {
    this.ntwk_catalina_trade_class_desc = ntwk_catalina_trade_class_desc;
    return this;
  }
  private String addr_ln_1_txt;
  public String get_addr_ln_1_txt() {
    return addr_ln_1_txt;
  }
  public void set_addr_ln_1_txt(String addr_ln_1_txt) {
    this.addr_ln_1_txt = addr_ln_1_txt;
  }
  public site_t with_addr_ln_1_txt(String addr_ln_1_txt) {
    this.addr_ln_1_txt = addr_ln_1_txt;
    return this;
  }
  private String addr_ln_2_txt;
  public String get_addr_ln_2_txt() {
    return addr_ln_2_txt;
  }
  public void set_addr_ln_2_txt(String addr_ln_2_txt) {
    this.addr_ln_2_txt = addr_ln_2_txt;
  }
  public site_t with_addr_ln_2_txt(String addr_ln_2_txt) {
    this.addr_ln_2_txt = addr_ln_2_txt;
    return this;
  }
  private String city_nm;
  public String get_city_nm() {
    return city_nm;
  }
  public void set_city_nm(String city_nm) {
    this.city_nm = city_nm;
  }
  public site_t with_city_nm(String city_nm) {
    this.city_nm = city_nm;
    return this;
  }
  private String postal_cd;
  public String get_postal_cd() {
    return postal_cd;
  }
  public void set_postal_cd(String postal_cd) {
    this.postal_cd = postal_cd;
  }
  public site_t with_postal_cd(String postal_cd) {
    this.postal_cd = postal_cd;
    return this;
  }
  private String postal_cd_suffix_cd;
  public String get_postal_cd_suffix_cd() {
    return postal_cd_suffix_cd;
  }
  public void set_postal_cd_suffix_cd(String postal_cd_suffix_cd) {
    this.postal_cd_suffix_cd = postal_cd_suffix_cd;
  }
  public site_t with_postal_cd_suffix_cd(String postal_cd_suffix_cd) {
    this.postal_cd_suffix_cd = postal_cd_suffix_cd;
    return this;
  }
  private String cntry_cd;
  public String get_cntry_cd() {
    return cntry_cd;
  }
  public void set_cntry_cd(String cntry_cd) {
    this.cntry_cd = cntry_cd;
  }
  public site_t with_cntry_cd(String cntry_cd) {
    this.cntry_cd = cntry_cd;
    return this;
  }
  private String cntry_subdiv_cd;
  public String get_cntry_subdiv_cd() {
    return cntry_subdiv_cd;
  }
  public void set_cntry_subdiv_cd(String cntry_subdiv_cd) {
    this.cntry_subdiv_cd = cntry_subdiv_cd;
  }
  public site_t with_cntry_subdiv_cd(String cntry_subdiv_cd) {
    this.cntry_subdiv_cd = cntry_subdiv_cd;
    return this;
  }
  private String cntry_subdiv_nm;
  public String get_cntry_subdiv_nm() {
    return cntry_subdiv_nm;
  }
  public void set_cntry_subdiv_nm(String cntry_subdiv_nm) {
    this.cntry_subdiv_nm = cntry_subdiv_nm;
  }
  public site_t with_cntry_subdiv_nm(String cntry_subdiv_nm) {
    this.cntry_subdiv_nm = cntry_subdiv_nm;
    return this;
  }
  private java.math.BigDecimal lat_coord;
  public java.math.BigDecimal get_lat_coord() {
    return lat_coord;
  }
  public void set_lat_coord(java.math.BigDecimal lat_coord) {
    this.lat_coord = lat_coord;
  }
  public site_t with_lat_coord(java.math.BigDecimal lat_coord) {
    this.lat_coord = lat_coord;
    return this;
  }
  private java.math.BigDecimal long_coord;
  public java.math.BigDecimal get_long_coord() {
    return long_coord;
  }
  public void set_long_coord(java.math.BigDecimal long_coord) {
    this.long_coord = long_coord;
  }
  public site_t with_long_coord(java.math.BigDecimal long_coord) {
    this.long_coord = long_coord;
    return this;
  }
  private Long str_phone_nbr;
  public Long get_str_phone_nbr() {
    return str_phone_nbr;
  }
  public void set_str_phone_nbr(Long str_phone_nbr) {
    this.str_phone_nbr = str_phone_nbr;
  }
  public site_t with_str_phone_nbr(Long str_phone_nbr) {
    this.str_phone_nbr = str_phone_nbr;
    return this;
  }
  private Integer mkt_nbr;
  public Integer get_mkt_nbr() {
    return mkt_nbr;
  }
  public void set_mkt_nbr(Integer mkt_nbr) {
    this.mkt_nbr = mkt_nbr;
  }
  public site_t with_mkt_nbr(Integer mkt_nbr) {
    this.mkt_nbr = mkt_nbr;
    return this;
  }
  private String mkt_nm;
  public String get_mkt_nm() {
    return mkt_nm;
  }
  public void set_mkt_nm(String mkt_nm) {
    this.mkt_nm = mkt_nm;
  }
  public site_t with_mkt_nm(String mkt_nm) {
    this.mkt_nm = mkt_nm;
    return this;
  }
  private String rgn_nm;
  public String get_rgn_nm() {
    return rgn_nm;
  }
  public void set_rgn_nm(String rgn_nm) {
    this.rgn_nm = rgn_nm;
  }
  public site_t with_rgn_nm(String rgn_nm) {
    this.rgn_nm = rgn_nm;
    return this;
  }
  private Integer rgn_nbr;
  public Integer get_rgn_nbr() {
    return rgn_nbr;
  }
  public void set_rgn_nbr(Integer rgn_nbr) {
    this.rgn_nbr = rgn_nbr;
  }
  public site_t with_rgn_nbr(Integer rgn_nbr) {
    this.rgn_nbr = rgn_nbr;
    return this;
  }
  private Integer district_nbr;
  public Integer get_district_nbr() {
    return district_nbr;
  }
  public void set_district_nbr(Integer district_nbr) {
    this.district_nbr = district_nbr;
  }
  public site_t with_district_nbr(Integer district_nbr) {
    this.district_nbr = district_nbr;
    return this;
  }
  private Integer mi_natl_nbr;
  public Integer get_mi_natl_nbr() {
    return mi_natl_nbr;
  }
  public void set_mi_natl_nbr(Integer mi_natl_nbr) {
    this.mi_natl_nbr = mi_natl_nbr;
  }
  public site_t with_mi_natl_nbr(Integer mi_natl_nbr) {
    this.mi_natl_nbr = mi_natl_nbr;
    return this;
  }
  private Long unique_site_tkn_nbr;
  public Long get_unique_site_tkn_nbr() {
    return unique_site_tkn_nbr;
  }
  public void set_unique_site_tkn_nbr(Long unique_site_tkn_nbr) {
    this.unique_site_tkn_nbr = unique_site_tkn_nbr;
  }
  public site_t with_unique_site_tkn_nbr(Long unique_site_tkn_nbr) {
    this.unique_site_tkn_nbr = unique_site_tkn_nbr;
    return this;
  }
  private Integer csdb_chn_nbr;
  public Integer get_csdb_chn_nbr() {
    return csdb_chn_nbr;
  }
  public void set_csdb_chn_nbr(Integer csdb_chn_nbr) {
    this.csdb_chn_nbr = csdb_chn_nbr;
  }
  public site_t with_csdb_chn_nbr(Integer csdb_chn_nbr) {
    this.csdb_chn_nbr = csdb_chn_nbr;
    return this;
  }
  private Integer csdb_chn_str_nbr;
  public Integer get_csdb_chn_str_nbr() {
    return csdb_chn_str_nbr;
  }
  public void set_csdb_chn_str_nbr(Integer csdb_chn_str_nbr) {
    this.csdb_chn_str_nbr = csdb_chn_str_nbr;
  }
  public site_t with_csdb_chn_str_nbr(Integer csdb_chn_str_nbr) {
    this.csdb_chn_str_nbr = csdb_chn_str_nbr;
    return this;
  }
  private Integer csdb_str_fips_nbr;
  public Integer get_csdb_str_fips_nbr() {
    return csdb_str_fips_nbr;
  }
  public void set_csdb_str_fips_nbr(Integer csdb_str_fips_nbr) {
    this.csdb_str_fips_nbr = csdb_str_fips_nbr;
  }
  public site_t with_csdb_str_fips_nbr(Integer csdb_str_fips_nbr) {
    this.csdb_str_fips_nbr = csdb_str_fips_nbr;
    return this;
  }
  private String cdsb_td_mkt_dma_cd;
  public String get_cdsb_td_mkt_dma_cd() {
    return cdsb_td_mkt_dma_cd;
  }
  public void set_cdsb_td_mkt_dma_cd(String cdsb_td_mkt_dma_cd) {
    this.cdsb_td_mkt_dma_cd = cdsb_td_mkt_dma_cd;
  }
  public site_t with_cdsb_td_mkt_dma_cd(String cdsb_td_mkt_dma_cd) {
    this.cdsb_td_mkt_dma_cd = cdsb_td_mkt_dma_cd;
    return this;
  }
  private String csdb_td_mkt_dma_nm;
  public String get_csdb_td_mkt_dma_nm() {
    return csdb_td_mkt_dma_nm;
  }
  public void set_csdb_td_mkt_dma_nm(String csdb_td_mkt_dma_nm) {
    this.csdb_td_mkt_dma_nm = csdb_td_mkt_dma_nm;
  }
  public site_t with_csdb_td_mkt_dma_nm(String csdb_td_mkt_dma_nm) {
    this.csdb_td_mkt_dma_nm = csdb_td_mkt_dma_nm;
    return this;
  }
  private String csdb_td_mkt_msa_mkt_cd;
  public String get_csdb_td_mkt_msa_mkt_cd() {
    return csdb_td_mkt_msa_mkt_cd;
  }
  public void set_csdb_td_mkt_msa_mkt_cd(String csdb_td_mkt_msa_mkt_cd) {
    this.csdb_td_mkt_msa_mkt_cd = csdb_td_mkt_msa_mkt_cd;
  }
  public site_t with_csdb_td_mkt_msa_mkt_cd(String csdb_td_mkt_msa_mkt_cd) {
    this.csdb_td_mkt_msa_mkt_cd = csdb_td_mkt_msa_mkt_cd;
    return this;
  }
  private String csdb_td_mkt_msa_mkt_nm;
  public String get_csdb_td_mkt_msa_mkt_nm() {
    return csdb_td_mkt_msa_mkt_nm;
  }
  public void set_csdb_td_mkt_msa_mkt_nm(String csdb_td_mkt_msa_mkt_nm) {
    this.csdb_td_mkt_msa_mkt_nm = csdb_td_mkt_msa_mkt_nm;
  }
  public site_t with_csdb_td_mkt_msa_mkt_nm(String csdb_td_mkt_msa_mkt_nm) {
    this.csdb_td_mkt_msa_mkt_nm = csdb_td_mkt_msa_mkt_nm;
    return this;
  }
  private Long orig_lgl_entity_key;
  public Long get_orig_lgl_entity_key() {
    return orig_lgl_entity_key;
  }
  public void set_orig_lgl_entity_key(Long orig_lgl_entity_key) {
    this.orig_lgl_entity_key = orig_lgl_entity_key;
  }
  public site_t with_orig_lgl_entity_key(Long orig_lgl_entity_key) {
    this.orig_lgl_entity_key = orig_lgl_entity_key;
    return this;
  }
  private Integer orig_lgl_entity_nbr;
  public Integer get_orig_lgl_entity_nbr() {
    return orig_lgl_entity_nbr;
  }
  public void set_orig_lgl_entity_nbr(Integer orig_lgl_entity_nbr) {
    this.orig_lgl_entity_nbr = orig_lgl_entity_nbr;
  }
  public site_t with_orig_lgl_entity_nbr(Integer orig_lgl_entity_nbr) {
    this.orig_lgl_entity_nbr = orig_lgl_entity_nbr;
    return this;
  }
  private String orig_lgl_entity_nm;
  public String get_orig_lgl_entity_nm() {
    return orig_lgl_entity_nm;
  }
  public void set_orig_lgl_entity_nm(String orig_lgl_entity_nm) {
    this.orig_lgl_entity_nm = orig_lgl_entity_nm;
  }
  public site_t with_orig_lgl_entity_nm(String orig_lgl_entity_nm) {
    this.orig_lgl_entity_nm = orig_lgl_entity_nm;
    return this;
  }
  private Integer orig_lgl_entity_sub_id;
  public Integer get_orig_lgl_entity_sub_id() {
    return orig_lgl_entity_sub_id;
  }
  public void set_orig_lgl_entity_sub_id(Integer orig_lgl_entity_sub_id) {
    this.orig_lgl_entity_sub_id = orig_lgl_entity_sub_id;
  }
  public site_t with_orig_lgl_entity_sub_id(Integer orig_lgl_entity_sub_id) {
    this.orig_lgl_entity_sub_id = orig_lgl_entity_sub_id;
    return this;
  }
  private String orig_lgl_entity_sub_nm;
  public String get_orig_lgl_entity_sub_nm() {
    return orig_lgl_entity_sub_nm;
  }
  public void set_orig_lgl_entity_sub_nm(String orig_lgl_entity_sub_nm) {
    this.orig_lgl_entity_sub_nm = orig_lgl_entity_sub_nm;
  }
  public site_t with_orig_lgl_entity_sub_nm(String orig_lgl_entity_sub_nm) {
    this.orig_lgl_entity_sub_nm = orig_lgl_entity_sub_nm;
    return this;
  }
  private Integer orig_ntwk_id;
  public Integer get_orig_ntwk_id() {
    return orig_ntwk_id;
  }
  public void set_orig_ntwk_id(Integer orig_ntwk_id) {
    this.orig_ntwk_id = orig_ntwk_id;
  }
  public site_t with_orig_ntwk_id(Integer orig_ntwk_id) {
    this.orig_ntwk_id = orig_ntwk_id;
    return this;
  }
  private String orig_ntwk_nm;
  public String get_orig_ntwk_nm() {
    return orig_ntwk_nm;
  }
  public void set_orig_ntwk_nm(String orig_ntwk_nm) {
    this.orig_ntwk_nm = orig_ntwk_nm;
  }
  public site_t with_orig_ntwk_nm(String orig_ntwk_nm) {
    this.orig_ntwk_nm = orig_ntwk_nm;
    return this;
  }
  private String orig_site_id_txt;
  public String get_orig_site_id_txt() {
    return orig_site_id_txt;
  }
  public void set_orig_site_id_txt(String orig_site_id_txt) {
    this.orig_site_id_txt = orig_site_id_txt;
  }
  public site_t with_orig_site_id_txt(String orig_site_id_txt) {
    this.orig_site_id_txt = orig_site_id_txt;
    return this;
  }
  private String orig_site_nm;
  public String get_orig_site_nm() {
    return orig_site_nm;
  }
  public void set_orig_site_nm(String orig_site_nm) {
    this.orig_site_nm = orig_site_nm;
  }
  public site_t with_orig_site_nm(String orig_site_nm) {
    this.orig_site_nm = orig_site_nm;
    return this;
  }
  private Integer orig_csdb_chn_nbr;
  public Integer get_orig_csdb_chn_nbr() {
    return orig_csdb_chn_nbr;
  }
  public void set_orig_csdb_chn_nbr(Integer orig_csdb_chn_nbr) {
    this.orig_csdb_chn_nbr = orig_csdb_chn_nbr;
  }
  public site_t with_orig_csdb_chn_nbr(Integer orig_csdb_chn_nbr) {
    this.orig_csdb_chn_nbr = orig_csdb_chn_nbr;
    return this;
  }
  private Integer orig_csdb_chn_str_nbr;
  public Integer get_orig_csdb_chn_str_nbr() {
    return orig_csdb_chn_str_nbr;
  }
  public void set_orig_csdb_chn_str_nbr(Integer orig_csdb_chn_str_nbr) {
    this.orig_csdb_chn_str_nbr = orig_csdb_chn_str_nbr;
  }
  public site_t with_orig_csdb_chn_str_nbr(Integer orig_csdb_chn_str_nbr) {
    this.orig_csdb_chn_str_nbr = orig_csdb_chn_str_nbr;
    return this;
  }
  private java.sql.Date orig_site_active_dt;
  public java.sql.Date get_orig_site_active_dt() {
    return orig_site_active_dt;
  }
  public void set_orig_site_active_dt(java.sql.Date orig_site_active_dt) {
    this.orig_site_active_dt = orig_site_active_dt;
  }
  public site_t with_orig_site_active_dt(java.sql.Date orig_site_active_dt) {
    this.orig_site_active_dt = orig_site_active_dt;
    return this;
  }
  private java.sql.Date orig_site_inactive_dt;
  public java.sql.Date get_orig_site_inactive_dt() {
    return orig_site_inactive_dt;
  }
  public void set_orig_site_inactive_dt(java.sql.Date orig_site_inactive_dt) {
    this.orig_site_inactive_dt = orig_site_inactive_dt;
  }
  public site_t with_orig_site_inactive_dt(java.sql.Date orig_site_inactive_dt) {
    this.orig_site_inactive_dt = orig_site_inactive_dt;
    return this;
  }
  private java.math.BigDecimal utc_offset_hh_nbr;
  public java.math.BigDecimal get_utc_offset_hh_nbr() {
    return utc_offset_hh_nbr;
  }
  public void set_utc_offset_hh_nbr(java.math.BigDecimal utc_offset_hh_nbr) {
    this.utc_offset_hh_nbr = utc_offset_hh_nbr;
  }
  public site_t with_utc_offset_hh_nbr(java.math.BigDecimal utc_offset_hh_nbr) {
    this.utc_offset_hh_nbr = utc_offset_hh_nbr;
    return this;
  }
  private java.math.BigDecimal utc_dst_offset_hh_nbr;
  public java.math.BigDecimal get_utc_dst_offset_hh_nbr() {
    return utc_dst_offset_hh_nbr;
  }
  public void set_utc_dst_offset_hh_nbr(java.math.BigDecimal utc_dst_offset_hh_nbr) {
    this.utc_dst_offset_hh_nbr = utc_dst_offset_hh_nbr;
  }
  public site_t with_utc_dst_offset_hh_nbr(java.math.BigDecimal utc_dst_offset_hh_nbr) {
    this.utc_dst_offset_hh_nbr = utc_dst_offset_hh_nbr;
    return this;
  }
  private String mngng_cntry_cd;
  public String get_mngng_cntry_cd() {
    return mngng_cntry_cd;
  }
  public void set_mngng_cntry_cd(String mngng_cntry_cd) {
    this.mngng_cntry_cd = mngng_cntry_cd;
  }
  public site_t with_mngng_cntry_cd(String mngng_cntry_cd) {
    this.mngng_cntry_cd = mngng_cntry_cd;
    return this;
  }
  private Integer batch_ld_nbr;
  public Integer get_batch_ld_nbr() {
    return batch_ld_nbr;
  }
  public void set_batch_ld_nbr(Integer batch_ld_nbr) {
    this.batch_ld_nbr = batch_ld_nbr;
  }
  public site_t with_batch_ld_nbr(Integer batch_ld_nbr) {
    this.batch_ld_nbr = batch_ld_nbr;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof site_t)) {
      return false;
    }
    site_t that = (site_t) o;
    boolean equal = true;
    equal = equal && (this.site_key == null ? that.site_key == null : this.site_key.equals(that.site_key));
    equal = equal && (this.site_nm == null ? that.site_nm == null : this.site_nm.equals(that.site_nm));
    equal = equal && (this.site_local_nm == null ? that.site_local_nm == null : this.site_local_nm.equals(that.site_local_nm));
    equal = equal && (this.site_id_txt == null ? that.site_id_txt == null : this.site_id_txt.equals(that.site_id_txt));
    equal = equal && (this.site_src_id == null ? that.site_src_id == null : this.site_src_id.equals(that.site_src_id));
    equal = equal && (this.site_data_src_cd == null ? that.site_data_src_cd == null : this.site_data_src_cd.equals(that.site_data_src_cd));
    equal = equal && (this.site_active_dt == null ? that.site_active_dt == null : this.site_active_dt.equals(that.site_active_dt));
    equal = equal && (this.site_inactive_dt == null ? that.site_inactive_dt == null : this.site_inactive_dt.equals(that.site_inactive_dt));
    equal = equal && (this.site_first_log_seen_dt == null ? that.site_first_log_seen_dt == null : this.site_first_log_seen_dt.equals(that.site_first_log_seen_dt));
    equal = equal && (this.site_last_log_seen_dt == null ? that.site_last_log_seen_dt == null : this.site_last_log_seen_dt.equals(that.site_last_log_seen_dt));
    equal = equal && (this.site_typ_cd == null ? that.site_typ_cd == null : this.site_typ_cd.equals(that.site_typ_cd));
    equal = equal && (this.site_typ_nm == null ? that.site_typ_nm == null : this.site_typ_nm.equals(that.site_typ_nm));
    equal = equal && (this.site_trade_class_cd == null ? that.site_trade_class_cd == null : this.site_trade_class_cd.equals(that.site_trade_class_cd));
    equal = equal && (this.site_trade_class_desc == null ? that.site_trade_class_desc == null : this.site_trade_class_desc.equals(that.site_trade_class_desc));
    equal = equal && (this.lgl_entity_key == null ? that.lgl_entity_key == null : this.lgl_entity_key.equals(that.lgl_entity_key));
    equal = equal && (this.lgl_entity_nbr == null ? that.lgl_entity_nbr == null : this.lgl_entity_nbr.equals(that.lgl_entity_nbr));
    equal = equal && (this.lgl_entity_nm == null ? that.lgl_entity_nm == null : this.lgl_entity_nm.equals(that.lgl_entity_nm));
    equal = equal && (this.lgl_entity_sub_id == null ? that.lgl_entity_sub_id == null : this.lgl_entity_sub_id.equals(that.lgl_entity_sub_id));
    equal = equal && (this.lgl_entity_sub_nm == null ? that.lgl_entity_sub_nm == null : this.lgl_entity_sub_nm.equals(that.lgl_entity_sub_nm));
    equal = equal && (this.ntwk_id == null ? that.ntwk_id == null : this.ntwk_id.equals(that.ntwk_id));
    equal = equal && (this.ntwk_nm == null ? that.ntwk_nm == null : this.ntwk_nm.equals(that.ntwk_nm));
    equal = equal && (this.ntwk_local_nm == null ? that.ntwk_local_nm == null : this.ntwk_local_nm.equals(that.ntwk_local_nm));
    equal = equal && (this.ntwk_purpose_cd == null ? that.ntwk_purpose_cd == null : this.ntwk_purpose_cd.equals(that.ntwk_purpose_cd));
    equal = equal && (this.ntwk_purpose_desc == null ? that.ntwk_purpose_desc == null : this.ntwk_purpose_desc.equals(that.ntwk_purpose_desc));
    equal = equal && (this.ntwk_seg_cd == null ? that.ntwk_seg_cd == null : this.ntwk_seg_cd.equals(that.ntwk_seg_cd));
    equal = equal && (this.ntwk_seg_desc == null ? that.ntwk_seg_desc == null : this.ntwk_seg_desc.equals(that.ntwk_seg_desc));
    equal = equal && (this.ntwk_credit_card_masking_ind == null ? that.ntwk_credit_card_masking_ind == null : this.ntwk_credit_card_masking_ind.equals(that.ntwk_credit_card_masking_ind));
    equal = equal && (this.ntwk_catalina_trade_class_cd == null ? that.ntwk_catalina_trade_class_cd == null : this.ntwk_catalina_trade_class_cd.equals(that.ntwk_catalina_trade_class_cd));
    equal = equal && (this.ntwk_catalina_trade_class_desc == null ? that.ntwk_catalina_trade_class_desc == null : this.ntwk_catalina_trade_class_desc.equals(that.ntwk_catalina_trade_class_desc));
    equal = equal && (this.addr_ln_1_txt == null ? that.addr_ln_1_txt == null : this.addr_ln_1_txt.equals(that.addr_ln_1_txt));
    equal = equal && (this.addr_ln_2_txt == null ? that.addr_ln_2_txt == null : this.addr_ln_2_txt.equals(that.addr_ln_2_txt));
    equal = equal && (this.city_nm == null ? that.city_nm == null : this.city_nm.equals(that.city_nm));
    equal = equal && (this.postal_cd == null ? that.postal_cd == null : this.postal_cd.equals(that.postal_cd));
    equal = equal && (this.postal_cd_suffix_cd == null ? that.postal_cd_suffix_cd == null : this.postal_cd_suffix_cd.equals(that.postal_cd_suffix_cd));
    equal = equal && (this.cntry_cd == null ? that.cntry_cd == null : this.cntry_cd.equals(that.cntry_cd));
    equal = equal && (this.cntry_subdiv_cd == null ? that.cntry_subdiv_cd == null : this.cntry_subdiv_cd.equals(that.cntry_subdiv_cd));
    equal = equal && (this.cntry_subdiv_nm == null ? that.cntry_subdiv_nm == null : this.cntry_subdiv_nm.equals(that.cntry_subdiv_nm));
    equal = equal && (this.lat_coord == null ? that.lat_coord == null : this.lat_coord.equals(that.lat_coord));
    equal = equal && (this.long_coord == null ? that.long_coord == null : this.long_coord.equals(that.long_coord));
    equal = equal && (this.str_phone_nbr == null ? that.str_phone_nbr == null : this.str_phone_nbr.equals(that.str_phone_nbr));
    equal = equal && (this.mkt_nbr == null ? that.mkt_nbr == null : this.mkt_nbr.equals(that.mkt_nbr));
    equal = equal && (this.mkt_nm == null ? that.mkt_nm == null : this.mkt_nm.equals(that.mkt_nm));
    equal = equal && (this.rgn_nm == null ? that.rgn_nm == null : this.rgn_nm.equals(that.rgn_nm));
    equal = equal && (this.rgn_nbr == null ? that.rgn_nbr == null : this.rgn_nbr.equals(that.rgn_nbr));
    equal = equal && (this.district_nbr == null ? that.district_nbr == null : this.district_nbr.equals(that.district_nbr));
    equal = equal && (this.mi_natl_nbr == null ? that.mi_natl_nbr == null : this.mi_natl_nbr.equals(that.mi_natl_nbr));
    equal = equal && (this.unique_site_tkn_nbr == null ? that.unique_site_tkn_nbr == null : this.unique_site_tkn_nbr.equals(that.unique_site_tkn_nbr));
    equal = equal && (this.csdb_chn_nbr == null ? that.csdb_chn_nbr == null : this.csdb_chn_nbr.equals(that.csdb_chn_nbr));
    equal = equal && (this.csdb_chn_str_nbr == null ? that.csdb_chn_str_nbr == null : this.csdb_chn_str_nbr.equals(that.csdb_chn_str_nbr));
    equal = equal && (this.csdb_str_fips_nbr == null ? that.csdb_str_fips_nbr == null : this.csdb_str_fips_nbr.equals(that.csdb_str_fips_nbr));
    equal = equal && (this.cdsb_td_mkt_dma_cd == null ? that.cdsb_td_mkt_dma_cd == null : this.cdsb_td_mkt_dma_cd.equals(that.cdsb_td_mkt_dma_cd));
    equal = equal && (this.csdb_td_mkt_dma_nm == null ? that.csdb_td_mkt_dma_nm == null : this.csdb_td_mkt_dma_nm.equals(that.csdb_td_mkt_dma_nm));
    equal = equal && (this.csdb_td_mkt_msa_mkt_cd == null ? that.csdb_td_mkt_msa_mkt_cd == null : this.csdb_td_mkt_msa_mkt_cd.equals(that.csdb_td_mkt_msa_mkt_cd));
    equal = equal && (this.csdb_td_mkt_msa_mkt_nm == null ? that.csdb_td_mkt_msa_mkt_nm == null : this.csdb_td_mkt_msa_mkt_nm.equals(that.csdb_td_mkt_msa_mkt_nm));
    equal = equal && (this.orig_lgl_entity_key == null ? that.orig_lgl_entity_key == null : this.orig_lgl_entity_key.equals(that.orig_lgl_entity_key));
    equal = equal && (this.orig_lgl_entity_nbr == null ? that.orig_lgl_entity_nbr == null : this.orig_lgl_entity_nbr.equals(that.orig_lgl_entity_nbr));
    equal = equal && (this.orig_lgl_entity_nm == null ? that.orig_lgl_entity_nm == null : this.orig_lgl_entity_nm.equals(that.orig_lgl_entity_nm));
    equal = equal && (this.orig_lgl_entity_sub_id == null ? that.orig_lgl_entity_sub_id == null : this.orig_lgl_entity_sub_id.equals(that.orig_lgl_entity_sub_id));
    equal = equal && (this.orig_lgl_entity_sub_nm == null ? that.orig_lgl_entity_sub_nm == null : this.orig_lgl_entity_sub_nm.equals(that.orig_lgl_entity_sub_nm));
    equal = equal && (this.orig_ntwk_id == null ? that.orig_ntwk_id == null : this.orig_ntwk_id.equals(that.orig_ntwk_id));
    equal = equal && (this.orig_ntwk_nm == null ? that.orig_ntwk_nm == null : this.orig_ntwk_nm.equals(that.orig_ntwk_nm));
    equal = equal && (this.orig_site_id_txt == null ? that.orig_site_id_txt == null : this.orig_site_id_txt.equals(that.orig_site_id_txt));
    equal = equal && (this.orig_site_nm == null ? that.orig_site_nm == null : this.orig_site_nm.equals(that.orig_site_nm));
    equal = equal && (this.orig_csdb_chn_nbr == null ? that.orig_csdb_chn_nbr == null : this.orig_csdb_chn_nbr.equals(that.orig_csdb_chn_nbr));
    equal = equal && (this.orig_csdb_chn_str_nbr == null ? that.orig_csdb_chn_str_nbr == null : this.orig_csdb_chn_str_nbr.equals(that.orig_csdb_chn_str_nbr));
    equal = equal && (this.orig_site_active_dt == null ? that.orig_site_active_dt == null : this.orig_site_active_dt.equals(that.orig_site_active_dt));
    equal = equal && (this.orig_site_inactive_dt == null ? that.orig_site_inactive_dt == null : this.orig_site_inactive_dt.equals(that.orig_site_inactive_dt));
    equal = equal && (this.utc_offset_hh_nbr == null ? that.utc_offset_hh_nbr == null : this.utc_offset_hh_nbr.equals(that.utc_offset_hh_nbr));
    equal = equal && (this.utc_dst_offset_hh_nbr == null ? that.utc_dst_offset_hh_nbr == null : this.utc_dst_offset_hh_nbr.equals(that.utc_dst_offset_hh_nbr));
    equal = equal && (this.mngng_cntry_cd == null ? that.mngng_cntry_cd == null : this.mngng_cntry_cd.equals(that.mngng_cntry_cd));
    equal = equal && (this.batch_ld_nbr == null ? that.batch_ld_nbr == null : this.batch_ld_nbr.equals(that.batch_ld_nbr));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof site_t)) {
      return false;
    }
    site_t that = (site_t) o;
    boolean equal = true;
    equal = equal && (this.site_key == null ? that.site_key == null : this.site_key.equals(that.site_key));
    equal = equal && (this.site_nm == null ? that.site_nm == null : this.site_nm.equals(that.site_nm));
    equal = equal && (this.site_local_nm == null ? that.site_local_nm == null : this.site_local_nm.equals(that.site_local_nm));
    equal = equal && (this.site_id_txt == null ? that.site_id_txt == null : this.site_id_txt.equals(that.site_id_txt));
    equal = equal && (this.site_src_id == null ? that.site_src_id == null : this.site_src_id.equals(that.site_src_id));
    equal = equal && (this.site_data_src_cd == null ? that.site_data_src_cd == null : this.site_data_src_cd.equals(that.site_data_src_cd));
    equal = equal && (this.site_active_dt == null ? that.site_active_dt == null : this.site_active_dt.equals(that.site_active_dt));
    equal = equal && (this.site_inactive_dt == null ? that.site_inactive_dt == null : this.site_inactive_dt.equals(that.site_inactive_dt));
    equal = equal && (this.site_first_log_seen_dt == null ? that.site_first_log_seen_dt == null : this.site_first_log_seen_dt.equals(that.site_first_log_seen_dt));
    equal = equal && (this.site_last_log_seen_dt == null ? that.site_last_log_seen_dt == null : this.site_last_log_seen_dt.equals(that.site_last_log_seen_dt));
    equal = equal && (this.site_typ_cd == null ? that.site_typ_cd == null : this.site_typ_cd.equals(that.site_typ_cd));
    equal = equal && (this.site_typ_nm == null ? that.site_typ_nm == null : this.site_typ_nm.equals(that.site_typ_nm));
    equal = equal && (this.site_trade_class_cd == null ? that.site_trade_class_cd == null : this.site_trade_class_cd.equals(that.site_trade_class_cd));
    equal = equal && (this.site_trade_class_desc == null ? that.site_trade_class_desc == null : this.site_trade_class_desc.equals(that.site_trade_class_desc));
    equal = equal && (this.lgl_entity_key == null ? that.lgl_entity_key == null : this.lgl_entity_key.equals(that.lgl_entity_key));
    equal = equal && (this.lgl_entity_nbr == null ? that.lgl_entity_nbr == null : this.lgl_entity_nbr.equals(that.lgl_entity_nbr));
    equal = equal && (this.lgl_entity_nm == null ? that.lgl_entity_nm == null : this.lgl_entity_nm.equals(that.lgl_entity_nm));
    equal = equal && (this.lgl_entity_sub_id == null ? that.lgl_entity_sub_id == null : this.lgl_entity_sub_id.equals(that.lgl_entity_sub_id));
    equal = equal && (this.lgl_entity_sub_nm == null ? that.lgl_entity_sub_nm == null : this.lgl_entity_sub_nm.equals(that.lgl_entity_sub_nm));
    equal = equal && (this.ntwk_id == null ? that.ntwk_id == null : this.ntwk_id.equals(that.ntwk_id));
    equal = equal && (this.ntwk_nm == null ? that.ntwk_nm == null : this.ntwk_nm.equals(that.ntwk_nm));
    equal = equal && (this.ntwk_local_nm == null ? that.ntwk_local_nm == null : this.ntwk_local_nm.equals(that.ntwk_local_nm));
    equal = equal && (this.ntwk_purpose_cd == null ? that.ntwk_purpose_cd == null : this.ntwk_purpose_cd.equals(that.ntwk_purpose_cd));
    equal = equal && (this.ntwk_purpose_desc == null ? that.ntwk_purpose_desc == null : this.ntwk_purpose_desc.equals(that.ntwk_purpose_desc));
    equal = equal && (this.ntwk_seg_cd == null ? that.ntwk_seg_cd == null : this.ntwk_seg_cd.equals(that.ntwk_seg_cd));
    equal = equal && (this.ntwk_seg_desc == null ? that.ntwk_seg_desc == null : this.ntwk_seg_desc.equals(that.ntwk_seg_desc));
    equal = equal && (this.ntwk_credit_card_masking_ind == null ? that.ntwk_credit_card_masking_ind == null : this.ntwk_credit_card_masking_ind.equals(that.ntwk_credit_card_masking_ind));
    equal = equal && (this.ntwk_catalina_trade_class_cd == null ? that.ntwk_catalina_trade_class_cd == null : this.ntwk_catalina_trade_class_cd.equals(that.ntwk_catalina_trade_class_cd));
    equal = equal && (this.ntwk_catalina_trade_class_desc == null ? that.ntwk_catalina_trade_class_desc == null : this.ntwk_catalina_trade_class_desc.equals(that.ntwk_catalina_trade_class_desc));
    equal = equal && (this.addr_ln_1_txt == null ? that.addr_ln_1_txt == null : this.addr_ln_1_txt.equals(that.addr_ln_1_txt));
    equal = equal && (this.addr_ln_2_txt == null ? that.addr_ln_2_txt == null : this.addr_ln_2_txt.equals(that.addr_ln_2_txt));
    equal = equal && (this.city_nm == null ? that.city_nm == null : this.city_nm.equals(that.city_nm));
    equal = equal && (this.postal_cd == null ? that.postal_cd == null : this.postal_cd.equals(that.postal_cd));
    equal = equal && (this.postal_cd_suffix_cd == null ? that.postal_cd_suffix_cd == null : this.postal_cd_suffix_cd.equals(that.postal_cd_suffix_cd));
    equal = equal && (this.cntry_cd == null ? that.cntry_cd == null : this.cntry_cd.equals(that.cntry_cd));
    equal = equal && (this.cntry_subdiv_cd == null ? that.cntry_subdiv_cd == null : this.cntry_subdiv_cd.equals(that.cntry_subdiv_cd));
    equal = equal && (this.cntry_subdiv_nm == null ? that.cntry_subdiv_nm == null : this.cntry_subdiv_nm.equals(that.cntry_subdiv_nm));
    equal = equal && (this.lat_coord == null ? that.lat_coord == null : this.lat_coord.equals(that.lat_coord));
    equal = equal && (this.long_coord == null ? that.long_coord == null : this.long_coord.equals(that.long_coord));
    equal = equal && (this.str_phone_nbr == null ? that.str_phone_nbr == null : this.str_phone_nbr.equals(that.str_phone_nbr));
    equal = equal && (this.mkt_nbr == null ? that.mkt_nbr == null : this.mkt_nbr.equals(that.mkt_nbr));
    equal = equal && (this.mkt_nm == null ? that.mkt_nm == null : this.mkt_nm.equals(that.mkt_nm));
    equal = equal && (this.rgn_nm == null ? that.rgn_nm == null : this.rgn_nm.equals(that.rgn_nm));
    equal = equal && (this.rgn_nbr == null ? that.rgn_nbr == null : this.rgn_nbr.equals(that.rgn_nbr));
    equal = equal && (this.district_nbr == null ? that.district_nbr == null : this.district_nbr.equals(that.district_nbr));
    equal = equal && (this.mi_natl_nbr == null ? that.mi_natl_nbr == null : this.mi_natl_nbr.equals(that.mi_natl_nbr));
    equal = equal && (this.unique_site_tkn_nbr == null ? that.unique_site_tkn_nbr == null : this.unique_site_tkn_nbr.equals(that.unique_site_tkn_nbr));
    equal = equal && (this.csdb_chn_nbr == null ? that.csdb_chn_nbr == null : this.csdb_chn_nbr.equals(that.csdb_chn_nbr));
    equal = equal && (this.csdb_chn_str_nbr == null ? that.csdb_chn_str_nbr == null : this.csdb_chn_str_nbr.equals(that.csdb_chn_str_nbr));
    equal = equal && (this.csdb_str_fips_nbr == null ? that.csdb_str_fips_nbr == null : this.csdb_str_fips_nbr.equals(that.csdb_str_fips_nbr));
    equal = equal && (this.cdsb_td_mkt_dma_cd == null ? that.cdsb_td_mkt_dma_cd == null : this.cdsb_td_mkt_dma_cd.equals(that.cdsb_td_mkt_dma_cd));
    equal = equal && (this.csdb_td_mkt_dma_nm == null ? that.csdb_td_mkt_dma_nm == null : this.csdb_td_mkt_dma_nm.equals(that.csdb_td_mkt_dma_nm));
    equal = equal && (this.csdb_td_mkt_msa_mkt_cd == null ? that.csdb_td_mkt_msa_mkt_cd == null : this.csdb_td_mkt_msa_mkt_cd.equals(that.csdb_td_mkt_msa_mkt_cd));
    equal = equal && (this.csdb_td_mkt_msa_mkt_nm == null ? that.csdb_td_mkt_msa_mkt_nm == null : this.csdb_td_mkt_msa_mkt_nm.equals(that.csdb_td_mkt_msa_mkt_nm));
    equal = equal && (this.orig_lgl_entity_key == null ? that.orig_lgl_entity_key == null : this.orig_lgl_entity_key.equals(that.orig_lgl_entity_key));
    equal = equal && (this.orig_lgl_entity_nbr == null ? that.orig_lgl_entity_nbr == null : this.orig_lgl_entity_nbr.equals(that.orig_lgl_entity_nbr));
    equal = equal && (this.orig_lgl_entity_nm == null ? that.orig_lgl_entity_nm == null : this.orig_lgl_entity_nm.equals(that.orig_lgl_entity_nm));
    equal = equal && (this.orig_lgl_entity_sub_id == null ? that.orig_lgl_entity_sub_id == null : this.orig_lgl_entity_sub_id.equals(that.orig_lgl_entity_sub_id));
    equal = equal && (this.orig_lgl_entity_sub_nm == null ? that.orig_lgl_entity_sub_nm == null : this.orig_lgl_entity_sub_nm.equals(that.orig_lgl_entity_sub_nm));
    equal = equal && (this.orig_ntwk_id == null ? that.orig_ntwk_id == null : this.orig_ntwk_id.equals(that.orig_ntwk_id));
    equal = equal && (this.orig_ntwk_nm == null ? that.orig_ntwk_nm == null : this.orig_ntwk_nm.equals(that.orig_ntwk_nm));
    equal = equal && (this.orig_site_id_txt == null ? that.orig_site_id_txt == null : this.orig_site_id_txt.equals(that.orig_site_id_txt));
    equal = equal && (this.orig_site_nm == null ? that.orig_site_nm == null : this.orig_site_nm.equals(that.orig_site_nm));
    equal = equal && (this.orig_csdb_chn_nbr == null ? that.orig_csdb_chn_nbr == null : this.orig_csdb_chn_nbr.equals(that.orig_csdb_chn_nbr));
    equal = equal && (this.orig_csdb_chn_str_nbr == null ? that.orig_csdb_chn_str_nbr == null : this.orig_csdb_chn_str_nbr.equals(that.orig_csdb_chn_str_nbr));
    equal = equal && (this.orig_site_active_dt == null ? that.orig_site_active_dt == null : this.orig_site_active_dt.equals(that.orig_site_active_dt));
    equal = equal && (this.orig_site_inactive_dt == null ? that.orig_site_inactive_dt == null : this.orig_site_inactive_dt.equals(that.orig_site_inactive_dt));
    equal = equal && (this.utc_offset_hh_nbr == null ? that.utc_offset_hh_nbr == null : this.utc_offset_hh_nbr.equals(that.utc_offset_hh_nbr));
    equal = equal && (this.utc_dst_offset_hh_nbr == null ? that.utc_dst_offset_hh_nbr == null : this.utc_dst_offset_hh_nbr.equals(that.utc_dst_offset_hh_nbr));
    equal = equal && (this.mngng_cntry_cd == null ? that.mngng_cntry_cd == null : this.mngng_cntry_cd.equals(that.mngng_cntry_cd));
    equal = equal && (this.batch_ld_nbr == null ? that.batch_ld_nbr == null : this.batch_ld_nbr.equals(that.batch_ld_nbr));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.site_key = JdbcWritableBridge.readLong(1, __dbResults);
    this.site_nm = JdbcWritableBridge.readString(2, __dbResults);
    this.site_local_nm = JdbcWritableBridge.readString(3, __dbResults);
    this.site_id_txt = JdbcWritableBridge.readString(4, __dbResults);
    this.site_src_id = JdbcWritableBridge.readInteger(5, __dbResults);
    this.site_data_src_cd = JdbcWritableBridge.readString(6, __dbResults);
    this.site_active_dt = JdbcWritableBridge.readDate(7, __dbResults);
    this.site_inactive_dt = JdbcWritableBridge.readDate(8, __dbResults);
    this.site_first_log_seen_dt = JdbcWritableBridge.readDate(9, __dbResults);
    this.site_last_log_seen_dt = JdbcWritableBridge.readDate(10, __dbResults);
    this.site_typ_cd = JdbcWritableBridge.readString(11, __dbResults);
    this.site_typ_nm = JdbcWritableBridge.readString(12, __dbResults);
    this.site_trade_class_cd = JdbcWritableBridge.readString(13, __dbResults);
    this.site_trade_class_desc = JdbcWritableBridge.readString(14, __dbResults);
    this.lgl_entity_key = JdbcWritableBridge.readLong(15, __dbResults);
    this.lgl_entity_nbr = JdbcWritableBridge.readInteger(16, __dbResults);
    this.lgl_entity_nm = JdbcWritableBridge.readString(17, __dbResults);
    this.lgl_entity_sub_id = JdbcWritableBridge.readInteger(18, __dbResults);
    this.lgl_entity_sub_nm = JdbcWritableBridge.readString(19, __dbResults);
    this.ntwk_id = JdbcWritableBridge.readInteger(20, __dbResults);
    this.ntwk_nm = JdbcWritableBridge.readString(21, __dbResults);
    this.ntwk_local_nm = JdbcWritableBridge.readString(22, __dbResults);
    this.ntwk_purpose_cd = JdbcWritableBridge.readString(23, __dbResults);
    this.ntwk_purpose_desc = JdbcWritableBridge.readString(24, __dbResults);
    this.ntwk_seg_cd = JdbcWritableBridge.readString(25, __dbResults);
    this.ntwk_seg_desc = JdbcWritableBridge.readString(26, __dbResults);
    this.ntwk_credit_card_masking_ind = JdbcWritableBridge.readString(27, __dbResults);
    this.ntwk_catalina_trade_class_cd = JdbcWritableBridge.readString(28, __dbResults);
    this.ntwk_catalina_trade_class_desc = JdbcWritableBridge.readString(29, __dbResults);
    this.addr_ln_1_txt = JdbcWritableBridge.readString(30, __dbResults);
    this.addr_ln_2_txt = JdbcWritableBridge.readString(31, __dbResults);
    this.city_nm = JdbcWritableBridge.readString(32, __dbResults);
    this.postal_cd = JdbcWritableBridge.readString(33, __dbResults);
    this.postal_cd_suffix_cd = JdbcWritableBridge.readString(34, __dbResults);
    this.cntry_cd = JdbcWritableBridge.readString(35, __dbResults);
    this.cntry_subdiv_cd = JdbcWritableBridge.readString(36, __dbResults);
    this.cntry_subdiv_nm = JdbcWritableBridge.readString(37, __dbResults);
    this.lat_coord = JdbcWritableBridge.readBigDecimal(38, __dbResults);
    this.long_coord = JdbcWritableBridge.readBigDecimal(39, __dbResults);
    this.str_phone_nbr = JdbcWritableBridge.readLong(40, __dbResults);
    this.mkt_nbr = JdbcWritableBridge.readInteger(41, __dbResults);
    this.mkt_nm = JdbcWritableBridge.readString(42, __dbResults);
    this.rgn_nm = JdbcWritableBridge.readString(43, __dbResults);
    this.rgn_nbr = JdbcWritableBridge.readInteger(44, __dbResults);
    this.district_nbr = JdbcWritableBridge.readInteger(45, __dbResults);
    this.mi_natl_nbr = JdbcWritableBridge.readInteger(46, __dbResults);
    this.unique_site_tkn_nbr = JdbcWritableBridge.readLong(47, __dbResults);
    this.csdb_chn_nbr = JdbcWritableBridge.readInteger(48, __dbResults);
    this.csdb_chn_str_nbr = JdbcWritableBridge.readInteger(49, __dbResults);
    this.csdb_str_fips_nbr = JdbcWritableBridge.readInteger(50, __dbResults);
    this.cdsb_td_mkt_dma_cd = JdbcWritableBridge.readString(51, __dbResults);
    this.csdb_td_mkt_dma_nm = JdbcWritableBridge.readString(52, __dbResults);
    this.csdb_td_mkt_msa_mkt_cd = JdbcWritableBridge.readString(53, __dbResults);
    this.csdb_td_mkt_msa_mkt_nm = JdbcWritableBridge.readString(54, __dbResults);
    this.orig_lgl_entity_key = JdbcWritableBridge.readLong(55, __dbResults);
    this.orig_lgl_entity_nbr = JdbcWritableBridge.readInteger(56, __dbResults);
    this.orig_lgl_entity_nm = JdbcWritableBridge.readString(57, __dbResults);
    this.orig_lgl_entity_sub_id = JdbcWritableBridge.readInteger(58, __dbResults);
    this.orig_lgl_entity_sub_nm = JdbcWritableBridge.readString(59, __dbResults);
    this.orig_ntwk_id = JdbcWritableBridge.readInteger(60, __dbResults);
    this.orig_ntwk_nm = JdbcWritableBridge.readString(61, __dbResults);
    this.orig_site_id_txt = JdbcWritableBridge.readString(62, __dbResults);
    this.orig_site_nm = JdbcWritableBridge.readString(63, __dbResults);
    this.orig_csdb_chn_nbr = JdbcWritableBridge.readInteger(64, __dbResults);
    this.orig_csdb_chn_str_nbr = JdbcWritableBridge.readInteger(65, __dbResults);
    this.orig_site_active_dt = JdbcWritableBridge.readDate(66, __dbResults);
    this.orig_site_inactive_dt = JdbcWritableBridge.readDate(67, __dbResults);
    this.utc_offset_hh_nbr = JdbcWritableBridge.readBigDecimal(68, __dbResults);
    this.utc_dst_offset_hh_nbr = JdbcWritableBridge.readBigDecimal(69, __dbResults);
    this.mngng_cntry_cd = JdbcWritableBridge.readString(70, __dbResults);
    this.batch_ld_nbr = JdbcWritableBridge.readInteger(71, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.site_key = JdbcWritableBridge.readLong(1, __dbResults);
    this.site_nm = JdbcWritableBridge.readString(2, __dbResults);
    this.site_local_nm = JdbcWritableBridge.readString(3, __dbResults);
    this.site_id_txt = JdbcWritableBridge.readString(4, __dbResults);
    this.site_src_id = JdbcWritableBridge.readInteger(5, __dbResults);
    this.site_data_src_cd = JdbcWritableBridge.readString(6, __dbResults);
    this.site_active_dt = JdbcWritableBridge.readDate(7, __dbResults);
    this.site_inactive_dt = JdbcWritableBridge.readDate(8, __dbResults);
    this.site_first_log_seen_dt = JdbcWritableBridge.readDate(9, __dbResults);
    this.site_last_log_seen_dt = JdbcWritableBridge.readDate(10, __dbResults);
    this.site_typ_cd = JdbcWritableBridge.readString(11, __dbResults);
    this.site_typ_nm = JdbcWritableBridge.readString(12, __dbResults);
    this.site_trade_class_cd = JdbcWritableBridge.readString(13, __dbResults);
    this.site_trade_class_desc = JdbcWritableBridge.readString(14, __dbResults);
    this.lgl_entity_key = JdbcWritableBridge.readLong(15, __dbResults);
    this.lgl_entity_nbr = JdbcWritableBridge.readInteger(16, __dbResults);
    this.lgl_entity_nm = JdbcWritableBridge.readString(17, __dbResults);
    this.lgl_entity_sub_id = JdbcWritableBridge.readInteger(18, __dbResults);
    this.lgl_entity_sub_nm = JdbcWritableBridge.readString(19, __dbResults);
    this.ntwk_id = JdbcWritableBridge.readInteger(20, __dbResults);
    this.ntwk_nm = JdbcWritableBridge.readString(21, __dbResults);
    this.ntwk_local_nm = JdbcWritableBridge.readString(22, __dbResults);
    this.ntwk_purpose_cd = JdbcWritableBridge.readString(23, __dbResults);
    this.ntwk_purpose_desc = JdbcWritableBridge.readString(24, __dbResults);
    this.ntwk_seg_cd = JdbcWritableBridge.readString(25, __dbResults);
    this.ntwk_seg_desc = JdbcWritableBridge.readString(26, __dbResults);
    this.ntwk_credit_card_masking_ind = JdbcWritableBridge.readString(27, __dbResults);
    this.ntwk_catalina_trade_class_cd = JdbcWritableBridge.readString(28, __dbResults);
    this.ntwk_catalina_trade_class_desc = JdbcWritableBridge.readString(29, __dbResults);
    this.addr_ln_1_txt = JdbcWritableBridge.readString(30, __dbResults);
    this.addr_ln_2_txt = JdbcWritableBridge.readString(31, __dbResults);
    this.city_nm = JdbcWritableBridge.readString(32, __dbResults);
    this.postal_cd = JdbcWritableBridge.readString(33, __dbResults);
    this.postal_cd_suffix_cd = JdbcWritableBridge.readString(34, __dbResults);
    this.cntry_cd = JdbcWritableBridge.readString(35, __dbResults);
    this.cntry_subdiv_cd = JdbcWritableBridge.readString(36, __dbResults);
    this.cntry_subdiv_nm = JdbcWritableBridge.readString(37, __dbResults);
    this.lat_coord = JdbcWritableBridge.readBigDecimal(38, __dbResults);
    this.long_coord = JdbcWritableBridge.readBigDecimal(39, __dbResults);
    this.str_phone_nbr = JdbcWritableBridge.readLong(40, __dbResults);
    this.mkt_nbr = JdbcWritableBridge.readInteger(41, __dbResults);
    this.mkt_nm = JdbcWritableBridge.readString(42, __dbResults);
    this.rgn_nm = JdbcWritableBridge.readString(43, __dbResults);
    this.rgn_nbr = JdbcWritableBridge.readInteger(44, __dbResults);
    this.district_nbr = JdbcWritableBridge.readInteger(45, __dbResults);
    this.mi_natl_nbr = JdbcWritableBridge.readInteger(46, __dbResults);
    this.unique_site_tkn_nbr = JdbcWritableBridge.readLong(47, __dbResults);
    this.csdb_chn_nbr = JdbcWritableBridge.readInteger(48, __dbResults);
    this.csdb_chn_str_nbr = JdbcWritableBridge.readInteger(49, __dbResults);
    this.csdb_str_fips_nbr = JdbcWritableBridge.readInteger(50, __dbResults);
    this.cdsb_td_mkt_dma_cd = JdbcWritableBridge.readString(51, __dbResults);
    this.csdb_td_mkt_dma_nm = JdbcWritableBridge.readString(52, __dbResults);
    this.csdb_td_mkt_msa_mkt_cd = JdbcWritableBridge.readString(53, __dbResults);
    this.csdb_td_mkt_msa_mkt_nm = JdbcWritableBridge.readString(54, __dbResults);
    this.orig_lgl_entity_key = JdbcWritableBridge.readLong(55, __dbResults);
    this.orig_lgl_entity_nbr = JdbcWritableBridge.readInteger(56, __dbResults);
    this.orig_lgl_entity_nm = JdbcWritableBridge.readString(57, __dbResults);
    this.orig_lgl_entity_sub_id = JdbcWritableBridge.readInteger(58, __dbResults);
    this.orig_lgl_entity_sub_nm = JdbcWritableBridge.readString(59, __dbResults);
    this.orig_ntwk_id = JdbcWritableBridge.readInteger(60, __dbResults);
    this.orig_ntwk_nm = JdbcWritableBridge.readString(61, __dbResults);
    this.orig_site_id_txt = JdbcWritableBridge.readString(62, __dbResults);
    this.orig_site_nm = JdbcWritableBridge.readString(63, __dbResults);
    this.orig_csdb_chn_nbr = JdbcWritableBridge.readInteger(64, __dbResults);
    this.orig_csdb_chn_str_nbr = JdbcWritableBridge.readInteger(65, __dbResults);
    this.orig_site_active_dt = JdbcWritableBridge.readDate(66, __dbResults);
    this.orig_site_inactive_dt = JdbcWritableBridge.readDate(67, __dbResults);
    this.utc_offset_hh_nbr = JdbcWritableBridge.readBigDecimal(68, __dbResults);
    this.utc_dst_offset_hh_nbr = JdbcWritableBridge.readBigDecimal(69, __dbResults);
    this.mngng_cntry_cd = JdbcWritableBridge.readString(70, __dbResults);
    this.batch_ld_nbr = JdbcWritableBridge.readInteger(71, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeLong(site_key, 1 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeString(site_nm, 2 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_local_nm, 3 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_id_txt, 4 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(site_src_id, 5 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(site_data_src_cd, 6 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeDate(site_active_dt, 7 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(site_inactive_dt, 8 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(site_first_log_seen_dt, 9 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(site_last_log_seen_dt, 10 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeString(site_typ_cd, 11 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_typ_nm, 12 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_trade_class_cd, 13 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_trade_class_desc, 14 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeLong(lgl_entity_key, 15 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(lgl_entity_nbr, 16 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(lgl_entity_nm, 17 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(lgl_entity_sub_id, 18 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(lgl_entity_sub_nm, 19 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(ntwk_id, 20 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_nm, 21 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_local_nm, 22 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_purpose_cd, 23 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_purpose_desc, 24 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_seg_cd, 25 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_seg_desc, 26 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_credit_card_masking_ind, 27 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_catalina_trade_class_cd, 28 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_catalina_trade_class_desc, 29 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(addr_ln_1_txt, 30 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(addr_ln_2_txt, 31 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(city_nm, 32 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(postal_cd, 33 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(postal_cd_suffix_cd, 34 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(cntry_cd, 35 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(cntry_subdiv_cd, 36 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(cntry_subdiv_nm, 37 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(lat_coord, 38 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(long_coord, 39 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(str_phone_nbr, 40 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(mkt_nbr, 41 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeString(mkt_nm, 42 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(rgn_nm, 43 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(rgn_nbr, 44 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(district_nbr, 45 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(mi_natl_nbr, 46 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeLong(unique_site_tkn_nbr, 47 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(csdb_chn_nbr, 48 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(csdb_chn_str_nbr, 49 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(csdb_str_fips_nbr, 50 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(cdsb_td_mkt_dma_cd, 51 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(csdb_td_mkt_dma_nm, 52 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(csdb_td_mkt_msa_mkt_cd, 53 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(csdb_td_mkt_msa_mkt_nm, 54 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeLong(orig_lgl_entity_key, 55 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_lgl_entity_nbr, 56 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(orig_lgl_entity_nm, 57 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_lgl_entity_sub_id, 58 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(orig_lgl_entity_sub_nm, 59 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_ntwk_id, 60 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(orig_ntwk_nm, 61 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(orig_site_id_txt, 62 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(orig_site_nm, 63 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_csdb_chn_nbr, 64 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_csdb_chn_str_nbr, 65 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeDate(orig_site_active_dt, 66 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(orig_site_inactive_dt, 67 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(utc_offset_hh_nbr, 68 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(utc_dst_offset_hh_nbr, 69 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeString(mngng_cntry_cd, 70 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(batch_ld_nbr, 71 + __off, 4, __dbStmt);
    return 71;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeLong(site_key, 1 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeString(site_nm, 2 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_local_nm, 3 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_id_txt, 4 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(site_src_id, 5 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(site_data_src_cd, 6 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeDate(site_active_dt, 7 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(site_inactive_dt, 8 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(site_first_log_seen_dt, 9 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(site_last_log_seen_dt, 10 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeString(site_typ_cd, 11 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_typ_nm, 12 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_trade_class_cd, 13 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(site_trade_class_desc, 14 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeLong(lgl_entity_key, 15 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(lgl_entity_nbr, 16 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(lgl_entity_nm, 17 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(lgl_entity_sub_id, 18 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(lgl_entity_sub_nm, 19 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(ntwk_id, 20 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_nm, 21 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_local_nm, 22 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_purpose_cd, 23 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_purpose_desc, 24 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_seg_cd, 25 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_seg_desc, 26 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_credit_card_masking_ind, 27 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_catalina_trade_class_cd, 28 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(ntwk_catalina_trade_class_desc, 29 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(addr_ln_1_txt, 30 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(addr_ln_2_txt, 31 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(city_nm, 32 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(postal_cd, 33 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(postal_cd_suffix_cd, 34 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(cntry_cd, 35 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(cntry_subdiv_cd, 36 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(cntry_subdiv_nm, 37 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(lat_coord, 38 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(long_coord, 39 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeLong(str_phone_nbr, 40 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(mkt_nbr, 41 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeString(mkt_nm, 42 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(rgn_nm, 43 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(rgn_nbr, 44 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(district_nbr, 45 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(mi_natl_nbr, 46 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeLong(unique_site_tkn_nbr, 47 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(csdb_chn_nbr, 48 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(csdb_chn_str_nbr, 49 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(csdb_str_fips_nbr, 50 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(cdsb_td_mkt_dma_cd, 51 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(csdb_td_mkt_dma_nm, 52 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(csdb_td_mkt_msa_mkt_cd, 53 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(csdb_td_mkt_msa_mkt_nm, 54 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeLong(orig_lgl_entity_key, 55 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_lgl_entity_nbr, 56 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(orig_lgl_entity_nm, 57 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_lgl_entity_sub_id, 58 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(orig_lgl_entity_sub_nm, 59 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_ntwk_id, 60 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeString(orig_ntwk_nm, 61 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(orig_site_id_txt, 62 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeString(orig_site_nm, 63 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_csdb_chn_nbr, 64 + __off, 5, __dbStmt);
    JdbcWritableBridge.writeInteger(orig_csdb_chn_str_nbr, 65 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeDate(orig_site_active_dt, 66 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeDate(orig_site_inactive_dt, 67 + __off, 91, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(utc_offset_hh_nbr, 68 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeBigDecimal(utc_dst_offset_hh_nbr, 69 + __off, 2, __dbStmt);
    JdbcWritableBridge.writeString(mngng_cntry_cd, 70 + __off, -9, __dbStmt);
    JdbcWritableBridge.writeInteger(batch_ld_nbr, 71 + __off, 4, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.site_key = null;
    } else {
    this.site_key = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.site_nm = null;
    } else {
    this.site_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_local_nm = null;
    } else {
    this.site_local_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_id_txt = null;
    } else {
    this.site_id_txt = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_src_id = null;
    } else {
    this.site_src_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.site_data_src_cd = null;
    } else {
    this.site_data_src_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_active_dt = null;
    } else {
    this.site_active_dt = new Date(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.site_inactive_dt = null;
    } else {
    this.site_inactive_dt = new Date(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.site_first_log_seen_dt = null;
    } else {
    this.site_first_log_seen_dt = new Date(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.site_last_log_seen_dt = null;
    } else {
    this.site_last_log_seen_dt = new Date(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.site_typ_cd = null;
    } else {
    this.site_typ_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_typ_nm = null;
    } else {
    this.site_typ_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_trade_class_cd = null;
    } else {
    this.site_trade_class_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.site_trade_class_desc = null;
    } else {
    this.site_trade_class_desc = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.lgl_entity_key = null;
    } else {
    this.lgl_entity_key = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.lgl_entity_nbr = null;
    } else {
    this.lgl_entity_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.lgl_entity_nm = null;
    } else {
    this.lgl_entity_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.lgl_entity_sub_id = null;
    } else {
    this.lgl_entity_sub_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.lgl_entity_sub_nm = null;
    } else {
    this.lgl_entity_sub_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_id = null;
    } else {
    this.ntwk_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_nm = null;
    } else {
    this.ntwk_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_local_nm = null;
    } else {
    this.ntwk_local_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_purpose_cd = null;
    } else {
    this.ntwk_purpose_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_purpose_desc = null;
    } else {
    this.ntwk_purpose_desc = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_seg_cd = null;
    } else {
    this.ntwk_seg_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_seg_desc = null;
    } else {
    this.ntwk_seg_desc = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_credit_card_masking_ind = null;
    } else {
    this.ntwk_credit_card_masking_ind = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_catalina_trade_class_cd = null;
    } else {
    this.ntwk_catalina_trade_class_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.ntwk_catalina_trade_class_desc = null;
    } else {
    this.ntwk_catalina_trade_class_desc = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.addr_ln_1_txt = null;
    } else {
    this.addr_ln_1_txt = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.addr_ln_2_txt = null;
    } else {
    this.addr_ln_2_txt = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.city_nm = null;
    } else {
    this.city_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.postal_cd = null;
    } else {
    this.postal_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.postal_cd_suffix_cd = null;
    } else {
    this.postal_cd_suffix_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.cntry_cd = null;
    } else {
    this.cntry_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.cntry_subdiv_cd = null;
    } else {
    this.cntry_subdiv_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.cntry_subdiv_nm = null;
    } else {
    this.cntry_subdiv_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.lat_coord = null;
    } else {
    this.lat_coord = org.apache.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.long_coord = null;
    } else {
    this.long_coord = org.apache.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.str_phone_nbr = null;
    } else {
    this.str_phone_nbr = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.mkt_nbr = null;
    } else {
    this.mkt_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.mkt_nm = null;
    } else {
    this.mkt_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.rgn_nm = null;
    } else {
    this.rgn_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.rgn_nbr = null;
    } else {
    this.rgn_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.district_nbr = null;
    } else {
    this.district_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.mi_natl_nbr = null;
    } else {
    this.mi_natl_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.unique_site_tkn_nbr = null;
    } else {
    this.unique_site_tkn_nbr = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.csdb_chn_nbr = null;
    } else {
    this.csdb_chn_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.csdb_chn_str_nbr = null;
    } else {
    this.csdb_chn_str_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.csdb_str_fips_nbr = null;
    } else {
    this.csdb_str_fips_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.cdsb_td_mkt_dma_cd = null;
    } else {
    this.cdsb_td_mkt_dma_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.csdb_td_mkt_dma_nm = null;
    } else {
    this.csdb_td_mkt_dma_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.csdb_td_mkt_msa_mkt_cd = null;
    } else {
    this.csdb_td_mkt_msa_mkt_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.csdb_td_mkt_msa_mkt_nm = null;
    } else {
    this.csdb_td_mkt_msa_mkt_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.orig_lgl_entity_key = null;
    } else {
    this.orig_lgl_entity_key = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_lgl_entity_nbr = null;
    } else {
    this.orig_lgl_entity_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_lgl_entity_nm = null;
    } else {
    this.orig_lgl_entity_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.orig_lgl_entity_sub_id = null;
    } else {
    this.orig_lgl_entity_sub_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_lgl_entity_sub_nm = null;
    } else {
    this.orig_lgl_entity_sub_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.orig_ntwk_id = null;
    } else {
    this.orig_ntwk_id = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_ntwk_nm = null;
    } else {
    this.orig_ntwk_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.orig_site_id_txt = null;
    } else {
    this.orig_site_id_txt = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.orig_site_nm = null;
    } else {
    this.orig_site_nm = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.orig_csdb_chn_nbr = null;
    } else {
    this.orig_csdb_chn_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_csdb_chn_str_nbr = null;
    } else {
    this.orig_csdb_chn_str_nbr = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_site_active_dt = null;
    } else {
    this.orig_site_active_dt = new Date(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.orig_site_inactive_dt = null;
    } else {
    this.orig_site_inactive_dt = new Date(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.utc_offset_hh_nbr = null;
    } else {
    this.utc_offset_hh_nbr = org.apache.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.utc_dst_offset_hh_nbr = null;
    } else {
    this.utc_dst_offset_hh_nbr = org.apache.sqoop.lib.BigDecimalSerializer.readFields(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.mngng_cntry_cd = null;
    } else {
    this.mngng_cntry_cd = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.batch_ld_nbr = null;
    } else {
    this.batch_ld_nbr = Integer.valueOf(__dataIn.readInt());
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.site_key) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_key);
    }
    if (null == this.site_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_nm);
    }
    if (null == this.site_local_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_local_nm);
    }
    if (null == this.site_id_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_id_txt);
    }
    if (null == this.site_src_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.site_src_id);
    }
    if (null == this.site_data_src_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_data_src_cd);
    }
    if (null == this.site_active_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_active_dt.getTime());
    }
    if (null == this.site_inactive_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_inactive_dt.getTime());
    }
    if (null == this.site_first_log_seen_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_first_log_seen_dt.getTime());
    }
    if (null == this.site_last_log_seen_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_last_log_seen_dt.getTime());
    }
    if (null == this.site_typ_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_typ_cd);
    }
    if (null == this.site_typ_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_typ_nm);
    }
    if (null == this.site_trade_class_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_trade_class_cd);
    }
    if (null == this.site_trade_class_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_trade_class_desc);
    }
    if (null == this.lgl_entity_key) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.lgl_entity_key);
    }
    if (null == this.lgl_entity_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.lgl_entity_nbr);
    }
    if (null == this.lgl_entity_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, lgl_entity_nm);
    }
    if (null == this.lgl_entity_sub_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.lgl_entity_sub_id);
    }
    if (null == this.lgl_entity_sub_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, lgl_entity_sub_nm);
    }
    if (null == this.ntwk_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ntwk_id);
    }
    if (null == this.ntwk_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_nm);
    }
    if (null == this.ntwk_local_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_local_nm);
    }
    if (null == this.ntwk_purpose_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_purpose_cd);
    }
    if (null == this.ntwk_purpose_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_purpose_desc);
    }
    if (null == this.ntwk_seg_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_seg_cd);
    }
    if (null == this.ntwk_seg_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_seg_desc);
    }
    if (null == this.ntwk_credit_card_masking_ind) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_credit_card_masking_ind);
    }
    if (null == this.ntwk_catalina_trade_class_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_catalina_trade_class_cd);
    }
    if (null == this.ntwk_catalina_trade_class_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_catalina_trade_class_desc);
    }
    if (null == this.addr_ln_1_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, addr_ln_1_txt);
    }
    if (null == this.addr_ln_2_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, addr_ln_2_txt);
    }
    if (null == this.city_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, city_nm);
    }
    if (null == this.postal_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, postal_cd);
    }
    if (null == this.postal_cd_suffix_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, postal_cd_suffix_cd);
    }
    if (null == this.cntry_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cntry_cd);
    }
    if (null == this.cntry_subdiv_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cntry_subdiv_cd);
    }
    if (null == this.cntry_subdiv_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cntry_subdiv_nm);
    }
    if (null == this.lat_coord) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.lat_coord, __dataOut);
    }
    if (null == this.long_coord) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.long_coord, __dataOut);
    }
    if (null == this.str_phone_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.str_phone_nbr);
    }
    if (null == this.mkt_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.mkt_nbr);
    }
    if (null == this.mkt_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, mkt_nm);
    }
    if (null == this.rgn_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, rgn_nm);
    }
    if (null == this.rgn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.rgn_nbr);
    }
    if (null == this.district_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.district_nbr);
    }
    if (null == this.mi_natl_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.mi_natl_nbr);
    }
    if (null == this.unique_site_tkn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.unique_site_tkn_nbr);
    }
    if (null == this.csdb_chn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.csdb_chn_nbr);
    }
    if (null == this.csdb_chn_str_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.csdb_chn_str_nbr);
    }
    if (null == this.csdb_str_fips_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.csdb_str_fips_nbr);
    }
    if (null == this.cdsb_td_mkt_dma_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cdsb_td_mkt_dma_cd);
    }
    if (null == this.csdb_td_mkt_dma_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, csdb_td_mkt_dma_nm);
    }
    if (null == this.csdb_td_mkt_msa_mkt_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, csdb_td_mkt_msa_mkt_cd);
    }
    if (null == this.csdb_td_mkt_msa_mkt_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, csdb_td_mkt_msa_mkt_nm);
    }
    if (null == this.orig_lgl_entity_key) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.orig_lgl_entity_key);
    }
    if (null == this.orig_lgl_entity_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_lgl_entity_nbr);
    }
    if (null == this.orig_lgl_entity_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_lgl_entity_nm);
    }
    if (null == this.orig_lgl_entity_sub_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_lgl_entity_sub_id);
    }
    if (null == this.orig_lgl_entity_sub_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_lgl_entity_sub_nm);
    }
    if (null == this.orig_ntwk_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_ntwk_id);
    }
    if (null == this.orig_ntwk_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_ntwk_nm);
    }
    if (null == this.orig_site_id_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_site_id_txt);
    }
    if (null == this.orig_site_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_site_nm);
    }
    if (null == this.orig_csdb_chn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_csdb_chn_nbr);
    }
    if (null == this.orig_csdb_chn_str_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_csdb_chn_str_nbr);
    }
    if (null == this.orig_site_active_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.orig_site_active_dt.getTime());
    }
    if (null == this.orig_site_inactive_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.orig_site_inactive_dt.getTime());
    }
    if (null == this.utc_offset_hh_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.utc_offset_hh_nbr, __dataOut);
    }
    if (null == this.utc_dst_offset_hh_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.utc_dst_offset_hh_nbr, __dataOut);
    }
    if (null == this.mngng_cntry_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, mngng_cntry_cd);
    }
    if (null == this.batch_ld_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.batch_ld_nbr);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.site_key) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_key);
    }
    if (null == this.site_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_nm);
    }
    if (null == this.site_local_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_local_nm);
    }
    if (null == this.site_id_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_id_txt);
    }
    if (null == this.site_src_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.site_src_id);
    }
    if (null == this.site_data_src_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_data_src_cd);
    }
    if (null == this.site_active_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_active_dt.getTime());
    }
    if (null == this.site_inactive_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_inactive_dt.getTime());
    }
    if (null == this.site_first_log_seen_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_first_log_seen_dt.getTime());
    }
    if (null == this.site_last_log_seen_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.site_last_log_seen_dt.getTime());
    }
    if (null == this.site_typ_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_typ_cd);
    }
    if (null == this.site_typ_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_typ_nm);
    }
    if (null == this.site_trade_class_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_trade_class_cd);
    }
    if (null == this.site_trade_class_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, site_trade_class_desc);
    }
    if (null == this.lgl_entity_key) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.lgl_entity_key);
    }
    if (null == this.lgl_entity_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.lgl_entity_nbr);
    }
    if (null == this.lgl_entity_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, lgl_entity_nm);
    }
    if (null == this.lgl_entity_sub_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.lgl_entity_sub_id);
    }
    if (null == this.lgl_entity_sub_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, lgl_entity_sub_nm);
    }
    if (null == this.ntwk_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ntwk_id);
    }
    if (null == this.ntwk_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_nm);
    }
    if (null == this.ntwk_local_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_local_nm);
    }
    if (null == this.ntwk_purpose_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_purpose_cd);
    }
    if (null == this.ntwk_purpose_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_purpose_desc);
    }
    if (null == this.ntwk_seg_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_seg_cd);
    }
    if (null == this.ntwk_seg_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_seg_desc);
    }
    if (null == this.ntwk_credit_card_masking_ind) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_credit_card_masking_ind);
    }
    if (null == this.ntwk_catalina_trade_class_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_catalina_trade_class_cd);
    }
    if (null == this.ntwk_catalina_trade_class_desc) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, ntwk_catalina_trade_class_desc);
    }
    if (null == this.addr_ln_1_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, addr_ln_1_txt);
    }
    if (null == this.addr_ln_2_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, addr_ln_2_txt);
    }
    if (null == this.city_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, city_nm);
    }
    if (null == this.postal_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, postal_cd);
    }
    if (null == this.postal_cd_suffix_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, postal_cd_suffix_cd);
    }
    if (null == this.cntry_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cntry_cd);
    }
    if (null == this.cntry_subdiv_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cntry_subdiv_cd);
    }
    if (null == this.cntry_subdiv_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cntry_subdiv_nm);
    }
    if (null == this.lat_coord) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.lat_coord, __dataOut);
    }
    if (null == this.long_coord) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.long_coord, __dataOut);
    }
    if (null == this.str_phone_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.str_phone_nbr);
    }
    if (null == this.mkt_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.mkt_nbr);
    }
    if (null == this.mkt_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, mkt_nm);
    }
    if (null == this.rgn_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, rgn_nm);
    }
    if (null == this.rgn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.rgn_nbr);
    }
    if (null == this.district_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.district_nbr);
    }
    if (null == this.mi_natl_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.mi_natl_nbr);
    }
    if (null == this.unique_site_tkn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.unique_site_tkn_nbr);
    }
    if (null == this.csdb_chn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.csdb_chn_nbr);
    }
    if (null == this.csdb_chn_str_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.csdb_chn_str_nbr);
    }
    if (null == this.csdb_str_fips_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.csdb_str_fips_nbr);
    }
    if (null == this.cdsb_td_mkt_dma_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cdsb_td_mkt_dma_cd);
    }
    if (null == this.csdb_td_mkt_dma_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, csdb_td_mkt_dma_nm);
    }
    if (null == this.csdb_td_mkt_msa_mkt_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, csdb_td_mkt_msa_mkt_cd);
    }
    if (null == this.csdb_td_mkt_msa_mkt_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, csdb_td_mkt_msa_mkt_nm);
    }
    if (null == this.orig_lgl_entity_key) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.orig_lgl_entity_key);
    }
    if (null == this.orig_lgl_entity_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_lgl_entity_nbr);
    }
    if (null == this.orig_lgl_entity_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_lgl_entity_nm);
    }
    if (null == this.orig_lgl_entity_sub_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_lgl_entity_sub_id);
    }
    if (null == this.orig_lgl_entity_sub_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_lgl_entity_sub_nm);
    }
    if (null == this.orig_ntwk_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_ntwk_id);
    }
    if (null == this.orig_ntwk_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_ntwk_nm);
    }
    if (null == this.orig_site_id_txt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_site_id_txt);
    }
    if (null == this.orig_site_nm) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, orig_site_nm);
    }
    if (null == this.orig_csdb_chn_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_csdb_chn_nbr);
    }
    if (null == this.orig_csdb_chn_str_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.orig_csdb_chn_str_nbr);
    }
    if (null == this.orig_site_active_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.orig_site_active_dt.getTime());
    }
    if (null == this.orig_site_inactive_dt) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.orig_site_inactive_dt.getTime());
    }
    if (null == this.utc_offset_hh_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.utc_offset_hh_nbr, __dataOut);
    }
    if (null == this.utc_dst_offset_hh_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    org.apache.sqoop.lib.BigDecimalSerializer.write(this.utc_dst_offset_hh_nbr, __dataOut);
    }
    if (null == this.mngng_cntry_cd) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, mngng_cntry_cd);
    }
    if (null == this.batch_ld_nbr) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.batch_ld_nbr);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 92, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(site_key==null?"null":"" + site_key, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_nm==null?"null":site_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_local_nm==null?"null":site_local_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_id_txt==null?"null":site_id_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_src_id==null?"null":"" + site_src_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_data_src_cd==null?"null":site_data_src_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_active_dt==null?"null":"" + site_active_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_inactive_dt==null?"null":"" + site_inactive_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_first_log_seen_dt==null?"null":"" + site_first_log_seen_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_last_log_seen_dt==null?"null":"" + site_last_log_seen_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_typ_cd==null?"null":site_typ_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_typ_nm==null?"null":site_typ_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_trade_class_cd==null?"null":site_trade_class_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_trade_class_desc==null?"null":site_trade_class_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_key==null?"null":"" + lgl_entity_key, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_nbr==null?"null":"" + lgl_entity_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_nm==null?"null":lgl_entity_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_sub_id==null?"null":"" + lgl_entity_sub_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_sub_nm==null?"null":lgl_entity_sub_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_id==null?"null":"" + ntwk_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_nm==null?"null":ntwk_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_local_nm==null?"null":ntwk_local_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_purpose_cd==null?"null":ntwk_purpose_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_purpose_desc==null?"null":ntwk_purpose_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_seg_cd==null?"null":ntwk_seg_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_seg_desc==null?"null":ntwk_seg_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_credit_card_masking_ind==null?"null":ntwk_credit_card_masking_ind, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_catalina_trade_class_cd==null?"null":ntwk_catalina_trade_class_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_catalina_trade_class_desc==null?"null":ntwk_catalina_trade_class_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(addr_ln_1_txt==null?"null":addr_ln_1_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(addr_ln_2_txt==null?"null":addr_ln_2_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(city_nm==null?"null":city_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(postal_cd==null?"null":postal_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(postal_cd_suffix_cd==null?"null":postal_cd_suffix_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cntry_cd==null?"null":cntry_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cntry_subdiv_cd==null?"null":cntry_subdiv_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cntry_subdiv_nm==null?"null":cntry_subdiv_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lat_coord==null?"null":lat_coord.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(long_coord==null?"null":long_coord.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(str_phone_nbr==null?"null":"" + str_phone_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mkt_nbr==null?"null":"" + mkt_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mkt_nm==null?"null":mkt_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rgn_nm==null?"null":rgn_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rgn_nbr==null?"null":"" + rgn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(district_nbr==null?"null":"" + district_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mi_natl_nbr==null?"null":"" + mi_natl_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(unique_site_tkn_nbr==null?"null":"" + unique_site_tkn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_chn_nbr==null?"null":"" + csdb_chn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_chn_str_nbr==null?"null":"" + csdb_chn_str_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_str_fips_nbr==null?"null":"" + csdb_str_fips_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cdsb_td_mkt_dma_cd==null?"null":cdsb_td_mkt_dma_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_td_mkt_dma_nm==null?"null":csdb_td_mkt_dma_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_td_mkt_msa_mkt_cd==null?"null":csdb_td_mkt_msa_mkt_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_td_mkt_msa_mkt_nm==null?"null":csdb_td_mkt_msa_mkt_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_key==null?"null":"" + orig_lgl_entity_key, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_nbr==null?"null":"" + orig_lgl_entity_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_nm==null?"null":orig_lgl_entity_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_sub_id==null?"null":"" + orig_lgl_entity_sub_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_sub_nm==null?"null":orig_lgl_entity_sub_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_ntwk_id==null?"null":"" + orig_ntwk_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_ntwk_nm==null?"null":orig_ntwk_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_id_txt==null?"null":orig_site_id_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_nm==null?"null":orig_site_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_csdb_chn_nbr==null?"null":"" + orig_csdb_chn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_csdb_chn_str_nbr==null?"null":"" + orig_csdb_chn_str_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_active_dt==null?"null":"" + orig_site_active_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_inactive_dt==null?"null":"" + orig_site_inactive_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(utc_offset_hh_nbr==null?"null":utc_offset_hh_nbr.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(utc_dst_offset_hh_nbr==null?"null":utc_dst_offset_hh_nbr.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mngng_cntry_cd==null?"null":mngng_cntry_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(batch_ld_nbr==null?"null":"" + batch_ld_nbr, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(site_key==null?"null":"" + site_key, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_nm==null?"null":site_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_local_nm==null?"null":site_local_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_id_txt==null?"null":site_id_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_src_id==null?"null":"" + site_src_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_data_src_cd==null?"null":site_data_src_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_active_dt==null?"null":"" + site_active_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_inactive_dt==null?"null":"" + site_inactive_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_first_log_seen_dt==null?"null":"" + site_first_log_seen_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_last_log_seen_dt==null?"null":"" + site_last_log_seen_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_typ_cd==null?"null":site_typ_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_typ_nm==null?"null":site_typ_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_trade_class_cd==null?"null":site_trade_class_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(site_trade_class_desc==null?"null":site_trade_class_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_key==null?"null":"" + lgl_entity_key, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_nbr==null?"null":"" + lgl_entity_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_nm==null?"null":lgl_entity_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_sub_id==null?"null":"" + lgl_entity_sub_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lgl_entity_sub_nm==null?"null":lgl_entity_sub_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_id==null?"null":"" + ntwk_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_nm==null?"null":ntwk_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_local_nm==null?"null":ntwk_local_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_purpose_cd==null?"null":ntwk_purpose_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_purpose_desc==null?"null":ntwk_purpose_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_seg_cd==null?"null":ntwk_seg_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_seg_desc==null?"null":ntwk_seg_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_credit_card_masking_ind==null?"null":ntwk_credit_card_masking_ind, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_catalina_trade_class_cd==null?"null":ntwk_catalina_trade_class_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ntwk_catalina_trade_class_desc==null?"null":ntwk_catalina_trade_class_desc, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(addr_ln_1_txt==null?"null":addr_ln_1_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(addr_ln_2_txt==null?"null":addr_ln_2_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(city_nm==null?"null":city_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(postal_cd==null?"null":postal_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(postal_cd_suffix_cd==null?"null":postal_cd_suffix_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cntry_cd==null?"null":cntry_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cntry_subdiv_cd==null?"null":cntry_subdiv_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cntry_subdiv_nm==null?"null":cntry_subdiv_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(lat_coord==null?"null":lat_coord.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(long_coord==null?"null":long_coord.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(str_phone_nbr==null?"null":"" + str_phone_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mkt_nbr==null?"null":"" + mkt_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mkt_nm==null?"null":mkt_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rgn_nm==null?"null":rgn_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rgn_nbr==null?"null":"" + rgn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(district_nbr==null?"null":"" + district_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mi_natl_nbr==null?"null":"" + mi_natl_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(unique_site_tkn_nbr==null?"null":"" + unique_site_tkn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_chn_nbr==null?"null":"" + csdb_chn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_chn_str_nbr==null?"null":"" + csdb_chn_str_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_str_fips_nbr==null?"null":"" + csdb_str_fips_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cdsb_td_mkt_dma_cd==null?"null":cdsb_td_mkt_dma_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_td_mkt_dma_nm==null?"null":csdb_td_mkt_dma_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_td_mkt_msa_mkt_cd==null?"null":csdb_td_mkt_msa_mkt_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(csdb_td_mkt_msa_mkt_nm==null?"null":csdb_td_mkt_msa_mkt_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_key==null?"null":"" + orig_lgl_entity_key, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_nbr==null?"null":"" + orig_lgl_entity_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_nm==null?"null":orig_lgl_entity_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_sub_id==null?"null":"" + orig_lgl_entity_sub_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_lgl_entity_sub_nm==null?"null":orig_lgl_entity_sub_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_ntwk_id==null?"null":"" + orig_ntwk_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_ntwk_nm==null?"null":orig_ntwk_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_id_txt==null?"null":orig_site_id_txt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_nm==null?"null":orig_site_nm, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_csdb_chn_nbr==null?"null":"" + orig_csdb_chn_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_csdb_chn_str_nbr==null?"null":"" + orig_csdb_chn_str_nbr, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_active_dt==null?"null":"" + orig_site_active_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(orig_site_inactive_dt==null?"null":"" + orig_site_inactive_dt, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(utc_offset_hh_nbr==null?"null":utc_offset_hh_nbr.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(utc_dst_offset_hh_nbr==null?"null":utc_dst_offset_hh_nbr.toPlainString(), delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(mngng_cntry_cd==null?"null":mngng_cntry_cd, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(batch_ld_nbr==null?"null":"" + batch_ld_nbr, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 92, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_key = null; } else {
      this.site_key = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_nm = null; } else {
      this.site_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_local_nm = null; } else {
      this.site_local_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_id_txt = null; } else {
      this.site_id_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_src_id = null; } else {
      this.site_src_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_data_src_cd = null; } else {
      this.site_data_src_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_active_dt = null; } else {
      this.site_active_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_inactive_dt = null; } else {
      this.site_inactive_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_first_log_seen_dt = null; } else {
      this.site_first_log_seen_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_last_log_seen_dt = null; } else {
      this.site_last_log_seen_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_typ_cd = null; } else {
      this.site_typ_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_typ_nm = null; } else {
      this.site_typ_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_trade_class_cd = null; } else {
      this.site_trade_class_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_trade_class_desc = null; } else {
      this.site_trade_class_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lgl_entity_key = null; } else {
      this.lgl_entity_key = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lgl_entity_nbr = null; } else {
      this.lgl_entity_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.lgl_entity_nm = null; } else {
      this.lgl_entity_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lgl_entity_sub_id = null; } else {
      this.lgl_entity_sub_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.lgl_entity_sub_nm = null; } else {
      this.lgl_entity_sub_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ntwk_id = null; } else {
      this.ntwk_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_nm = null; } else {
      this.ntwk_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_local_nm = null; } else {
      this.ntwk_local_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_purpose_cd = null; } else {
      this.ntwk_purpose_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_purpose_desc = null; } else {
      this.ntwk_purpose_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_seg_cd = null; } else {
      this.ntwk_seg_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_seg_desc = null; } else {
      this.ntwk_seg_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_credit_card_masking_ind = null; } else {
      this.ntwk_credit_card_masking_ind = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_catalina_trade_class_cd = null; } else {
      this.ntwk_catalina_trade_class_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_catalina_trade_class_desc = null; } else {
      this.ntwk_catalina_trade_class_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.addr_ln_1_txt = null; } else {
      this.addr_ln_1_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.addr_ln_2_txt = null; } else {
      this.addr_ln_2_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.city_nm = null; } else {
      this.city_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.postal_cd = null; } else {
      this.postal_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.postal_cd_suffix_cd = null; } else {
      this.postal_cd_suffix_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cntry_cd = null; } else {
      this.cntry_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cntry_subdiv_cd = null; } else {
      this.cntry_subdiv_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cntry_subdiv_nm = null; } else {
      this.cntry_subdiv_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lat_coord = null; } else {
      this.lat_coord = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.long_coord = null; } else {
      this.long_coord = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.str_phone_nbr = null; } else {
      this.str_phone_nbr = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.mkt_nbr = null; } else {
      this.mkt_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.mkt_nm = null; } else {
      this.mkt_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.rgn_nm = null; } else {
      this.rgn_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.rgn_nbr = null; } else {
      this.rgn_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.district_nbr = null; } else {
      this.district_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.mi_natl_nbr = null; } else {
      this.mi_natl_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.unique_site_tkn_nbr = null; } else {
      this.unique_site_tkn_nbr = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.csdb_chn_nbr = null; } else {
      this.csdb_chn_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.csdb_chn_str_nbr = null; } else {
      this.csdb_chn_str_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.csdb_str_fips_nbr = null; } else {
      this.csdb_str_fips_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cdsb_td_mkt_dma_cd = null; } else {
      this.cdsb_td_mkt_dma_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.csdb_td_mkt_dma_nm = null; } else {
      this.csdb_td_mkt_dma_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.csdb_td_mkt_msa_mkt_cd = null; } else {
      this.csdb_td_mkt_msa_mkt_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.csdb_td_mkt_msa_mkt_nm = null; } else {
      this.csdb_td_mkt_msa_mkt_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_lgl_entity_key = null; } else {
      this.orig_lgl_entity_key = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_lgl_entity_nbr = null; } else {
      this.orig_lgl_entity_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_lgl_entity_nm = null; } else {
      this.orig_lgl_entity_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_lgl_entity_sub_id = null; } else {
      this.orig_lgl_entity_sub_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_lgl_entity_sub_nm = null; } else {
      this.orig_lgl_entity_sub_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_ntwk_id = null; } else {
      this.orig_ntwk_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_ntwk_nm = null; } else {
      this.orig_ntwk_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_site_id_txt = null; } else {
      this.orig_site_id_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_site_nm = null; } else {
      this.orig_site_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_csdb_chn_nbr = null; } else {
      this.orig_csdb_chn_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_csdb_chn_str_nbr = null; } else {
      this.orig_csdb_chn_str_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_site_active_dt = null; } else {
      this.orig_site_active_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_site_inactive_dt = null; } else {
      this.orig_site_inactive_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.utc_offset_hh_nbr = null; } else {
      this.utc_offset_hh_nbr = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.utc_dst_offset_hh_nbr = null; } else {
      this.utc_dst_offset_hh_nbr = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.mngng_cntry_cd = null; } else {
      this.mngng_cntry_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.batch_ld_nbr = null; } else {
      this.batch_ld_nbr = Integer.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_key = null; } else {
      this.site_key = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_nm = null; } else {
      this.site_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_local_nm = null; } else {
      this.site_local_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_id_txt = null; } else {
      this.site_id_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_src_id = null; } else {
      this.site_src_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_data_src_cd = null; } else {
      this.site_data_src_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_active_dt = null; } else {
      this.site_active_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_inactive_dt = null; } else {
      this.site_inactive_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_first_log_seen_dt = null; } else {
      this.site_first_log_seen_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.site_last_log_seen_dt = null; } else {
      this.site_last_log_seen_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_typ_cd = null; } else {
      this.site_typ_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_typ_nm = null; } else {
      this.site_typ_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_trade_class_cd = null; } else {
      this.site_trade_class_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.site_trade_class_desc = null; } else {
      this.site_trade_class_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lgl_entity_key = null; } else {
      this.lgl_entity_key = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lgl_entity_nbr = null; } else {
      this.lgl_entity_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.lgl_entity_nm = null; } else {
      this.lgl_entity_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lgl_entity_sub_id = null; } else {
      this.lgl_entity_sub_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.lgl_entity_sub_nm = null; } else {
      this.lgl_entity_sub_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ntwk_id = null; } else {
      this.ntwk_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_nm = null; } else {
      this.ntwk_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_local_nm = null; } else {
      this.ntwk_local_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_purpose_cd = null; } else {
      this.ntwk_purpose_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_purpose_desc = null; } else {
      this.ntwk_purpose_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_seg_cd = null; } else {
      this.ntwk_seg_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_seg_desc = null; } else {
      this.ntwk_seg_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_credit_card_masking_ind = null; } else {
      this.ntwk_credit_card_masking_ind = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_catalina_trade_class_cd = null; } else {
      this.ntwk_catalina_trade_class_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.ntwk_catalina_trade_class_desc = null; } else {
      this.ntwk_catalina_trade_class_desc = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.addr_ln_1_txt = null; } else {
      this.addr_ln_1_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.addr_ln_2_txt = null; } else {
      this.addr_ln_2_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.city_nm = null; } else {
      this.city_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.postal_cd = null; } else {
      this.postal_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.postal_cd_suffix_cd = null; } else {
      this.postal_cd_suffix_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cntry_cd = null; } else {
      this.cntry_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cntry_subdiv_cd = null; } else {
      this.cntry_subdiv_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cntry_subdiv_nm = null; } else {
      this.cntry_subdiv_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.lat_coord = null; } else {
      this.lat_coord = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.long_coord = null; } else {
      this.long_coord = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.str_phone_nbr = null; } else {
      this.str_phone_nbr = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.mkt_nbr = null; } else {
      this.mkt_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.mkt_nm = null; } else {
      this.mkt_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.rgn_nm = null; } else {
      this.rgn_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.rgn_nbr = null; } else {
      this.rgn_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.district_nbr = null; } else {
      this.district_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.mi_natl_nbr = null; } else {
      this.mi_natl_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.unique_site_tkn_nbr = null; } else {
      this.unique_site_tkn_nbr = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.csdb_chn_nbr = null; } else {
      this.csdb_chn_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.csdb_chn_str_nbr = null; } else {
      this.csdb_chn_str_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.csdb_str_fips_nbr = null; } else {
      this.csdb_str_fips_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.cdsb_td_mkt_dma_cd = null; } else {
      this.cdsb_td_mkt_dma_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.csdb_td_mkt_dma_nm = null; } else {
      this.csdb_td_mkt_dma_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.csdb_td_mkt_msa_mkt_cd = null; } else {
      this.csdb_td_mkt_msa_mkt_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.csdb_td_mkt_msa_mkt_nm = null; } else {
      this.csdb_td_mkt_msa_mkt_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_lgl_entity_key = null; } else {
      this.orig_lgl_entity_key = Long.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_lgl_entity_nbr = null; } else {
      this.orig_lgl_entity_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_lgl_entity_nm = null; } else {
      this.orig_lgl_entity_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_lgl_entity_sub_id = null; } else {
      this.orig_lgl_entity_sub_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_lgl_entity_sub_nm = null; } else {
      this.orig_lgl_entity_sub_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_ntwk_id = null; } else {
      this.orig_ntwk_id = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_ntwk_nm = null; } else {
      this.orig_ntwk_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_site_id_txt = null; } else {
      this.orig_site_id_txt = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.orig_site_nm = null; } else {
      this.orig_site_nm = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_csdb_chn_nbr = null; } else {
      this.orig_csdb_chn_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_csdb_chn_str_nbr = null; } else {
      this.orig_csdb_chn_str_nbr = Integer.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_site_active_dt = null; } else {
      this.orig_site_active_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.orig_site_inactive_dt = null; } else {
      this.orig_site_inactive_dt = java.sql.Date.valueOf(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.utc_offset_hh_nbr = null; } else {
      this.utc_offset_hh_nbr = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.utc_dst_offset_hh_nbr = null; } else {
      this.utc_dst_offset_hh_nbr = new java.math.BigDecimal(__cur_str);
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null")) { this.mngng_cntry_cd = null; } else {
      this.mngng_cntry_cd = __cur_str;
    }

    if (__it.hasNext()) {
        __cur_str = __it.next();
    } else {
        __cur_str = "null";
    }
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.batch_ld_nbr = null; } else {
      this.batch_ld_nbr = Integer.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    site_t o = (site_t) super.clone();
    o.site_active_dt = (o.site_active_dt != null) ? (java.sql.Date) o.site_active_dt.clone() : null;
    o.site_inactive_dt = (o.site_inactive_dt != null) ? (java.sql.Date) o.site_inactive_dt.clone() : null;
    o.site_first_log_seen_dt = (o.site_first_log_seen_dt != null) ? (java.sql.Date) o.site_first_log_seen_dt.clone() : null;
    o.site_last_log_seen_dt = (o.site_last_log_seen_dt != null) ? (java.sql.Date) o.site_last_log_seen_dt.clone() : null;
    o.orig_site_active_dt = (o.orig_site_active_dt != null) ? (java.sql.Date) o.orig_site_active_dt.clone() : null;
    o.orig_site_inactive_dt = (o.orig_site_inactive_dt != null) ? (java.sql.Date) o.orig_site_inactive_dt.clone() : null;
    return o;
  }

  public void clone0(site_t o) throws CloneNotSupportedException {
    o.site_active_dt = (o.site_active_dt != null) ? (java.sql.Date) o.site_active_dt.clone() : null;
    o.site_inactive_dt = (o.site_inactive_dt != null) ? (java.sql.Date) o.site_inactive_dt.clone() : null;
    o.site_first_log_seen_dt = (o.site_first_log_seen_dt != null) ? (java.sql.Date) o.site_first_log_seen_dt.clone() : null;
    o.site_last_log_seen_dt = (o.site_last_log_seen_dt != null) ? (java.sql.Date) o.site_last_log_seen_dt.clone() : null;
    o.orig_site_active_dt = (o.orig_site_active_dt != null) ? (java.sql.Date) o.orig_site_active_dt.clone() : null;
    o.orig_site_inactive_dt = (o.orig_site_inactive_dt != null) ? (java.sql.Date) o.orig_site_inactive_dt.clone() : null;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new HashMap<String, Object>();
    __sqoop$field_map.put("site_key", this.site_key);
    __sqoop$field_map.put("site_nm", this.site_nm);
    __sqoop$field_map.put("site_local_nm", this.site_local_nm);
    __sqoop$field_map.put("site_id_txt", this.site_id_txt);
    __sqoop$field_map.put("site_src_id", this.site_src_id);
    __sqoop$field_map.put("site_data_src_cd", this.site_data_src_cd);
    __sqoop$field_map.put("site_active_dt", this.site_active_dt);
    __sqoop$field_map.put("site_inactive_dt", this.site_inactive_dt);
    __sqoop$field_map.put("site_first_log_seen_dt", this.site_first_log_seen_dt);
    __sqoop$field_map.put("site_last_log_seen_dt", this.site_last_log_seen_dt);
    __sqoop$field_map.put("site_typ_cd", this.site_typ_cd);
    __sqoop$field_map.put("site_typ_nm", this.site_typ_nm);
    __sqoop$field_map.put("site_trade_class_cd", this.site_trade_class_cd);
    __sqoop$field_map.put("site_trade_class_desc", this.site_trade_class_desc);
    __sqoop$field_map.put("lgl_entity_key", this.lgl_entity_key);
    __sqoop$field_map.put("lgl_entity_nbr", this.lgl_entity_nbr);
    __sqoop$field_map.put("lgl_entity_nm", this.lgl_entity_nm);
    __sqoop$field_map.put("lgl_entity_sub_id", this.lgl_entity_sub_id);
    __sqoop$field_map.put("lgl_entity_sub_nm", this.lgl_entity_sub_nm);
    __sqoop$field_map.put("ntwk_id", this.ntwk_id);
    __sqoop$field_map.put("ntwk_nm", this.ntwk_nm);
    __sqoop$field_map.put("ntwk_local_nm", this.ntwk_local_nm);
    __sqoop$field_map.put("ntwk_purpose_cd", this.ntwk_purpose_cd);
    __sqoop$field_map.put("ntwk_purpose_desc", this.ntwk_purpose_desc);
    __sqoop$field_map.put("ntwk_seg_cd", this.ntwk_seg_cd);
    __sqoop$field_map.put("ntwk_seg_desc", this.ntwk_seg_desc);
    __sqoop$field_map.put("ntwk_credit_card_masking_ind", this.ntwk_credit_card_masking_ind);
    __sqoop$field_map.put("ntwk_catalina_trade_class_cd", this.ntwk_catalina_trade_class_cd);
    __sqoop$field_map.put("ntwk_catalina_trade_class_desc", this.ntwk_catalina_trade_class_desc);
    __sqoop$field_map.put("addr_ln_1_txt", this.addr_ln_1_txt);
    __sqoop$field_map.put("addr_ln_2_txt", this.addr_ln_2_txt);
    __sqoop$field_map.put("city_nm", this.city_nm);
    __sqoop$field_map.put("postal_cd", this.postal_cd);
    __sqoop$field_map.put("postal_cd_suffix_cd", this.postal_cd_suffix_cd);
    __sqoop$field_map.put("cntry_cd", this.cntry_cd);
    __sqoop$field_map.put("cntry_subdiv_cd", this.cntry_subdiv_cd);
    __sqoop$field_map.put("cntry_subdiv_nm", this.cntry_subdiv_nm);
    __sqoop$field_map.put("lat_coord", this.lat_coord);
    __sqoop$field_map.put("long_coord", this.long_coord);
    __sqoop$field_map.put("str_phone_nbr", this.str_phone_nbr);
    __sqoop$field_map.put("mkt_nbr", this.mkt_nbr);
    __sqoop$field_map.put("mkt_nm", this.mkt_nm);
    __sqoop$field_map.put("rgn_nm", this.rgn_nm);
    __sqoop$field_map.put("rgn_nbr", this.rgn_nbr);
    __sqoop$field_map.put("district_nbr", this.district_nbr);
    __sqoop$field_map.put("mi_natl_nbr", this.mi_natl_nbr);
    __sqoop$field_map.put("unique_site_tkn_nbr", this.unique_site_tkn_nbr);
    __sqoop$field_map.put("csdb_chn_nbr", this.csdb_chn_nbr);
    __sqoop$field_map.put("csdb_chn_str_nbr", this.csdb_chn_str_nbr);
    __sqoop$field_map.put("csdb_str_fips_nbr", this.csdb_str_fips_nbr);
    __sqoop$field_map.put("cdsb_td_mkt_dma_cd", this.cdsb_td_mkt_dma_cd);
    __sqoop$field_map.put("csdb_td_mkt_dma_nm", this.csdb_td_mkt_dma_nm);
    __sqoop$field_map.put("csdb_td_mkt_msa_mkt_cd", this.csdb_td_mkt_msa_mkt_cd);
    __sqoop$field_map.put("csdb_td_mkt_msa_mkt_nm", this.csdb_td_mkt_msa_mkt_nm);
    __sqoop$field_map.put("orig_lgl_entity_key", this.orig_lgl_entity_key);
    __sqoop$field_map.put("orig_lgl_entity_nbr", this.orig_lgl_entity_nbr);
    __sqoop$field_map.put("orig_lgl_entity_nm", this.orig_lgl_entity_nm);
    __sqoop$field_map.put("orig_lgl_entity_sub_id", this.orig_lgl_entity_sub_id);
    __sqoop$field_map.put("orig_lgl_entity_sub_nm", this.orig_lgl_entity_sub_nm);
    __sqoop$field_map.put("orig_ntwk_id", this.orig_ntwk_id);
    __sqoop$field_map.put("orig_ntwk_nm", this.orig_ntwk_nm);
    __sqoop$field_map.put("orig_site_id_txt", this.orig_site_id_txt);
    __sqoop$field_map.put("orig_site_nm", this.orig_site_nm);
    __sqoop$field_map.put("orig_csdb_chn_nbr", this.orig_csdb_chn_nbr);
    __sqoop$field_map.put("orig_csdb_chn_str_nbr", this.orig_csdb_chn_str_nbr);
    __sqoop$field_map.put("orig_site_active_dt", this.orig_site_active_dt);
    __sqoop$field_map.put("orig_site_inactive_dt", this.orig_site_inactive_dt);
    __sqoop$field_map.put("utc_offset_hh_nbr", this.utc_offset_hh_nbr);
    __sqoop$field_map.put("utc_dst_offset_hh_nbr", this.utc_dst_offset_hh_nbr);
    __sqoop$field_map.put("mngng_cntry_cd", this.mngng_cntry_cd);
    __sqoop$field_map.put("batch_ld_nbr", this.batch_ld_nbr);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("site_key", this.site_key);
    __sqoop$field_map.put("site_nm", this.site_nm);
    __sqoop$field_map.put("site_local_nm", this.site_local_nm);
    __sqoop$field_map.put("site_id_txt", this.site_id_txt);
    __sqoop$field_map.put("site_src_id", this.site_src_id);
    __sqoop$field_map.put("site_data_src_cd", this.site_data_src_cd);
    __sqoop$field_map.put("site_active_dt", this.site_active_dt);
    __sqoop$field_map.put("site_inactive_dt", this.site_inactive_dt);
    __sqoop$field_map.put("site_first_log_seen_dt", this.site_first_log_seen_dt);
    __sqoop$field_map.put("site_last_log_seen_dt", this.site_last_log_seen_dt);
    __sqoop$field_map.put("site_typ_cd", this.site_typ_cd);
    __sqoop$field_map.put("site_typ_nm", this.site_typ_nm);
    __sqoop$field_map.put("site_trade_class_cd", this.site_trade_class_cd);
    __sqoop$field_map.put("site_trade_class_desc", this.site_trade_class_desc);
    __sqoop$field_map.put("lgl_entity_key", this.lgl_entity_key);
    __sqoop$field_map.put("lgl_entity_nbr", this.lgl_entity_nbr);
    __sqoop$field_map.put("lgl_entity_nm", this.lgl_entity_nm);
    __sqoop$field_map.put("lgl_entity_sub_id", this.lgl_entity_sub_id);
    __sqoop$field_map.put("lgl_entity_sub_nm", this.lgl_entity_sub_nm);
    __sqoop$field_map.put("ntwk_id", this.ntwk_id);
    __sqoop$field_map.put("ntwk_nm", this.ntwk_nm);
    __sqoop$field_map.put("ntwk_local_nm", this.ntwk_local_nm);
    __sqoop$field_map.put("ntwk_purpose_cd", this.ntwk_purpose_cd);
    __sqoop$field_map.put("ntwk_purpose_desc", this.ntwk_purpose_desc);
    __sqoop$field_map.put("ntwk_seg_cd", this.ntwk_seg_cd);
    __sqoop$field_map.put("ntwk_seg_desc", this.ntwk_seg_desc);
    __sqoop$field_map.put("ntwk_credit_card_masking_ind", this.ntwk_credit_card_masking_ind);
    __sqoop$field_map.put("ntwk_catalina_trade_class_cd", this.ntwk_catalina_trade_class_cd);
    __sqoop$field_map.put("ntwk_catalina_trade_class_desc", this.ntwk_catalina_trade_class_desc);
    __sqoop$field_map.put("addr_ln_1_txt", this.addr_ln_1_txt);
    __sqoop$field_map.put("addr_ln_2_txt", this.addr_ln_2_txt);
    __sqoop$field_map.put("city_nm", this.city_nm);
    __sqoop$field_map.put("postal_cd", this.postal_cd);
    __sqoop$field_map.put("postal_cd_suffix_cd", this.postal_cd_suffix_cd);
    __sqoop$field_map.put("cntry_cd", this.cntry_cd);
    __sqoop$field_map.put("cntry_subdiv_cd", this.cntry_subdiv_cd);
    __sqoop$field_map.put("cntry_subdiv_nm", this.cntry_subdiv_nm);
    __sqoop$field_map.put("lat_coord", this.lat_coord);
    __sqoop$field_map.put("long_coord", this.long_coord);
    __sqoop$field_map.put("str_phone_nbr", this.str_phone_nbr);
    __sqoop$field_map.put("mkt_nbr", this.mkt_nbr);
    __sqoop$field_map.put("mkt_nm", this.mkt_nm);
    __sqoop$field_map.put("rgn_nm", this.rgn_nm);
    __sqoop$field_map.put("rgn_nbr", this.rgn_nbr);
    __sqoop$field_map.put("district_nbr", this.district_nbr);
    __sqoop$field_map.put("mi_natl_nbr", this.mi_natl_nbr);
    __sqoop$field_map.put("unique_site_tkn_nbr", this.unique_site_tkn_nbr);
    __sqoop$field_map.put("csdb_chn_nbr", this.csdb_chn_nbr);
    __sqoop$field_map.put("csdb_chn_str_nbr", this.csdb_chn_str_nbr);
    __sqoop$field_map.put("csdb_str_fips_nbr", this.csdb_str_fips_nbr);
    __sqoop$field_map.put("cdsb_td_mkt_dma_cd", this.cdsb_td_mkt_dma_cd);
    __sqoop$field_map.put("csdb_td_mkt_dma_nm", this.csdb_td_mkt_dma_nm);
    __sqoop$field_map.put("csdb_td_mkt_msa_mkt_cd", this.csdb_td_mkt_msa_mkt_cd);
    __sqoop$field_map.put("csdb_td_mkt_msa_mkt_nm", this.csdb_td_mkt_msa_mkt_nm);
    __sqoop$field_map.put("orig_lgl_entity_key", this.orig_lgl_entity_key);
    __sqoop$field_map.put("orig_lgl_entity_nbr", this.orig_lgl_entity_nbr);
    __sqoop$field_map.put("orig_lgl_entity_nm", this.orig_lgl_entity_nm);
    __sqoop$field_map.put("orig_lgl_entity_sub_id", this.orig_lgl_entity_sub_id);
    __sqoop$field_map.put("orig_lgl_entity_sub_nm", this.orig_lgl_entity_sub_nm);
    __sqoop$field_map.put("orig_ntwk_id", this.orig_ntwk_id);
    __sqoop$field_map.put("orig_ntwk_nm", this.orig_ntwk_nm);
    __sqoop$field_map.put("orig_site_id_txt", this.orig_site_id_txt);
    __sqoop$field_map.put("orig_site_nm", this.orig_site_nm);
    __sqoop$field_map.put("orig_csdb_chn_nbr", this.orig_csdb_chn_nbr);
    __sqoop$field_map.put("orig_csdb_chn_str_nbr", this.orig_csdb_chn_str_nbr);
    __sqoop$field_map.put("orig_site_active_dt", this.orig_site_active_dt);
    __sqoop$field_map.put("orig_site_inactive_dt", this.orig_site_inactive_dt);
    __sqoop$field_map.put("utc_offset_hh_nbr", this.utc_offset_hh_nbr);
    __sqoop$field_map.put("utc_dst_offset_hh_nbr", this.utc_dst_offset_hh_nbr);
    __sqoop$field_map.put("mngng_cntry_cd", this.mngng_cntry_cd);
    __sqoop$field_map.put("batch_ld_nbr", this.batch_ld_nbr);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if (!setters.containsKey(__fieldName)) {
      throw new RuntimeException("No such field:"+__fieldName);
    }
    setters.get(__fieldName).setField(__fieldVal);
  }

}
