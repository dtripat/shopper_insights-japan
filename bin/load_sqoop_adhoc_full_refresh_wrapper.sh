#############################################################################################################
#                               General Details                                                             #
#############################################################################################################
# Name        : Datametica                                                                                  #
# File        : load_sqoop_adhoc_full_refresh_wrapper.sh                                                    #
# Description : This script performs creating all hive tables                                               #
# Inputs      : Sheduled date & Hive objects file name                                                      #
# Note        : This script performs sqoop ingestion from netezza database into hive refined                #
#               adhoc full fresh tables                                                                     #
#############################################################################################################
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

# Function to print the logs with current utc time 
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly four arguments"
        log_print "[INFO:] USAGE : "$0" <Sheduled date(YYYY-MM-DD)> <source db name> <table name> <field to extract>"
        exit 1
}
load_resource()
{
        log_print "[INFO:] Loading  $1 ."
        . $1
        if [ $? -eq 0 ]
        then    
            log_print "[INFO:] $1 Loaded successfully "
        else
            log_print "[ERROR:] $1 Loading  failed " 
            exit 1
        fi
}

if [ $# -ne 4 ]
then
    show_usage
fi

echo "****************Variable info*****************"
START_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
START_TIME_UTC=$(date +%s)
SHEDULED_DATE=$1
SRC_DB_NAME=$2
TABLE_NAME=$3
EXTRACT_COLUMN=$4
CEDL_BATCH_DATE=$(date -d "$SHEDULED_DATE - 1 days" +%Y-%m-%d)
LOG_DATE=$(date -d "$SHEDULED_DATE - 1 days" +%Y%m%d)
DIR_TIME=$(date -u +'%Y%m%d_%H%M%S')
ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"

# Loading environment based properties file.
load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties

#Jobname for Audit: <lnd/ref/app>_ <modulename>_<sourcedb>_<tablename>
JOB_NAME=$(echo ${REF_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME})
#Creating directory for logging 

mkdir -p  "$CEDL_BASE_PATH"/log/"$LOG_DATE"/

if [ $? -eq 0 ]
then
        log_print "[INFO:] Log path directory created $CEDL_BASE_PATH/log/$LOG_DATE/ "
else
        log_print "[INFO:] Log path directory creation $CEDL_BASE_PATH/log/$LOG_DATE/"
        exit 1
        
fi
log_print "[INFO:] Logs Directory location: ${CEDL_BASE_PATH}/log/${LOG_DATE}/${JOB_NAME}_${DIR_TIME}.log "

{
log_print "[INFO:] Wrapper Script Name : $0"
log_print "[INFO:] The value of all command-line arguments <Sheduled date(YYYY-MM-DD)> <name of the cedl hive objects file name>  : $*"
log_print "[INFO:] Job started time = $START_TIME "
log_print "[INFO:] Environment source code  path = $CEDL_BASE_PATH"
log_print "[INFO:] JOB Name = $JOB_NAME"

#Extracting source credentials
EDW4X_DB_HOST_IP_PORT=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f1`
EDW4X_DB_HOST_USER=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f2`
EDW4X_DB_HOST_PASSWORD=`echo ${CEDL_EDW4X_SOURCE_CREDENTIAL} | cut -d '|' -f3-`
SQOOP_CONNNECTION_URL="jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} --username ${EDW4X_DB_HOST_USER}  --password ${EDW4X_DB_HOST_PASSWORD}"

#Extracting MYSQL Audit Connection Related parameters 
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1` 
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}" 

#Extracting Beeline Connection Related parameters 
CEDL_BEELINE_HOST_NAME_PORT=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f1`
CEDL_BEELINE_USER=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f2`
CEDL_BEELINE_PASSWORD=`echo ${CEDL_BEELINE_CREDENTIAL} | cut -d '|' -f3-`
#BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/default -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"
BEELINE_CONNECTION_URL="beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD}"

log_print "*************SQOOP IMPORT JOB STARTS*******************"

#Populating sqoop related variables 
TEMP_DATA_DIRECTORY=$CEDL_ADLS_ENV_BUCKET/$TEMP_ADLS_BASEPATH/$SRC_DB_NAME.db/$TABLE_NAME
REF_DATA_DIRECTORY=$CEDL_ADLS_ENV_BUCKET/$REF_ADLS_BASEPATH/$SRC_DB_NAME.db/$TABLE_NAME
HIVE_TEMP_DB_NAME=$(echo ${TEMP_DB_PREFIX}_${SRC_DB_NAME})
HIVE_REF_DB_NAME=$(echo ${REF_DB_PREFIX}_${SRC_DB_NAME})

# Checking sqoop target location .
hdfs dfs -test -d $TEMP_DATA_DIRECTORY
if [ $? -eq 0 ] 
then
                log_print "[INFO :] TEMP_DATA_DIRECTORY path avaiable : $TEMP_DATA_DIRECTORY "
else
                log_print "[INFO :] $TEMP_DATA_DIRECTORY path not avaiable " 
                log_print "[INFO :] Creating TEMP_DATA_DIRECTORY path: $TEMP_DATA_DIRECTORY"
                hdfs dfs -mkdir -p $TEMP_DATA_DIRECTORY 
                if [ $? -eq 0 ]  
                 then
                   log_print "[INFO :] TEMP_DATA_DIRECTORY path created successfully : $TEMP_DATA_DIRECTORY " 
                    # Deleting the sqoop target location, if it already exists .            
                    hdfs dfs -rm -r  $TEMP_DATA_DIRECTORY/* 
                    if [ $? -eq 0 ]  
                    then
                    log_print "[INFO:] Files deleted successfully  in $TEMP_DATA_DIRECTORY "               
                    else 
                    log_print "[INFO:] $TEMP_DATA_DIRECTORY no files to be delete  "
                    fi
                else 
                   log_print "[ERROR :] $TEMP_DATA_DIRECTORY path creation failed  "
                 fi
                 
fi 
# Dropping & creating the target temp table .
log_print "DROP TABLE IF EXISTS ${HIVE_TEMP_DB_NAME}.${TABLE_NAME};
        CREATE TABLE ${HIVE_TEMP_DB_NAME}.${TABLE_NAME} STORED AS ORC LOCATION '${TEMP_DATA_DIRECTORY}' 
        TBLPROPERTIES ('orc.compress'='ZLIB','transactional'='false') as SELECT * FROM ${HIVE_REF_DB_NAME}.${TABLE_NAME}
        WHERE 1=0;"

${BEELINE_CONNECTION_URL} -e " DROP TABLE IF EXISTS ${HIVE_TEMP_DB_NAME}.${TABLE_NAME};
        CREATE TABLE ${HIVE_TEMP_DB_NAME}.${TABLE_NAME} STORED AS ORC LOCATION '${TEMP_DATA_DIRECTORY}' 
        TBLPROPERTIES ('orc.compress'='ZLIB','transactional'='false') as SELECT * FROM ${HIVE_REF_DB_NAME}.${TABLE_NAME} WHERE 1=0;"

    if [ $? -eq 0 ]  
    then
    log_print "[INFO:] Target Tempoprary table  ${HIVE_TEMP_DB_NAME}.${TABLE_NAME} created successfully"               
    else 
    log_print "[INFO:] Target Tempoprary table  ${HIVE_TEMP_DB_NAME}.${TABLE_NAME} creation failed  "
	exit 1
    fi      
        
rm ${CEDL_BASE_PATH}/tmp/sqoop_eval_${SRC_DB_NAME}_${TABLE_NAME}.txt

sqoop eval --connect ${SQOOP_CONNNECTION_URL} --query "select distinct ${EXTRACT_COLUMN} from ${TABLE_NAME} " | sed 's/|//g;s/-//g;/^\s*$/d;1,4d;s/^.//;s/ *$//' > ${CEDL_BASE_PATH}/tmp/sqoop_eval_${SRC_DB_NAME}_${TABLE_NAME}.txt


IFS=$'\n' # make newlines the only separator
for i in `cat ${CEDL_BASE_PATH}/tmp/sqoop_eval_${SRC_DB_NAME}_${TABLE_NAME}.txt`
do
        
        EXTRACT_COLUMN_VALUE=$i
        log_print "[INFO:] Distinct column value  : ${EXTRACT_COLUMN_VALUE}"
        log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
        --table $TABLE_NAME \
        --direct \
        --where '${EXTRACT_COLUMN} = ${EXTRACT_COLUMN_VALUE}' \
        --hcatalog-database $HIVE_TEMP_DB_NAME \
        --hcatalog-table $TABLE_NAME \
        --hcatalog-storage-stanza 'stored as orc tblproperties (orc.compress=ZLIB)' \
        --escaped-by '\' \
        -m $DEF_SQOOP_ADHOC_FULL_REFRESH_MAPPER_CNT"
        sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} --username ${EDW4X_DB_HOST_USER}  --password ${EDW4X_DB_HOST_PASSWORD} \
             --table $TABLE_NAME \
             --direct \
             --where "${EXTRACT_COLUMN} = '${EXTRACT_COLUMN_VALUE}' " \
             --hcatalog-database $HIVE_TEMP_DB_NAME \
             -hcatalog-table $TABLE_NAME   \
             --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
             --escaped-by '\' \
             -m $DEF_SQOOP_ADHOC_FULL_REFRESH_MAPPER_CNT
    if [ $? -eq 0 ]
        then
            log_print "[INFO:] sqoop job success  "
    else
        log_print "[INFO:] sqoop job failed"
    exit 1
        
fi

done

if [ $? -eq 0 ]
then
             log_print "Success : Data Ingested into  directory $TEMP_DATA_DIRECTORY"
             # Removing refine data  .
             hdfs dfs -rm -r  $REF_DATA_DIRECTORY/* 
             
             #Moving the data from temporary table location to refine location 
             hdfs dfs -mv $TEMP_DATA_DIRECTORY/* $REF_DATA_DIRECTORY
             
             # Getting hive incoming table count for auditing .
             TRGT_TABLE_RECORD_COUNT=`beeline -u jdbc:hive2://${CEDL_BEELINE_HOST_NAME_PORT}/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2 -n ${CEDL_BEELINE_USER} -p ${CEDL_BEELINE_PASSWORD} --silent=true -e "select count(*) cnt from $HIVE_REF_DB_NAME.$TABLE_NAME" | grep -Po "\d+"`                
             if [ $? -eq 0 ]
             then
                    log_print "[INFO:] Target hive table count = $TRGT_TABLE_RECORD_COUNT"
                    JOB_STAT_NM="success"
                    INSERT_ROWS_CNT=$TRGT_TABLE_RECORD_COUNT
             else
                    log_print "[ERROR:] Failed to get Target hive table records count" 
                    JOB_STAT_NM="failure"
                    INSERT_ROWS_CNT=NULL
fi
else
             log_print "[ERROR:] Data Sqoop Ingestion Job is Failed" 
             JOB_STAT_NM="failure"
             INSERT_ROWS_CNT=NULL 
fi
        
log_print "**************************Audit entry***********************************" 

#Audit my sql details

#Populating job key value without any extra log info 
JOB_KEY=`mysql -h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD} -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '$JOB_NAME' "` 

log_print "[INFO:] JOB_KEY : $JOB_KEY"

#Not Applicable fields are intialised to Null  
UPD_ROWS_CNT=NULL     
DEL_ROWS_CNT=NULL     
MOST_RECENT_VAL_TXT=NULL
VAL_DATATYPE_NM=NULL

#End time 
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Executing audit inserting query"
log_print "[INFO:] Query : insert into cedl_operations.cedl_audit_t
                                        (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                        src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                        insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                        begin_tms,end_tms) 
                            values 
                                        ($JOB_KEY,'$START_TIME','$JOB_NAME','$JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                        '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$HIVE_TRGT_DB_NAME','$TABLE_NAME',
                                        $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,$MOST_RECENT_VAL_TXT,$VAL_DATATYPE_NM,
                                        '$START_TIME','$END_TIME');"

mysql -h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD} -e "insert into cedl_operations.cedl_audit_t
                                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                                begin_tms,end_tms) 
                                       values      ($JOB_KEY,'$START_TIME','$JOB_NAME','$JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$HIVE_TRGT_DB_NAME','$TABLE_NAME',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,$MOST_RECENT_VAL_TXT,$VAL_DATATYPE_NM,
                                                '$START_TIME','$END_TIME');" 

if [ $? -eq 0 ] 
then
                log_print "[INFO:] $JOB_STAT_NM Job record inserted into audit table"
else
                log_print "[INFO:]  $JOB_STAT_NM record into audit table" 
                log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r  $TEMP_DATA_DIRECTORY/*"
                hdfs dfs -rm -r  $TEMP_DATA_DIRECTORY/* 
                if [ $? -eq 0 ]  
                    then
                        log_print "[INFO:] Files deleted successfully  in $TEMP_DATA_DIRECTORY "               
                    else 
                        log_print "[ERROR:] $TEMP_DATA_DIRECTORY files deletion failed  "
                fi  
                exit 1                
fi        
        
END_TIME_UTC=$(date +%s)
log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
log_print "[INFO:] Job Completed successfully."
 
} >> "${CEDL_BASE_PATH}"/log/"${LOG_DATE}"/${JOB_NAME}_${DIR_TIME}.log 2>&1;
