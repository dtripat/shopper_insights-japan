################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : Audit_Tables_Entry_wrapper.sh                                  #
# Description : This script make entry for the New Ingestion Job for Netezza   #
#                               table                                                                                      #
################################################################################
export ENV=prod
export HOME=/home/svc-edm
export COMMON_RESOURCE_BASE_PATH=/apps/common/resources

# Usage descriptor for invalid input arguments to the wrapper
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}


Show_Usage()
{

if [ "$LOAD_TYPE" == "full" ]
then
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[ERROR:] Invalid arguments please pass exactly four arguments"
        log_print "Usage: "$0" <databasename> <tablename> <load_type-full/incremental/upsert> < Refine Script Name> "
        exit 1

else
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        log_print "[ERROR:] Invalid arguments please pass exactly seven arguments"
        log_print "Usage: "$0" <databasename> <tablename> <load_type-full/incremental/upsert> < Refine Script Name> <Delta Column Name> <Landing Script Name> <BATCH_LD_NBR_VALUE> "
        exit 1

fi

}

load_resource()
{
        log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        . $1
        if [ $? -eq 0 ]
        then
            log_print "[INFO:] Successfully $1 Loaded "
        else
            log_print "[ERROR:] Failed to $1 Load "
            exit 1
        fi
}

LOAD_TYPE="$3"

if [ "$LOAD_TYPE" == "full" ]
then
        if [ $# -lt 4 ]
        then
        Show_Usage
        fi

else
        if [ $# -lt 7 ]
        then
        Show_Usage
        fi

fi



TABLE_NAME="$2"
SRC_DB_NAME="$1"
DELTA_COLUMN="$5"
REF_SCRIPT_NAME="/apps/data-ingestion/edw4x/bin/$4"
LND_SCRIPT_NAME="/apps/data-ingestion/edw4x/bin/$6"
BATCH_LD_NBR_VAL="$7"

if [ -z "${BATCH_LD_NBR_VAL}" ]
then
        BATCH_LD_NBR_VAL="0"
fi

DIR_TIME=$(date -u +'%Y-%m-%d %H:%M:%S')
LOG_DATE=$(date +'%Y%m%d')

ENVIRONMENT_FILE="cedl-env-$ENV.properties"
PASSWORD_FILE=".passwd"


# Loading environment based properties file.
load_resource ${COMMON_RESOURCE_BASE_PATH}/${ENVIRONMENT_FILE}

# Loading environment based properties file.
load_resource $HOME/${PASSWORD_FILE}

# Loading edw4x common properties file .
load_resource $CEDL_BASE_PATH/resources/edw4x_data_ingestion.properties


#Extracting MYSQL Audit Connection Related parameters
CEDL_AUDITDB_IP=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f1`
CEDL_AUDITDB_USER=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f2`
CEDL_AUDITDB_PASSWORD=`echo ${CEDL_AUDIT_MYSQL_CREDENTIAL} | cut -d '|' -f3-`
AUDIT_MYSQL_CONNECTION_URL="-h ${CEDL_AUDITDB_IP} -u ${CEDL_AUDITDB_USER} -p${CEDL_AUDITDB_PASSWORD}"

#Jobname for Audit:
LND_JOB_NAME="${LND_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"
REF_JOB_NAME="${REF_DB_PREFIX}_${SRC_DB_NAME}_${TABLE_NAME}"

echo "LND_JOB_NAME = ${LND_JOB_NAME}"
echo "REF_JOB_NAME = ${REF_JOB_NAME}"

LND_LOG_FILE="${CEDL_BASE_PATH}/log/YYYYMMDD/${LND_JOB_NAME}_%Y%m%d_%H%M%S.log"
REF_LOG_FILE="${CEDL_BASE_PATH}/log/YYYYMMDD/${REF_JOB_NAME}_%Y%m%d_%H%M%S.log"



if [ "$LOAD_TYPE" == "full" ]
then
                #########################Make Entry in Audit Table : cedl_operations.cedl_job_t###########################################

                        echo "[INFO:]Making Entry for refine Job : ${REF_JOB_NAME}  in audit table cedl_operations.cedl_job_t "

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_job_t(job_nm,job_grp_nm,parm_txt,log_file_desc,create_tms)
                        values ('${REF_JOB_NAME}','${REF_JOB_NAME}','${REF_SCRIPT_NAME} <Scheduled date(YYYY-MM-DD)> ${SRC_DB_NAME} ${TABLE_NAME}','${REF_LOG_FILE}','${DIR_TIME}')"`
                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit table cedl_job_t for job : ${REF_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit table cedl_job_t for job : ${REF_JOB_NAME}"
                        fi
else

                #########################Make Entry in Audit Table : cedl_operations.cedl_job_t###########################################

                        echo "[INFO:]Making Entry for landing Job : ${LND_JOB_NAME} in audit table cedl_operations.cedl_job_t "

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_job_t(job_nm,job_grp_nm,parm_txt,log_file_desc,create_tms)
                        values ('${LND_JOB_NAME}','${LND_JOB_NAME}','${LND_SCRIPT_NAME}','${LND_LOG_FILE}','${DIR_TIME}')"`
                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit table cedl_job_t for job : ${LND_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit table cedl_job_t for job : ${LND_JOB_NAME}"
                        fi

                        echo "[INFO:]Making Entry for refine Job : ${REF_JOB_NAME}  in audit table cedl_operations.cedl_job_t "

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_job_t(job_nm,job_grp_nm,parm_txt,log_file_desc,create_tms)
                        values ('${REF_JOB_NAME}','${REF_JOB_NAME}','${REF_SCRIPT_NAME}','${REF_LOG_FILE}','${DIR_TIME}')"`
                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit table cedl_job_t for job : ${REF_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit table cedl_job_t for job : ${REF_JOB_NAME}"
                        fi

                        #################Make Entry in Audit Table : cedl_operations.cedl_change_data_capture_t###############################
                        echo "[INFO:] Making Entry for landing Job : ${LND_JOB_NAME} in audit table cedl_operations.cedl_change_data_capture_t "

                        LND_JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select max(job_key) from cedl_operations.cedl_job_t where job_nm= '${LND_JOB_NAME}' "`

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_change_data_capture_t    (job_key,job_nm,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,src_column_nm,most_recent_val_txt,val_datatype_nm,cdc_tms) values (${LND_JOB_KEY},'${LND_JOB_NAME}','edw4x','netezza','${SRC_DB_NAME}','${TABLE_NAME}','${DELTA_COLUMN}','${BATCH_LD_NBR_VAL}','INT','1970-10-10 00:00:00')"`

                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit cedl_change_data_capture_t table for job : ${LND_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit cedl_change_data_capture_t table for job : ${LND_JOB_NAME}"
                        fi

                        #####################

                        echo "[INFO:] Making Entry for landing Job : ${REF_JOB_NAME} in audit table cedl_operations.cedl_change_data_capture_t "

                        REF_JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select max(job_key) from cedl_operations.cedl_job_t where job_nm= '${REF_JOB_NAME}' "`

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_change_data_capture_t    (job_key,job_nm,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,src_column_nm,most_recent_val_txt,val_datatype_nm,cdc_tms) values (${REF_JOB_KEY},'${REF_JOB_NAME}','edw4x','netezza','${SRC_DB_NAME}','${TABLE_NAME}','${DELTA_COLUMN}','${BATCH_LD_NBR_VAL}','INT','1970-10-10 00:00:00')"`

                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit cedl_change_data_capture_t table for job : ${REF_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit cedl_change_data_capture_t table for job : ${REF_JOB_NAME}"
                        fi

                        ############################################################################################################
                        #################Make Entry in Audit Table : cedl_operations.batch_mapping_t###############################
                        echo "[INFO:] Making Entry for landing Job : ${LND_JOB_NAME} in audit table cedl_operations.batch_mapping_t"

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_batch_mapping_t (job_nm, table_partition, min_batch_ld_nbr,max_batch_ld_nbr ) values ('${LND_JOB_NAME}','1970-10-10','0000','00000')"`

                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit batch_mapping_t table for job : ${LND_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit batch_mapping_t table for job : ${LND_JOB_NAME}"
                        fi


                        echo "[INFO:] Making Entry for landing Job : ${REF_JOB_NAME} in audit table cedl_operations.batch_mapping_t"

                        `mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_batch_mapping_t (job_nm, table_partition, min_batch_ld_nbr,max_batch_ld_nbr ) values ('${REF_JOB_NAME}','1970-10-10','${BATCH_LD_NBR_VAL}','${BATCH_LD_NBR_VAL}')"`

                        if [ $? -eq 0 ]
                        then
                                        log_print  "[INFO:] successfully Inserted record into audit batch_mapping_t table for job : ${REF_JOB_NAME}"
                        else

                                        log_print  "[INFO:] failed to insert record into audit batch_mapping_t table for job : ${REF_JOB_NAME}"
                        fi

fi

