SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_HOME=`dirname $SCRIPT_PATH`

#Loading namespace_dev.properties/common_function.sh / function.sh
.  ${SCRIPT_HOME}/../config/namespace_prod.properties
.  ${SCRIPT_HOME}/../Common/file_functions.sh
.  ${SCRIPT_HOME}/../Common/common_function.sh
.  ${SCRIPT_HOME}/../Common/functions.sh

job_name=$1

max_value=`mysql $AUDIT_MYSQL_CONNECTION_URL -e "select max(max_batch_ld_nbr) from cedl_operations.cedl_batch_mapping_t where job_nm='${job_name}';"| grep -Po "\d+" |sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`


echo "max value=$max_value"

most_recent_value=`mysql $AUDIT_MYSQL_CONNECTION_URL -e "select most_recent_val_txt from  cedl_operations.cedl_change_data_capture_t where job_nm='$job_name';"| grep -Po "\d+" |sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'`
echo "most recent value=${most_recent_value}"
