#!/bin/bash
exec >> /apps/data-ingestion/edw4x/bin/out.txt

ip_address="vm-pr-eastus2-10-165-17-0-24-master2" ##Put RM Host IP
uname="mansari"
upass="chikku@123@"
email_id="Mohammad.Javedansari@catalina.com"
b=$(curl --silent -v -u "$uname":"$upass" -H "Accept: application/json" -H "X-Requested-By: ambari" -i -X GET -k http://$ip_address:8088/ws/v1/cluster/metrics|grep -o -P -m 1 -h  '(?<=allocatedMB).*(?=,)'|cut -d ':' -f 2|cut -d ',' -f 1)

a=$(curl --silent -v -u "$uname":"$upass" -H "Accept: application/json" -H "X-Requested-By: ambari" -i -X GET -k http://$ip_address:8088/ws/v1/cluster/metrics|grep -o -P -m 1 -h  '(?<=allocatedVirtualCores).*(?=,)'|cut -d ':' -f 2|cut -d ',' -f 1 )

totb=$(curl --silent -v -u "$uname":"$upass" -H "Accept: application/json" -H "X-Requested-By: ambari" -i -X GET -k http://$ip_address:8088/ws/v1/cluster/metrics|grep -o -P -m 1 -h  '(?<=totalMB).*(?=,)'|cut -d ':' -f 2|cut -d ',' -f 1)

tota=$(curl --silent -v -u "$uname":"$upass" -H "Accept: application/json" -H "X-Requested-By: ambari" -i -X GET -k http://$ip_address:8088/ws/v1/cluster/metrics|grep -o -P -m 1 -h  '(?<=totalVirtualCores).*(?=,)'|cut -d ':' -f 2|cut -d ',' -f 1 )

perb=$(awk "BEGIN {print ($b/$totb)*100}")
perb=$perb"%"
pera=$(awk "BEGIN {print ($a/$tota)*100}")
pera=$pera"%"

sr_no="SerialNo."
DATE=`date +%Y-%m-%d`
timestamp=`date '+%H:%M:%S'`
DATE_="Date"
timestamp_="Time"
used_mem="AllocatedMemory(%)"
virtual_cores="AllocatedVirtualCores(%)"
touch utilization.txt
touch alert.txt
n=0
pattern=$(cat utilization.txt|grep "Serial"| wc -l)
if [ "$pattern" = 0 ];then
final_str=$sr_no"|"$DATE_"|"$timestamp_"|"$used_mem"|"$virtual_cores
echo "$final_str" > utilization.txt
fi
n=$(cat utilization.txt |cut -d '|' -f 1|wc -l)
final_str=$n"|"$DATE"|"$timestamp"|"$perb"|"$pera
echo "$final_str" >> utilization.txt

if [ "$n" == 24 ];then
column -t -s"|" utilization.txt >alert.txt
echo "YARN Alert:"|mail -s "Yarn Utilization Alert" -a alert.txt $email_id
rm -rf utilization.txt alert.txt
fi
