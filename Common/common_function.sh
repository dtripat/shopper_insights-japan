################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : common_function.sh                                                         #
# Description :                                                                #
#                                                                              #
################################################################################

# Function to print the logs with current utc time
fn_log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

#Function to source files \ resources
fn_load_resource()
{
        fn_log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        . $1
        if [ $? -eq 0 ]
        then
            fn_log_print "[INFO:] Successfully Loaded $1"
        else
            fn_log_print "[ERROR:] Failed to Load $1"
            exit 1
        fi
}

#function to get columns list for landing layer table that is to be processed
fn_get_column_lists() {
fn_log_print "[INFO:] Function Name : ${FUNCNAME}()"
fn_log_print "[INFO:] The value of all command-line arguments : $*"
LND_DB_NAME=$1
LND_TABLE_NAME=$2
rm $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
rm $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

${BEELINE_CONNECTION_URL} --silent=true -e "show columns in ${LND_DB_NAME}.${LND_TABLE_NAME}" > $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt
cat $CEDL_BASE_PATH/tmp/column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt |sed 's/|$//g'|sed 's/^|//g'|sed -n '4,$p' | sed '$d'| sed ':a;N;$!ba;s/\n/,/g'|sed 's/ //g'| sed 's/,'"${PARTITION_COL}"'//g'  > $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt

COLUMNS_LIST=`cat $CEDL_BASE_PATH/tmp/final_column_list_${LND_DB_NAME}_${LND_TABLE_NAME}.txt`
fn_log_print "COLUMNS_LIST:$COLUMNS_LIST"
}

#Function to create directory
fn_create_dir() {
mkdir -p  $1
if [ $? -eq 0 ]
then
        fn_log_print "[INFO:] Success :  Path directory created $1 "
else
        fn_log_print "[ERROR:] Failed  :  Path directory creation $1"
        exit 1
fi
}

fn_remove_hdfs_dir(){

hdfs dfs -test -d $1
        if [ $? -eq 0 ]
        then
                hdfs dfs -rm -r $1
                if [ $? -eq 0 ]
                then

                        fn_log_print "[INFO:] Files deleted successfully in $1"

                else

                        fn_log_print "[ERROR:] $1 files deletion failed"

                fi
        fi

}
#Function to check if variable is empty or not
function fn_assert_variable_is_set() {
VARIABLE_NAME=$1
VARIABLE_VALUE=$2
if [ ! -z "$VARIABLE_VALUE" ]; then
    echo "$VARIABLE_NAME set to $VARIABLE_VALUE"
else
    variablesNotSetExit $VARIABLE_NAME
	exit 1
fi
}

#function to check data type of any variable whether it is a numeric / date type
function fn_check_data_type(){
var=$1
echo $var|egrep '^[0-9]+$'
    if [ $? -eq 0 ]; then
       DATA_TYPE="NUMERIC"
    else
        DATA_TYPE="DATE"

    fi
}

#Function to print variable if set
function fn_print_variable_is_set() {
VARIABLE_NAME=$1
VARIABLE_VALUE=$2
if [ ! -z "$VARIABLE_VALUE" ]; then
    echo "$VARIABLE_NAME = $VARIABLE_VALUE"
fi

}





############################## Append Functions #########################################


fn_sqoop_eval_for_bch_lst_nrml (){

sqoop eval --connect  $SQOOP_CONNNECTION_URL --query "select distinct ${ICR_LOAD_COL_NM} from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM} > '${LAST_PRC_BATCH_LD_NBR}' order by ${ICR_LOAD_COL_NM}"

}



fn_sqoop_eval_for_bch_lst_rerun_range(){

sqoop eval  --connect $SQOOP_CONNNECTION_URL  --query "select distinct $ICR_LOAD_COL_NM from $LND_TABLE_NAME where ${ICR_LOAD_COL_NM} >= '${MIN_BATCH_LD_NBR}' and ${ICR_LOAD_COL_NM} <= '${MAX_BATCH_LD_NBR}'"
}


fn_show_usage_for_ingest_sqoop_append()
{
        fn_log_print "[INFO:] Function Name : ${FUNCNAME}()"
        fn_log_print "[INFO:] The value of all command-line arguments : $*"
        fn_log_print "[ERROR:] Invalid arguments please pass atleast three arguments"
        fn_log_print "[INFO:] Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename>"
        exit 1
}

