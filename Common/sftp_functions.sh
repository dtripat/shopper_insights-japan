################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : sftp_function.sh                                     		   #
# Description :                                                                #
#                                                                              #
################################################################################

#Function to Copy the zip files from SFTP server to Linux location.

function fn_copy_from_sftp_to_local()
{
fn_remove_and_create_dir ${ZIPPED_FILE_LOCATION}
fn_remove_and_create_dir "${FILE_INGESTION_BASE_PATH}/tmp/"
if [ $? -eq 1 ]
then
return 1
fi
DATE_LIST_FILE="${FILE_INGESTION_BASE_PATH}/tmp/date_list.txt"
fn_create_file ${DATE_LIST_FILE}
if [ $? -eq 1 ]
then
return 1
fi
INCOMING_FILE_CHECK_LIST="${FILE_INGESTION_BASE_PATH}/tmp/incoming_file_check_list.txt"
fn_create_file ${INCOMING_FILE_CHECK_LIST}
if [ $? -eq 1 ]
then
return 1
fi

SFTP_FILE_DATE_LIST=`echo "ls -ltr  catalina_*.zip" |sftp ${INFOSCOUT_SFTP_USER_AND_HOST}:${INFOSCOUT_SFTP_FILE_LOCATION}/ | grep "catalina_.........zip" | awk '{print $9}' | awk -F"[_.]" '{print $2}'`
if [ $? -eq 0 ];then
log_print "[INFO:] SUCCESFULLY EXTRACTED ZIP FILES DATE LIST FROM THE SFTP LOCATION "	
else
log_print "[INFO:] FAILED TO EXTRACT ZIP FILES DATE LIST FROM THE SFTP LOCATION "	
log_print "[INFO:] PLEASE CHECK THE SFTP SERVER CONNECTION MANUALLY"
return 1
fi

LAST_PROCESSED_VALUE=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select most_recent_val_txt from cedl_operations.cedl_audit_t where job_nm= '${SFTP_JOB_NAME}' and job_stat_nm='success' order by audit_key desc limit 1"` 
if [ $? -eq 0 ];then
  log_print "[INFO:] SUCCESFULLY EXTRACTED LAST_PROCESSED_VALUE :${LAST_PROCESSED_VALUE} FOR THE ZIP FILE PROCESSED IN THE LAST EXECUTION"
  else
  log_print "[INFO:] FAILED TO EXTRACT LAST_PROCESSED_VALUE  OF THE ZIP FILE PROCESSED IN THE LAST EXECUTION "
 return 1
  fi

for FILE_DATE in ${SFTP_FILE_DATE_LIST}
do
FORMAT_CHECK=`echo ${FILE_DATE} | awk -F '"' '{ for(i=1;i<=NF;i++) if($i ~ "^([0-9]){8}$") print $i }'`
DATE_FORMAT_CHECK=`date '+%Y%m%d' -d "${FILE_DATE}" 2>/dev/null`

if [ ! -z "${FORMAT_CHECK}" -a  ! -z "${DATE_FORMAT_CHECK}" ]
then

        if [ "$FILE_DATE" -gt "$LAST_PROCESSED_VALUE" ];
        then
        echo ${FILE_DATE} >> ${DATE_LIST_FILE}
			if [ $? -eq 0 ];then
			log_print "[INFO:] SUCCESFULLY WRITING FILE DATE ${FILE_DATE} TO FILE :${DATE_LIST_FILE}"	
			else
			log_print "[INFO:] FAILED TO WRITE FILE DATE ${FILE_DATE} TO FILE :${DATE_LIST_FILE}"	
			return 1
			fi
        fi
fi

done
DATE_COUNT=` cat ${DATE_LIST_FILE} | wc -l `
if [ ${DATE_COUNT} -eq 0 ];
then
log_print "[INFO:] NO NEW ZIP FILES TO PROCESS AT SFTP LOCATION. LAST ZIP FILE PROCESSESED = catalina_${LAST_PROCESSED_VALUE}.zip "
echo "zip_file_status#NOT_FOUND#NO NEW ZIP FILES TO PROCESS AT SFTP LOCATION.LAST ZIP FILE PROCESSESED = catalina_${LAST_PROCESSED_VALUE}.zip" > ${INCOMING_FILE_CHECK_LIST}
END_TIME_UTC=$(date +%s)
log_print "[INFO :] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO :] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO :] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
log_print "[INFO :] Job Completed successfully."
fn_check_file_and_send_mail success
exit 0
fi

TOP_1_FILE_DATES=`cat ${DATE_LIST_FILE} | sort -r | head -1 `

for FILE_DATE_VALUE in ${TOP_1_FILE_DATES}
do
sftp ${INFOSCOUT_SFTP_USER_AND_HOST}:${INFOSCOUT_SFTP_FILE_LOCATION}/catalina_${FILE_DATE_VALUE}.zip ${ZIPPED_FILE_LOCATION}
  if [ $? -eq 0 ];then
  log_print "[INFO:] SUCCESFULLY COPIED ZIP FILE /home/catalina_${FILE_DATE_VALUE}.zip  TO LOCATION :$ZIPPED_FILE_LOCATION"	
  else
  log_print "[INFO:] FAILED TO COPY ZIP FILE /home/catalina_${FILE_DATE_VALUE}.zip  TO LOCATION :$ZIPPED_FILE_LOCATION"	
  return 1
  fi
done
return 0

}

#Functions
function fn_extract_files() {
ZIP_FILE_NAME_LIST=$1
fn_remove_and_create_dir ${EXTRACTED_FILE_LOCATION}
if [ $? -eq 1 ]
then
return 1
fi
fn_remove_and_create_dir ${CSV_FILE_LOCATION}
if [ $? -eq 1 ]
then
return 1
fi
INCOMING_FILE_CHECK_LIST="${FILE_INGESTION_BASE_PATH}/tmp/incoming_file_check_list.txt"
fn_create_file ${INCOMING_FILE_CHECK_LIST}
if [ $? -eq 1 ]
then
return 1
fi

zip_status="zip_file_status#FOUND"
#UNZIP AND EXTRACTION BEGINS
for ZIP_FILE_NAME in ${ZIP_FILE_NAME_LIST}
do
	zip=`basename ${ZIP_FILE_NAME}`
        zip_status="${zip_status}#${zip}"
	unzip ${ZIP_FILE_NAME} -d ${EXTRACTED_FILE_LOCATION}
	if [ $? -eq 0 ];then
  	log_print "[INFO:] SUCCESFULLY DECOMPRESSED ZIP FILE $ZIP_FILE_NAME TO LOCATION :${EXTRACTED_FILE_LOCATION}"	
  	else
  	log_print "[INFO:] FAILED TO DECOMPRESS ZIP FILE $ZIP_FILE_NAME TO LOCATION :${EXTRACTED_FILE_LOCATION}"
 	 return 1
	fi
	EXTRACTED_FILE_DIR=` basename "${ZIP_FILE_NAME}" | cut -d"." -f1`
	EXTRACTED_FILE_LIST=`ls ${EXTRACTED_FILE_LOCATION}/${EXTRACTED_FILE_DIR}`

	for EXTRACTED_FILE in ${EXTRACTED_FILE_LIST}
	do
   	FILE=`echo ${EXTRACTED_FILE} | cut -d "." -f1`
   	FILENAME="${FILE}.csv"
   	gunzip -dc < ${EXTRACTED_FILE_LOCATION}/${EXTRACTED_FILE_DIR}/${EXTRACTED_FILE} >  ${CSV_FILE_LOCATION}/${FILENAME}
   	if [ $? -eq 0 ];then
  	log_print "[INFO:] SUCCESFULLY DECOMPRESSED FILE ${EXTRACTED_FILE_LOCATION}/${EXTRACTED_FILE_DIR}/${EXTRACTED_FILE} TO LOCATION :${CSV_FILE_LOCATION}"	
  	echo "$zip#${FILENAME}" >> ${INCOMING_FILE_CHECK_LIST}
	else
  	log_print "[INFO:] FAILED TO DECOMPRESS FILE ${EXTRACTED_FILE_LOCATION}/${EXTRACTED_FILE_DIR}/${EXTRACTED_FILE} TO LOCATION :${CSV_FILE_LOCATION} "
  	return 1
   	fi
	done
done
echo "${zip_status}" >> ${INCOMING_FILE_CHECK_LIST}
return 0 
 
}

#Audit function
function fn_audit_sftp()
{
 #Populating job key value without any extra log info 
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '$SFTP_JOB_NAME' "` 
JOB_STAT_NM=$1
MOST_RECENT_VAL_TXT=$2
log_print "[INFO:] JOB_KEY : $JOB_KEY"
if [ -z "${MOST_RECENT_VAL_TXT}" ]
then
MOST_RECENT_VAL_TXT=NULL
fi
#Not Applicable fields are intialised to Null  
UPD_ROWS_CNT=NULL     
DEL_ROWS_CNT=NULL     
VAL_DATATYPE_NM=NULL
SRC_DB_TYPE=NULL
SRC_DB_NAME=NULL
INSERT_ROWS_CNT=NULL
FILE_REF_TABLE=NULL
FILE_REF_DB=NULL
#End time 
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Executing audit inserting query"
log_print "[INFO:] Query : insert into cedl_operations.cedl_audit_t
                                        (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                        src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                        insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                        begin_tms,end_tms) 
                           values      ($JOB_KEY,'$START_TIME','$SFTP_JOB_NAME','$SFTP_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$FILE_REF_TABLE','$FILE_REF_DB','$FILE_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');" 



mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
                                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                                begin_tms,end_tms) 
                                       values      ($JOB_KEY,'$START_TIME','$SFTP_JOB_NAME','$SFTP_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$FILE_REF_TABLE','$FILE_REF_DB','$FILE_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');" 

if [ $? -eq 0 ] 
then
                log_print "[INFO:] $JOB_STAT_NM Job record inserted into audit table"
				if [ "$JOB_STAT_NM" == "failure" ]
				then            #fn_check_file_and_send_mail failure 
						log_print "[ERROR:] Job failed...Please check........."
						exit 1
				fi
else
                log_print "[INFO:] Failed to insert $JOB_STAT_NM record into audit table"   
                exit 1                
fi

END_TIME_UTC=$(date +%s)
log_print "[INFO :] Job Started at :  $(date -d @$START_TIME_UTC) "
log_print "[INFO :] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO :] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
log_print "[INFO :] Job Completed successfully."
fn_check_file_and_send_mail success 
 }
 
 
fn_remove_and_create_dir()
{
dir_name=$1
			test -d $dir_name
                        if [ $? -eq 0 ]
                        then
                              rm -r $dir_name
                                if [ $? -eq 0 ]
                                then
                                     log_print "[INFO:]Successfully removed linux directory $dir_name"
                                
				else
                                     log_print "[ERROR:]Failed to remove linux directory $dir_name"
                                  	return 1          
                                fi
                        fi


mkdir -p $dir_name	
if [ $? -eq 0 ]
then
     log_print "[INFO:]Successfully created linux directory $dir_name"
else
     log_print "[ERROR:]Failed to create linux directory $dir_name"
          return 1  
fi
}
 
fn_create_file()
{
file_name=$1
			test -f $file_name
                        if [ $? -eq 0 ]
                        then
                              rm -f $file_name
                                if [ $? -eq 0 ]
                                then
                                     log_print "[INFO:]Successfully removed linux file $file_name"
                                else
                                     log_print "[ERROR:]Failed to remove linux file $file_name"
                    			 return 1                        
                                fi
                        fi


touch $file_name	
if [ $? -eq 0 ]
then
     log_print "[INFO:]Successfully created linux file $file_name"
else
     log_print "[ERROR:]Failed to create linux file $file_name"
            return 1
fi

}


fn_check_file_and_send_mail(){
job_status=$1
INCOMING_FILE_CHECK_LIST="${FILE_INGESTION_BASE_PATH}/tmp/incoming_file_check_list.txt"
SFTP_MAIL_BODY="${FILE_INGESTION_BASE_PATH}/tmp/sftp_mail_body.html"

fn_create_file ${SFTP_MAIL_BODY}

echo "<!DOCTYPE html>" >>$SFTP_MAIL_BODY
echo "<html>" >>$SFTP_MAIL_BODY
echo "<body text=\" \">" >>$SFTP_MAIL_BODY
if [ "$job_status" == "success" ]
then
       cat <<EOT >> ${SFTP_MAIL_BODY}
	<p>
	<pre><b>Hello All ,

PFB STATUS FOR NUMERATOR-SFTP FILES :-
	</b></pre>
	</p>
EOT
	zip_file_entry=`grep 'zip_file_status#' ${INCOMING_FILE_CHECK_LIST} `
        zip_file_status=`echo ${zip_file_entry} | cut -d "#" -f2`
        zip_files=`echo ${zip_file_entry} | cut -d "#" -f3`

        #No New zip files found
        if [ "$zip_file_status" == "NOT_FOUND" ]
        then
        echo "<p><b>`echo "${zip_files}" `</b></p>" >>$SFTP_MAIL_BODY
        #New zip file found
        elif [ "$zip_file_status" == "FOUND" ];then
                for i in $(echo $zip_files | sed "s/#/ /g")
                do
                        echo "<p><b>`echo "ZIP FILE PROCESSED = $i" `</b></p>" >>$SFTP_MAIL_BODY
                        echo "<p><b>`echo "CSV FILES PROCESSED ARE AS FOLLOWING :-" `</b></p>" >>$SFTP_MAIL_BODY
                         echo "<table style=\"width:70%;background-color: white; border:2px solid #E2E8EA; border-collapse: collapse; text-align: center; padding: 20px;  font-weight: bold;font-size: 95%;   \">" >>$SFTP_MAIL_BODY
		        echo "<tr style=\"text-align:left; border:2px solid #E2E8EA; background:#679bef;\" >">>$SFTP_MAIL_BODY
        		echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">FILE NAME</th>">>$SFTP_MAIL_BODY
        		echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">STATUS</th>">>$SFTP_MAIL_BODY
        		echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">CSV FILENAME</th>">>$SFTP_MAIL_BODY
        		echo "</tr>">>$SFTP_MAIL_BODY
        		       
			for j in $(echo $FILE_NAME_TO_CHECK | sed "s/,/ /g")
                                do
                                        echo "<tr style=\" background:#F3F6F7;   \" >" >>$SFTP_MAIL_BODY
					file_search=` grep "${i}#${j}_.........csv" ${INCOMING_FILE_CHECK_LIST} | cut -d "#" -f2`
                                        if [ ! -z "${file_search}" ]
                                        then
                                               
                                        echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">$j</td>">>$SFTP_MAIL_BODY
					echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">Found</td>">>$SFTP_MAIL_BODY
					echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">$file_search</td>">>$SFTP_MAIL_BODY
					else
                                              
                                        echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">$j</td>">>$SFTP_MAIL_BODY
					echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">Not Found</td>">>$SFTP_MAIL_BODY
					echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">$file_search</td>">>$SFTP_MAIL_BODY
					fi
 					echo "</tr>">>$SFTP_MAIL_BODY
                                done
                echo "</table>">>$SFTP_MAIL_BODY
		done
        fi
fi

echo "</body>" >>$SFTP_MAIL_BODY
echo "</HTML>" >>$SFTP_MAIL_BODY


send_email

}

send_email() {
echo "####sending mail####"
(
FILE="${SFTP_MAIL_BODY}"
echo To:"${MAIL_LIST}"
echo From:SVC-EDM@cat-production-vep6.localdomain
  echo "Content-Type: text/html; "
  echo Subject: NUMERATOR - SFTP ZIP FILE COPY JOB `date --date="-1 days" '+%Y-%m-%d'`  
  echo
  cat $SFTP_MAIL_BODY
) | /usr/sbin/sendmail -t
echo "####email sent####"
}




 
