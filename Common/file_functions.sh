################################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : file_ingestion_function.sh                                     #
# Description :                                                                #
#                                                                              #
################################################################################
# Function to print the logs with current utc time
log_print()
{
    echo "$(date -u '+%Y-%m-%d %H:%M:%S,%3N') $1 "
}

# Usage descriptor for invalid input arguments to the wrapper
show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly three arguments"
        log_print "Usage: "$0" <scheduled date> <source-name> <file-name>  "
        exit 1
}
sftp_show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly two arguments"
        log_print "Usage: "$0" <scheduled date> <source-name> "
        exit 1
}

iri_show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly two arguments"
        log_print "Usage: "$0" <scheduled date> <source-name> "
        exit 1
}

label_insight_show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly one argument"
        log_print "Usage: "$0" <scheduled date> <tablename> "
        exit 1
}

sftp_label_insight_show_usage()
{
        log_print "[INFO:] Invalid arguments please pass exactly one argument"
        log_print "Usage: "$0" <scheduled date> "
        exit 1
}


load_resource()
{
        log_print "[INFO:] Loading  $1 ."
        . $1
        if [ $? -eq 0 ]
        then
            log_print "[INFO:] $1 Loaded successfully "
        else
            log_print "[ERROR:] $1 Loading  failed "
            exit 1
        fi
}

create_hdfs_dir(){
dir_name=$1
hdfs dfs -test -d $dir_name
                        if [ $? -eq 0 ]
                        then
                                hdfs dfs -rm -r $dir_name
                                if [ $? -eq 0 ]
                                then
                                     log_print "[INFO:]Successfully removed directory $dir_name"
                                else
                                     log_print "[ERROR:]Failed to removed directory $dir_name"
					return 1
                                fi
                        fi


hadoop fs -mkdir -p $dir_name
if [ $? -eq 0 ]
then
     log_print "[INFO:]Successfully created directory $dir_name"
else
     log_print "[ERROR:]Failed to create directory $dir_name"
	return 1
fi

}
####################################validations#####################################################

function fn_validate_files()
{
FILE_LOCATION=$1
FILE_HEADER=$4
FILE_NAME_VAL=$2
FILE_FORMAT=$3
FILE_LIST=`ls ${FILE_LOCATION}/${FILE_NAME_VAL}*${FILE_FORMAT}`
FILE_COUNT=`ls ${FILE_LOCATION}/${FILE_NAME_VAL}*${FILE_FORMAT} | wc -l`
if [ ${FILE_COUNT} -eq 0 ]
then
log_print " [INFO:] No files to be ingested at the location : ${FILE_LOCATION} "
return 1
fi

for FILE_VAL in ${FILE_LIST}
do
        log_print "[INFO:] VALIDATION STARTED FOR FILE = ${FILE_VAL}"

        #CHECK SIZE OF THE FILE
        if [ ! -s "${FILE_VAL}" ]
        then
        log_print "[INFO:] Size of the file : ${FILE_VAL} is zero.Empty file"
        return 1
        fi

        #Column / Header validation
        validateColumnName "${FILE_VAL}" "${FILE_HEADER}"
        if [ $? -gt 0 ]
        then
        unset IFS
        return 1
        fi
        log_print "[INFO:] VALIDATION SUCCESSFULLY COMPLETED FOR FILE = ${FILE_VAL}"

done
unset IFS
return 0

}

function validateColumnName() {
    FILE_PATH=$1
        FILE_HEADER_VALUE=$2

     NO_HEADER=$(awk 'NR==1' ${FILE_PATH})
     IFS=','
     read -a IN_arr <<< "${NO_HEADER}"
     read -a arr <<< "${FILE_HEADER_VALUE}"
        # log_print ${IN_arr[@]}
        # log_print ${arr[@]}

        if [ ${#IN_arr[@]} == ${#arr[@]} ];
                then

                        for i in "${!arr[@]}"
                        do
                                for j in "${!IN_arr[@]}"
                                do
                                        if [ $i == $j ];
                                        then
                                                if [ "${arr[$i],,}" != "${IN_arr[$j],,}" ];
                                                then
                                                        log_print  "[INFO:] ${IN_arr[$j]} Column order/format issue"
                                                        return 1
                                                fi
                                        fi
                                done
                        done
                        return 0
                else
                        log_print "[INFO:] Number of columns are different "
                        return 1
                fi
 }

function audit_file_ingestion()
 {
 #Populating job key value without any extra log info
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '$FILE_JOB_NAME' "`
JOB_STAT_NM=$1
INSERT_ROWS_CNT=$2
log_print "[INFO:] JOB_KEY : $JOB_KEY"
if [ -z "${INSERT_ROWS_CNT}" ]
then
INSERT_ROWS_CNT=NULL
fi
#Not Applicable fields are intialised to Null
UPD_ROWS_CNT=NULL
DEL_ROWS_CNT=NULL
MOST_RECENT_VAL_TXT=NULL
VAL_DATATYPE_NM=NULL
SRC_DB_TYPE=NULL
SRC_DB_NAME=NULL

#End time
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Executing audit inserting query"
log_print "[INFO:] Query : insert into cedl_operations.cedl_audit_t
                                        (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                        src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                        insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                        begin_tms,end_tms)
                           values      ($JOB_KEY,'$START_TIME','$FILE_JOB_NAME','$FILE_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$FILE_REF_TABLE','$FILE_REF_DB','$FILE_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"



mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
                                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                                begin_tms,end_tms)
                                       values      ($JOB_KEY,'$START_TIME','$FILE_JOB_NAME','$FILE_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$FILE_REF_TABLE','$FILE_REF_DB','$FILE_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"

if [ $? -eq 0 ]
then
                log_print "[INFO:] $JOB_STAT_NM Job record inserted into audit table"
                                if [ "$JOB_STAT_NM" == "failure" ]
                                then
                                                log_print "[ERROR:] Job failed...Please check........."
                                                exit 1
                                elif [ "$JOB_STAT_NM" == "success" ]
				then
					log_print "[INFO :] Job Completed successfully."
				
				fi
else
                log_print "[INFO:] Failed to insert $JOB_STAT_NM record into audit table"
                exit 1
fi
 }


function archiveFiles(){
FILE_LOCATION=$1
FILE_ARCHIVE_DATA_DIRECTORY=$2
partition_date=$(date '+%Y-%m-%d')
archive_data_directory=${FILE_ARCHIVE_DATA_DIRECTORY}
hdfs dfs -test -d ${archive_data_directory}
if [ $? -eq 0 ]
then
	log_print "[INFO:]ARCHIVIVAL DIRECTORY ALREADY EXIST = ${archive_data_directory}"
else
	log_print "[INFO:]ARCHIVIVAL DIRECTORY DOES NOT EXIST = ${archive_data_directory}"
	log_print "[INFO:]CREATING ARCHIVIVAL DIRECTORY = ${archive_data_directory}"
	hadoop fs -mkdir -p ${archive_data_directory}
	if [ $? -eq 0 ]
	then
		log_print "[INFO:]Successfully CREATED ARCHIVIVAL DIRECTORY = ${archive_data_directory}"
	else
		log_print "[ERROR:]Failed TO CREATE ARCHIVIVAL DIRECTORY = ${archive_data_directory}"
	
	fi
fi
        

log_print "[INFO:]Copying file from $FILE_LOCATION to $archive_data_directory"
hadoop fs -put -f ${FILE_LOCATION}/${FILE_NAME}*${FILE_FORMAT} ${archive_data_directory}
if [ $? -eq 0 ]
then
     log_print "[INFO:]Successfully copied files from $FILE_LOCATION to $archive_data_directory"
        return 0
else
     log_print "[ERROR:]Failed to copy files from $FILE_LOCATION to $archive_data_directory"
        return 1
fi


}


function audit_iri_ingestion()
 {
 #Populating job key value without any extra log info
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '$IRI_JOB_NAME' "`
JOB_STAT_NM=$1
MOST_RECENT_VAL_TXT=$2
log_print "[INFO:] JOB_KEY : $JOB_KEY"
if [ -z "${MOST_RECENT_VAL_TXT}" ]
then
MOST_RECENT_VAL_TXT=NULL
fi

#Not Applicable fields are intialised to Null
INSERT_ROWS_CNT=NULL
UPD_ROWS_CNT=NULL
DEL_ROWS_CNT=NULL
VAL_DATATYPE_NM=NULL
SRC_DB_TYPE=NULL
SRC_DB_NAME=NULL

#End time
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Executing audit inserting query"
log_print "[INFO:] Query : insert into cedl_operations.cedl_audit_t
                                        (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                        src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                        insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                        begin_tms,end_tms)
                           values      ($JOB_KEY,'$START_TIME','$IRI_JOB_NAME','$IRI_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$IRI_REF_TABLE','$IRI_REF_DB','$IRI_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"



mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
                                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                                begin_tms,end_tms)
                                       values      ($JOB_KEY,'$START_TIME','$IRI_JOB_NAME','$IRI_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$IRI_REF_TABLE','$IRI_REF_DB','$IRI_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"

if [ $? -eq 0 ]
then
                log_print "[INFO:] $JOB_STAT_NM Job record inserted into audit table"
                                if [ "$JOB_STAT_NM" == "failure" ]
                                then
                                                log_print "[ERROR:] IRI INGESTION JOB ABORTING........"
			   			log_print "[ERROR:] Job failed...Please check........."
                                                exit 1
                                fi
else
               
				log_print "[INFO:] Failed to insert $JOB_STAT_NM record into audit table"
        		        log_print "[ERROR:] IRI INGESTION JOB ABORTING........"
				log_print "[ERROR:] Job failed...Please check........."
				exit 1
fi

}

fn_create_file()
{
file_name=$1
                        test -f $file_name
                        if [ $? -eq 0 ]
                        then
                              rm -f $file_name
                                if [ $? -eq 0 ]
                                then
                                     log_print "[INFO:]Successfully removed linux file $file_name"
                                else
                                     log_print "[ERROR:]Failed to remove linux file $file_name"
                                         return 1
                                fi
                        fi


touch $file_name
if [ $? -eq 0 ]
then
     log_print "[INFO:]Successfully created linux file $file_name"
else
     log_print "[ERROR:]Failed to create linux file $file_name"
            return 1
fi

}




send_email() {
echo "####sending mail####"
(
MAIL_SUBJECT="NUMERATOR - FILE INGESTION JOB `date --date="-1 days" '+%Y-%m-%d'`"
if [ ! -z "${1}" ]
then
MAIL_SUBJECT="NUMERATOR - FILE INGESTION JOB `date --date="-1 days" '+%Y-%m-%d'` : ${1}"
fi
echo To:"${MAIL_LIST}"
echo From:SVC-EDM@cat-production-vep6.localdomain
  echo "Content-Type: text/html; "
  echo Subject: ${MAIL_SUBJECT}
  echo
  cat $FILE_INGESTION_MAIL_BODY
) | /usr/sbin/sendmail -t
echo "####email sent####"
}

iri_send_email() {
echo "####sending mail####"
(
echo To:"${MAIL_LIST}"
echo From:SVC-EDM@cat-production-vep6.localdomain
  echo "Content-Type: text/html; "
  echo Subject: IRI - FILE INGESTION JOB `date --date="-1 days" '+%Y-%m-%d'`
  echo
  cat $IRI_INGESTION_MAIL_BODY
) | /usr/sbin/sendmail -t
echo "####email sent####"
}



function audit_label_insight_ingestion()
{
#Populating job key value without any extra log info
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '$LABEL_INSIGHT_JOB_NAME' "`
JOB_STAT_NM=$1
log_print "[INFO:] JOB_KEY : $JOB_KEY"

#Not Applicable fields are intialised to Null
INSERT_ROWS_CNT=NULL
UPD_ROWS_CNT=NULL
DEL_ROWS_CNT=NULL
MOST_RECENT_VAL_TXT=NULL
VAL_DATATYPE_NM=NULL
SRC_DB_TYPE=NULL
SRC_DB_NAME=NULL
REF_TABLE=NULL
REF_DB=NULL
#End time
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Executing audit inserting query"
log_print "[INFO:] Query : insert into cedl_operations.cedl_audit_t
                                        (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                        src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                        insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                        begin_tms,end_tms)
                           values      ($JOB_KEY,'$START_TIME','$LABEL_INSIGHT_JOB_NAME','$LABEL_INSIGHT_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$LABEL_INSIGHT_REF_TABLE','$LABEL_INSIGHT_REF_DB','$LABEL_INSIGHT_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"



mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
                                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                                begin_tms,end_tms)
                                       values      ($JOB_KEY,'$START_TIME','$LABEL_INSIGHT_JOB_NAME','$LABEL_INSIGHT_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$LABEL_INSIGHT_REF_TABLE','$LABEL_INSIGHT_REF_DB','$LABEL_INSIGHT_REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"

if [ $? -eq 0 ]
then
                log_print "[INFO:] $JOB_STAT_NM Job record inserted into audit table"
                                if [ "$JOB_STAT_NM" == "failure" ]
                                then
                                                log_print "[ERROR:] LABEL INSIGHT INGESTION JOB ABORTING........"
                                                log_print "[ERROR:] Job failed...Please check........."
                                                exit 1
                                fi
else

                                log_print "[ERROR:] Failed to insert $JOB_STAT_NM record into audit table"
                                log_print "[ERROR:] LABEL INSIGHT INGESTION JOB ABORTING........"
                                log_print "[ERROR:] Job failed...Please check........."
                                exit 1
fi

}


label_insight_send_email(){
echo "####sending mail####"
(
echo To:"${MAIL_LIST}"
echo From:SVC-EDM@cat-production-vep6.localdomain
  echo "Content-Type: text/html; "
  echo Subject: LABEL INSIGHT - FILE INGESTION JOB `date --date="-1 days" '+%Y-%m-%d'`
  echo
  cat $LABEL_INSIGHT_INGESTION_MAIL_BODY
) | /usr/sbin/sendmail -t
echo "####email sent####"
} 


function label_insight_sftp_audit()
{
 #Populating job key value without any extra log info 
JOB_KEY=`mysql $AUDIT_MYSQL_CONNECTION_URL -sN -e "select job_key from cedl_operations.cedl_job_t where job_nm= '$LABEL_INSIGHT_SFTP_JOB_NAME' "` 
JOB_STAT_NM=$1
MOST_RECENT_VAL_TXT=$3
SRC_NAME=$2
log_print "[INFO:] JOB_KEY : $JOB_KEY"
if [ -z "${MOST_RECENT_VAL_TXT}" ]
then
MOST_RECENT_VAL_TXT=NULL
fi
#Not Applicable fields are intialised to Null
INSERT_ROWS_CNT=NULL
UPD_ROWS_CNT=NULL
DEL_ROWS_CNT=NULL
VAL_DATATYPE_NM=NULL
SRC_DB_TYPE=NULL
SRC_DB_NAME=NULL
REF_TABLE=NULL
REF_DB=NULL
#End time
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')

log_print "[INFO:] Executing audit inserting query"
log_print "[INFO:] Query : insert into cedl_operations.cedl_audit_t
                                        (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                        src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                        insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                        begin_tms,end_tms)
                           values      ($JOB_KEY,'$START_TIME','$LABEL_INSIGHT_SFTP_JOB_NAME','$LABEL_INSIGHT_SFTP_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$REF_TABLE','$REF_DB','$REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"



mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
                                                (job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,
                                                src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,
                                                insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,
                                                begin_tms,end_tms)
                                       values      ($JOB_KEY,'$START_TIME','$LABEL_INSIGHT_SFTP_JOB_NAME','$LABEL_INSIGHT_SFTP_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE',
                                                '$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$REF_TABLE','$REF_DB','$REF_TABLE',
                                                $INSERT_ROWS_CNT,$UPD_ROWS_CNT,$DEL_ROWS_CNT,'$MOST_RECENT_VAL_TXT','$VAL_DATATYPE_NM',
                                                '$START_TIME','$END_TIME');"


if [ $? -eq 0 ] 
then
                log_print "[INFO:] $JOB_STAT_NM Job record inserted into audit table"
				if [ "$JOB_STAT_NM" == "failure" ]
				then            
						log_print "[ERROR:] Job failed...Please check........."
						exit 1
				fi
else
                log_print "[ERROR:] Failed to insert $JOB_STAT_NM record into audit table"   
                log_print "[ERROR:] Job failed...Please check........."
				exit 1                
fi
 
 }

label_insight_sftp_send_mail(){
LABEL_INSIGHT_SFTP_MAIL_BODY="${LABEL_INSIGHT_BASE_LOCATION}/tmp/label_insight_sftp_mail_body.html"
fn_create_file ${LABEL_INSIGHT_SFTP_MAIL_BODY}

echo "<!DOCTYPE html>" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<html>" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<body text=\" \">" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
cat <<EOT >> ${LABEL_INSIGHT_SFTP_MAIL_BODY}
        <p>
        <pre><b>Hello All ,

PFB STATUS FOR  LABEL INSIGHT SFTP FILES :-
        </b></pre>
        </p>
EOT
 
echo "<table style=\"width:70%;background-color: white; border:2px solid #E2E8EA; border-collapse: collapse; text-align: center; padding: 20px;  font-weight: bold;font-size: 95%;   \">" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<tr style=\"text-align:left; border:2px solid #E2E8EA; background:#679bef;\" >">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">SOURCE NAME</th>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">STATUS</th>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">FILENAME</th>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<th style=\"  height:30px; border:2px solid #E2E8EA; width:100px;\">TOTAL JSON FILES</th>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "</tr>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
        		       
cat ${LABEL_INSIGHT_INCOMING_FILE_CHECK_LIST} | while read LINE
do
SOURCE=`echo $LINE|cut -d"#" -f1`
STATUS=`echo $LINE|cut -d"#" -f2`
FILENAME=`echo $LINE|cut -d"#" -f3`
TOTAL_JSON_FILES=`echo $LINE|cut -d"#" -f4`
echo "<tr style=\" background:#F3F6F7;   \" >" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">"$SOURCE"</td>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">"$STATUS"</td>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">"$FILENAME"</td>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "<td style=\"width:100px;border:2px solid #E2E8EA;  \">"$TOTAL_JSON_FILES"</td>">>$LABEL_INSIGHT_SFTP_MAIL_BODY			
echo "</tr>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
done
echo "</table>">>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "</body>" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "</HTML>" >>$LABEL_INSIGHT_SFTP_MAIL_BODY
echo "####sending mail####"
(
echo To:"${MAIL_LIST}"
echo From:SVC-EDM@cat-production-vep6.localdomain
  echo "Content-Type: text/html; "
  echo Subject: LABEL INSIGHT - SFTP ZIP FILE COPY JOB `date --date="-1 days" '+%Y-%m-%d'`
  echo
  cat $LABEL_INSIGHT_SFTP_MAIL_BODY
) | /usr/sbin/sendmail -t
echo "####email sent####"

}

