##############################################################################
#                               General Details                                #
################################################################################
# Name        : Datametica                                                     #
# File        : function.sh                                                                        #
# Description :                                                                #
#                                                                              #
################################################################################

# Function to show usage / arguments passing for script : ingest_sqoop_upsert_wrapper.sh
fn_show_usage_for_ingest_sqoop_upsert()
{
        fn_log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        fn_log_print "[ERROR:] Invalid arguments please pass exactly three arguments"
        fn_log_print "Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename> <batch_load_nbr> "
        exit 1

}

# Function to show usage / arguments passing for script : load_hive_upsert_wrapper.sh
fn_show_usage_for_load_hive_upsert()
{
        fn_log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        fn_log_print "[Error:] Invalid arguments please pass exactly three arguments"
        fn_log_print "Usage: "$0" <Sheduled date(YYYY-MM-DD)> <databasename> <tablename> <merge key1,merge kye2,...(Comma Separated)> <batch_load_number>"
        exit 1
}


#Audit clean up function for : Upsert
audit_cleanup_for_ingest_sqoop_upsert(){

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
LND_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
LND_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
LND_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
START_TIME_UTC="${16}"
fn_log_print "********************************************Audit Entry*****************************************************"
fn_log_print "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) values ($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','$END_TIME')"
`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,
data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,
src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms)
values ($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM',
'$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME',
'$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,'$MAX_BATCH_LD_NBR','INT','$START_TIME','${END_TIME}')"`
if [ $? -eq 0 ]
then
        fn_log_print  "[INFO:] successfully Inserted record into audit table"
fi


if [ "$JOB_STAT_NM" == "failure" ] && [ "$LND_DATA_DIRECTORY" != NULL ]
then
        #Clean Up Activity If Job is Failed
        fn_log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        fn_log_print "[INFO:] Starting clean up activity..."
        fn_log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
        hdfs dfs -test -d ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
        if [ $? -eq 0 ]
        then
                hdfs dfs -rm -r ${LND_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
                if [ $? -eq 0 ]
                then

                        fn_log_print "[INFO:] Files deleted successfully in $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"

                else

                        fn_log_print "[ERROR:] $LND_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE files deletion failed"

                fi
        fi
        #Removing Partition For Respective Date
        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $LND_DATABASE_NAME.$TABLE_NAME
        DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
        if [ $? -eq 0 ]
        then
                    fn_log_print "[INFO:] Successfully removed partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
        else
                    fn_log_print "[ERROR:] failed to removed partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
        fi
        fn_log_print "[ERROR:] Job Failed after Sqoop Utility.........Please check"
        exit 1
elif [ "$JOB_STAT_NM" == "failure" ] && [ "$LND_DATA_DIRECTORY" == NULL ]
then
        fn_log_print "[ERROR:] Job Failed before Sqoop Utility.........Please check"
        exit 1
else
        fn_log_print  "[INFO:] successfully Job record inserted into audit table"
        fn_log_print "[INFO:] Job completed successfully"
fi
END_TIME_UTC=$(date +%s)
fn_log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
fn_log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
exit 0
}


fn_sqoop_import_upsert(){
MODE=$1
fn_log_print  "[INFO:] Starting Sqoop Import Job : $1"
  if  [[ $MODE == "NORMAL" ]]
       then
           
             fn_log_print "[INFO:] Running Sqoop Query using select * statement for table $TABLE_NAME."
             fn_log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
                                      --query '${NORMAL_QUERY} and \$CONDITIONS' \
                                      --split-by $SPLIT_KEY \
                                      --fetch-size '1000' \
                                      --hcatalog-database $LND_DB_NAME \
                                      --hcatalog-table $TABLE_NAME \
                                      --hcatalog-partition-keys $PARTITION_COL \
                                      --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                      --hcatalog-storage-stanza 'stored as orc tblproperties (orc.compress=ZLIB)' \
                                      -m $DEF_SQOOP_UPSERT_MAPPER_CNT"
             sqoop import --connect $SQOOP_CONNNECTION_URL \
                                      --query "${NORMAL_QUERY} and \$CONDITIONS" \
                                      --split-by $SPLIT_KEY \
                                      --fetch-size '1000' \
                                      --hcatalog-database $LND_DB_NAME \
                                      --hcatalog-table $TABLE_NAME \
                                      --hcatalog-partition-keys $PARTITION_COL \
                                      --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                      --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
                                      -m $DEF_SQOOP_UPSERT_MAPPER_CNT

               
       else
            fn_log_print "[INFO:] Running Sqoop Query using select * statement for table $TABLE_NAME."
             fn_log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
                                      --query '${RANGE_OR_RERUN_QUERY} and \$CONDITIONS' \
                                      --split-by $SPLIT_KEY \
                                      --fetch-size '1000' \
                                      --hcatalog-database $LND_DB_NAME \
                                      --hcatalog-table $TABLE_NAME \
                                      --hcatalog-partition-keys $PARTITION_COL \
                                      --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                      --hcatalog-storage-stanza 'stored as orc tblproperties (orc.compress=ZLIB)' \
                                      -m $DEF_SQOOP_UPSERT_MAPPER_CNT"
             sqoop import --connect $SQOOP_CONNNECTION_URL \
                                      --query "${RANGE_OR_RERUN_QUERY} and \$CONDITIONS" \
                                      --split-by $SPLIT_KEY \
                                      --fetch-size '1000' \
                                      --hcatalog-database $LND_DB_NAME \
                                      --hcatalog-table $TABLE_NAME \
                                      --hcatalog-partition-keys $PARTITION_COL \
                                      --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                      --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
                                      -m $DEF_SQOOP_UPSERT_MAPPER_CNT
									  
		fi 					  

}

audit_cleanup_for_load_hive_upsert()
{
END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
REF_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
REF_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
REF_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
REF_CEDL_BATCH_DATE="${16}"
fn_log_print "***********************************************Audit Entry**************************************************************"
fn_log_print "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms) values ($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','$END_TIME')"
`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t
(job_key,job_shld_tm,job_nm,job_grp_nm,
job_stat_nm,data_proc_dt_txt,src_sys_nm,src_db_typ_nm,src_db_nm,
src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,val_datatype_nm,begin_tms,end_tms)
values ($JOB_KEY,'$START_TIME','$REF_JOB_NAME','$REF_JOB_NAME',
'$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE','$SRC_DB_NAME',
'$TABLE_NAME','$REF_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,'$MAX_BATCH_LD_NBR','INT','$START_TIME','${END_TIME}')"`
if [ $? -eq 0 ]
then
        fn_log_print  "[INFO:] successfully Inserted record into audit table"
fi

if [ "$JOB_STAT_NM" == "failure" ] && [ "$REF_DATA_DIRECTORY" != NULL ]
then
        fn_log_print "[INFO:] Fuction Name : ${FUNCNAME}()"
        #Clean Up Activity If Job is Failed
        fn_log_print "[INFO:] Starting clean up activity..."
        fn_log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"
        hdfs dfs -rm -r ${REF_DATA_DIRECTORY}/${PARTITION_COL}=${CEDL_BATCH_DATE}
        if [ $? -eq 0 ]
        then

                fn_log_print "[INFO:] Successfully deleted directory : $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"

        else

                fn_log_print "[ERROR:] Failed to delete directory : $REF_DATA_DIRECTORY/$PARTITION_COL=$CEDL_BATCH_DATE"

        fi
        #Removing Partition For Respective Date
        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME
        DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
        if [ $? -eq 0 ]
        then
                    fn_log_print "[INFO:] Successfully removed partition $CEDL_BATCH_DATE from $REF_DATABASE_NAME.$TABLE_NAME"
        else
                    fn_log_print "[ERROR:] Failed to removed partition $CEDL_BATCH_DATE from $REF_DATABASE_NAME.$TABLE_NAME"
        fi
        #altering Hive table Old Partition as Base
        `${BEELINE_CONNECTION_URL} --silent=true -e "ALTER TABLE $REF_DATABASE_NAME.$TABLE_NAME
        ADD IF NOT EXISTS PARTITION($PARTITION_COL='$REF_CEDL_BATCH_DATE') LOCATION '$REF_DATA_DIRECTORY/$PARTITION_COL=$REF_CEDL_BATCH_DATE'"`
        if [ $? -eq 0 ]
        then
                fn_log_print "[INFO:] Successfully add partition $REF_CEDL_BATCH_DATE into table $REF_DATABASE_NAME.$TABLE_NAME"
        else
                fn_log_print "[ERROR:] Failed to add partition $REF_CEDL_BATCH_DATE into table $REF_DATABASE_NAME.$TABLE_NAME"

        fi
        fn_log_print "[INFO:] Job Failed after Hive job.........Please check"
        exit 1
elif [ "$JOB_STAT_NM" == "failure" ] && [ "$REF_DATA_DIRECTORY" == NULL ]
then
        fn_log_print "[INFO:] Job Failed before Hive job.........Please check"
        exit 1
else
        fn_log_print "[INFO:] Job completed successfully"

fi
END_TIME_UTC=$(date +%s)
fn_log_print "[INFO:] Job Started at :  $(date -d @$START_TIME_UTC) "
fn_log_print "[INFO:] Job Ended at   :  $(date -d @$END_TIME_UTC) "
deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
exit 0

}

#Getting column names for Refine table
fn_get_column_list_upsert_refine()
{
rm "$CEDL_BASE_PATH"/tmp/ref_columns_list.txt
rm "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt
${BEELINE_CONNECTION_URL} --silent=true  --headerInterval=1000 -e "describe ${REF_DB_NAME}.${TABLE_NAME}" > "$CEDL_BASE_PATH"/tmp/ref_columns_list.txt

cat "$CEDL_BASE_PATH"/tmp/ref_columns_list.txt | awk -F" " '{print $2}'| sed 's/ //g'|sed 's/'"${PARTITION_COL}"'//g'| sed 's/col_name//g' | sed 's/loadtimestamp//g' | sed 's/|//g' | sed 's/#//g' | sed '/^$/d '|sed ':a;N;$!ba;s/\n/,/g' | sed 's/,/,a./g' > "$CEDL_BASE_PATH"/tmp/ref_final_column_list.txt

}





####################################### Append Functions ##################################################
. /apps/data-ingestion/edw4x/Common/common_function.sh


audit_cleanup_for_ingest_sqoop_append(){

END_TIME=$(date -u '+%Y-%m-%d %H:%M:%S')
JOB_STAT_NM=$1
JOB_KEY=$2
START_TIME=$3
LND_JOB_NAME=$4
CEDL_BATCH_DATE=$5
SRC_NAME=$6
SRC_DB_NAME=$7
SRC_DB_TYPE=$8
DATABASE_NAME=$9
TABLE_NAME="${10}"
LND_DATABASE_NAME="${11}"
HIVE_COUNT="${12}"
MAX_BATCH_LD_NBR="${13}"
LND_DATA_DIRECTORY="${14}"
PARTITION_COL="${15}"
EXTRACTION_MODE="${16}"
ICR_LOAD_COL_NM="${17}"

fn_log_print "[INFO:] Function Name : ${FUNCNAME}()"
fn_log_print "[INFO:] The value of all command-line arguments : $*"

fn_log_print "[INFO:] Executing MySQL Query: insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,
src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,
val_datatype_nm,begin_tms,end_tms)
values
($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE',
'$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}');"

`mysql $AUDIT_MYSQL_CONNECTION_URL -e "insert into cedl_operations.cedl_audit_t(job_key,job_shld_tm,job_nm,job_grp_nm,job_stat_nm,data_proc_dt_txt,src_sys_nm,
src_db_typ_nm,src_db_nm,src_obj_nm,tgt_db_nm,tgt_obj_nm,insert_rows_cnt,upd_rows_cnt,del_rows_cnt,most_recent_val_txt,
val_datatype_nm,begin_tms,end_tms)
values
($JOB_KEY,'$START_TIME','$LND_JOB_NAME','$LND_JOB_NAME','$JOB_STAT_NM','$CEDL_BATCH_DATE','$SRC_NAME','$SRC_DB_TYPE',
'$SRC_DB_NAME','$TABLE_NAME','$LND_DATABASE_NAME','$TABLE_NAME',$HIVE_COUNT,NULL,NULL,$MAX_BATCH_LD_NBR,'INT','$START_TIME','${END_TIME}');"`
if [ $? -eq 0 ]
then
        fn_log_print  "[INFO:] Successfully Job record inserted into audit table"
fi
if [ "${JOB_STAT_NM}" == "success" ]
then
        if [ "${EXTRACTION_MODE}" == "normal" ]
        then
                fn_log_print "[INFO:] Executing MySQL Query: update cedl_operations.cedl_change_data_capture_t set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
                src_column_nm='${ICR_LOAD_COL_NM}',most_recent_val_txt='${MAX_BATCH_LD_NBR}',val_datatype_nm='INT',
                cdc_tms='$START_TIME' where job_nm='${LND_JOB_NAME}'"
                `mysql $AUDIT_MYSQL_CONNECTION_URL -e "update cedl_operations.cedl_change_data_capture_t set src_sys_nm='$SRC_NAME',src_db_typ_nm='$SRC_DB_TYPE',src_db_nm='$SRC_DB_NAME',src_obj_nm='$TABLE_NAME',
                src_column_nm='${ICR_LOAD_COL_NM}',most_recent_val_txt='${MAX_BATCH_LD_NBR}',val_datatype_nm='INT',
                cdc_tms='$START_TIME' where job_nm='${LND_JOB_NAME}'"`
        fi
        END_TIME_UTC=`date +%s`
        fn_log_print "[INFO:] Started at : `date -d @$START_TIME_UTC`"
        fn_log_print "[INFO:] Ended at : `date -d @$END_TIME_UTC`"
        deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
        printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))
        fn_log_print "[INFO:] Job Completed Successfully."
else
        if [ "${EXTRACTION_MODE}" == "normal" ]
        then
                #deleting data from cedl_batch_mapping_t if it exists
                fn_log_print "Executing MySQL Query: DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
                mysql $AUDIT_MYSQL_CONNECTION_URL -e "DELETE FROM cedl_operations.cedl_batch_mapping_t where job_nm='${LND_JOB_NAME}' and table_partition  ='${CEDL_BATCH_DATE}';"
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Success : Deleted mapping for ${CEDL_BATCH_DATE} in cedl_operations.cedl_batch_mapping_t  "
                else
                        fn_log_print "Failed to delete Mapping for ${CEDL_BATCH_DATE} in cedl_operations.cedl_batch_mapping_t"
                fi
        fi
        if [ "${LND_DATA_DIRECTORY}" != "NULL" ]
        then
                #Clean up activity
                fn_log_print "[INFO:] Starting clean up activity..."
                fn_log_print "[INFO:] Cleaning data directory : hdfs dfs -rm -r $LND_DATA_DIRECTORY"
                hdfs dfs -rm -r  ${LND_DATA_DIRECTORY}
                if [ $? -eq 0 ]
                then
                        fn_log_print "[INFO:] Files deleted successfully in $LND_DATA_DIRECTORY"
                else
                        fn_log_print "[ERROR:] ${LND_DATA_DIRECTORY} doesn't exists"
                fi
                #Removing Partition For Respective Date
                `${BEELINE_CONNECTION_URL} -e "ALTER TABLE $LND_DATABASE_NAME.$TABLE_NAME DROP IF EXISTS PARTITION($PARTITION_COL='${CEDL_BATCH_DATE}')"`
                if [ $? -eq 0 ]
                then
                                        fn_log_print "[INFO:] Successfully dropped partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
                else
                                        fn_log_print "[ERROR:] failed to dropped partition $CEDL_BATCH_DATE from $LND_DATABASE_NAME.$TABLE_NAME"
                fi
        fi
                fn_log_print "[INFO:] Job Failed.........Please check"
                END_TIME_UTC=`date +%s`
                fn_log_print "[INFO:] Started at : `date -d @$START_TIME_UTC`"
                fn_log_print "[INFO:] Ended at : `date -d @$END_TIME_UTC`"
                deltatime=`expr $END_TIME_UTC - $START_TIME_UTC`
                printf '[INFO:] Total Time Taken : %dd:%02dh:%02dm:%02ds\n' $(((deltatime/3600/24)%24)) $(((deltatime/3600)%24)) $(((deltatime/60)%60)) $((deltatime%60))

                exit 1
fi
}
fn_sqoop_import_append(){
MODE=$1
fn_log_print  "[INFO:] Starting Sqoop Import Job : $1"
  if  [[ $MODE == "NORMAL" ]]
       then

             fn_log_print "[INFO:] Running Sqoop Query using select * statement for table $TABLE_NAME."
             fn_log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
                                --table $LND_TABLE_NAME \
                                --direct \
                                --where $NORMAL_CONDITION \
                                --hcatalog-database $LND_DB_NAME \
                                --hcatalog-table $LND_TABLE_NAME   \
                                --hcatalog-partition-keys $PARTITION_COL \
                                --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                --hcatalog-storage-stanza 'stored as orc tblproperties ('orc.compress'='ZLIB')' \
                                --escaped-by '\' \
                                -m ${DEF_SQOOP_APPEND_MAPPER_CNT}"
                                sqoop import --connect ${SQOOP_CONNNECTION_URL} \
                                --table $LND_TABLE_NAME \
                                --direct \
                                --where "$NORMAL_CONDITION" \
                                --hcatalog-database $LND_DB_NAME \
                                --hcatalog-table $LND_TABLE_NAME   \
                                --hcatalog-partition-keys $PARTITION_COL \
                                --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
                                --escaped-by '\' \
                                -m ${DEF_SQOOP_APPEND_MAPPER_CNT}
       else
            fn_log_print "[INFO:] Running Sqoop Query using select * statement for table $TABLE_NAME."
             fn_log_print "sqoop import --connect jdbc:$SRC_DB_TYPE://${EDW4X_DB_HOST_IP_PORT}/${SRC_DB_NAME} \
                                       --table $LND_TABLE_NAME \
                                       --direct \
                                       --where $RANGE_RERUN_CONDITION \
                                       --hcatalog-database $LND_DB_NAME \
                                       --hcatalog-table $LND_TABLE_NAME   \
                                       --hcatalog-partition-keys $PARTITION_COL \
                                       --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                       --hcatalog-storage-stanza 'stored as orc tblproperties ('orc.compress'='ZLIB')' \
                                       --escaped-by '\' \
                                       -m ${DEF_SQOOP_APPEND_MAPPER_CNT}"
                                       sqoop import --connect ${SQOOP_CONNNECTION_URL} \
                                       --direct \
                                       --table $LND_TABLE_NAME \
                                       --where '$RANGE_RERUN_CONDITION' \
                                       --hcatalog-database $LND_DB_NAME \
                                       --hcatalog-table $LND_TABLE_NAME   \
                                       --hcatalog-partition-keys $PARTITION_COL \
                                       --hcatalog-partition-values ${CEDL_BATCH_DATE} \
                                       --hcatalog-storage-stanza 'stored as orc tblproperties ("orc.compress"="ZLIB")' \
                                       --escaped-by '\' \
                                       -m ${DEF_SQOOP_APPEND_MAPPER_CNT}

                fi

}


